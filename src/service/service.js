// export const _ip = 'http://202.28.95.221';
// export const _ip = 'http://172.16.63.41:3053';
// export const _ip = 'http://localhost:3053';
export const _ip = 'https://amsapplication.kku.ac.th';
// export const _ip = window.location.host;
export const ip = _ip + '/api/v1/';

export const GET = (path) => {
	return new Promise((resolve, reject) => {
		fetch(ip + path, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		})
			.then((response) => response.json())
			.then((json) => resolve(json))
			.catch((err) => reject(err));
	});
};

export const POST = (path, obj, formData) => {
	return new Promise((resolve, reject) => {
		fetch(ip + path, {
			method: 'POST',
			headers: formData
				? {}
				: {
						'Content-Type': 'application/json'
					},
			body: formData ? obj : JSON.stringify(obj),
			credentials: 'include'
		})
			.then((response) => response.json())
			.then((json) => resolve(json))
			.catch((err) => reject(err));
	});
};

// -------------------- URL API -------------------------------------------

export const LOGIN = 'user/login';
//  -------- Home ---------------------------------------------------------
export const GET_LIST = (type, date) => `get_list/${type}/${date}`;
export const GET_USERS = (id) => `user/detail/${id}`;
export const GET_ROLE = 'get_role';
export const GET_UNIT = 'get_unit';
export const GET_TYPE = 'get_type';
export const GET_STATUS = 'get_status';

//  -------- AddCustomer ------------------------------------------------------------------------------------------
export const GET_TREATMENT = (id) => `get_treatment/${id}`;
export const GET_PACKAGE = (id) => `get_package/${id}`;
export const CREATE_LIST_BY_FRONT = (type) => `create_list_by_front/${type}`;
export const CREATE_COMPANY_LIST_BY_FRONT = (type) => `create_company_list_by_front/${type}`;
export const CREATE_DELIVERY_LIST_BY_FRONT = (type) => `create_delivery_list_by_front/${type}`;
export const CREATE_COMPANY_DELIVERY_LIST_BY_FRONT = (type) => `create_company_delivery_list_by_front/${type}`;
export const UPDATE_LIST_BY_FRONT = (id) => `update_list_by_front/${id}`;
export const DONE_LIST_BY_DOCTOR = (id) => `done_list_by_doctor/${id}`;
export const GET_LIST_BY_ID = (id) => `get_list_by_id/${id}`;
export const CANCEL_LIST = (id) => `cancel_list/${id}`;
export const COMFIRM_CUSTOMER = (id) => `user/confirm_customer/${id}`;
export const CREATE_USER = 'user/create_user';
export const GET_CLAIM = (id) => `get_claim/${id}`;
export const GET_PICTURE = (id) => `user/avatar/${id}`;
export const GET_PICTURE_SYMPTOMS = (id) => `symptoms/${id}`;
export const GET_USER_DETAIL = (id) => `user/detail/${id}`;
export const GET_BED = 'get_bed';
export const GET_BOOKING = (type, date) => `get_booking/${type}/${date}`;
export const UPDATE_PROFILE = 'user/update_profile';
export const UPDATE_USER_PROFILE = 'user/update_user_profile';
export const DELETE_USER = (id) => `user/delete_customer/${id}`;
export const UNDELETE_USER = (id) => `user/undelete_customer/${id}`;
export const ACTIVE = (id, type) => `user/active/${type}/${id}`;
export const INACTIVE = (id, type) => `user/inactive/${type}/${id}`;
export const ACTIVE_USER = (id) => `user/active_user/${id}`;
export const INACTIVE_USER = (id) => `user/inactive_user/${id}`;
export const UPDATE_SYMPTOM = (id) => `update_symptom/${id}`;

//  -------- Customer History --------
export const GET_CUSTOMER_HISTORY = (id, type, date) => `get_customer_history/${id}/${type}/${date}`;

//  -------- Marketing --------
export const GET_BANNER = (id) => `get_banner/${id}`;
export const GET_BANNER_PIC = (id) => `${ip}banner/${id}`;
export const EDIT_BANNER = 'update_banner';
export const CREATE_BANNER = 'create_banner';
export const REMOVE_BANNER = (id) => `delete_banner/${id}`;

//--------------- Graph ------------------
export const GET_DATE_INCOME = (claim = 'all', date) => `get_date_income/${claim}/${date}`;
export const GET_MONTH_INCOME = (claim = 'all', month) => `get_month_income/${claim}/${month}`;
export const GET_YEAR_INCOME = (claim = 'all', year) => `get_year_income/${claim}/${year}`;
export const GET_YEAR_INCOME_ALL = (claim = 'all', year) => `get_year_income_all/${claim}/${year}`;

//--------------- Notify ------------------

export const UPDATE_NOTIFY = (user, id) => `update_notify/${user}/${id}`;

//--------------- Update Result ------------------
export const UPLOAD_RESULT = 'upload_result';
export const GET_RESULT = (type = 'all', date) => `get_result/${type}/${date}`;
export const APPROVE_RESULT = (result_id) => `approve_result/${result_id}`;
export const CREATE_TREATMENT = 'create_treatment';
export const DELETE_TREATMENT = (id) => `/delete_treatment/${id}`;
export const UPDATE_TREATMENT = (id) => `/update_treatment/${id}`;
export const CREATE_HOLIDAY = 'insert_holiday';
export const CREATE_BILL = 'create_bill';
export const PLATFORM = 'get_device';
export const LAST_CLAIM = 'last_claim';
export const INVOICE = 'invoice';
export const UP_INVOICE = 'up_invoice';
export const LIST_CUSTOMER = `list_customer`;
export const LIST_MONTH = `list_month`;
export const RESULTPDF = `resultPdf`;
export const GET_RESULTPDF = (id) => ip + `result_pdf/${id}`;
