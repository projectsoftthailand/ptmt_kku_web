export const subRole = [
	// { id: 1, name: 'นวดไทย', en_name: 'ThaiMassage' },
	{ id: 2, name: 'กระดูกและกล้ามเนื้อ', en_name: 'Ortho' },
	{ id: 3, name: 'ระบบประสาท', en_name: 'Neuro' },
	{ id: 4, name: 'ระบบทรวงอก', en_name: 'Chest' }
	// { id: 5, name: 'ระบบเด็ก', en_name: 'Child' }
];
export const arrList = [
	{
		name: 'Hydrocollator',
		detail: 'การประคบร้อน',
		cgd_code: '56001',
		cost: 60,
		type: 'แผ่น/จุด'
	},
	{
		name: 'Cryotherapy',
		detail: 'การประคบเย็น',
		cgd_code: '56001',
		cost: 60,
		type: 'แผ่น/จุด'
	},
	{
		name: 'Electrical Stimulation / TENS',
		detail: null,
		cgd_code: '56002',
		cost: 60,
		type: 'ครั้ง'
	},
	{
		name: 'Ultrasound (US)',
		detail: null,
		cgd_code: '56003',
		cost: 120,
		type: 'ครั้ง/จุด'
	},
	{
		name: 'Lasertherapy',
		detail: null,
		cgd_code: '56005',
		cost: 60,
		type: 'ครั้ง'
	},
	{
		name: 'Paraffin bath',
		detail: null,
		cgd_code: '56006',
		cost: 80,
		type: 'ครั้ง'
	},
	{
		name: 'Acupressure&massage',
		detail: null,
		cgd_code: '56007',
		cost: 80,
		type: 'ครั้ง/วัน'
	},
	{
		name: 'Taping',
		detail: null,
		cgd_code: '56008',
		cost: 170,
		type: 'ครั้ง'
	},
	{
		name: 'exercise therapy / strenghtening / stretching',
		detail: null,
		cgd_code: '56009',
		cost: 80,
		type: 'ครั้ง'
	},
	{
		name: 'Home program',
		detail: null,
		cgd_code: '56020',
		cost: 100,
		type: 'ครั้ง'
	},
	{ name: 'CP training', detail: null, cgd_code: '56101', cost: 100, type: 'ครั้ง' },
	{ name: 'Tilt table', detail: null, cgd_code: '56110', cost: 80, type: 'ครั้ง' },
	{
		name: 'Ambulation training',
		detail: null,
		cgd_code: '56111',
		cost: 80,
		type: 'ครั้ง'
	},
	{
		name: 'Cervical traction',
		detail: null,
		cgd_code: '56120',
		cost: 140,
		type: 'ครั้ง'
	},
	{
		name: 'Pelvic traction',
		detail: null,
		cgd_code: '56121',
		cost: 140,
		type: 'ครั้ง'
	},
	{
		name: 'Passive movement',
		detail: null,
		cgd_code: '56701',
		cost: 80,
		type: 'ครั้ง'
	},
	{
		name: 'manipulation/mobilization',
		detail: null,
		cgd_code: '56702',
		cost: 80,
		type: 'ครั้ง'
	},
	{
		name: 'Microwave Diathermy',
		detail: 'การบำบัดด้วยไมโครเวฟ',
		cgd_code: null,
		cost: 70,
		type: 'ครั้ง'
	},
	{
		name: 'Others ระบุ...........................',
		detail: null,
		cgd_code: null,
		cost: 100,
		type: ''
	}
];
export const arrYearly = [
	{
		thName: 'ตรวจหากลุ่มเลือด',
		engName: '(Blood group)',
		find: 'Blood group'
	},
	{
		thName: 'ตรวจภาวะเลือดจางและแยกชนิดเม็ดเลือดขาว',
		engName: '(CBC)',
		find: 'CBC'
	},
	{
		thName: 'ตรวจพยาธิในอุจจาระ',
		engName: '(Stool examination)',
		find: 'Stool examination'
	},
	{
		thName: 'ตรวจปัสสาวะอย่างละเอียด',
		engName: '(Urine examination)',
		find: 'Urine analysis'
	},
	{
		thName: 'เอ็กซเรย์ปอด',
		engName: '(Film chest)',
		find: 'X-Ray'
	},
	{
		thName: 'ตรวจน้ำตาลในเลือด',
		engName: '(Sugar)',
		find: 'Glucose'
	},
	{
		thName: 'ตรวจบลัดยูเรียไนโตรเจน',
		engName: '(BUN)',
		find: 'BUN'
	},
	{
		thName: 'ตรวจครีเอตีนีน',
		engName: '(Creatinine)',
		find: 'Creatinine'
	},
	{
		thName: 'ตรวจเอสจีโอที',
		engName: '(SGOT)',
		find: 'AST'
	},
	{
		thName: 'ตรวจเอสจีพีที',
		engName: '(SGPT)',
		find: 'ALT'
	},
	{
		thName: 'ตรวจอัลคาไลฟอสฟาเตส',
		engName: '(Alk. Phosphatase)',
		find: 'ALP'
	},
	{
		thName: 'ตรวจโคเลสเตอรอล',
		engName: '(Cholesterol)',
		find: 'Cholesterol'
	},
	{
		thName: 'ตรวจไตรกลีเซอไรด์',
		engName: '(Triglyceride)',
		find: 'Triglyceride'
	},
	{
		thName: 'ตรวจยูริคเอซิด',
		engName: '(Uric acid)',
		find: 'Uric Acid'
	}
];
export const DMYmt = [
	'ว/ด/ป',
	'ประเภทบริการ',
	'เวลา',
	'ชื่อ-นามสกุล',
	'เบอร์โทร',
	'สิทธิ์การรักษา',
	'หน่วยบริการ',
	'นักเทคนิคการแพทย์',
	'Tracking',
	'รายได้ (บาท)'
];
export const DMYpt = [
	'ว/ด/ป',
	'ประเภทบริการ',
	'เวลา',
	'ชื่อ-นามสกุล',
	'เบอร์โทร',
	'สิทธิ์การรักษา',
	'หน่วยบริการ',
	'นักกายภาพบำบัด',
	'Tracking',
	'รายได้ (บาท)',
	'ค่าตอบแทน (บาท)'
];
export const types = [
	// {
	// 	value: 1,
	// 	type: 'all',
	// 	name: 'การให้บริการทั้งหมด'
	// },
	{
		value: 2,
		type: 'mt',
		name: 'บริการตรวจสุขภาพ'
	},
	{
		value: 3,
		type: 'pt',
		name: 'บริการกายภาพบำบัด'
	},
	{
		value: 4,
		type: 'tm',
		name: 'บริการนวดไทย'
	}
];
export const units = [
	// {
	// 	value: 1,
	// 	name: 'หน่วยบริการทั้งหมด'
	// },
	{
		value: 2,
		name: 'ศูนย์บริการมหาวิทยาลัยขอนแก่น'
	},
	{
		value: 3,
		name: 'ศูนย์บริการมหาวิทยาลัยขอนแก่น สาขาในเมืองขอนแก่น'
	}
	// {
	// 	value: 4,
	// 	name: 'นอกสถานที่'
	// }
];
export const status_claim = [
	{
		value: 999,
		name: 'สิทธิ์ทั้งหมด'
	},
	{
		value: 1,
		name: 'ไม่ใช้สิทธิ์'
	},
	{
		value: 2,
		name: 'สิทธิ์ข้าราชการทั่วไป'
	},
	{
		value: 3,
		name: 'สิทธิ์ข้าราชการมหาวิทยาลัยขอนแก่น(ใบเหลือง)'
	}
	// {
	// 	value: 4,
	// 	name: 'รอชำระเงิน'
	// },
	// {
	// 	value: 5,
	// 	name: 'เสร็จสิ้น'
	// },
	// {
	// 	value: 98,
	// 	name: 'เลยเวลา'
	// },
	// {
	// 	value: 99,
	// 	name: 'ยกเลิกบริการ'
	// }
];
export const DDMMYYS = [
	// {
	// 	value: 1,
	// 	name: 'ทั้งหมด'
	// },
	{
		value: 2,
		name: 'วันที่'
	}
	// {
	// 	value: 3,
	// 	name: 'เดือน'
	// }
	// {
	// 	value: 4,
	// 	name: 'ปี'
	// }
];
export const Invoice_claim = [
	{
		value: 1,
		name: 'ข้าราชการและลูกจ้างประจำ'
	},
	{
		value: 2,
		name: 'บุคคลในครอบครัวข้าราชการและลูกจ้างประจำ'
	},
	{
		value: 3,
		name: 'ข้าราชการบำนาญ'
	},
	{
		value: 4,
		name: 'บุคคลในครอบครัวข้าราชการบำนาญ'
	}
];
export const months = [
	{
		type: 1,
		name: 'มกราคม'
	},
	{
		type: 2,
		name: 'กุมภาพันธ์'
	},
	{
		type: 3,
		name: 'มีนาคม'
	},
	{
		type: 4,
		name: 'เมษายน'
	},
	{
		type: 5,
		name: 'พฤษภาคม'
	},
	{
		type: 6,
		name: 'มิถุนายน'
	},
	{
		type: 7,
		name: 'กรกฏาคม'
	},
	{
		type: 8,
		name: 'สิงหาคม'
	},
	{
		type: 9,
		name: 'กันยายน'
	},
	{
		type: 10,
		name: 'ตุลาคม'
	},
	{
		type: 11,
		name: 'พฤศจิกายน'
	},
	{
		type: 12,
		name: 'ธันวาคม'
	}
];
