import React from 'react';
import WrapperComponent from '../WrapperComponent';
import { Route } from 'react-router-dom';
import User from '../mobx/user/user';
import { profile } from '../pages';
import { fHome, fGraph, fTableDetail, fAllWork, fCustomerDetail, fAddCustomer } from '../pages';
import { mHome, mAddBanner } from '../pages';
import { aHome, aAddMember, aMemberDetail } from '../pages';
import { mtHome, mtAllWork, mtTableDetail, mtHistory } from '../pages';
import Income_document from '../components/Graph/Income_document';
import Results from '../components/Outpatient_results';
import download from '../pages/Downloadform';
import checklist from '../pages/admin/AddChecklist/checklist';
import addchecklist from '../pages/admin/AddChecklist/addchecklist';

//  หลังจาก  LOGIN
export default () =>
	User.role === 'frontPT' || User.role === 'frontMT' ? (
		<WrapperComponent>
			<Route exact path={'/home'} component={fHome} />
			<Route exact path={'/home/addnew'} component={fAddCustomer} />
			<Route exact path={'/home/addnew/:id'} component={fAddCustomer} />
			<Route exact path={'/home/table'} component={fAllWork} />
			<Route exact path={'/home/table/customer/:unit/:status/:id/:list_id'} component={fTableDetail} />
			<Route exact path={'/history/table/customer/:unit/:status/:id/:list_id'} component={fTableDetail} />
			<Route exact path={'/home/addnew/detail/:id'} component={fCustomerDetail} />
			<Route exact path={'/history'} component={mtHistory} />
			<Route exact path={'/history/:id'} component={mtHistory} />
			<Route exact path={'/Income_document'} component={Income_document} />
			<Route exact path={'/result'} component={Results} />
			<Route exact path={'/graph'} component={fGraph} />
			<Route exact path={'/profile/:id'} component={profile} />

			<Route exact path={'/home-marketing'} component={mHome} />
			<Route exact path={'/home-marketing/add-banner'} component={mAddBanner} />

			<Route exact path={'/home-admin'} component={aHome} />
			<Route exact path={'/home-admin/member/:id'} component={aMemberDetail} />
			<Route exact path={'/home-admin/add-member'} component={aAddMember} />

			<Route exact path={'/checklist/addchecklist'} component={addchecklist} />
			<Route exact path={'/checklist'} component={checklist} />
		</WrapperComponent>
	) : User.role === 'pt' ? (
		<WrapperComponent>
			<Route exact path={'/home'} component={mtHome} />
			<Route exact path={'/home/table'} component={mtAllWork} />
			<Route exact path={'/home/table/customer/:unit/:status/:id/:list_id'} component={mtTableDetail} />
			<Route exact path={'/history/table/customer/:unit/:status/:id/:list_id'} component={mtTableDetail} />
			<Route exact path={'/history'} component={mtHistory} />
			<Route exact path={'/history/:id'} component={mtHistory} />
			<Route exact path={'/profile/:id'} component={profile} />
			<Route exact path={'/result'} component={Results} />
			<Route exact path={'/home/addnew/:id'} component={fAddCustomer} />
		</WrapperComponent>
	) : User.role === 'mt' ? (
		<WrapperComponent>
			<Route exact path={'/home'} component={mtHome} />
			<Route exact path={'/home/table'} component={mtAllWork} />
			<Route exact path={'/home/table/customer/:unit/:status/:id/:list_id'} component={mtTableDetail} />
			<Route exact path={'/history/table/customer/:unit/:status/:id/:list_id'} component={mtTableDetail} />
			<Route exact path={'/history'} component={mtHistory} />
			<Route exact path={'/history/:id'} component={mtHistory} />
			<Route exact path={'/profile/:id'} component={profile} />
			<Route exact path={'/result'} component={Results} />
			<Route exact path={'/home/addnew/:id'} component={fAddCustomer} />
		</WrapperComponent>
	) : User.role === 'admin' ? (
		<WrapperComponent>
			<Route exact path={'/home'} component={aHome} />
			<Route exact path={'/home/member/:id'} component={aMemberDetail} />
			<Route exact path={'/add-member'} component={aAddMember} />
			<Route exact path={'/graph'} component={fGraph} />
			<Route exact path={'/Income_document'} component={Income_document} />
			<Route exact path={'/profile/:id'} component={profile} />
			<Route exact path={'/checklist/addchecklist'} component={addchecklist} />
			<Route exact path={'/checklist'} component={checklist} />
		</WrapperComponent>
	) : User.role === 'marketing' ? (
		<WrapperComponent>
			<Route exact path={'/home'} component={mHome} />
			<Route exact path={'/home/add-banner'} component={mAddBanner} />
			<Route exact path={'/profile/:id'} component={profile} />
		</WrapperComponent>
	) : (
		<WrapperComponent>
			<Route exact path={'/*'} component={fHome} />
			<Route exact path={'/download-form'} component={download} />
		</WrapperComponent>
	);
