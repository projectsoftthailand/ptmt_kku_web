import React, { Component } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import App from './App';
import { fLogin } from '../pages';
import Platform from '../pages/Platform';

export default class Root extends Component {
	render() {
		return (
			<BrowserRouter forceRefresh={false}>
				<Switch>
					<Route path="/" exact component={fLogin} />
					<Route path="/Platform" exact component={Platform} />
					<App />
				</Switch>
			</BrowserRouter>
		);
	}
}
