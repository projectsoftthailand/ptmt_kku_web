import React, { Component } from 'react';
import download from 'downloadjs';
import { GET } from '../service/service';

export default class Downloadform extends Component {
	async componentDidMount() {
		let res = await GET('/register_form');
		const blob = new Blob([ new Uint8Array(res.data) ]);
		download(blob, 'register-form.xls');
	}
	render() {
		return <div />;
	}
}
