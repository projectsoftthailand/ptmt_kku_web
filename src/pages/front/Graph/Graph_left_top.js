import React, { Component } from 'react';
import './Styled/Graph_left_top.css';
import './Styled/Graph_right.css';
import { Label, Input, TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import ReactSvgPieChart from 'react-svg-piechart';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import moment from 'moment';

// Alternatively, this is how to load Highstock. Highmaps is similar.
// import Highcharts from 'highcharts/highstock';
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
import classnames from 'classnames';
// Initialize exporting module.
Exporting(Highcharts);
// moment.locale("th");
const COLORS = [ '#467ac8', '#e8ae00', '#FFBB28', '#FF8042' ];

export default class Graph_left_top extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeIndex: 0,
			click: 0,
			types: [
				{
					type: 1,
					name: 'มกราคม'
				},
				{
					type: 2,
					name: 'กุมภาพันธ์'
				},
				{
					type: 3,
					name: 'มีนาคม'
				},
				{
					type: 4,
					name: 'เมษายน'
				},
				{
					type: 5,
					name: 'พฤษภาคม'
				},
				{
					type: 6,
					name: 'มิถุนายน'
				},
				{
					type: 7,
					name: 'กรกฏาคม'
				},
				{
					type: 8,
					name: 'สิงหาคม'
				},
				{
					type: 9,
					name: 'กันยายน'
				},
				{
					type: 10,
					name: 'ตุลาคม'
				},
				{
					type: 11,
					name: 'พฤศจิกายน'
				},
				{
					type: 12,
					name: 'ธันวาคม'
				}
			],
			activeTab: '1',
			check_Income: true,
			month: '',
			options: [],
			date_month: '',
			date_year: '',
			date_date: ''
		};
	}
	componentDidMount() {
		let th = require('moment/locale/th');
moment.updateLocale('th', th);

		let date_month = this.props.date_month;
		let date_year = this.props.date_year;
		var start = 2000;
		var end = new Date().getFullYear();
		var options = [];
		for (var year = start; year <= end; year++) {
			options.push({ year: year, value: year });
		}
		let date_date = this.props.date_date;
		this.setState({
			options: options,
			date_month: date_month,
			date_year: date_year,
			date_date: date_date
		});
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}
	renderActiveShape = (props) => {
		const { cx, cy, innerRadius, outerRadius, startAngle, endAngle, fill } = props;
		return (
			<g>
				<text style={{ fontSize: '30px' }} x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
					{this.props.sum_data_donut_list}
				</text>
				<Sector
					cx={cx}
					cy={cy}
					innerRadius={innerRadius}
					outerRadius={outerRadius}
					startAngle={startAngle}
					endAngle={endAngle}
					fill={fill}
				/>
			</g>
		);
	};
	render() {
		let { types, check_Income, options, date_month, date_year, date_date } = this.state;
		let {
			option_pt,
			list_pt,
			graphselectMonthPT,
			graphselectYearPT,
			option_pt_year,
			list_pt_year,
			data_donut,
			sum_data_donut,
			data_donut_intime,
			data_donut_overtime,
			data_donut_list,
			data_donut_intime_list,
			data_donut_overtime_list
		} = this.props;
		return (
			<div>
				<div id="font-color" className="mb-2">
					คลินิกกายภาพบำบัด
				</div>
				<Row id="position-graph" className="px-0 mx-0">
					<Col id="border-right">
						<Row className="py-4">
							<Col sm={6} className="d-flex justify-content-center text-center">
								<div style={{ width: '10rem', height: '10rem' }}>
									<ReactSvgPieChart strokeWidth={0} data={data_donut} expandOnHover />
								</div>
							</Col>
							<Col sm={6} className="d-flex flex-column justify-content-center">
								<div style={{ fontSize: '1.4rem' }}>รายได้ส่วนของคลินิกกายภาพบำบัด</div>
								<div style={{ fontSize: '2.6rem' }}>{sum_data_donut.toLocaleString()} บาท</div>
								<div style={{ fontSize: '1.2rem' }}>
									รายได้รวมวันที่ {moment(date_date).format('DD MMMM YYYY')}
								</div>
							</Col>
						</Row>
						<Row id="border-solid">
							<Col className="py-2" style={{ borderRight: '1px solid #dededf' }}>
								<Row className="d-flex align-items-center px-4" style={{ fontSize: '1.2rem' }}>
									<div id="block-intime" />
									<div>ในเวลา</div>
								</Row>
								<Row className="px-4" style={{ fontSize: '1.4rem' }}>
									{data_donut_intime.toLocaleString()} บาท
								</Row>
							</Col>
							<Col className="py-2">
								<Row className="d-flex align-items-center px-4" style={{ fontSize: '1.2rem' }}>
									<div id="block-outtime" />
									<div>นอกเวลา</div>
								</Row>
								<Row className="px-4" style={{ fontSize: '1.4rem' }}>
									{data_donut_overtime.toLocaleString()} บาท
								</Row>
							</Col>
						</Row>
					</Col>
					<Col>
						<Row className="py-4">
							<Col sm={5} className="d-flex justify-content-center text-center">
								<PieChart width={100} height={100}>
									<Pie
										activeIndex={this.state.activeIndex}
										activeShape={this.renderActiveShape}
										data={data_donut_list}
										innerRadius={45}
										outerRadius={50}
										paddingAngle={1}
										dataKey="value"
										fill="#8884d8"
									>
										{data_donut_list.map((entry, index) => (
											<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
										))}
									</Pie>
								</PieChart>
							</Col>
							<Col sm={7} className="d-flex flex-column justify-content-center">
								<div className="d-flex align-items-center" style={{ fontSize: '1.4rem' }}>
									จำนวนคนที่ใช้บริการกายภาพบำบัด วันที่ {moment(date_date).format('DD MMMM YYYY')}
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.2rem' }}>
									<div id="block-intime" />
									<div>ในเวลา {data_donut_intime_list} รายการ</div>
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.2rem' }}>
									<div id="block-outtime" />
									<div>นอกเวลา {data_donut_overtime_list} รายการ</div>
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.2rem' }}>
									ข้อมูลวันที่ {moment(date_date).format('DD MMMM YYYY')}
								</div>
							</Col>
						</Row>
					</Col>
				</Row>
				<div id="s-graph-right" className="my-4">
					<Row className="px-0 mx-0">
						<Nav tabs className="w-100">
							<NavItem>
								<NavLink
									className={classnames({ active: this.state.activeTab === '1' })}
									onClick={() => {
										this.toggle('1');
									}}
								>
									<h4>กราฟรายได้</h4>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink
									className={classnames({ active: this.state.activeTab === '2' })}
									onClick={() => {
										this.toggle('2');
									}}
								>
									<h4>กราฟลูกค้า</h4>
								</NavLink>
							</NavItem>
						</Nav>
					</Row>
					<TabContent activeTab={this.state.activeTab}>
						<TabPane tabId="1">
							<Row id="s-graph-right-1" className="px-0 mx-0">
								<div className="d-flex justify-content-end m-4 w-100">
									<Row className="px-0 mx-2">
										<input
											onClick={() => this.setState({ check_Income: true })}
											checked={check_Income === true ? true : false}
											value="check_Income"
											name="check_Income"
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											เดือน
										</Label>
										<Input
											value={date_month}
											onChange={(e) => {
												graphselectMonthPT(e.target.value);
												this.setState({ date_month: e.target.value });
											}}
											style={{ width: '15rem' }}
											className="mt-1"
											type="select"
											disabled={check_Income === false}
										>
											{types.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Row>
									<Row className="px-0 mx-2">
										<input
											onClick={() => this.setState({ check_Income: false })}
											checked={check_Income === false ? true : false}
											value="check_Income"
											name="check_Income"
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											ปี
										</Label>
										<Input
											value={date_year}
											onChange={(e) => {
												graphselectYearPT(e.target.value);
												this.setState({ date_year: e.target.value });
											}}
											style={{ width: '15rem' }}
											className="mt-1"
											type="select"
											disabled={check_Income === true}
										>
											{options.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Row>
								</div>
								<div className="w-100">
									<HighchartsReact
										highcharts={Highcharts}
										options={check_Income ? option_pt : option_pt_year}
									/>
								</div>
							</Row>
						</TabPane>
						<TabPane tabId="2">
							<Row id="s-graph-right-1" className="px-0 mx-0">
								<div className="d-flex justify-content-end m-4 w-100">
									<Row className="px-0 mx-2">
										<input
											onClick={() => this.setState({ check_Income: true })}
											checked={check_Income === true ? true : false}
											value="check_List"
											name="check_List"
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											เดือน
										</Label>
										<Input
											value={date_month}
											onChange={(e) => {
												graphselectMonthPT(e.target.value);
												this.setState({ date_month: e.target.value });
											}}
											style={{ width: '15rem' }}
											className="mt-1"
											type="select"
											disabled={check_Income === false}
										>
											{types.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Row>
									<Row className="px-0 mx-2">
										<input
											onClick={() => this.setState({ check_Income: false })}
											checked={check_Income === false ? true : false}
											value="check_List"
											name="check_List"
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											ปี
										</Label>
										<Input
											value={date_year}
											onChange={(e) => {
												graphselectYearPT(e.target.value);
												this.setState({ date_year: e.target.value });
											}}
											style={{ width: '15rem' }}
											className="mt-1"
											type="select"
											disabled={check_Income === true}
										>
											{options.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Row>
								</div>
								<div className="w-100">
									<HighchartsReact
										highcharts={Highcharts}
										options={check_Income ? list_pt : list_pt_year}
									/>
								</div>
							</Row>
						</TabPane>
					</TabContent>
				</div>
			</div>
		);
	}
}
