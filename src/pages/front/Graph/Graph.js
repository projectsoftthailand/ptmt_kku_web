import React, { Component } from 'react';
import './Styled/Graph.css';
import $ from 'jquery';
// import InProgress from '../../../components/InProgress/InProgress';
import { Row, Col } from 'reactstrap';
import Graph_left_top from './Graph_left_top';
// import Graph_left_bot from './Graph_left_bot';
import Graph_right from './Graph_right';
import Graph_right_top from './Graph_right_top';
import { GET, GET_MONTH_INCOME, GET_YEAR_INCOME, GET_DATE_INCOME } from '../../../service/service';
import moment from 'moment';
import { Link } from 'react-router-dom';

export default class Graph extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user_type: '',
			total_graph: [],
			total: [],
			option: {},
			list: {},
			option_year: {},
			list_year: {},
			option_pt: {},
			list_pt: {},
			option_pt_year: {},
			list_pt_year: {},
			date_date: new Date(),
			date_month: new Date().getMonth() + 1,
			date_year: new Date().getFullYear(),
			//----------------------DATA--DONUT--PT----------------------------------
			sum_data_donut: '',
			data_donut: [],
			data_donut_intime: '',
			data_donut_overtime: '',
			sum_data_donut_list: '',
			data_donut_list: [],
			data_donut_intime_list: '',
			data_donut_overtime_list: '',
			//----------------------DATA--DONUT--MT----------------------------------
			sum_data_donut_mt: '',
			data_donut_mt: [],
			data_donut_intime_mt: '',
			data_donut_overtime_mt: '',
			sum_data_donut_list_mt: '',
			data_donut_list_mt: [],
			data_donut_intime_list_mt: '',
			data_donut_overtime_list_mt: '',
			//----------------------DATA--DONUT--TM----------------------------------
			sum_data_donut_tm: '',
			data_donut_tm: [],
			data_donut_intime_tm: '',
			data_donut_overtime_tm: '',
			sum_data_donut_list_tm: '',
			data_donut_list_tm: [],
			data_donut_intime_list_tm: '',
			data_donut_overtime_list_tm: '',
			option_tm: {},
			list_tm: {},
			option_tm_year: {},
			list_tm_year: {}
		};
	}
	componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);

		let res = localStorage.getItem('USER');
		let user_type = JSON.parse(res);
		// console.log('user_type :', user_type.role);
		this.setState({
			user_type: user_type.role
		});
	}

	componentDidMount = async () => {
		let date = moment(new Date()).format('YYYY-MM-DD');
		// let date = '2019-03-26';
		let year = moment(new Date()).format('YYYY');
		let month = moment(new Date()).format('YYYY-MM');
		let claim = 'all';
		let res_date = await GET(GET_DATE_INCOME(claim, date));
		let res = await GET(GET_MONTH_INCOME(claim, month));
		let res_year = await GET(GET_YEAR_INCOME(claim, year));
		// console.log(res_date, res, res_year);

		if (res_date.success || res.success || res_year.success) {
			//--------------------MT-----------------------------------------------
			//------------------MONTH----------------------------------------------
			let intime_income_mt = res.result.mt.sum_income.intime_income;
			let overtime_income_mt = res.result.mt.sum_income.overtime_income;
			let total = [];
			intime_income_mt.forEach((el, i) => {
				let sum = intime_income_mt[i] + overtime_income_mt[i];
				total.push(sum);
			});
			let total_graph = total.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let sumlist_intime_mt = res.result.mt.sum_list.intime_list;
			let sumlist_overtime_mt = res.result.mt.sum_list.overtime_list;
			let date_sumlist_mt = sumlist_intime_mt.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let option = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: total_graph
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total
					}
				]
			};
			let list = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: date_sumlist_mt
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_mt
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_mt,
						color: 'orange'
					}
				]
			};

			//----------------------------YEAR------------------------------------------------------

			let intime_income_mt_year = res_year.result.mt.sum_income.intime_income;
			let overtime_income_mt_year = res_year.result.mt.sum_income.overtime_income;
			let total_year = [];
			intime_income_mt_year.forEach((el, i) => {
				let sum = intime_income_mt_year[i] + overtime_income_mt_year[i];
				total_year.push(sum);
			});

			let sumlist_intime_mt_year = res_year.result.mt.sum_list.intime_list;
			let sumlist_overtime_mt_year = res_year.result.mt.sum_list.overtime_list;

			let option_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_year
					}
				]
			};
			let list_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_mt_year
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_mt_year,
						color: 'orange'
					}
				]
			};
			//-----------------------------------------------------------------------
			//--------------------------------PT-------------------------------------
			let intime_income_pt = res.result.pt.sum_income.intime_income;
			let overtime_income_pt = res.result.pt.sum_income.overtime_income;
			let total_pt = [];

			intime_income_pt.forEach((el, i) => {
				let sum = intime_income_pt[i] + overtime_income_pt[i];
				total_pt.push(sum);
			});
			let total_graph_pt = total_pt.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let sumlist_intime_pt = res.result.pt.sum_list.intime_list;
			let sumlist_overtime_pt = res.result.pt.sum_list.overtime_list;
			let date_sumlist_pt = sumlist_intime_pt.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let option_pt = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: total_graph_pt
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_pt
					}
				]
			};
			let list_pt = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: date_sumlist_pt
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_pt
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_pt,
						color: 'orange'
					}
				]
			};
			////////////////////////////---YEAR-----------------*******************--

			let intime_income_pt_year = res_year.result.pt.sum_income.intime_income;
			let overtime_income_pt_year = res_year.result.pt.sum_income.overtime_income;
			let total_pt_year = [];
			intime_income_pt_year.forEach((el, i) => {
				let sum = intime_income_pt_year[i] + overtime_income_pt_year[i];
				total_pt_year.push(sum);
			});

			let sumlist_intime_pt_year = res_year.result.pt.sum_list.intime_list;
			let sumlist_overtime_pt_year = res_year.result.pt.sum_list.overtime_list;

			let option_pt_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_pt_year
					}
				]
			};
			let list_pt_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_pt_year
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_pt_year,
						color: 'orange'
					}
				]
			};
			//--------------------------------TM-------------------------------------
			let intime_income_tm = res.result.tm.sum_income.intime_income;
			let overtime_income_tm = res.result.tm.sum_income.overtime_income;
			let total_tm = [];

			intime_income_tm.forEach((el, i) => {
				let sum = intime_income_tm[i] + overtime_income_tm[i];
				total_tm.push(sum);
			});
			let total_graph_tm = total_tm.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let sumlist_intime_tm = res.result.tm.sum_list.intime_list;
			let sumlist_overtime_tm = res.result.tm.sum_list.overtime_list;
			let date_sumlist_tm = sumlist_intime_tm.map((e, i) => {
				let a = i;
				return a + 1;
			});

			let option_tm = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: total_graph_tm
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_tm
					}
				]
			};
			let list_tm = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: date_sumlist_tm
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_tm
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_tm,
						color: 'orange'
					}
				]
			};
			////////////////////////////---YEAR-----------------*******************--

			let intime_income_tm_year = res_year.result.tm.sum_income.intime_income;
			let overtime_income_tm_year = res_year.result.tm.sum_income.overtime_income;
			let total_tm_year = [];
			intime_income_tm_year.forEach((el, i) => {
				let sum = intime_income_tm_year[i] + overtime_income_tm_year[i];
				total_tm_year.push(sum);
			});

			let sumlist_intime_tm_year = res_year.result.tm.sum_list.intime_list;
			let sumlist_overtime_tm_year = res_year.result.tm.sum_list.overtime_list;

			let option_tm_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_tm_year
					}
				]
			};
			let list_tm_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_tm_year
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_tm_year,
						color: 'orange'
					}
				]
			};
			//----------------------DATA--DONUT--PT----------------------------------
			let data_donut_intime = res_date.result.pt.sum_income.intime_income;
			let data_donut_overtime = res_date.result.pt.sum_income.overtime_income;
			let sum_data_donut = parseInt(data_donut_intime) + parseInt(data_donut_overtime);
			let data_donut = [
				{
					title: 'ในเวลา ' + data_donut_intime.toLocaleString() + ' บาท',
					value: data_donut_intime,
					color: '#467ac8'
				},
				{
					title: 'นอกเวลา ' + data_donut_intime.toLocaleString() + ' บาท',
					value: data_donut_overtime,
					color: '#e8ae00'
				}
			];
			let data_donut_intime_list = res_date.result.pt.sum_list.intime_list;
			let data_donut_overtime_list = res_date.result.pt.sum_list.overtime_list;
			let sum_data_donut_list = parseInt(data_donut_intime_list) + parseInt(data_donut_overtime_list);
			let data_donut_list = [
				{ name: 'ในเวลา', value: data_donut_intime_list },
				{ name: 'นอกเวลา', value: data_donut_overtime_list }
			];

			//----------------------DATA--DONUT--MT----------------------------------
			let data_donut_intime_mt = res_date.result.mt.sum_income.intime_income;
			let data_donut_overtime_mt = res_date.result.mt.sum_income.overtime_income;
			let sum_data_donut_mt = parseInt(data_donut_intime_mt) + parseInt(data_donut_overtime_mt);
			let data_donut_mt = [
				{
					title: 'ในเวลา ' + data_donut_intime_mt.toLocaleString() + ' บาท',
					value: data_donut_intime_mt,
					color: '#467ac8'
				},
				{
					title: 'นอกเวลา ' + data_donut_overtime_mt.toLocaleString() + ' บาท',
					value: data_donut_overtime_mt,
					color: '#e8ae00'
				}
			];
			let data_donut_intime_list_mt = res_date.result.mt.sum_list.intime_list;
			let data_donut_overtime_list_mt = res_date.result.mt.sum_list.overtime_list;
			let sum_data_donut_list_mt = parseInt(data_donut_intime_list_mt) + parseInt(data_donut_overtime_list_mt);
			let data_donut_list_mt = [
				{ name: 'ในเวลา', value: data_donut_intime_list_mt },
				{ name: 'นอกเวลา', value: data_donut_overtime_list_mt }
			];
			//----------------------DATA--DONUT--TM----------------------------------
			let data_donut_intime_tm = res_date.result.tm.sum_income.intime_income;
			let data_donut_overtime_tm = res_date.result.tm.sum_income.overtime_income;
			let sum_data_donut_tm = parseInt(data_donut_intime_tm) + parseInt(data_donut_overtime_tm);
			let data_donut_tm = [
				{
					title: 'ในเวลา ' + data_donut_intime_tm.toLocaleString() + ' บาท',
					value: data_donut_intime_tm,
					color: '#467ac8'
				},
				{
					title: 'นอกเวลา ' + data_donut_overtime_tm.toLocaleString() + ' บาท',
					value: data_donut_overtime_tm,
					color: '#e8ae00'
				}
			];
			let data_donut_intime_list_tm = res_date.result.tm.sum_list.intime_list;
			let data_donut_overtime_list_tm = res_date.result.tm.sum_list.overtime_list;
			let sum_data_donut_list_tm = parseInt(data_donut_intime_list_tm) + parseInt(data_donut_overtime_list_tm);
			let data_donut_list_tm = [
				{ name: 'ในเวลา', value: data_donut_intime_list_tm },
				{ name: 'นอกเวลา', value: data_donut_overtime_list_tm }
			];
			//-----------------------------------------------------------------------
			this.setState({
				//----------MT-------------
				option: option,
				list: list,
				option_year: option_year,
				list_year: list_year,
				//------PT----------
				option_pt: option_pt,
				list_pt: list_pt,
				option_pt_year: option_pt_year,
				list_pt_year: list_pt_year,
				//------TM---------------
				option_tm: option_tm,
				list_tm: list_tm,
				option_tm_year: option_tm_year,
				list_tm_year: list_tm_year,
				//----------------------DATA--DONUT--PT----------------------------------
				data_donut: data_donut,
				sum_data_donut: sum_data_donut,
				data_donut_intime: data_donut_intime,
				data_donut_overtime: data_donut_overtime,
				data_donut_list: data_donut_list,
				sum_data_donut_list: sum_data_donut_list,
				data_donut_intime_list: data_donut_intime_list,
				data_donut_overtime_list: data_donut_overtime_list,
				//----------------------DATA--DONUT--MT----------------------------------
				data_donut_mt: data_donut_mt,
				sum_data_donut_mt: sum_data_donut_mt,
				data_donut_intime_mt: data_donut_intime_mt,
				data_donut_overtime_mt: data_donut_overtime_mt,
				data_donut_list_mt: data_donut_list_mt,
				sum_data_donut_list_mt: sum_data_donut_list_mt,
				data_donut_intime_list_mt: data_donut_intime_list_mt,
				data_donut_overtime_list_mt: data_donut_overtime_list_mt,
				//----------------------DATA--DONUT--TM----------------------------------
				data_donut_tm: data_donut_tm,
				sum_data_donut_tm: sum_data_donut_tm,
				data_donut_intime_tm: data_donut_intime_tm,
				data_donut_overtime_tm: data_donut_overtime_tm,
				data_donut_list_tm: data_donut_list_tm,
				sum_data_donut_list_tm: sum_data_donut_list_tm,
				data_donut_intime_list_tm: data_donut_intime_list_tm,
				data_donut_overtime_list_tm: data_donut_overtime_list_tm
			});
		}

		// console.log('intime_income', intime_income);
		// console.log('overtime_income', overtime_income);
		// console.log('total_graph', total_graph);
		// console.log('total', total);
	};
	formatTooltip(tooltip, x = this.x, y = this.y, series = this.series) {
		return `<b>วันที่ ${x}</b><br/><br/>${series.name} : ${y.toLocaleString()} บาท`;
	}
	formatTooltipYear(tooltip, x = this.x, y = this.y, series = this.series) {
		return `<b>เดือน ${x}</b><br/><br/>${series.name} : ${y.toLocaleString()} บาท`;
	}
	graphselectMonthMT = async (months) => {
		let y = new Date().getFullYear();
		let month = y + '-' + months;
		let claim = 'all';
		let res = await GET(GET_MONTH_INCOME(claim, month));
		// console.log("res", res);
		if (res.success) {
			let intime_income_mt = res.result.mt.sum_income.intime_income;
			let overtime_income_mt = res.result.mt.sum_income.overtime_income;
			let total = [];
			intime_income_mt.forEach((el, i) => {
				let sum = intime_income_mt[i] + overtime_income_mt[i];
				total.push(sum);
			});
			let total_graph = total.map((e, i) => {
				let a = i;
				return a + 1;
			});
			let sumlist_intime_mt = res.result.mt.sum_list.intime_list;
			let sumlist_overtime_mt = res.result.mt.sum_list.overtime_list;
			let date_sumlist_mt = sumlist_intime_mt.map((e, i) => {
				let a = i;
				return a + 1;
			});
			let option = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: total_graph
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total
					}
				]
			};
			let list = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: date_sumlist_mt
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_mt
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_mt,
						color: 'orange'
					}
				]
			};
			this.setState({
				option: option,
				list: list
			});
		}
	};
	graphselectYearMT = async (years) => {
		let year = years;
		let claim = 'all';
		let res_year = await GET(GET_YEAR_INCOME(claim, year));
		if (res_year.success) {
			let intime_income_mt_year = res_year.result.mt.sum_income.intime_income;
			let overtime_income_mt_year = res_year.result.mt.sum_income.overtime_income;
			let total_year = [];
			intime_income_mt_year.forEach((el, i) => {
				let sum = intime_income_mt_year[i] + overtime_income_mt_year[i];
				total_year.push(sum);
			});

			let sumlist_intime_mt_year = res_year.result.mt.sum_list.intime_list;
			let sumlist_overtime_mt_year = res_year.result.mt.sum_list.overtime_list;

			let option_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'จำนวนเงิน',
						data: total_year
					}
				]
			};
			let list_year = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: [
						'มกราคม',
						'กุมภาพันธ์',
						'มีนาคม',
						'เมษายน',
						'พฤษภาคม',
						'มิถุนายน',
						'กรกฏาคม',
						'สิงหาคม',
						'กันยายน',
						'ตุลาคม',
						'พฤศจิกายน',
						'ธันวาคม'
					]
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'ในเวลา',
						data: sumlist_intime_mt_year
					},
					{
						name: 'นอกเวลา',
						data: sumlist_overtime_mt_year,
						color: 'orange'
					}
				]
			};
			this.setState({
				option_year: option_year,
				list_year: list_year
			});
		}
	};
	graphselectMonthPT = async (months) => {
		let y = new Date().getFullYear();
		let month = y + '-' + months;
		let claim = 'all';
		let res = await GET(GET_MONTH_INCOME(claim, month));
		// console.log('res', res);
		let intime_income_pt = res.result.pt.sum_income.intime_income;
		let overtime_income_pt = res.result.pt.sum_income.overtime_income;
		let total_pt = [];

		intime_income_pt.forEach((el, i) => {
			let sum = intime_income_pt[i] + overtime_income_pt[i];
			total_pt.push(sum);
		});
		let total_graph_pt = total_pt.map((e, i) => {
			let a = i;
			return a + 1;
		});

		let sumlist_intime_pt = res.result.pt.sum_list.intime_list;
		let sumlist_overtime_pt = res.result.pt.sum_list.overtime_list;
		let date_sumlist_pt = sumlist_intime_pt.map((e, i) => {
			let a = i;
			return a + 1;
		});

		let option_pt = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'วันที่'
				},
				categories: total_graph_pt
			},
			yAxis: {
				title: {
					text: 'จำนวนเงิน'
				}
			},
			tooltip: {
				crosshairs: true,
				formatter: this.formatTooltip
			},
			series: [
				{
					name: 'จำนวนเงิน',
					data: total_pt
				}
			]
		};
		let list_pt = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'วันที่'
				},
				categories: date_sumlist_pt
			},
			yAxis: {
				title: {
					text: 'จำนวนรายการ'
				}
			},
			tooltip: {
				formatter: function() {
					var s = '<b>' + 'วันที่  ' + this.x + '</b>',
						sum = 0;
					$.each(this.points, function(i, point) {
						s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
						sum += point.y;
					});
					s += '<br/>ทั้งหมด : ' + sum;
					return s;
				},
				crosshairs: true,
				shared: true
			},
			series: [
				{
					name: 'ในเวลา',
					data: sumlist_intime_pt
				},
				{
					name: 'นอกเวลา',
					data: sumlist_overtime_pt,
					color: 'orange'
				}
			]
		};
		this.setState({
			option_pt: option_pt,
			list_pt: list_pt
		});
	};
	graphselectYearPT = async (years) => {
		let year = years;
		let claim = 'all';
		let res_year = await GET(GET_YEAR_INCOME(claim, year));

		let intime_income_pt_year = res_year.result.pt.sum_income.intime_income;
		let overtime_income_pt_year = res_year.result.pt.sum_income.overtime_income;
		let total_pt_year = [];
		intime_income_pt_year.forEach((el, i) => {
			let sum = intime_income_pt_year[i] + overtime_income_pt_year[i];
			total_pt_year.push(sum);
		});

		let sumlist_intime_pt_year = res_year.result.pt.sum_list.intime_list;
		let sumlist_overtime_pt_year = res_year.result.pt.sum_list.overtime_list;

		let option_pt_year = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'เดือน'
				},
				categories: [
					'มกราคม',
					'กุมภาพันธ์',
					'มีนาคม',
					'เมษายน',
					'พฤษภาคม',
					'มิถุนายน',
					'กรกฏาคม',
					'สิงหาคม',
					'กันยายน',
					'ตุลาคม',
					'พฤศจิกายน',
					'ธันวาคม'
				]
			},
			yAxis: {
				title: {
					text: 'จำนวนเงิน'
				}
			},
			tooltip: {
				crosshairs: true,
				formatter: this.formatTooltipYear
			},
			series: [
				{
					name: 'จำนวนเงิน',
					data: total_pt_year
				}
			]
		};
		let list_pt_year = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'เดือน'
				},
				categories: [
					'มกราคม',
					'กุมภาพันธ์',
					'มีนาคม',
					'เมษายน',
					'พฤษภาคม',
					'มิถุนายน',
					'กรกฏาคม',
					'สิงหาคม',
					'กันยายน',
					'ตุลาคม',
					'พฤศจิกายน',
					'ธันวาคม'
				]
			},
			yAxis: {
				title: {
					text: 'จำนวนรายการ'
				}
			},
			tooltip: {
				formatter: function() {
					var s = '<b>' + 'เดือน  ' + this.x + '</b>',
						sum = 0;
					$.each(this.points, function(i, point) {
						s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
						sum += point.y;
					});
					s += '<br/>ทั้งหมด : ' + sum;
					return s;
				},
				crosshairs: true,
				shared: true
			},
			series: [
				{
					name: 'ในเวลา',
					data: sumlist_intime_pt_year
				},
				{
					name: 'นอกเวลา',
					data: sumlist_overtime_pt_year,
					color: 'orange'
				}
			]
		};
		this.setState({
			option_pt_year: option_pt_year,
			list_pt_year: list_pt_year
		});
	};
	graphselectMonthTM = async (months) => {
		let y = new Date().getFullYear();
		let month = y + '-' + months;
		let claim = 'all';
		let res = await GET(GET_MONTH_INCOME(claim, month));
		// console.log('res', res);
		let intime_income_tm = res.result.tm.sum_income.intime_income;
		let overtime_income_tm = res.result.tm.sum_income.overtime_income;
		let total_tm = [];

		intime_income_tm.forEach((el, i) => {
			let sum = intime_income_tm[i] + overtime_income_tm[i];
			total_tm.push(sum);
		});
		let total_graph_tm = total_tm.map((e, i) => {
			let a = i;
			return a + 1;
		});

		let sumlist_intime_tm = res.result.tm.sum_list.intime_list;
		let sumlist_overtime_tm = res.result.tm.sum_list.overtime_list;
		let date_sumlist_tm = sumlist_intime_tm.map((e, i) => {
			let a = i;
			return a + 1;
		});

		let option_tm = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'วันที่'
				},
				categories: total_graph_tm
			},
			yAxis: {
				title: {
					text: 'จำนวนเงิน'
				}
			},
			tooltip: {
				crosshairs: true,
				formatter: this.formatTooltip
			},
			series: [
				{
					name: 'จำนวนเงิน',
					data: total_tm
				}
			]
		};
		let list_tm = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'วันที่'
				},
				categories: date_sumlist_tm
			},
			yAxis: {
				title: {
					text: 'จำนวนรายการ'
				}
			},
			tooltip: {
				formatter: function() {
					var s = '<b>' + 'วันที่  ' + this.x + '</b>',
						sum = 0;
					$.each(this.points, function(i, point) {
						s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
						sum += point.y;
					});
					s += '<br/>ทั้งหมด : ' + sum;
					return s;
				},
				crosshairs: true,
				shared: true
			},
			series: [
				{
					name: 'ในเวลา',
					data: sumlist_intime_tm
				},
				{
					name: 'นอกเวลา',
					data: sumlist_overtime_tm,
					color: 'orange'
				}
			]
		};
		this.setState({
			option_tm: option_tm,
			list_tm: list_tm
		});
	};
	graphselectYearTM = async (years) => {
		let year = years;
		let claim = 'all';
		let res_year = await GET(GET_YEAR_INCOME(claim, year));

		let intime_income_tm_year = res_year.result.tm.sum_income.intime_income;
		let overtime_income_tm_year = res_year.result.tm.sum_income.overtime_income;
		let total_tm_year = [];
		intime_income_tm_year.forEach((el, i) => {
			let sum = intime_income_tm_year[i] + overtime_income_tm_year[i];
			total_tm_year.push(sum);
		});

		let sumlist_intime_tm_year = res_year.result.tm.sum_list.intime_list;
		let sumlist_overtime_tm_year = res_year.result.tm.sum_list.overtime_list;

		let option_tm_year = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'เดือน'
				},
				categories: [
					'มกราคม',
					'กุมภาพันธ์',
					'มีนาคม',
					'เมษายน',
					'พฤษภาคม',
					'มิถุนายน',
					'กรกฏาคม',
					'สิงหาคม',
					'กันยายน',
					'ตุลาคม',
					'พฤศจิกายน',
					'ธันวาคม'
				]
			},
			yAxis: {
				title: {
					text: 'จำนวนเงิน'
				}
			},
			tooltip: {
				crosshairs: true,
				formatter: this.formatTooltipYear
			},
			series: [
				{
					name: 'จำนวนเงิน',
					data: total_tm_year
				}
			]
		};
		let list_tm_year = {
			title: {
				text: null
			},
			xAxis: {
				title: {
					text: 'เดือน'
				},
				categories: [
					'มกราคม',
					'กุมภาพันธ์',
					'มีนาคม',
					'เมษายน',
					'พฤษภาคม',
					'มิถุนายน',
					'กรกฏาคม',
					'สิงหาคม',
					'กันยายน',
					'ตุลาคม',
					'พฤศจิกายน',
					'ธันวาคม'
				]
			},
			yAxis: {
				title: {
					text: 'จำนวนรายการ'
				}
			},
			tooltip: {
				formatter: function() {
					var s = '<b>' + 'เดือน  ' + this.x + '</b>',
						sum = 0;
					$.each(this.points, function(i, point) {
						s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
						sum += point.y;
					});
					s += '<br/>ทั้งหมด : ' + sum;
					return s;
				},
				crosshairs: true,
				shared: true
			},
			series: [
				{
					name: 'ในเวลา',
					data: sumlist_intime_tm_year
				},
				{
					name: 'นอกเวลา',
					data: sumlist_overtime_tm_year,
					color: 'orange'
				}
			]
		};
		this.setState({
			option_tm_year: option_tm_year,
			list_tm_year: list_tm_year
		});
	};
	render() {
		let {
			date_month,
			date_year,
			//---------MT------------
			option,
			list,
			option_year,
			list_year,
			//---------PT---------------
			option_pt,
			list_pt,
			option_pt_year,
			list_pt_year,
			//---------TM-------------
			option_tm,
			list_tm,
			option_tm_year,
			list_tm_year,
			//--------------DATA-DONUT-PT---------
			data_donut,
			sum_data_donut,
			data_donut_intime,
			data_donut_overtime,
			data_donut_list,
			sum_data_donut_list,
			data_donut_intime_list,
			data_donut_overtime_list,
			//--------------DATA-DONUT-MT---------
			data_donut_mt,
			sum_data_donut_mt,
			data_donut_intime_mt,
			data_donut_overtime_mt,
			data_donut_list_mt,
			sum_data_donut_list_mt,
			data_donut_intime_list_mt,
			data_donut_overtime_list_mt,
			//--------------DATA-DONUT-TM---------
			data_donut_tm,
			sum_data_donut_tm,
			data_donut_intime_tm,
			data_donut_overtime_tm,
			data_donut_list_tm,
			sum_data_donut_list_tm,
			data_donut_intime_list_tm,
			data_donut_overtime_list_tm,
			//-------------------------------------
			date_date,
			user_type
		} = this.state;
		return (
			<div className="div-graph">
				<Row className="w-100">
					<Col className=" d-flex justify-content-end mt-2">
						{/* <Link to="/Income_document">
							<button id="div-button">สรุปรายได้</button>
						</Link> */}
					</Col>
				</Row>
				{user_type === 'frontMT' ? data_donut.length > 0 ? (
					<Row className="w-100">
						<Col>
							<Graph_right
								option={option}
								list={list}
								graphselectMonthMT={(e) => this.graphselectMonthMT(e)}
								graphselectYearMT={(e) => this.graphselectYearMT(e)}
								date_month={date_month}
								option_year={option_year}
								list_year={list_year}
								date_year={date_year}
								//-------------------------------
								data_donut_mt={data_donut_mt}
								sum_data_donut_mt={sum_data_donut_mt}
								data_donut_intime_mt={data_donut_intime_mt}
								data_donut_overtime_mt={data_donut_overtime_mt}
								data_donut_list_mt={data_donut_list_mt}
								sum_data_donut_list_mt={sum_data_donut_list_mt}
								data_donut_intime_list_mt={data_donut_intime_list_mt}
								data_donut_overtime_list_mt={data_donut_overtime_list_mt}
								//------------------------------------------------------------------
								date_date={date_date}
							/>
						</Col>
					</Row>
				) : (
					<h5>---ไม่พบข้อมูล---</h5>
				) : user_type === 'frontPT' || user_type === 'frontTM' ? (
					<Row className="w-100">
						<Col xl="6">
							<Graph_left_top
								option_pt={option_pt}
								list_pt={list_pt}
								date_month={date_month}
								date_year={date_year}
								graphselectMonthPT={(e) => this.graphselectMonthPT(e)}
								graphselectYearPT={(e) => this.graphselectYearPT(e)}
								option_pt_year={option_pt_year}
								list_pt_year={list_pt_year}
								data_donut={data_donut}
								sum_data_donut={sum_data_donut}
								data_donut_intime={data_donut_intime}
								data_donut_overtime={data_donut_overtime}
								data_donut_list={data_donut_list}
								sum_data_donut_list={sum_data_donut_list}
								data_donut_intime_list={data_donut_intime_list}
								data_donut_overtime_list={data_donut_overtime_list}
								date_date={date_date}
							/>
						</Col>
						<Col xl="6">
							<Graph_right_top
								option_tm={option_tm}
								list_tm={list_tm}
								date_month={date_month}
								date_year={date_year}
								graphselectMonthTM={(e) => this.graphselectMonthTM(e)}
								graphselectYearTM={(e) => this.graphselectYearTM(e)}
								option_tm_year={option_tm_year}
								list_tm_year={list_tm_year}
								//-----------------------------------------------------------
								data_donut_tm={data_donut_tm}
								sum_data_donut_tm={sum_data_donut_tm}
								data_donut_intime_tm={data_donut_intime_tm}
								data_donut_overtime_tm={data_donut_overtime_tm}
								data_donut_list_tm={data_donut_list_tm}
								sum_data_donut_list_tm={sum_data_donut_list_tm}
								data_donut_intime_list_tm={data_donut_intime_list_tm}
								data_donut_overtime_list_tm={data_donut_overtime_list_tm}
								//-----------------------------------------------------------
								date_date={date_date}
							/>
						</Col>
					</Row>
				) : data_donut.length > 0 ? (
					<Row className="w-100">
						<Col xl="6">
							<Graph_left_top
								option_pt={option_pt}
								list_pt={list_pt}
								date_month={date_month}
								date_year={date_year}
								graphselectMonthPT={(e) => this.graphselectMonthPT(e)}
								graphselectYearPT={(e) => this.graphselectYearPT(e)}
								option_pt_year={option_pt_year}
								list_pt_year={list_pt_year}
								data_donut={data_donut}
								sum_data_donut={sum_data_donut}
								data_donut_intime={data_donut_intime}
								data_donut_overtime={data_donut_overtime}
								data_donut_list={data_donut_list}
								sum_data_donut_list={sum_data_donut_list}
								data_donut_intime_list={data_donut_intime_list}
								data_donut_overtime_list={data_donut_overtime_list}
								date_date={date_date}
							/>
						</Col>
						<Col xl="6">
							<Graph_right_top
								option_tm={option_tm}
								list_tm={list_tm}
								date_month={date_month}
								date_year={date_year}
								graphselectMonthTM={(e) => this.graphselectMonthTM(e)}
								graphselectYearTM={(e) => this.graphselectYearTM(e)}
								option_tm_year={option_tm_year}
								list_tm_year={list_tm_year}
								//-----------------------------------------------------------
								data_donut_tm={data_donut_tm}
								sum_data_donut_tm={sum_data_donut_tm}
								data_donut_intime_tm={data_donut_intime_tm}
								data_donut_overtime_tm={data_donut_overtime_tm}
								data_donut_list_tm={data_donut_list_tm}
								sum_data_donut_list_tm={sum_data_donut_list_tm}
								data_donut_intime_list_tm={data_donut_intime_list_tm}
								data_donut_overtime_list_tm={data_donut_overtime_list_tm}
								//-----------------------------------------------------------
								date_date={date_date}
							/>
						</Col>
						<Col>
							<Graph_right
								option={option}
								list={list}
								graphselectMonthMT={(e) => this.graphselectMonthMT(e)}
								graphselectYearMT={(e) => this.graphselectYearMT(e)}
								date_month={date_month}
								option_year={option_year}
								list_year={list_year}
								date_year={date_year}
								//----------------------------------------------
								data_donut_mt={data_donut_mt}
								sum_data_donut_mt={sum_data_donut_mt}
								data_donut_intime_mt={data_donut_intime_mt}
								data_donut_overtime_mt={data_donut_overtime_mt}
								data_donut_list_mt={data_donut_list_mt}
								sum_data_donut_list_mt={sum_data_donut_list_mt}
								data_donut_intime_list_mt={data_donut_intime_list_mt}
								data_donut_overtime_list_mt={data_donut_overtime_list_mt}
								//------------------------------------------------------------------
								date_date={date_date}
							/>
						</Col>
					</Row>
				) : (
					<h5>---ไม่พบข้อมูล---</h5>
				)}
			</div>
		);
	}
}
