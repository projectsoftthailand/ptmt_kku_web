import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Clock from 'react-live-clock';
import moment from 'moment';
import Modal from 'react-modal';
import { Input } from 'reactstrap';
import { IoMdClose } from 'react-icons/io';
import Color from '../../../../components/Color';
import { Animated } from 'react-animated-css';
import withDragDropContext from '../../../../libs/scheddule/example/withDnDContext';
import Scheduler, {
	SchedulerData,
	ViewTypes,
	DATE_FORMAT,
	DemoData,
	AddMorePopover,
	CellUnits
} from '../../../../libs/scheddule/src/index';
import { GET, GET_USERS, GET_LIST } from '../../../../service/service';
import 'react-big-scheduler/lib/css/style.css';
// Modal.setAppElement(document.getElementById('root'));

class ScheduleModal extends Component {
	constructor(props) {
		super(props);

		let schedulerData = new SchedulerData(moment(new Date()).format('YYYY-MM-DD'), ViewTypes.Day, false, false, {
			nonAgendaDayCellHeaderFormat: 'HH:mm'
		});
		schedulerData.localeMoment.locale('th-TH');
		schedulerData.setResources(DemoData.resources);
		schedulerData.setEvents(DemoData.events);
		this.state = {
			viewModel: schedulerData
		};
	}

	componentWillMount = async () => {
		let r = localStorage.getItem('USER');
		let role = JSON.parse(r);
		// console.log(role.role);
		if (role.role === 'frontMT') {
			try {
				let res = await GET(GET_USERS('all'));
				let ress = await GET(GET_LIST('all', 'all'));
				let mt = [];
				let patient_mt = [];
				// console.log('ress', ress);
				// console.log(
				// 	'ress',
				// 	ress.result.map((e) => {
				// 		if (e.service_status < 99) {
				// 			return e;
				// 		}
				// 	})
				// );
				res.result.forEach((e, index) => {
					if (e.role_name === 'mt') {
						mt.push({ id: 'r' + e.user_id, name: e.th_name + ' ' + e.th_lastname });
					}
				});

				ress.result.forEach((e, index) => {
					if (e.type === 'mt' && e.service_status < 99) {
						patient_mt.push({
							id: e.list_id,
							start: e.service_at,
							end: moment(e.service_at).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
							resourceId: 'r' + e.doctor,
							title: e.th_prefix + e.th_name + ' ' + e.th_lastname,
							resizable: false,
							movable: false
						});
					}
				});
				// console.log('patient_mt', patient_mt);
				this.setState({ mt, patient_mt }, () => {
					let schedulerData = new SchedulerData(
						moment(new Date()).format('YYYY-MM-DD'),
						ViewTypes.Day,
						false,
						false,
						{
							nonAgendaDayCellHeaderFormat: 'HH:mm'
						}
					);
					let { mt, patient_mt } = this.state;
					schedulerData.setResources(mt);
					schedulerData.setEvents(patient_mt);
					this.setState({
						viewModel: schedulerData
					});
				});
			} catch (error) {
				// console.log('error GetDoctor : ', error)
			}
		}
		if (role.role === 'frontPT') {
			try {
				let res = await GET(GET_USERS('all'));
				let ress = await GET(GET_LIST('all', 'all'));
				let pt = [];
				let patient_pt = [];
				// console.log('ress', ress);
				// console.log(
				// 	'ress',
				// 	ress.result.map((e) => {
				// 		if (e.service_status < 99) {
				// 			return e;
				// 		}
				// 	})
				// );
				res.result.forEach((e, index) => {
					if (e.role_name === 'pt') {
						pt.push({ id: 'r' + e.user_id, name: e.th_name + ' ' + e.th_lastname });
					}
				});

				ress.result.forEach((e, index) => {
					if (e.type === 'pt' && e.service_status < 99) {
						patient_pt.push({
							id: e.list_id,
							start: e.service_at,
							end: moment(e.service_at).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
							resourceId: 'r' + e.doctor,
							title: e.th_prefix + e.th_name + ' ' + e.th_lastname,
							resizable: false,
							movable: false
						});
						// pt.push({ id: 'r' + e.user_id, name: e.th_name + ' ' + e.th_lastname });
					}
				});
				// console.log('patient_mt', patient_mt);
				this.setState({ pt, patient_pt }, () => {
					let schedulerData = new SchedulerData(
						moment(new Date()).format('YYYY-MM-DD'),
						ViewTypes.Day,
						false,
						false,
						{
							nonAgendaDayCellHeaderFormat: 'HH:mm'
						}
					);
					let { pt, patient_pt } = this.state;
					schedulerData.setResources(pt);
					schedulerData.setEvents(patient_pt);
					this.setState({
						viewModel: schedulerData
					});
				});
			} catch (error) {
				// console.log('error GetDoctor : ', error)
			}
		}
	};

	render() {
		const { viewModel, mt } = this.state;
		let { openModal, Modalfalse, cashIn, SetCashIn, servicePrice, outSidePrice, OnSubmit } = this.props;
		// console.log('viewModel', viewModel);

		return (
			<div
				style={{
					backgroundColor: 'white',
					borderRadius: '15px'
				}}
			>
				<div
					style={{
						paddingLeft: '25px',
						paddingRight: '25px',
						paddingTop: '25px',
						paddingBottom: '25px'
					}}
				>
					<div
						className="btn-exit-hover"
						onClick={() => Modalfalse()}
						style={{
							position: 'absolute',
							backgroundColor: Color.Blue,
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'center',
							borderRadius: '100%',
							width: 20,
							height: 20,
							cursor: 'pointer',
							top: 20,
							right: 20,
							marginRight: '25px',
							marginTop: '25px'
						}}
					>
						<IoMdClose color="white" size={18} />
					</div>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<Scheduler
							schedulerData={viewModel}
							prevClick={this.prevClick}
							nextClick={this.nextClick}
							onSelectDate={this.onSelectDate}
							// onScrollLeft={this.onScrollLeft}
							// onScrollRight={this.onScrollRight}
						/>
					</Animated>
				</div>
			</div>
		);
	}

	prevClick = (schedulerData) => {
		let { patient_mt } = this.state;
		schedulerData.prev();
		schedulerData.setEvents(patient_mt);
		this.setState({
			viewModel: schedulerData
		});
	};
	nextClick = (schedulerData) => {
		let { patient_mt } = this.state;
		schedulerData.next();
		schedulerData.setEvents(patient_mt);
		this.setState({
			viewModel: schedulerData
		});
	};
	onSelectDate = (schedulerData, date) => {
		let { patient_mt } = this.state;
		schedulerData.setDate(date);
		schedulerData.setEvents(patient_mt);
		this.setState({
			viewModel: schedulerData
		});
	};
	// onScrollRight = (schedulerData, schedulerContent, maxScrollLeft) => {
	// 	if (schedulerData.ViewTypes === ViewTypes.Day) {
	// 		schedulerData.next();
	// 		schedulerData.setEvents(DemoData.events);
	// 		this.setState({
	// 			viewModel: schedulerData
	// 		});

	// 		schedulerContent.scrollLeft = maxScrollLeft - 10;
	// 	}
	// };
	// onScrollLeft = (schedulerData, schedulerContent, maxScrollLeft) => {
	// 	if (schedulerData.ViewTypes === ViewTypes.Day) {
	// 		schedulerData.prev();
	// 		schedulerData.setEvents(DemoData.events);
	// 		this.setState({
	// 			viewModel: schedulerData
	// 		});

	// 		schedulerContent.scrollLeft = 10;
	// 	}
	// };
	// toggleExpandFunc = (schedulerData, slotId) => {
	// 	schedulerData.toggleExpandStatus(slotId);
	// 	this.setState({
	// 		viewModel: schedulerData
	// 	});
	// };
}
export default withDragDropContext(ScheduleModal);
