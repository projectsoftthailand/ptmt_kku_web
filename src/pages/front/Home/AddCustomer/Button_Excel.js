import React, { Component } from 'react';
import './style/Addcustomer.css';
import XLSX from 'xlsx';
import { make_cols } from './cardData/MakeColumns';
import excel from '../../../../assets/image/uploadfileExcel.png';
import ReactExport from 'react-export-excel';
import moment from 'moment';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const dataSet1 = [];

export default class Button_Excel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			file: {},
			data: [],
			cols: []
		};
		this.uploadFiles = this.uploadFiles.bind(this);
	}
	uploadFiles(e) {
		const files = e.target.files;
		if (files && files[0])
			this.setState({ file: files[0] }, () => {
				/* Boilerplate to set up FileReader */
				const reader = new FileReader();
				const rABS = !!reader.readAsBinaryString;

				reader.onload = (e) => {
					/* Parse data */
					const bstr = e.target.result;
					const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA: true });
					/* Get first worksheet */
					const wsname = wb.SheetNames[0];
					const ws = wb.Sheets[wsname];
					/* Convert array of arrays */
					const data = XLSX.utils.sheet_to_json(ws);
					/* Update state */
					this.setState({ data: data, cols: make_cols(ws['!ref']) }, () => {
						let data = this.state.data.map((e) => {
							let unit = this.props.unit;
							let date = moment().format('DD-MM-YYYY');
							let time = moment().format('HH:mm:ss');
							let age = moment().diff(e['เกิดวันที่(ปี-เดือน-วัน)'], 'years');
							return {
								frist: e['ชื่อ'],
								last: e['นามสกุล'],
								date,
								time,
								unit,
								key: e['คำนำหน้า'],
								index: '',
								age
							};
						});
						let objs = this.state.data.map((e) => {
							return {
								citizen_id: e['เลขประจำตัวประชาชน'],
								th_prefix: e['คำนำหน้า'],
								th_name: e['ชื่อ'],
								th_lastname: e['นามสกุล'],
								en_prefix: '',
								en_name: '',
								en_lastname: '',
								foreigner: '',
								password: e.Password,
								role_id: 6,
								birthday: e['เกิดวันที่(ปี-เดือน-วัน)'],
								sex: e['เพศ'],
								phone: e['เบอร์โทรศัพท์'],
								allergy: e['ยาที่แพ้'],
								congenital_disease: e['โรคประจำตัว'],
								issue: e['วันออกบัตร(ปี-เดือน-วัน)'],
								expire: e['วันหมดอายุบัตร(ปี-เดือน-วัน)'],
								address: e['ที่อยู่'],
								nationality: e['สัญชาติ'],
								religion: e['ศาสนา']
							};
						});
						this.props.obj(data);
						this.props.organization_list(objs);
					});
				};

				if (rABS) {
					reader.readAsBinaryString(this.state.file);
				} else {
					reader.readAsArrayBuffer(this.state.file);
				}
			});
	}

	render() {
		return (
			<div style={{ display: 'flex' }}>
				<ExcelFile
					element={
						<label style={{ marginBottom: 'unset' }} id="btn-form-excel">
							<div>
								<img src={excel} style={{ width: '20px', height: '20px' }} /> <div>แบบฟอร์ม</div>
							</div>
						</label>
					}
				>
					<ExcelSheet data={dataSet1} name="แบบฟอร์มกรอกรายชื่อ">
						<ExcelColumn label="เลขประจำตัวประชาชน" />
						<ExcelColumn label="Password" />
						<ExcelColumn label="คำนำหน้า" />
						<ExcelColumn label="ชื่อ" />
						<ExcelColumn label="นามสกุล" />
						<ExcelColumn label="เบอร์โทรศัพท์" />
						<ExcelColumn label="เพศ" />
						<ExcelColumn label="เกิดวันที่(ปี-เดือน-วัน)" />
						<ExcelColumn label="สัญชาติ" />
						<ExcelColumn label="ศาสนา" />
						<ExcelColumn label="วันออกบัตร(ปี-เดือน-วัน)" />
						<ExcelColumn label="วันหมดอายุบัตร(ปี-เดือน-วัน)" />
						<ExcelColumn label="ที่อยู่" />
						<ExcelColumn label="โรคประจำตัว" />
						<ExcelColumn label="ยาที่แพ้" />
					</ExcelSheet>
				</ExcelFile>

				<label style={{ marginBottom: 'unset' }} className="btn-excel">
					<div>
						<input
							type="file"
							// id="uploadfile"
							onChange={this.uploadFiles}
							accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
						/>
						<div style={{ fontWeight: 'lighter' }}>+ เพิ่มผู้รับบริการ Excel</div>
					</div>
				</label>
			</div>
		);
	}
}
