import React, { Component } from 'react';
import Color from '../../../../components/Color';
import { Label, Input, Table } from 'reactstrap';
import { GET, GET_BED, GET_STATUS, GET_BOOKING, _ip } from '../../../../service/service';
import Table_2 from '../../../../components/Table_2';
import moment from 'moment';
import socketIOClient from 'socket.io-client';
import BedItem from './BedItem';
import swal from 'sweetalert';

const timeString = [
	'08:30 - 09:00',
	'09:01 - 09:30',
	'09:31 - 10:00',
	'10:01 - 10:30',
	'10:31 - 11:00',
	'11:00 - 11:30',
	'11:31 - 12:00',
	'12:01 - 12:30',
	'12:31 - 13:00',
	'13:01 - 13:30',
	'13:31 - 14:00',
	'14:00 - 14:30',
	'14:31 - 15:00',
	'15:01 - 15:30',
	'15:31 - 16:00',
	'16:01 - 16:30',
	'16:31 - 17:00',
	'17:00 - 17:30',
	'17:31 - 18:00',
	'18:01 - 18:30'
];

export default class ChooseBed extends Component {
	constructor(props) {
		super(props);

		this.state = {
			bedData: [],
			displayBedComponent: [],
			isReady: false,
			timeMnArray: [],
			bedStatus: [],

			bedTimeArray: [],
			displayBed: [],

			bookDate: '',
			status: [],

			initDateTimeArray: []
		};
	}

	componentDidMount = async () => {
		let curDate = moment().format('YYYY-MM-DD');
		await this.setState({ bookDate: curDate });
		await this.GetStatus();
		await this.GetBed();
		await this.createTimeMnArray();
		//   await this.GetBooking()
		//   await this.createBedArray()
		await this.setState({ isReady: true });
		// this.response();
	};

	response() {
		socketIOClient(_ip).on('rerender', (res) => {
			this.reRender();
		});
	}

	reRender() {
		// console.log("fetch..");
		// this.GetBooking()
	}

	isBetween(x, min, max) {
		//   if(x>=min && x<=max) {
		//     console.log('<'+min+' , '+x+' , '+max+'>')
		//   }
		return x >= min && x <= max;
	}

	async createTimeMnArray() {
		let { bedData } = this.state;
		let timeArray = [];

		timeString.forEach((e, index) => {
			let tString = e.split(' - ');
			let startTimeString = tString[0];
			let endTimeString = tString[1];

			let startTimeInt = startTimeString.split(':');
			startTimeInt = +startTimeInt[0] * 60 + +startTimeInt[1];

			let endTimeInt = endTimeString.split(':');
			endTimeInt = +endTimeInt[0] * 60 + +endTimeInt[1];

			timeArray.push({ start: startTimeInt, end: endTimeInt, status: -1 });
		});
		let bedTimeArray = [];
		// console.log('timeArray is ', timeArray)
		for (let i = 0; i < bedData.length; i++) {
			let t = JSON.parse(JSON.stringify(timeArray)); // without reference
			bedTimeArray.push({ id: bedData[i].id, timeArray: t });
		}
		this.setState(
			{
				displayBed: bedTimeArray,
				bedTimeArray: bedTimeArray,
				initDateTimeArray: JSON.parse(JSON.stringify(bedTimeArray))
			},
			() => {
				// console.log('bedTimeArray : ', this.state.displayBed)
				this.GetBooking();
			}
		);
	}

	async GetBed() {
		try {
			let res = await GET(GET_BED);
			// console.log('res GetBed : ', res)
			await this.setState({ bedData: res.result });
		} catch (error) {
			// console.log("error GetBed : ", error);
		}
	}

	async GetStatus() {
		try {
			let res = await GET(GET_STATUS);
			// console.log("res GetStatus : ", res.result);
			this.setState({ status: res.result });
		} catch (error) {
			// console.log("error GetStatus : ", error);
		}
	}

	async GetBooking() {
		// clear 1st
		await this.setState({ displayBed: this.state.initDateTimeArray });
		try {
			let bedTimeArray = this.state.bedTimeArray;
			let bookDate = this.state.bookDate;
			//   if(!isChange) {
			//       bedTimeArray = this.state.bedTimeArray
			//   }else {
			//       bedTimeArray = this.state.initDateTimeArray
			//   }
			let res = await GET(GET_BOOKING('pt', bookDate));
			// console.log("res GetBooking : ", res.result);
			if (res.success === false) {
				this.createBedArray(true);
				return;
			}
			res.result.forEach((e, i) => {
				let bookingAtString = e.booking_at.split('T')[1].split('.')[0].split(':');
				let bookingAtInt = +bookingAtString[0] * 60 + +bookingAtString[1];

				let bookingEndString = e.booking_end.split('T')[1].split('.')[0].split(':');
				let bookingEndInt = +bookingEndString[0] * 60 + +bookingEndString[1];

				let index = bedTimeArray.findIndex((x) => x.id === e.bed); // bedindex
				bedTimeArray[index].timeArray.forEach(async (ei, i) => {
					if (this.isBetween(bookingAtInt, ei.start, ei.end)) {
						for (let j = i; j < bedTimeArray[index].timeArray.length; j++) {
							if (
								this.isBetween(
									bookingEndInt,
									bedTimeArray[index].timeArray[j].start,
									bedTimeArray[index].timeArray[j].end
								)
							) {
								let db = JSON.parse(JSON.stringify(this.state.displayBed));
								// let db = this.state.displayBed
								for (let m = i; m <= j; m++) {
									db[index].timeArray[m].status = e.service_status;
								}
								await this.setState({ displayBed: db });
								break;
							}
						}
						return;
					}
				});

				if (i === res.result.length - 1) {
					this.createBedArray();
				}
			});
		} catch (error) {
			// console.log("error GetBooking : ", error);
		}
	}

	createBedArray(isNew) {
		let { status, bookDate, initDateTimeArray } = this.state;
		let displayBed;
		let d = [];
		let today = moment().format('YYYY-MM-DD');

		let todayInt = today.split('-');
		todayInt = +todayInt[0] * 365 + +todayInt[1] * 30 + +todayInt[2];
		let bookdateInt = bookDate.split('-');
		bookdateInt = +bookdateInt[0] * 365 + +bookdateInt[1] * 30 + +bookdateInt[2];

		// if isNew go check date and time for disable booking
		// if same day need to check time, else not need to check time

		if (isNew) {
			// is new is mean no data in that selected date
			console.log('is not have');
			// set displayBed to init
			displayBed = this.state.initDateTimeArray;
			// if is old day
			// console.log("bookdate is " + bookdateInt + " , curdate is " + todayInt);
			if (bookdateInt < todayInt) {
				// is old date
				//set all can not book
				displayBed.forEach((e, i) => {
					let s = [];
					e.timeArray.forEach((e2, j) => {
						s.push(<BedItem status={-2} statusTypes={status} number={e.id} />);
					});
					d.push(s);
				});
			} else {
				// is today or future
				// set can book
				if (todayInt === bookdateInt) {
					// if today
					// have to check cur time
					// if still in time set can book but not in time set cannot book
				} else {
					// is future
					displayBed.forEach((e, i) => {
						let s = [];
						e.timeArray.forEach((e2, j) => {
							s.push(<BedItem status={-1} statusTypes={status} number={e.id} />);
						});
						d.push(s);
					});
				}
			}
		} else {
			// is mean have some booked in that date
			console.log('is have');
			if (bookdateInt < todayInt) {
				// is old date
				// set all can not book with still have old status booked
				displayBed = this.state.displayBed;
				displayBed.forEach((e, i) => {
					let s = [];
					e.timeArray.forEach((e2, j) => {
						s.push(
							<BedItem status={e2.status !== -1 ? e2.status : -2} statusTypes={status} number={e.id} />
						);
					});
					d.push(s);
				});
			} else {
				// is today of future
				// set can book
				if (todayInt === bookdateInt) {
					// if today
					// have to check cur time
					// if still in time set can book but not in time set cannot book
				} else {
					// is future
					// set can book all with old status
					displayBed = this.state.displayBed;
					displayBed.forEach((e, i) => {
						let s = [];
						e.timeArray.forEach((e2, j) => {
							s.push(
								<BedItem
									status={e2.status !== -1 ? e2.status : -1}
									statusTypes={status}
									number={e.id}
								/>
							);
						});
						d.push(s);
					});
				}
			}
		}
		this.setState({ displayBedComponent: d });
	}

	OnChangeDate = async (newDate) => {
		let init = this.state.initDateTimeArray;
		// console.log("init is ", init);
		await this.setState({ bookDate: newDate });
		// clear data 1st
		this.GetBooking();
	};

	render() {
		let { isReady, bedData, displayBedComponent, bookDate } = this.state;
		// console.log('data : ', bedData)

		if (isReady) {
			return (
				<div style={{ marginTop: -10 }} className="bg-white rounded">
					<div className="py-4 px-2">
						<h4 className="ml-2" style={{ color: Color.Blue }}>
							เลือกเตียง
						</h4>
						<div className="d-flex justify-content-between px-2 align-items-center">
							<div className="d-flex">
								<Label className="mt-2" style={{ width: 100 }}>
									เวลานัดหมาย
								</Label>
								<Input
									onChange={(e) => this.OnChangeDate(e.target.value)}
									value={bookDate}
									className="mt-1"
									style={{ width: '55%' }}
									type="date"
								/>
							</div>
							<div>เลือก เตียง 2 เวลา 10:30-11:30</div>
						</div>
					</div>
					<div className="px-3">
						<Table_2 head={timeString} rows={displayBedComponent} />
					</div>
				</div>
			);
		} else {
			return <div>Loading...</div>;
		}
	}
}
