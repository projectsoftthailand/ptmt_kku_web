import React, { Component } from 'react';
import { Container, Row, Col, Button, ButtonGroup, Input, FormGroup } from 'reactstrap';
import { GET, POST, GET_USERS, GET_BOOKING } from '../../../../service/service';
import moment from 'moment';
import './style/Time_docterPT.css';
import Responsive from 'react-responsive';
import DatePicker from 'react-datepicker';
let Min = (props) => <Responsive {...props} minWidth={631} maxWidth={991} />;
let Max = (props) => <Responsive {...props} minWidth={1091} />;

var ThaiMassage_in = [ '08:30', '10:00', '11:30', '12:30', '14:00', '15:30' ];
var ThaiMassage_out = [ '17:00' ];

var Ortho_in = [ '08:30', '09:15', '10:00', '10:45', '13:00', '13:45', '14:30', '15:15' ];
var Ortho_out = [ '16:30', '17:15', '18:00', '18:45' ];
var Ortho_full = [
	'08:30',
	'09:15',
	'10:00',
	'10:45',
	'13:00',
	'13:45',
	'14:30',
	'15:15',
	'16:30',
	'17:15',
	'18:00',
	'18:45'
];

let Neuro_in = [ '08:30', '10:00', '13:00', '14:30' ];
let Neuro_out = [ '16:30', '18:00' ];
let Neuro_full = [ '08:30', '10:00', '13:00', '14:30', '16:30', '18:00' ];

export default class inPT extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataDoctor: [],
			dataBooking: [],
			startDate: new Date(),
			startTime: '',
			docterID: '',
			role: '',
			time_select: ''
		};
	}

	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		let USER = JSON.parse(localStorage.getItem('USER'));
		this.props.onRef(this);
		// await setTimeout(async () => {
		// 	let state = await JSON.parse(localStorage.getItem('stateinPT'));
		// 	if (state) {
		// 		if (state.list_id == this.props.list_id && this.props.save == false) {
		// 			await this.setState(state);
		// 			console.log('stateinPT', state);
		// 		} else {
		await Promise.all([ this.getDoctor(), this.getBooking() ]);
		// }
		// 	} else {
		// 		await Promise.all([ this.getDoctor(), this.getBooking() ]);
		// 		this.state['list_id'] = this.props.list_id;
		// 		localStorage.setItem('stateinPT', JSON.stringify(this.state));
		// 	}
		// }, 0.1);
		await this.setState({
			role: USER.role
		});
	}
	componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		localStorage.setItem('stateinPT', JSON.stringify(this.state));
	}
	async getDoctor() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		if (USER.role == 'frontPT' || USER.role == 'pt') {
			let getuser = await GET(GET_USERS('pt'));
			let { type_Service, type_Time } = this.props;
			let type =
				type_Service == 1
					? 'ThaiMassage'
					: type_Service == 2 ? 'Ortho' : type_Service == 3 ? 'Neuro' : type_Service == 4 ? 'Chest' : 'Child';
			if (getuser.success) {
				this.setState({
					dataDoctor: getuser.result
						.filter((el, i) => {
							if (el.sub_role.some((e) => e == type)) {
								return true;
							}
						})
						.filter((e) => (type_Time == 1 ? e.activeIntime == 1 : e.activeOuttime == 2))
				});
			}
		}
	}

	async getBooking() {
		let date = moment(this.state.startDate).format('YYYY-MM-DD');
		let { type_Service } = this.props;
		// let getbook = await GET(GET_BOOKING(type_Service == 1 ? 'tm' : 'pt', date));
		let getbook = await GET(GET_BOOKING('all', date));
		// console.log('getbook', getbook);
		if (getbook.success) {
			this.setState({
				dataBooking: getbook.result
					.filter((e) => e.type == 'pt' || e.type == 'tm')
					.filter((e) => e.service_status < 99)
			});
		}
	}

	returnState() {
		let { startDate, startTime, docterID } = this.state;
		let service_at = moment(startDate).format('YYYY-MM-DD') + ' ' + startTime;
		return {
			service_at,
			docterID
		};
	}
	render() {
		let { dataDoctor, dataBooking, startDate, role, time_select, docterID } = this.state;
		let { type_Time, type_Service, holiDay } = this.props;
		let type = type_Service === 1 ? 'tm' : 'pt';
		let Booking = dataBooking.filter((e) => e.type === type);
		// console.log('date', moment(new Date()).add(7, 'day').format('DD MMMM YYYY'));

		return (
			<div style={{ backgroundColor: '#fff', borderRadius: '0.2rem', padding: '1rem', paddingBottom: 'unset' }}>
				<Row>
					<Col xs={12}>
						<div className="headselect">
							<text className="selectDoc">
								เลือก{Number(type_Service) === 1 ? (
									'เจ้าหน้าที่นวดไทย'
								) : Number(type_Service) === 2 || Number(type_Service) === 3 ? (
									'นักกายภาพบำบัด'
								) : (
									'นักเทคนิคการแพทย์'
								)}
							</text>
							<FormGroup>
								{/* <Input
									className="text-center"
									value={startDate}
									type="date"
									onChange={(e) => {
										console.log('e.target.value', e.target.value);
										if (this.state.role !== 'frontMT') {
											this.props.Date_tmpt(e.target.value);
										}
										holiDay(e.target.value);
										this.setState(
											{
												startDate: e.target.value,
												dataBooking: [],
												time_select: '',
												docterID: ''
											},
											() => {
												this.getBooking();
											}
										);
									}}
								/> */}
								<DatePicker
									id="div-datepicker"
									className="sub-income-input text-center"
									selected={startDate}
									onChange={(e) => {
										let dates = moment(e).format('YYYY-MM-DD');
										if (this.state.role !== 'frontMT') {
											this.props.Date_tmpt(dates);
										}
										holiDay(dates);
										this.setState(
											{
												startDate: e,
												dataBooking: [],
												time_select: '',
												docterID: ''
											},
											() => {
												this.getBooking();
											}
										);
									}}
									locale="th"
									dateFormat="dd-MM-yyyy"
								/>
							</FormGroup>
							<Min>
								<div style={{ fontSize: 'small' }}>
									<Button
										id="btn-times-select-disable"
										disabled
										style={{ width: '6rem', marginRight: '0.2rem' }}
									>
										จองไม่ได้
									</Button>
									<Button id="btn-times-select" style={{ width: '6rem' }}>
										จองได้
									</Button>
								</div>
							</Min>
							<Max>
								<div style={{ fontSize: 'small' }}>
									<Button
										id="btn-times-select-disable"
										disabled
										style={{ width: '6rem', marginRight: '0.2rem' }}
									>
										จองไม่ได้
									</Button>
									<Button id="btn-times-select" style={{ width: '6rem' }}>
										จองได้
									</Button>
								</div>
							</Max>
						</div>
					</Col>
					<Col xs={12} style={{ height: '22rem', overflow: 'auto', paddingBottom: '2rem' }}>
						{dataDoctor.length > 0 ? (
							dataDoctor.map((e) => (
								<div>
									<br />
									<div style={{ fontSize: 'small' }}>คุณหมอ{e.th_name + ' ' + e.th_lastname}</div>
									<hr />
									<Row>
										{/**------------------------------------- นวดไทย --------------------------------------- */}
										{moment(startDate).format('YYYY-MM-DD') ==
											moment(new Date()).format('YYYY-MM-DD') && type_Service == 1 ? (
											(type_Time == 1
												? ThaiMassage_in
												: type_Time == 2 ? ThaiMassage_out : null).map((i) => (
												<Col xs={4} sm={2} className="massage">
													<Button
														id={
															(Booking &&
																Booking.some(
																	(el) =>
																		el.doctor == e.user_id &&
																		moment(el.booking_at).format('HH:mm') == i
																)) ||
															i <=
																moment(new Date())
																	.add(-45, 'minutes')
																	.format('HH:mm') ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															(Booking &&
																Booking.some(
																	(el) =>
																		el.doctor == e.user_id &&
																		moment(el.booking_at).format('HH:mm') == i
																)) ||
															i <=
																moment(new Date())
																	.add(-45, 'minutes')
																	.format('HH:mm') ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false)
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										) : (
											type_Service == 1 &&
											(type_Time == 1
												? ThaiMassage_in
												: type_Time == 2 ? ThaiMassage_out : null).map((i) => (
												<Col xs={4} sm={2} className="massage">
													<Button
														id={
															(Booking &&
																Booking.some(
																	(el) =>
																		el.doctor == e.user_id &&
																		moment(el.booking_at).format('HH:mm') == i
																)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date())
																	.add(6, 'day')
																	.format('YYYY-MM-DD') ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															(Booking &&
																Booking.some(
																	(el) =>
																		el.doctor == e.user_id &&
																		moment(el.booking_at).format('HH:mm') == i
																)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date()).add(6, 'day').format('YYYY-MM-DD')
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										)}

										{/**------------------------------------- กระดูกและกล้ามเนื้อ --------------------------------------- */}
										{moment(startDate).format('YYYY-MM-DD') ==
											moment(new Date()).format('YYYY-MM-DD') && type_Service == 2 ? (
											(type_Time == 1 &&
											moment(startDate).format('ddd') !== 'เสาร์' &&
											moment(startDate).format('ddd') !== 'อาทิตย์'
												? Ortho_in
												: type_Time == 2 &&
													moment(startDate).format('ddd') !== 'เสาร์' &&
													moment(startDate).format('ddd') !== 'อาทิตย์'
													? Ortho_out
													: type_Service == 2 ? Ortho_full : null).map((i) => (
												<Col xs={3} sm={3} className="ortho">
													<Button
														id={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at).format('HH:mm') <= i &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Neuro' ? 89 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') >= i
															// 	)) ||
															// i <= moment(new Date()).add(-10, 'minutes').format('HH:mm') ||
															(e.active == 0 ? (
																true
															) : e.active == 1 &&
															e.activeIntime == 0 &&
															type_Time == 1 ? (
																true
															) : e.active == 1 &&
															e.activeOuttime == 0 &&
															type_Time == 2 ? (
																true
															) : (
																false
															)) ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at).format('HH:mm') <= i &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Neuro' ? 89 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') >= i
															// 	)) ||
															// i <= moment(new Date()).add(-10, 'minutes').format('HH:mm') ||
															e.active == 0 ? (
																true
															) : e.active == 1 &&
															e.activeIntime == 0 &&
															type_Time == 1 ? (
																true
															) : e.active == 1 &&
															e.activeOuttime == 0 &&
															type_Time == 2 ? (
																true
															) : (
																false
															)
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										) : (
											type_Service == 2 &&
											(type_Time == 1 &&
											moment(startDate).format('ddd') !== 'เสาร์' &&
											moment(startDate).format('ddd') !== 'อาทิตย์'
												? Ortho_in
												: type_Time == 2 &&
													moment(startDate).format('ddd') !== 'เสาร์' &&
													moment(startDate).format('ddd') !== 'อาทิตย์'
													? Ortho_out
													: type_Service == 2 && moment(startDate).format('ddd') === 'เสาร์'
														? Ortho_full
														: type_Service == 2 && moment(startDate).format('ddd') === 'อาทิตย์'
															? Ortho_in
															: null).map((i) => (
												<Col xs={3} sm={3} className="ortho">
													<Button
														id={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at).format('HH:mm') <= i &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Neuro' ? 89 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') >= i
															// 	)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date())
																	.add(1, 'month')
																	.format('YYYY-MM-DD') ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at).format('HH:mm') <= i &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Neuro' ? 89 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') >= i
															// 	)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date()).add(1, 'month').format('YYYY-MM-DD')
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										)}
										{/**------------------------------------- ระบบประสาท --------------------------------------- */}
										{moment(startDate).format('YYYY-MM-DD') ==
											moment(new Date()).format('YYYY-MM-DD') && type_Service == 3 ? (
											(type_Time == 1 &&
											moment(startDate).format('ddd') !== 'เสาร์' &&
											moment(startDate).format('ddd') !== 'อาทิตย์'
												? Neuro_in
												: type_Time == 2 &&
													moment(startDate).format('ddd') !== 'เสาร์' &&
													moment(startDate).format('ddd') !== 'อาทิตย์'
													? Neuro_out
													: type_Service == 3 ? Neuro_full : null).map((i) => (
												<Col
													xs={moment(startDate).format('ddd') === 'เสาร์' ? 4 : 3}
													sm={moment(startDate).format('ddd') === 'เสาร์' ? 2 : 3}
													className="text-center"
												>
													<Button
														id={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Ortho' ? -45 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') <= i &&
															// 			moment(el.booking_at).format('HH:mm') >= i
															// 	)) ||
															// i <= moment(new Date()).add(-10, 'minutes').format('HH:mm') ||
															(e.active == 0 ? (
																true
															) : e.active == 1 &&
															e.activeIntime == 0 &&
															type_Time == 1 ? (
																true
															) : e.active == 1 &&
															e.activeOuttime == 0 &&
															type_Time == 2 ? (
																true
															) : (
																false
															)) ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Ortho' ? -45 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') <= i &&
															// 			moment(el.booking_at).format('HH:mm') >= i
															// 	)) ||
															// i <= moment(new Date()).add(-10, 'minutes').format('HH:mm') ||
															e.active == 0 ? (
																true
															) : e.active == 1 &&
															e.activeIntime == 0 &&
															type_Time == 1 ? (
																true
															) : e.active == 1 &&
															e.activeOuttime == 0 &&
															type_Time == 2 ? (
																true
															) : (
																false
															)
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										) : (
											type_Service == 3 &&
											(type_Time == 1 &&
											moment(startDate).format('ddd') !== 'เสาร์' &&
											moment(startDate).format('ddd') !== 'อาทิตย์'
												? Neuro_in
												: type_Time == 2 &&
													moment(startDate).format('ddd') !== 'เสาร์' &&
													moment(startDate).format('ddd') !== 'อาทิตย์'
													? Neuro_out
													: type_Service == 3 && moment(startDate).format('ddd') === 'เสาร์'
														? Neuro_full
														: type_Service == 3 && moment(startDate).format('ddd') === 'อาทิตย์'
															? Neuro_in
															: null).map((i) => (
												<Col
													xs={moment(startDate).format('ddd') === 'เสาร์' ? 4 : 3}
													sm={moment(startDate).format('ddd') === 'เสาร์' ? 2 : 3}
													className="text-center"
												>
													<Button
														id={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Ortho' ? -45 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') <= i &&
															// 			moment(el.booking_at).format('HH:mm') >= i
															// 	)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date())
																	.add(1, 'month')
																	.format('YYYY-MM-DD') ? (
																'btn-times-select-disable'
															) : time_select == i && docterID == e.user_id ? (
																'btn-times-select-sed'
															) : (
																'btn-times-select'
															)
														}
														value={i}
														disabled={
															// (Booking &&
															// 	Booking.some(
															// 		(el) =>
															// 			el.doctor == e.user_id &&
															// 			moment(el.booking_at)
															// 				.add(
															// 					el.sub_type === 'Ortho' ? -45 : 0,
															// 					'minutes'
															// 				)
															// 				.format('HH:mm') <= i &&
															// 			moment(el.booking_at).format('HH:mm') >= i
															// 	)) ||
															(e.active == 0
																? true
																: e.active == 1 && e.activeIntime == 0 && type_Time == 1
																	? true
																	: e.active == 1 &&
																		e.activeOuttime == 0 &&
																		type_Time == 2
																		? true
																		: false) ||
															moment(startDate).format('YYYY-MM-DD') <
																moment(new Date()).format('YYYY-MM-DD') ||
															moment(startDate).format('YYYY-MM-DD') >
																moment(new Date()).add(1, 'month').format('YYYY-MM-DD')
														}
														onClick={() => {
															if (this.state.role !== 'frontMT') {
																this.props.Time_tmpt(i);
															}
															this.setState({
																startTime: i,
																docterID: e.user_id,
																time_select: i
															});
														}}
													>
														{i}
													</Button>
												</Col>
											))
										)}
									</Row>
								</div>
							))
						) : (
							<div style={{ fontSize: 'small' }}>
								<br />---ไม่พบหมอในระบบ---
							</div>
						)}
					</Col>
				</Row>
			</div>
		);
	}
}
