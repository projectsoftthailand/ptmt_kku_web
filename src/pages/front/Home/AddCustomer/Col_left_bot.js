import React, { Component } from 'react';
import { Table } from 'reactstrap';
import Responsive from 'react-responsive';
import moment from 'moment';
import Color from '../../../../components/Color';
import {
	POST,
	CREATE_USER,
	ip,
	GET_PICTURE,
	GET,
	GET_USER_DETAIL,
	COMFIRM_CUSTOMER,
	GET_USERS,
	UPDATE_USER_PROFILE
} from '../../../../service/service';
import swal from 'sweetalert';
import { Link, withRouter } from 'react-router-dom';
import ReactLoading from 'react-loading';
import socketIOClient from 'socket.io-client';
import './style/Col_right.css';
import '../../../admin/AddMember/style/AddMember.css';

var cardReaderIP = 'http://localhost:3099';

const Mobile = (props) => <Responsive {...props} maxWidth={767} />;
const Default = (props) => <Responsive {...props} minWidth={768} />;

export default class Col_left_bot extends Component {
	constructor(props) {
		super(props);

		this.state = {
			head: [ 'ว/ด/ป', 'เวลา', 'คำนำหน้า', 'ชื่อ', 'นามสกุล', 'อายุ', 'หน่วยตรวจ', '' ]
		};
	}
	// componentWillMount = () => {
	// 	// let obj = this.props.obj;
	// 	this.setState({
	// 		obj: this.props.obj
	// 	});
	// };
	del_obj = (indexs) => {
		let obj = this.props.obj;
		let organization_list = this.props.organization_list;
		// this.setState({ delete_obj: aaa }, () => {
		// 	let asd = this.state.delete_obj.splice(index, 1);
		// 	this.setState({ delete_obj2: asd }, () => {
		// 		console.log('this.state.delete_obj', this.state.delete_obj);
		// 		console.log('this.state.delete_obj2', this.state.delete_obj2);
		// 	});
		// });
		// let i = obj.findIndex((index) => index === indexs);
		obj.splice(indexs, 1);
		organization_list.splice(indexs, 1);
		// console.log('obj:', obj);
		this.props.setObj(obj);
		this.props.setorganization_list(organization_list);
	};
	render() {
		let { head } = this.state;
		let { obj, organization_list } = this.props;
		// console.log('obj', obj);
		// console.log(obj);
		return (
			<div>
				<Table responsive>
					{head && (
						<thead>
							<tr>
								{head.map((el, index) => (
									<th
										style={{ borderBottomWidth: 0, backgroundColor: 'white', whiteSpace: 'pre' }}
										key={'a' + index}
									>
										{el}
									</th>
								))}
							</tr>
						</thead>
					)}
					<tbody>
						{obj.map((row, index) => (
							<tr style={{ borderWidth: 0 }} key={'b' + index}>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.date}
								</td>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.time}
								</td>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.key}
								</td>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.frist}
								</td>

								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.last}
								</td>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.age}
								</td>
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.unit == 1 ? (
										'ศูนย์บริการมหาวิทยาลัยขอนแก่น'
									) : (
										'ศูนย์บริการมหาวิทยาลัยขอนแก่น สาขาในเมืองขอนแก่น'
									)}
								</td>
								{/* <td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									{row.index == 0 ? (
										'กำหนดเอง'
									) : row.index == 1 ? (
										'Standard (ต่ำกว่า 35 ปี)'
									) : row.index == 2 ? (
										'Standard (มากกว่า 35 ปี)'
									) : row.index == 3 ? (
										'Silver'
									) : row.index == 4 ? (
										'Gold'
									) : (
										'Platinum'
									)}
								</td> */}
								<td
									key={'c' + index}
									style={{ backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0 }}
								>
									<button
										style={{ border: 'none', outline: 'none' }}
										onClick={() => this.del_obj(index)}
									>
										ลบ
									</button>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			</div>
		);
	}
}
