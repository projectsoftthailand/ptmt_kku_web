import React, { Component } from 'react';
import Table_3 from '../../../../components/Table_3';
import Color from '../../../../components/Color';
import { FaSearch } from 'react-icons/fa';
import { Input } from 'reactstrap';
import { Button } from 'reactstrap';
import Pagination from '../../../../components/Pagination';

let numberPage = [ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
export default class Col_right_top extends Component {
	constructor(props) {
		super(props);

		// let schedulerData = new SchedulerData('2017-12-18', ViewTypes.Day, false, false, {
		// 	nonAgendaDayCellHeaderFormat: 'HH:mm'
		// });
		// schedulerData.localeMoment.locale('th-TH');
		// schedulerData.setResources(DemoData.resources);
		// schedulerData.setEvents(DemoData.events);

		this.state = {
			doctor: '',

			currentPage: 1,
			pageSize: 10,

			searchText: '',

			pt: [],
			display_pt: [],
			mt: [],
			display_mt: [],

			curServiceType: ''
		};
	}

	componentWillMount() {
		let { onRef, pt, mt, curServiceType } = this.props;
		onRef(this);
		this.setState({
			pt,
			mt,
			curServiceType,
			display_mt: mt,
			display_pt: pt
		});
	}

	returnState() {
		let { doctor } = this.state;
		return {
			doctor
		};
	}

	Search = (e) => {
		let text = e.target.value;
		let { pt, mt } = this.state;

		let displayData = [];
		pt.forEach((e, i) => {
			if (e.th_name.indexOf(text) !== -1 || e.th_lastname.indexOf(text) !== -1) displayData.push(e);
		});
		this.setState({
			display_pt: displayData
		});

		let displayData2 = [];
		mt.forEach((e, i) => {
			if (e.th_name.indexOf(text) !== -1 || e.th_lastname.indexOf(text) !== -1) displayData2.push(e);
		});
		this.setState({
			display_mt: displayData2
		});
	};

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { curServiceType, mt, BasicSchedule } = this.props;
		let { doctor, pageSize, currentPage, display_pt, viewModel } = this.state;
		let rowsData = [];
		if (curServiceType === 'mt') {
			this.state.display_mt.slice((currentPage - 1) * pageSize, currentPage * pageSize).forEach((e, index) => {
				rowsData.push([
					e.th_name,
					e.th_lastname,
					e.phone,
					<input
						checked={doctor == e.user_id ? true : false}
						onChange={(e) => this.setState({ doctor: e.target.value })}
						value={e.user_id}
						type="radio"
						name="doc-type"
					/>
				]);
			});
		} else if (curServiceType === 'pt') {
			this.state.display_pt.slice((currentPage - 1) * pageSize, currentPage * pageSize).forEach((e, index) => {
				rowsData.push([
					e.th_name,
					e.th_lastname,
					e.phone,
					<input
						checked={doctor == e.user_id ? true : false}
						onChange={(e) => this.setState({ doctor: e.target.value })}
						value={e.user_id}
						type="radio"
						name="doc-type"
					/>
				]);
			});
		}
		return (
			<div style={{ fontSize: 12 }} className="mb-3 py-3 rounded bg-white">
				<div className="d-flex justify-content-between mx-2">
					<div style={{ display: 'flex' }}>
						<h4 className="ml-2" style={{ color: Color.Blue }}>
							เลือกนักเทคนิคการแพทย์
						</h4>
					</div>
					<div className="row m-0">
						<Button className="m-2" outline color="secondary" size="sm" onClick={() => BasicSchedule()}>
							ตารางงานนักเทคนิคการแพทย์
						</Button>
						<div className="search m-2">
							<Input name="searchText" onChange={this.Search} type="text" />
							<FaSearch className="ic" />
						</div>
					</div>
				</div>
				{rowsData.length > 0 ? (
					<div>
						<Table_3 head={[ 'ชื่อ', 'นามสกุล', 'เบอร์โทร', 'เลือก' ]} rows={rowsData} />
						<div className="d-flex justify-content-between mx-2 mb-2">
							<select
								className="select-page-size"
								name="pageSize"
								onChange={async (e) => {
									await this.setState({ [e.target.name]: +e.target.value });
								}}
							>
								{numberPage.map((el, i) => (
									<option key={i} value={el}>
										{el}
									</option>
								))}
							</select>
							<Pagination
								totalRecords={rowsData.length}
								pageLimit={pageSize}
								pageNeighbours={2}
								onPageChanged={this.onPageChanged}
							/>
						</div>
					</div>
				) : (
					<div className="text-center py-5 my-2">---ไม่พบข้อมูล---</div>
				)}
			</div>
		);
	}
}
