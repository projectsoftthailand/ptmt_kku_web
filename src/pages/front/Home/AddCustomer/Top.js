import React, { Component } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Label, Input, FormGroup } from 'reactstrap';
import Responsive from 'react-responsive';
import { withRouter } from 'react-router-dom';
import moment from 'moment-timezone';
import { getType } from '../../../../function/function';
import getCurDateTime from '../../../../function/GetCurDateTime';
import user from '../../../../mobx/user/user';

const Expect = (props) => <Responsive {...props} minWidth={831} />;
const NotExpect = (props) => <Responsive {...props} maxWidth={830} />;
const TimeOutHour = 16;
const TimeOutMinute = 30;

var Types = [
	{ id: 2, name: 'กระดูกและกล้ามเนื้อ', en_name: 'Ortho' },
	{ id: 3, name: 'ระบบประสาท', en_name: 'Neuro' },
	{ id: 1, name: 'นวดไทย', en_name: 'ThaiMassage' }
	// { id: 4, name: 'ระบบทรวงอก', en_name: 'Chest' }
	// { id: 5, name: 'ระบบเด็ก', en_name: 'Child' }
];

@withRouter
class Top extends Component {
	constructor(props) {
		super(props);

		this.state = {
			personType: 'บุคคล',
			bookDate: '',
			serviceType: '',
			placeType: 'inside',
			curInOutTime: '',
			outSidePrice: 0,
			organization: '',
			type_service: 2,
			type_time: 1
		};
	}

	initInOutByTime(time) {
		let d = new Date(); // for now
		let limitTimeMn = TimeOutHour * 60 + TimeOutMinute;
		let curTimeMinute = time
			? moment(time).hours() * 60 + moment(time).minutes()
			: d.getHours() * 60 + d.getMinutes();
		this.setState({
			curInOutTime: curTimeMinute > limitTimeMn ? 'ผู้รับบริการนอกเวลา' : 'ผู้รับบริการในเวลา'
		});
	}

	componentWillMount = () => {
		this.props.onRef(this);
		let { units, types, isDefaultType } = this.props;
		let curDate = getCurDateTime();
		if (units && types) {
			if (units.length > 0 && types.length > 0) {
				this.setState({ bookDate: curDate, serviceType: isDefaultType });
				this.initInOutByTime();
			}
		}
	};

	returnState() {
		let { personType, bookDate, serviceType, placeType, outSidePrice, organization } = this.state;
		return { personType, bookDate, serviceType, placeType, outSidePrice, organization };
	}

	render() {
		let { personType, bookDate, serviceType, placeType, curInOutTime, type_service, type_time } = this.state;
		let {
			unit,
			units,
			types,
			setDocType,
			personTypes,
			personTypes2,
			setUnit,
			isCheckAddList,
			onChangeAddList,
			type_Service,
			type_Time,
			holiDay
		} = this.props;
		if (serviceType === 'mt') {
			return (
				<div id="addcustomer-top">
					<Expect>
						<div className="d-flex px-4 mx-auto w-100">
							<div className="mx-auto" style={{ width: '15vw' }}>
								<div className="d-flex mx-auto mt-3">
									{/* {personType === true ? ( */}
									<div>
										<input
											checked={isCheckAddList}
											onChange={(e) => onChangeAddList(e.target.checked)}
											type="checkbox"
											name=""
											id="only-cus"
										/>
										<Label htmlFor="only-cus" className="top-label mx-2">
											เพิ่มรายการตรวจ
										</Label>
									</div>
									{/* ) : null} */}
								</div>
								<div className="d-flex mx-auto mt-3">
									<Label className="btn-back top-label" onClick={() => this.props.history.goBack()}>
										{'< กลับ'}
									</Label>
								</div>
							</div>
							<div className="mx-auto" style={{ width: '30vw' }}>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">รูปแบบบริการ</Label>
									<div
										className="d-flex"
										style={{ width: '20rem' }}
										onChange={(e) => this.setState({ personType: e.target.value })}
									>
										<input
											onChange={(e) => personTypes(true)}
											checked={personTypes2 === true ? true : false}
											value="บุคคล"
											name="person-type"
											type="radio"
											id="person-type1"
										/>
										<Label className="mx-2" style={{ fontWeight: 200 }} for="person-type1">
											บุคคล
										</Label>
										<input
											onChange={(e) => personTypes(false)}
											checked={personTypes2 === false ? true : false}
											value="องค์กร"
											name="person-type"
											type="radio"
											id="person-type2"
										/>
										<Label className="mx-2" style={{ fontWeight: 200 }} for="person-type2">
											องค์กร
										</Label>
									</div>
								</div>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">หน่วยตรวจ</Label>
									<Input
										value={unit}
										className="top-input"
										onChange={(e) => setUnit(e.target.value)}
										type="select"
									>
										{units.map((e, index) => (
											<option disabled={e.name == 'นอกสถานที่'} key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</div>
							</div>
							<div className="mx-auto" style={{ width: '30vw' }}>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">ประเภทบริการ</Label>
									<Input
										value={serviceType}
										className="top-input"
										onChange={(e) => {
											setDocType(e.target.value);
											this.setState({ serviceType: e.target.value });
										}}
										type="select"
									>
										{types
											.filter((el) => {
												switch (getType(user.role)) {
													case 'mt':
														return el.type === 'mt';
													case 'pt':
														return el.type !== 'mt';
													default:
														return el;
												}
											})
											.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
									</Input>
								</div>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">เวลานัดหมาย</Label>
									<Input
										value={bookDate}
										className="top-input"
										onChange={(e) => {
											this.setState({ bookDate: e.target.value });
											this.initInOutByTime(e.target.value);
										}}
										type="datetime-local"
									/>
								</div>
							</div>
							<div className="mx-auto" style={{ width: '25vw' }}>
								<div className="d-flex mx-4 mt-3">
									<Label className="top-label">ประเภทผู้รับบริการ</Label>
									<Label
										style={{
											color: curInOutTime === 'ผู้รับบริการในเวลา' ? 'green' : 'red'
										}}
									>
										{curInOutTime}
									</Label>
								</div>
								{personTypes2 === true ? null : (
									<div className="d-flex mx-4 mt-3">
										<Label className="top-label">ชื่อบริษัท</Label>
										<Label>
											<input
												onChange={(e) => {
													this.setState({ organization: e.target.value });
													this.props.setNameHR(e.target.value);
												}}
												style={{
													border: '1px solid #ced4da',
													borderRadius: '3px',
													paddingLeft: '3px',
													height: '24px'
												}}
												placeholder="กรอกชื่อบริษัท"
											/>
										</Label>
									</div>
								)}
							</div>
						</div>
					</Expect>
					<NotExpect>
						<div className="ml-3 py-3">
							<div>
								<input
									checked={isCheckAddList}
									onChange={(e) => onChangeAddList(e.target.checked)}
									type="checkbox"
									name=""
									id="only-cus"
								/>
								<Label htmlFor="only-cus" className="top-label mx-2">
									เพิ่มรายการตรวจ
								</Label>
							</div>
							<div onChange={(e) => this.setState({ personType: e.target.value })}>
								<Label className="mr-5" style={{ width: 125 }}>
									รูปแบบบริการ
								</Label>
								<FormGroup check inline>
									<Label check>
										<Input
											onChange={(e) => personTypes(true)}
											checked={personTypes2 === true ? true : false}
											value="บุคคล"
											name="type-person"
											type="radio"
										/>
										บุคคล
									</Label>
								</FormGroup>
								<FormGroup check inline>
									<Label check>
										<Input
											onChange={(e) => personTypes(false)}
											checked={personTypes2 === false ? true : false}
											value="องค์กร"
											name="type-person"
											type="radio"
										/>
										องค์กร
									</Label>
								</FormGroup>
							</div>
							<div>
								<FormGroup inline className="d-flex mt-3">
									<Label className="mt-3" style={{ width: 200 }}>
										ประเภทบริการ
									</Label>
									<Input
										value={serviceType}
										onChange={(e) => {
											setDocType(e.target.value);
											this.setState({ serviceType: e.target.value });
										}}
										className="mr-3 ml-1"
										type="select"
									>
										{types
											.filter((el) => {
												switch (getType(user.role)) {
													case 'mt':
														return el.type === 'mt';
													case 'pt':
														return el.type !== 'mt';
													default:
														return el;
												}
											})
											.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
									</Input>
								</FormGroup>
							</div>
							<div>
								<FormGroup inline className="d-flex mt-3">
									<Label className="mt-3" style={{ width: 200 }}>
										หน่วยตรวจ
									</Label>
									<Input
										value={unit}
										onChange={(e) => setUnit(e.target.value)}
										className="mr-3 ml-1"
										type="select"
									>
										{units.map((e, index) => (
											<option disabled={e.name == 'นอกสถานที่'} key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</FormGroup>
							</div>
							{/* <div onChange={e => this.setState({ placeType: e.target.value })}>
                <FormGroup check inline>
                  <Label check>
                    <Label style={{ marginRight: 80 }}>การให้บริการ</Label>
                    <Input
                      onChange={e => console.log(e.target.value)}
                      checked={placeType === "inside" ? true : false}
                      value="inside"
                      name="type-place"
                      type="radio"
                    />
                    ในสถานที่
                  </Label>
                </FormGroup>
                <FormGroup check inline>
                  <Label check>
                    <Input
                      onChange={e => console.log(e.target.value)}
                      checked={placeType === "outside" ? true : false}
                      value="outside"
                      name="type-place"
                      type="radio"
                    />
                    นอกสถานที่
                  </Label>
                </FormGroup>
              </div> */}
							<div>
								<FormGroup inline className="d-flex mt-3">
									<Label className="mt-3" style={{ width: 203 }}>
										เวลานัดหมาย
									</Label>
									<Input
										value={bookDate}
										onChange={(e) => {
											this.setState({ bookDate: e.target.value });
											this.initInOutByTime(e.target.value);
										}}
										className="mr-3"
										type="datetime-local"
									/>
								</FormGroup>
							</div>
							{personTypes2 === true ? null : (
								<div>
									<FormGroup inline className="d-flex">
										<Label className="mt-3" style={{ width: 158 }}>
											ชื่อบริษัท
										</Label>
										<Label className="mt-3">
											<input
												onChange={(e) => {
													this.setState({ organization: e.target.value });
													this.props.setNameHR(e.target.value);
												}}
												style={{
													border: '1px solid #ced4da',
													borderRadius: '3px',
													paddingLeft: '3px',
													height: '24px'
												}}
												placeholder="กรอกชื่อบริษัท"
											/>
										</Label>
									</FormGroup>
								</div>
							)}
							<div>
								<FormGroup inline className="d-flex">
									<Label className="mt-3" style={{ width: 160 }}>
										ประเภทผู้รับบริการ
									</Label>
									<Label
										className="mt-3"
										style={{
											color: curInOutTime === 'ผู้รับบริการในเวลา' ? 'green' : 'red'
										}}
									>
										{curInOutTime}
									</Label>
								</FormGroup>
							</div>
						</div>
					</NotExpect>
				</div>
			);
		} else if (serviceType === 'pt') {
			return (
				<div id="addcustomer-top" className="pb-3 ">
					<Expect>
						<div className="d-flex px-4 mx-auto w-100">
							<div className="mx-auto" style={{ width: '15vw' }}>
								<div className="d-flex mx-auto mt-3">
									{/* {personType === true ? ( */}
									<div>
										<input
											checked={isCheckAddList}
											onChange={(e) => onChangeAddList(e.target.checked)}
											type="checkbox"
											name=""
											id="only-cus"
										/>
										<Label htmlFor="only-cus" className="top-label mx-2">
											เพิ่มรายการตรวจ
										</Label>
									</div>
									{/* ) : null} */}
								</div>
								<div className="d-flex mx-auto mt-3">
									<Label className="btn-back top-label" onClick={() => this.props.history.goBack()}>
										{'< กลับ'}
									</Label>
								</div>
							</div>
							<div className="mx-auto" style={{ width: '30vw' }}>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">รูปแบบบริการ</Label>
									<div
										className="d-flex"
										style={{ width: '20rem' }}
										onChange={(e) => this.setState({ personType: e.target.value })}
									>
										<input
											onChange={(e) => personTypes(true)}
											checked={personTypes2 === true ? true : false}
											value="บุคคล"
											name="person-type"
											type="radio"
											id="person-type1"
										/>
										<Label className="mx-2" style={{ fontWeight: 200 }} for="person-type1">
											บุคคล
										</Label>
										{/* <input
											onChange={(e) => personTypes(false)}
											checked={personTypes2 === false ? true : false}
											value="องค์กร"
											name="person-type"
											type="radio"
											id="person-type2"
										/>
										<Label className="mx-2" style={{ fontWeight: 200 }} for="person-type2">
											องค์กร
										</Label> */}
									</div>
								</div>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">หน่วยตรวจ</Label>
									<Input
										value={unit}
										className="top-input"
										onChange={(e) => setUnit(e.target.value)}
										type="select"
									>
										{units.map((e, index) => (
											<option disabled={e.name == 'นอกสถานที่'} key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</div>
							</div>
							<div className="mx-auto" style={{ width: '30vw' }}>
								<div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label">ประเภทบริการ</Label>
									<Input
										value={type_service}
										className="top-input"
										onChange={(e) => {
											setDocType('pt');
											type_Service(e.target.value);
											this.setState({ type_service: e.target.value });
										}}
										type="select"
									>
										{Types.map((e, index) => (
											<option key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</div>
								{/* <div className="d-flex justify-content-end mx-auto mt-3">
									<Label className="top-label" />
									<Input
										value={type_service}
										className="top-input"
										onChange={(e) => {
											setDocType(e.target.value);
											this.setState({ type_service: e.target.value });
										}}
										type="select"
									>
										{Types.map((e, index) => (
											<option key={'' + index} value={e.type}>
												{e.name}
											</option>
										))}
									</Input>
								</div> */}
							</div>
							<div className="mx-auto" style={{ width: '25vw' }}>
								<div className="d-flex mx-4 mt-3">
									<Label className="top-label">ประเภทผู้รับบริการ</Label>
									{Number(type_service) == 1 ? (
										<div
											className="d-flex"
											style={{ width: '20rem' }}
											// onChange={(e) => this.setState({ personType: e.target.value })}
										>
											<input
												onChange={() => {
													this.setState({ type_time: 1 });
													type_Time(1);
												}}
												checked={type_time === 1 ? true : false}
												value="intime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'green' }}
												for="person-type2"
											>
												ในเวลา
											</Label>
											<input
												onChange={() => {
													this.setState({ type_time: 2 });
													type_Time(2);
												}}
												checked={type_time === 1 ? false : true}
												value="outtime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'red' }}
												for="person-type2"
											>
												นอกเวลา
											</Label>
										</div>
									) : Number(type_service) >= 2 && (holiDay == 'เสาร์' || holiDay == 'อาทิตย์') ? (
										<Label
											style={{
												color: 'red'
											}}
										>
											ผู้รับบริการนอกเวลา
										</Label>
									) : (
										<div
											className="d-flex"
											style={{ width: '20rem' }}
											// onChange={(e) => this.setState({ personType: e.target.value })}
										>
											<input
												onChange={() => {
													this.setState({ type_time: 1 });
													type_Time(1);
												}}
												checked={type_time === 1 ? true : false}
												value="intime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'green' }}
												for="person-type1"
											>
												ในเวลา
											</Label>
											<input
												onChange={() => {
													this.setState({ type_time: 2 });
													type_Time(2);
												}}
												checked={type_time === 1 ? false : true}
												value="outtime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'red' }}
												for="person-type2"
											>
												นอกเวลา
											</Label>
										</div>
									)}
								</div>
								{/* {personTypes2 === true ? null : (
									<div className="d-flex mx-4 mt-3">
										<Label className="top-label">ชื่อบริษัท</Label>
										<Label>
											<input
												onChange={(e) => {
													this.setState({ organization: e.target.value });
													this.props.setNameHR(e.target.value);
												}}
												style={{
													border: '1px solid #ced4da',
													borderRadius: '3px',
													paddingLeft: '3px',
													height: '24px'
												}}
												placeholder="กรอกชื่อบริษัท"
											/>
										</Label>
									</div>
								)} */}
							</div>
						</div>
					</Expect>
					<NotExpect>
						<div className="ml-3 py-3">
							<div>
								<input
									checked={isCheckAddList}
									onChange={(e) => onChangeAddList(e.target.checked)}
									type="checkbox"
									name=""
									id="only-cus"
								/>
								<Label htmlFor="only-cus" className="top-label mx-2">
									เพิ่มรายการตรวจ
								</Label>
							</div>
							{/* <div onChange={(e) => this.setState({ personType: e.target.value })}>
								<Label className="mr-5" style={{ width: 125 }}>
									รูปแบบบริการ
								</Label>
								<FormGroup check inline>
									<Label check>
										<Input
											onChange={(e) => personTypes(true)}
											checked={personTypes2 === true ? true : false}
											value="บุคคล"
											name="type-person"
											type="radio"
										/>
										บุคคล
									</Label>
								</FormGroup>
								<FormGroup check inline>
									<Label check>
										<Input
											onChange={(e) => personTypes(false)}
											checked={personTypes2 === false ? true : false}
											value="องค์กร"
											name="type-person"
											type="radio"
										/>
										องค์กร
									</Label>
								</FormGroup>
							</div> */}
							<div>
								<FormGroup inline className="d-flex mt-3">
									<Label className="mt-3" style={{ width: 200 }}>
										หน่วยตรวจ
									</Label>
									<Input
										value={unit}
										onChange={(e) => setUnit(e.target.value)}
										className="mr-3 ml-1"
										type="select"
									>
										{units.map((e, index) => (
											<option disabled={e.name == 'นอกสถานที่'} key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</FormGroup>
							</div>
							<div>
								<FormGroup inline className="d-flex mt-3">
									<Label className="mt-3" style={{ width: 200 }}>
										ประเภทบริการ
									</Label>
									<Input
										value={type_service}
										className="mr-3 ml-1"
										onChange={(e) => {
											setDocType('pt');
											type_Service(e.target.value);
											this.setState({ type_service: e.target.value });
										}}
										type="select"
									>
										{Types.map((e, index) => (
											<option key={'' + index} value={e.id}>
												{e.name}
											</option>
										))}
									</Input>
								</FormGroup>
							</div>
							<div>
								<FormGroup inline className="d-flex">
									<Label className="mt-3" style={{ width: 160 }}>
										ประเภทผู้รับบริการ
									</Label>
									{Number(type_service) == 1 ? (
										<div
											className="d-flex"
											style={{ width: '20rem' }}
											// onChange={(e) => this.setState({ personType: e.target.value })}
										>
											<input
												onChange={() => {
													this.setState({ type_time: 1 });
													type_Time(1);
												}}
												checked={type_time === 1 ? true : false}
												value="intime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'green' }}
												for="person-type2"
											>
												ในเวลา
											</Label>
											<input
												onChange={() => {
													this.setState({ type_time: 2 });
													type_Time(2);
												}}
												checked={type_time === 1 ? false : true}
												value="outtime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'red' }}
												for="person-type2"
											>
												นอกเวลา
											</Label>
										</div>
									) : Number(type_service) == 2 || Number(type_service) == 3 ? holiDay == 'เสาร์' ||
									holiDay == 'อาทิตย์' ? (
										<Label
											style={{
												color: 'red'
											}}
										>
											ผู้รับบริการนอกเวลา
										</Label>
									) : (
										<div
											className="d-flex"
											style={{ width: '20rem' }}
											// onChange={(e) => this.setState({ personType: e.target.value })}
										>
											<input
												onChange={() => {
													this.setState({ type_time: 1 });
													type_Time(1);
												}}
												checked={type_time === 1 ? true : false}
												value="intime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'green' }}
												for="person-type1"
											>
												ในเวลา
											</Label>
											<input
												onChange={() => {
													this.setState({ type_time: 2 });
													type_Time(2);
												}}
												checked={type_time === 1 ? false : true}
												value="outtime"
												name="type_time"
												type="radio"
											/>
											<Label
												className="mx-2"
												style={{ fontWeight: 200, paddingTop: '0.3rem', color: 'red' }}
												for="person-type2"
											>
												นอกเวลา
											</Label>
										</div>
									) : (
										<Label
											style={{
												color: curInOutTime === 'ผู้รับบริการในเวลา' ? 'green' : 'red'
											}}
										>
											{curInOutTime}
										</Label>
									)}
								</FormGroup>
							</div>
						</div>
					</NotExpect>
				</div>
			);
		} else {
			return <div>not expert type mt, pt!!!</div>;
		}
	}
}

export default Top;
