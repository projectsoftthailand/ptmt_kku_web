import React, { Component } from 'react';
import Top from './Top';
import {
	GET,
	GET_TREATMENT,
	GET_PACKAGE,
	GET_UNIT,
	GET_TYPE,
	GET_USERS,
	POST,
	CREATE_LIST_BY_FRONT,
	CREATE_COMPANY_LIST_BY_FRONT,
	CREATE_DELIVERY_LIST_BY_FRONT,
	CREATE_COMPANY_DELIVERY_LIST_BY_FRONT,
	LAST_CLAIM
} from '../../../../service/service';
import Google_map from './Google_map';
import Col_left from './Col_left';
import Col_left_bot from './Col_left_bot';
import Col_right from './Col_right';
import Col_right_bot from './Col_right_bot';
import Color from '../../../../components/Color';
import Col_right_top from './Col_right_top';
import Observe from './Observe';
import DoctorPT from './Time_docterPT';
import PT_body from './PT_body';
import swal from 'sweetalert';

import { withRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import './style/Addcustomer.css';
import ReactLoading from 'react-loading';

import PayModal from './PayModal';
import ScheduleModal from './ScheduleModal';
// import ChooseBed from "../../../../components/Bed/ChooseBed";
import { Label, Col, Row } from 'reactstrap';
// import GetCurTimeInt from "../../../../function/GetCurTimeInt";
import FixedBotButton from '../../../../components/FixedBotButton';
// import InProgress from "../../../../components/InProgress/InProgress";
import User from '../../../../mobx/user/user';
import CardData from './cardData/CardData';
import moment from 'moment';
import Modal from 'react-modal';
import fn from '../../../../function/function';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

@withRouter
class AddCustomer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isCustomer: -1,
			datas_pdf: [],
			_package: [],
			treatment: [],
			treatment_discount: [],
			packageCheck: [],
			treatmentData: [],
			curCheck: [],
			types: [],
			units: [],
			loading: true,

			ptPackage: [],
			mtPackage: [],

			// User Input
			curServiceType: '',
			servicePrice: 0,
			outSidePrice: 0,
			totalPrice: 0,

			isCheckAddList: true,

			cashIn: 0,
			bedSelectedFromHome: [],
			personType: true,
			organization: '',
			unit: 1,
			index: -999,
			obj: [],
			organization_list: [],
			user_id: [],
			BasicSchedule: false,
			priceNewPackage: 0,
			newPackage: [],
			newTreatment: [],
			f_name: '',
			l_name: '',
			nameHR: '',
			address: '',
			payType: 1,
			confirm: '',
			ln: '',
			discount: 0,
			confirm: 0,
			type_service: 2,
			type_time: 1,
			holiday: new Date(),
			loadingSave: false,
			MS: [],
			last_claim: '',
			searchtext: ''
		};
	}

	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		let res = localStorage.getItem('USER');
		let name = JSON.parse(res);
		// console.log(name.lastname);
		this.setState({
			f_name: name.name,
			l_name: name.lastname,
			type_service: name.role == 'frontPT' || name.role == 'pt' ? 2 : 0
		});
		let cusID = this.props.match.params.id;

		this._isMounted = true;
		if (!this._isMounted) return;
		if (cusID) {
			await this.setState({ isCustomer: cusID });
		}
		//----------------------------------------------
		// ******************************************

		await Promise.all([
			this.GetTreatment(),
			this.GetPackage(),
			this.GetType(),
			this.GetUnit(),
			this.GetDoctor(),
			this.lastClaim()
		]);
		await this.setServiceType(this.state.curServiceType);
		await this.setState({ loading: false });
	}
	lastClaim = async () => {
		try {
			let id = this.props.match.params.id;
			let res = await POST(LAST_CLAIM, { id });
			if (res.success) {
				this.setState({ payType: Number(res.result.service_claim) });
			} else this.setState({ payType: 1 });
		} catch (error) {}
	};
	componentDidMount = () => {
		// this.google_map.returnState();
	};

	clearStateInRoute = async () => {
		// for clear pt booked from home page
		// clear state in Route
		const history = createBrowserHistory();
		if (history.location && history.location.state && history.location.state.from) {
			const state = { ...history.location.state };
			history.replace({ ...history.location, state: {} });
		}
		await this.setState({ curServiceType: this.state.types[0].type || null });
		// console.log('is clear..')
	};

	componentWillUnmount() {
		this.clearStateInRoute();
		this._isMounted = false;
	}
	async GetUnit() {
		try {
			let res = await GET(GET_UNIT);
			// console.log('res GetsssUnit : ', res.result)
			this.setState({ units: res.result });
		} catch (error) {
			// console.log('error Get Unit : ', error)
		}
	}

	async GetType() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		if (USER.role == 'frontPT' || USER.role == 'pt') {
			let res = await GET(GET_TYPE);
			this.setState({ curServiceType: 'pt', types: res.result });
		} else {
			let res = await GET(GET_TYPE);
			this.setState({ curServiceType: 'mt', types: res.result });
		}
	}

	async GetTreatment() {
		try {
			let USER = JSON.parse(localStorage.getItem('USER'));
			let id = USER.role == 'frontPT' || USER.role == 'pt' ? 'pt' : 'mt';
			let res = await GET(GET_TREATMENT(id));
			let ms = await GET(GET_TREATMENT('tm'));
			let d = [];
			let cur = [];
			let curPrice = 0;
			// console.log('res GetTreatment : ', res);
			res.result.forEach((e, index) => {
				d.push([
					e.treatment_name,
					e.cost,
					Number(e.cost) - Number(e.disburseable),
					<input
						key={'in' + index}
						id={'' + index}
						onClick={() => this.onClick(index)}
						onChange={(ei) => console.log(ei.target.value)}
						checked={false}
						type="checkbox"
					/>,
					e.detail,
					e.cgd_code
				]);
				cur.push(false);
			});
			// console.log('d', d);
			this.setState(
				{
					old_treatment: d,
					treatment: d,
					treatmentData: res.result,
					curCheck: cur,
					servicePrice: curPrice,
					MS: ms.result
				},
				() => {
					// console.log('treatmentData ::: ', this.state.treatment);
				}
			);
		} catch (error) {
			// console.log('error GetTreatment : ', error)
		}
	}

	async GetDoctor() {
		// pt, mt
		try {
			let res = await GET(GET_USERS('all'));
			// console.log('res is Getd', res);
			// console.log('res GetDoctor: ', res.result)
			let pt = [];
			let mt = [];
			res.result.forEach((e, index) => {
				if (e.role_name === 'pt') pt.push(e);
				else if (e.role_name === 'mt') mt.push(e);
			});
			this.setState({ pt, mt });
		} catch (error) {
			// console.log('error GetDoctor : ', error)
		}
	}

	onClick = (index) => {
		// console.log('index is ', index);
		// console.log('click', click);
		let cur = this.state.curCheck;
		let payType = this.state.payType;
		let curPrice = 0;
		let discounts = 0;
		// console.log('cur is ', cur)
		cur[index] = !cur[index];
		let d = [];
		this.state.treatmentData.forEach((e, i) => {
			let number = Number(e.cost) - Number(e.disburseable);
			if (this.state.curServiceType === e.type) {
				d.push([
					e.treatment_name,
					e.cost,
					number,
					<input
						key={'in' + i}
						id={'' + i}
						onClick={() => this.onClick(i)}
						value={e.treatment_name}
						onChange={(ei) => console.log(ei.target.value)}
						checked={cur[i]}
						type="checkbox"
					/>,
					e.detail,
					e.cgd_code
				]);
				if (cur[i]) discounts += +e.disburseable;
				// if (payType < 2) {
				if (cur[i]) curPrice += +e.cost;
				// } else {
				// 	if (cur[i]) curPrice += +(Number(e.cost) - Number(e.disburseable));
				// }
			}
		});
		this.setState({
			old_treatment: d,
			treatment: d,
			curCheck: cur,
			servicePrice: curPrice,
			discount: discounts
		});
	};

	selectPackage = (index) => {
		// console.log('index : ', index);
		let { curServiceType } = this.state;
		let cur = [];
		let curPrice = 0;
		let discounts = 0;
		if (index === -999) {
			let d = [];
			this.state.treatmentData.forEach((e, index) => {
				if (curServiceType === e.type) {
					d.push([
						e.treatment_name,
						e.cost,
						Number(e.cost) - Number(e.disburseable),
						<input
							value={e.treatment_name || ''}
							key={'in' + index}
							id={'' + index}
							onClick={() => this.onClick(index)}
							onChange={(ei) => console.log(ei.target.value)}
							checked={false}
							type="checkbox"
						/>,
						e.detail,
						e.cgd_code
					]);
				}
				cur.push(false);
			});
			this.setState({
				old_treatment: d,
				treatment: d,
				curCheck: cur,
				servicePrice: curPrice,
				discount: curPrice,
				index: index
			});
		} else {
			let payType = this.state.payType;
			let tmArray = this.state._package[index].treatment;
			let d = [];
			this.state.treatmentData.forEach((e, index) => {
				if (curServiceType === e.type) {
					d.push([
						e.treatment_name,
						e.cost,
						Number(e.cost) - Number(e.disburseable),
						<input
							value={''}
							key={'in' + index}
							id={'' + index}
							onClick={() => this.onClick(index)}
							onChange={(ei) => console.log(ei.target.value)}
							checked={tmArray.some((el) => el === e.treatment_name) ? true : false}
							type="checkbox"
						/>,
						e.detail,
						e.cgd_code
					]);
				}
				if (tmArray.some((el) => el === e.treatment_name)) {
					discounts += +e.disburseable;
				}

				// if (payType < 2) {
				if (tmArray.some((el) => el === e.treatment_name)) {
					cur.push(true);
					curPrice += +e.cost;
				} else {
					cur.push(false);
				}
				// } else {
				// 	if (tmArray.indexOf(index + 1) !== -1) {
				// 		cur.push(true);
				// 		curPrice += +(Number(e.cost) - Number(e.disburseable));
				// 	} else {
				// 		cur.push(false);
				// 	}
				// }
			});
			this.setState(
				{
					old_treatment: d,
					treatment: d,
					curCheck: cur,
					servicePrice: curPrice,
					discount: discounts,
					index: index
				},
				() => {}
			);
		}
	};

	async GetPackage() {
		try {
			let res = await GET(GET_PACKAGE('all'));
			this.setState({ _package: res.result });
		} catch (error) {}
	}

	// ########################## FOR MT ##########################
	CheckInput(data) {
		// MT
		let { confirm, payType } = this.state;
		let timeString = data.service.service_at.split(' ')[1];
		let timeInt = +timeString.split(':')[0] * 60 + +timeString.split(':')[1];
		let present = moment();
		let times = moment(data.service.bookdate);

		let dataColRight = this.colRight.returnState();
		let dataColRightBot = this.col_right_bot.returnState();
		let ages = moment(dataColRight.dateBirth).format('MM/DD/YYYY');
		let years = moment().diff(ages, 'years');

		if (confirm === 0) {
			swal('ผิดพลาด!', 'กรุณายืนยันตัวตนก่อนทำรายการ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (!data.user.customer || data.user.customer === '') {
			swal('ผิดพลาด!', 'กรุณาสมัครสมาชิกก่อนชำระเงิน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (data.service.service_treatment.length === 0) {
			swal('ผิดพลาด!', 'กรุณาเลือก Treatment ก่อนชำระเงิน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (
			Number(payType) === 3 &&
			dataColRightBot.relation === 'บุตร' &&
			Number(dataColRightBot.right_id) === 2 &&
			years >= 20
		) {
			swal('อายุของบุตรท่านเกิน 20 ปีบริบูรณ์แล้ว', 'ผิดพลาด!', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (Number(payType) === 3 && Number(dataColRightBot.right_id) === 2 && !dataColRightBot.right_user) {
			swal('กรุณากรอกชื่อผู้ใช้สิทธิ์', 'ผิดพลาด!', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		// if (!data.user.doctor || data.user.doctor === '') {
		// 	swal('ผิดพลาด!', 'กรุณาเลือกนักเทคนิคการแพทย์ก่อนชำระเงิน', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }
		// if (times < present) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกวันเวลานัดหมายล่วงหน้า', 'error', {
		// 		buttons: false,
		// 		timer: 3000
		// 	});
		// 	return false;
		// }
		if (this.state.unit == 3) {
			let a = this.google_map.returnState();
			let b = a.lats;
			if (b === 16.439644) {
				swal('ผิดพลาด!', 'กรุณาปักหมุดก่อน', 'error', {
					buttons: false,
					timer: 2000
				});
				return false;
			}
			if (this.state.address === '') {
				swal('ผิดพลาด!', 'กรุณากรอกที่อยู่', 'error', {
					buttons: false,
					timer: 2000
				});
				return false;
			}
		}
		return true;
	}
	CheckInput_Organization(data) {
		// MT
		let { organization_list, unit } = this.state;
		let timeString = data.service.service_at.split(' ')[1];
		let timeInt = +timeString.split(':')[0] * 60 + +timeString.split(':')[1];
		let dataTop = this.top.returnState();
		// console.log('time String is ', timeString2);

		let present = moment();
		let times = moment(data.service.bookdate);

		if (organization_list.length === 0) {
			swal('ผิดพลาด!', 'กรุณาเพิ่มผู้รับบริการก่อน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (dataTop.organization === '') {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อบริษัท', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}

		if (data.service.service_treatment.length === 0) {
			swal('ผิดพลาด!', 'กรุณาเลือก Treatment ก่อนชำระเงิน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		// if (times < present) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกวันเวลานัดหมายล่วงหน้า', 'error', {
		// 		buttons: false,
		// 		timer: 3000
		// 	});
		// 	return false;
		// }

		if (unit == 3) {
			let a = this.google_map.returnState();
			let b = a.lats;
			if (b === 16.439644) {
				swal('ผิดพลาด!', 'กรุณาปักหมุดก่อน', 'error', {
					buttons: false,
					timer: 2000
				});
				return false;
			}
			if (this.state.address === '') {
				swal('ผิดพลาด!', 'กรุณากรอกที่อยู่', 'error', {
					buttons: false,
					timer: 2000
				});
				return false;
			}
		}

		return true;
	}
	async GetDataAllAndCreateListByFront() {
		//MT
		let dataColRight = this.colRight.returnState();
		let dataTop = this.top.returnState();
		let dataColRightBot = this.col_right_bot.returnState();
		// let dataColRightTop = this.col_right_top.returnState();
		let service_treatment = [];
		this.state.curCheck.forEach((e, index) => {
			if (e) service_treatment.push(index + 1);
		});
		let { servicePrice, outSidePrice, payType } = this.state;
		let data = {
			user: {
				customer: dataColRight.user_id
				// doctor: dataColRightTop.doctor
			},
			service: {
				service_treatment: service_treatment,
				service_unit: this.state.unit,
				service_require: '',
				service_claim: payType,
				service_price: servicePrice + outSidePrice,
				service_at: dataTop.bookDate.split('T')[0] + ' ' + dataTop.bookDate.split('T')[1],
				bookdate: dataTop.bookDate
			}
		};
		let data2 = service_treatment.map((el) => {
			let id = el;
			return { id };
		});
		let data3 = this.state.treatmentData.filter((el, i) => {
			if (data2.some((e) => e.id === el.id)) {
				return true;
			}
		});

		// check input
		// then send to service `create_list_by_front`
		if (this.state.personType == true) {
			if (this.CheckInput(data)) {
				this.setState({ cashIn: 0, datas_pdf: data3, modal: true }, () => {
					// this.SendPostMT()
				});
			}
		} else {
			if (this.CheckInput_Organization(data)) {
				this.setState({ cashIn: 0, datas_pdf: data3, modal: true }, () => {
					// this.SendPostMT()
				});
			}
		}
	}

	async SendPostMT() {
		let { personType } = this.state;
		//---------------------------------------บุคคล------------------------------------------------
		if (personType == true) {
			let dataColRight = this.colRight.returnState();
			let dataTop = this.top.returnState();
			let dataColRightBot = this.col_right_bot.returnState();
			let service_treatment = [];
			this.state.curCheck.forEach((e, index) => {
				if (e) service_treatment.push(index);
			});
			service_treatment.forEach((e, i) => {
				this.state.treatmentData.filter((el, index) => {
					if (e === index) {
						let { newTreatment, payType } = this.state;
						let obj = {
							id: el.id,
							treatment_name: el.treatment_name,
							cost: el.cost,
							disburseable: el.disburseable,
							detail: el.detail,
							cgd_code: el.cgd_code,
							type: el.type
						};
						newTreatment.push(obj);
						this.setState({ newTreatment: newTreatment });
					}
				});
			});

			let {
				servicePrice,
				outSidePrice,
				cashIn,
				priceNewPackage,
				newTreatment,
				newPackage,
				unit,
				address,
				index,
				payType,
				_package
			} = this.state;
			if (unit == 3) {
				let google_map = this.google_map.returnState();
				let data = {
					customer: parseInt(dataColRight.user_id),
					// doctor: parseInt(dataColRightTop.doctor),
					service_treatment: JSON.stringify(newTreatment.concat(newPackage)),
					service_unit: parseInt(unit),
					service_require: '',
					service_claim: Number(payType),
					right_id: Number(dataColRightBot.right_id),
					relation: String(dataColRightBot.relation),
					right_user: String(dataColRightBot.right_user),
					birthday_baby: moment(dataColRight.dateBirth).format('YYYY-MM-DD'),
					official: Number(dataColRightBot.official),
					service_price: servicePrice + outSidePrice + priceNewPackage,
					service_at: dataTop.bookDate.split('T')[0] + ' ' + dataTop.bookDate.split('T')[1],
					service_package:
						index == -999
							? 'กำหนดเอง'
							: index == 1
								? 'Standard (< 35 ปี)'
								: index == 2
									? 'Standard (> 35 ปี)'
									: index == 3 ? 'Silver' : index == 4 ? 'Gold' : 'Platinum',
					location: {
						latitude: google_map.lats.toString(),
						longitude: google_map.lngs.toString(),
						address: address
					}
				};
				let { curServiceType } = this.state;
				//----------------Check input is cashIn >= totalPrice------------------------
				if (cashIn >= servicePrice + outSidePrice) {
					try {
						this.setState({ loadingSave: true, modal: false });
						let res = await POST(CREATE_DELIVERY_LIST_BY_FRONT(curServiceType), data);
						if (res.success) {
							this.setState({
								hn: res.result.hn,
								ln: res.result.ln,
								loadingSave: false
							});
							swal('สำเร็จ!', 'บันทึกรายการเสร็จสิ้น', 'success', {
								buttons: false,
								timer: 2000
							}).then(() => {
								this.props.history.push('/home');
							});
							// move to home page
						} else {
							this.setState({ loadingSave: false });
							swal('ผิดพลาด!', res.message, 'error', {
								buttons: false,
								timer: 2000
							});
							this.setState({ newTreatment: [] });
						}
					} catch (error) {
						this.setState({ loadingSave: false });
						swal('ผิดพลาด!', 'Network Error', 'error', {
							buttons: false,
							timer: 2000
						});
						this.setState({ newTreatment: [] });
					}
				} else {
					swal('ผิดพลาด!', 'กรุณาชำระเงินให้ครบถ้วน', 'error', {
						buttons: false,
						timer: 2000
					});
					this.setState({ newTreatment: [] });
				}
			} else {
				let data = {
					customer: parseInt(dataColRight.user_id),
					// doctor: parseInt(dataColRightTop.doctor),
					service_treatment: JSON.stringify(newTreatment.concat(newPackage)),
					// service_treatment: newTreatment.concat(newPackage),
					service_unit: parseInt(unit),
					service_require: '',
					service_claim: Number(payType),
					right_id: Number(dataColRightBot.right_id),
					relation: String(dataColRightBot.relation),
					right_user: String(dataColRightBot.right_user),
					birthday_baby: moment(dataColRight.dateBirth).format('YYYY-MM-DD'),
					official: Number(dataColRightBot.official),
					service_price: servicePrice + outSidePrice + priceNewPackage,
					service_at: dataTop.bookDate.split('T')[0] + ' ' + dataTop.bookDate.split('T')[1],
					service_package:
						index == -999
							? 'กำหนดเอง'
							: index == 1
								? 'Standard (< 35 ปี)'
								: index == 2
									? 'Standard (> 35 ปี)'
									: index == 3 ? 'Silver' : index == 4 ? 'Gold' : 'Platinum'
				};
				let { curServiceType } = this.state;
				//----------------Check input is cashIn >= totalPrice------------------------
				if (cashIn >= servicePrice + outSidePrice) {
					try {
						this.setState({ loadingSave: true, modal: false });
						let res = await POST(CREATE_LIST_BY_FRONT(curServiceType), data);
						if (res.success) {
							this.setState({
								hn: res.result.hn,
								ln: res.result.ln,
								loadingSave: false
							});
							swal('สำเร็จ!', 'บันทึกรายการเสร็จสิ้น', 'success', {
								buttons: false,
								timer: 2000
							}).then(() => {
								this.props.history.push('/home');
							});
							// move to home page
						} else {
							this.setState({ loadingSave: false });
							swal('ผิดพลาด!', res.message, 'error', {
								buttons: false,
								timer: 2000
							});
							this.setState({ newTreatment: [] });
						}
					} catch (error) {
						this.setState({ loadingSave: false });
						swal('ผิดพลาด!', 'Network Error', 'error', {
							buttons: false,
							timer: 2000
						});
						this.setState({ newTreatment: [] });
					}
				} else {
					swal('ผิดพลาด!', 'กรุณาชำระเงินให้ครบถ้วน', 'error', {
						buttons: false,
						timer: 2000
					});
					this.setState({ newTreatment: [] });
				}
			}
		} else {
			//------------------------------------------องค์กร-----------------------------------------
			let dataColRight = this.colRight.returnState();
			let dataTop = this.top.returnState();
			let dataColRightBot = this.col_right_bot.returnState();
			// let dataColRightTop = this.col_right_top.returnState();
			let service_treatment = [];
			this.state.curCheck.forEach((e, index) => {
				if (e) service_treatment.push(index);
			});
			service_treatment.forEach((e, i) => {
				this.state.treatmentData.filter((el, index) => {
					if (e === index) {
						let { newTreatment, payType } = this.state;
						let obj = {
							id: el.id,
							treatment_name: el.treatment_name,
							cost: el.cost,
							disburseable: el.disburseable,
							detail: el.detail,
							cgd_code: el.cgd_code,
							type: el.type
						};
						newTreatment.push(obj);
						this.setState({ newTreatment: newTreatment });
					}
				});
			});
			let {
				servicePrice,
				outSidePrice,
				cashIn,
				user_id,
				unit,
				organization_list,
				priceNewPackage,
				newTreatment,
				newPackage,
				address,
				index,
				payType
			} = this.state;

			if (unit == 3) {
				let google_map = this.google_map.returnState();
				let data = {
					customers: organization_list,
					company: dataTop.organization,
					doctor: 3,
					service_treatment: JSON.stringify(newTreatment.concat(newPackage)),
					service_unit: parseInt(unit),
					service_require: '',
					service_claim: Number(payType),
					right_id: Number(dataColRightBot.right_id),
					relation: String(dataColRightBot.relation),
					right_user: String(dataColRightBot.right_user),
					birthday_baby: moment(dataColRight.dateBirth).format('YYYY-MM-DD'),
					official: Number(dataColRightBot.official),
					service_price: servicePrice + outSidePrice + priceNewPackage,
					service_at: dataTop.bookDate.split('T')[0] + ' ' + dataTop.bookDate.split('T')[1],
					service_package:
						index == -999
							? 'กำหนดเอง'
							: index == 1
								? 'Standard (< 35 ปี)'
								: index == 2
									? 'Standard (> 35 ปี)'
									: index == 3 ? 'Silver' : index == 4 ? 'Gold' : 'Platinum',
					location: {
						latitude: google_map.lats.toString(),
						longitude: google_map.lngs.toString(),
						address: address
					}
				};
				let { curServiceType } = this.state;

				//----- Check input is cashIn >= totalPrice
				if (cashIn >= servicePrice + outSidePrice) {
					try {
						this.setState({ loadingSave: true, modal: false });
						let res = await POST(CREATE_COMPANY_DELIVERY_LIST_BY_FRONT(curServiceType), data);
						if (res.success) {
							this.setState({
								ln: res.result.ln,
								loadingSave: false
							});
							swal('สำเร็จ!', 'บันทึกรายการเสร็จสิ้น', 'success', {
								buttons: false,
								timer: 2000
							}).then(() => {
								this.props.history.push('/home');
							});
							// move to home page
						} else {
							this.setState({ loadingSave: false });
							swal('ผิดพลาด!', res.message, 'error', {
								buttons: false,
								timer: 2000
							});
							this.setState({ newTreatment: [] });
						}
					} catch (error) {
						this.setState({ loadingSave: false });
						swal('ผิดพลาด!', 'Network Error', 'error', {
							buttons: false,
							timer: 2000
						});
						this.setState({ newTreatment: [] });
					}
				} else {
					swal('ผิดพลาด!', 'กรุณาชำระเงินให้ครบถ้วน', 'error', {
						buttons: false,
						timer: 2000
					});
					this.setState({ newTreatment: [] });
				}
			} else {
				let data = {
					customers: organization_list,
					company: dataTop.organization,
					doctor: 3,
					service_treatment: JSON.stringify(newTreatment.concat(newPackage)),
					service_unit: parseInt(unit),
					service_require: '',
					service_claim: Number(payType),
					right_id: Number(dataColRightBot.right_id),
					relation: String(dataColRightBot.relation),
					right_user: String(dataColRightBot.right_user),
					birthday_baby: moment(dataColRight.dateBirth).format('YYYY-MM-DD'),
					official: Number(dataColRightBot.official),
					service_price: servicePrice + outSidePrice + priceNewPackage,
					service_at: dataTop.bookDate.split('T')[0] + ' ' + dataTop.bookDate.split('T')[1],
					service_package:
						index == -999
							? 'กำหนดเอง'
							: index == 1
								? 'Standard (< 35 ปี)'
								: index == 2
									? 'Standard (> 35 ปี)'
									: index == 3 ? 'Silver' : index == 4 ? 'Gold' : 'Platinum'
				};
				let { curServiceType } = this.state;
				//----- Check input is cashIn >= totalPrice
				if (cashIn >= servicePrice + outSidePrice) {
					try {
						this.setState({ loadingSave: true, modal: false });
						let res = await POST(CREATE_COMPANY_LIST_BY_FRONT(curServiceType), data);
						if (res.success) {
							this.setState({
								ln: res.result.ln,
								loadingSave: false
							});
							swal('สำเร็จ!', 'บันทึกรายการเสร็จสิ้น', 'success', {
								buttons: false,
								timer: 2000
							}).then(() => {
								this.props.history.push('/home');
							});
							// move to home page
						} else {
							this.setState({ loadingSave: false });
							swal('ผิดพลาด!', res.message, 'error', {
								buttons: false,
								timer: 2000
							});
							this.setState({ newTreatment: [] });
						}
					} catch (error) {
						this.setState({ loadingSave: false });
						swal('ผิดพลาด!', 'Network Error', 'error', {
							buttons: false,
							timer: 2000
						});
						this.setState({ newTreatment: [] });
					}
				} else {
					swal('ผิดพลาด!', 'กรุณาชำระเงินให้ครบถ้วน', 'error', {
						buttons: false,
						timer: 2000
					});
					this.setState({ newTreatment: [] });
				}
			}
		}
	}
	// ########################## END FOR MT ##########################

	// ########################## FOR PT #########################
	async SendPostPT() {
		let dataColRight = this.colRight.returnState();
		// let dataPTBody = this._ptBody.getData();
		let dataTop = this.top.returnState();
		// let bedAndBook = this._ChooseBedBox.getBookedBed();
		let dataColRightBot = this.col_right_bot.returnState();
		// let dataColRightTop = this.col_right_top.returnState();
		let observe = this.observe.returnState();
		let doctorPT = this.DoctorPT.returnState();
		let tm = this.state.MS;
		let {
			servicePrice,
			outSidePrice,
			cashIn,
			priceNewPackage,
			newTreatment,
			newPackage,
			unit,
			address,
			index,
			type_service,
			payType
		} = this.state;

		let data = {
			customer: Number(dataColRight.user_id),
			doctor: parseInt(doctorPT.docterID == '' ? 0 : doctorPT.docterID),
			service_treatment:
				type_service == 1
					? JSON.stringify([
							{
								cgd_code: tm[0].cgd_code,
								cost: tm[0].cost,
								detail: tm[0].detail,
								disburseable: payType == 3 ? tm[0].cost : tm[0].disburseable,
								id: tm[0].id,
								treatment_name: tm[0].treatment_name,
								type: tm[0].type
							}
						])
					: type_service == 4 || type_service == 5
						? JSON.stringify([])
						: JSON.stringify(newTreatment.concat(newPackage)),
			service_unit: parseInt(unit), // defaule มหาวิทยาลัยขอนแก่น
			service_price: Number(type_service == 1 ? tm[0].cost : servicePrice + outSidePrice + priceNewPackage),
			service_require: 'test', // dont know what it is
			service_claim: Number(payType),
			right_id: Number(dataColRightBot.right_id),
			relation: String(dataColRightBot.relation),
			right_user: String(dataColRightBot.right_user),
			birthday_baby: moment(dataColRight.dateBirth).format('YYYY-MM-DD'),
			official: Number(dataColRightBot.official),
			service_at: doctorPT.service_at,
			weight: Number(observe.weight),
			heart_rate: Number(observe.heart_rate),
			height: Number(observe.height),
			blood_pressure: Number(observe.blood_pressure),
			sub_type:
				type_service == 1
					? 'ThaiMassage'
					: type_service == 2 ? 'Ortho' : type_service == 3 ? 'Neuro' : type_service == 4 ? 'Chest' : 'Child',
			symptom: {
				diagnosis: '',
				chief_complaint: '',
				problem: '',
				so_examination: '',
				symptom_picture: ''
			}
		};
		try {
			this.setState({ loadingSave: true, modal: false });
			let res = await POST(CREATE_LIST_BY_FRONT(type_service == 1 ? 'tm' : 'pt'), data);
			if (res.success) {
				this.setState({ loadingSave: false });
				swal('สำเร็จ!', 'บันทึกรายการเสร็จสิ้น', 'success', {
					buttons: false,
					timer: 2000
				}).then(() => {
					this.props.history.push('/home');
				});
				// move to home page
			} else {
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
				this.setState({ newTreatment: [], loadingSave: false });
			}
		} catch (error) {
			this.setState({ loadingSave: false });
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
			this.setState({ newTreatment: [] });
		}
	}

	CheckInputPT() {
		// // Col_right, Col_left, PT_Body
		let dataColRight = this.colRight.returnState();
		// let dataPTBody = this._ptBody.getData();
		let dataTop = this.top.returnState();
		let ages = moment(dataColRight.dateBirth).format('MM/DD/YYYY');
		let years = moment().diff(ages, 'years');
		// let bedAndBook = this._ChooseBedBox.getBookedBed();
		let dataColRightBot = this.col_right_bot.returnState();
		// let dataColRightTop = this.col_right_top.returnState();
		let observe = this.observe.returnState();
		let doctorPT = this.DoctorPT.returnState();
		let present = moment();
		let { servicePrice, outSidePrice, cashIn, unit, priceNewPackage, type_service, payType } = this.state;

		let service_treatment = [];
		this.state.curCheck.forEach((e, index) => {
			if (e) service_treatment.push(index + 1);
		});

		let data = {
			customer: dataColRight.user_id,
			doctor: parseInt(doctorPT.docterID),
			service_treatment: JSON.stringify(service_treatment),
			service_unit: parseInt(unit), // defaule มหาวิทยาลัยขอนแก่น
			service_price: servicePrice + outSidePrice + priceNewPackage,
			service_require: 'test', // dont know what it is
			service_claim: payType,
			service_at: doctorPT.service_at,
			weight: observe.weight,
			heart_rate: observe.heart_rate,
			height: observe.height,
			blood_pressure: observe.blood_pressure,
			// service_at,
			symptom: {
				diagnosis: '',
				chief_complaint: '',
				problem: '',
				so_examination: '',
				picture: ''
			}
		};
		// console.log('dataColRightBot.right_user', dataColRightBot.right_user);

		if (!data.customer || data.customer === '') {
			swal('ผิดพลาด!', 'กรุณาสมัครสมาชิกก่อนจอง', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (
			Number(payType) === 3 &&
			dataColRightBot.relation === 'บุตร' &&
			Number(dataColRightBot.right_id) === 2 &&
			years >= 20
		) {
			swal('อายุของบุตรท่านเกิน 20 ปีบริบูรณ์แล้ว', 'ผิดพลาด!', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (Number(payType) === 3 && Number(dataColRightBot.right_id) === 2 && !dataColRightBot.right_user) {
			swal('กรุณากรอกชื่อผู้ใช้สิทธิ์', 'ผิดพลาด!', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		// if (type_service == 2 || type_service == 3) {
		// 	if (JSON.parse(data.service_treatment).length === 0) {
		// 		swal('ผิดพลาด!', 'กรุณาเลือก Treatment ก่อนชำจอง', 'error', {
		// 			buttons: false,
		// 			timer: 2000
		// 		});
		// 		return false;
		// 	}
		// }

		if (type_service == 1 || type_service == 2 || type_service == 3) {
			if (!data.doctor || data.doctor === '' || data.service_at.length < 12) {
				swal('ผิดพลาด!', 'กรุณาเลือกนักเทคนิคการแพทย์และวันเวลานัดหมายก่อนจอง', 'error', {
					buttons: false,
					timer: 2000
				});
				return false;
			}
		}

		// if (data.weight == 0) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกน้ำหนัก', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }
		// if (data.height == 0) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกส่วนสูง', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }
		// if (data.heart_rate == 0) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกอัตราการเต้นของหัวใจ', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }
		// if (data.blood_pressure == 0) {
		// 	swal('ผิดพลาด!', 'กรุณากรอกความดันโลหิต', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }
		// if ( data.symptom.problem == '') {
		// 	swal('ผิดพลาด!', 'กรุณากรอกแบบบันทึกการรักษาให้ครบ', 'error', {
		// 		buttons: false,
		// 		timer: 2000
		// 	});
		// 	return false;
		// }

		return true;
	}

	// ########################## END FOR PT #########################

	async setServiceType() {
		let { curServiceType, treatmentData } = this.state;
		// Render new Package, Treatment follow by type
		// set Treament
		let d = [];
		treatmentData.forEach((e, index) => {
			if (e.type === curServiceType) {
				d.push([
					e.treatment_name,
					e.cost,
					Number(e.cost) - Number(e.disburseable),
					<input
						value={e.treatment_name}
						key={'in' + index}
						id={'' + index}
						onClick={() => this.onClick(index)}
						onChange={(ei) => console.log(ei.target.value)}
						checked={false}
						type="checkbox"
					/>,
					e.detail,
					e.cgd_code
				]);
			}
		});
		this.setState({ treatment: d, servicePrice: 0 });
	}

	onSubmit = () => {
		let { curServiceType } = this.state;
		if (curServiceType === 'mt') {
			this.SendPostMT();
		} else if (curServiceType === 'pt') {
			this.SendPostPT();
		}
	};

	onSavePT = () => {
		// let ptData = this._ptBody.getData()
		if (this.CheckInputPT() === true) {
			if (User.role === 'frontPT' || User.role === 'pt') {
				this.setState({ cashIn: 0 }, () => this.onSubmit());
			} else {
				this.setState({ modal: true, cashIn: 0 });
			}
			// this.setState({ modal: true, cashIn: 0 });
		}
		// console.log('ptData is ', ptData.base64)
	};

	setDocType = async (type) => {
		await this.setState({ curServiceType: type });
		this.setServiceType();
	};
	Searchtext = () => {
		let texts = this.state.searchtext.toLowerCase();
		let copyTreatment = this.state.old_treatment;
		let data = copyTreatment.filter((e) => e[0].toLowerCase().match(texts));
		this.setState({ treatment: data });
	};
	render() {
		const {
			cashIn,
			modal,
			isCheckAddList,
			_package,
			treatment,
			units,
			types,
			curServiceType,
			pt,
			mt,
			servicePrice,
			outSidePrice,
			isCustomer,
			loading,
			bedSelectedFromHome,
			personType,
			organization,
			unit,
			index,
			obj,
			organization_list,
			BasicSchedule,
			treatmentData,
			priceNewPackage,
			newPackage,
			payType,
			discount,
			type_service,
			type_time,
			holiday,
			loadingSave,
			searchtext
		} = this.state;
		// console.log('payType', payType);
		let tm = this.state.MS;
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : isCheckAddList ? (
			<div id="addcustomer" className="mb-5">
				<Modal
					isOpen={BasicSchedule}
					onRequestClose={() => this.setState({ BasicSchedule: false })}
					style={customStyles}
				>
					<ScheduleModal Modalfalse={() => this.setState({ BasicSchedule: false })} />
				</Modal>
				<PayModal
					cashIn={cashIn}
					isOpenModal={modal}
					CloseModal={() => this.setState({ modal: false })}
					SetCashIn={(e) => this.setState({ cashIn: e })}
					servicePrice={type_service == 1 ? tm[0].cost : servicePrice + priceNewPackage}
					outSidePrice={outSidePrice}
					OnSubmit={() => this.onSubmit()}
					loadingSave={loadingSave}
				/>
				<Top
					onRef={(ref) => (this.top = ref)}
					isCheckAddList={isCheckAddList}
					types={types}
					unit={unit}
					units={units}
					setUnit={(e) => this.setState({ unit: e })}
					setDocType={(type) => this.setDocType(type)}
					isDefaultType={curServiceType}
					isCustomer={isCustomer}
					personTypes2={personType}
					personTypes={(e) => this.setState({ personType: e })}
					outSidePrice={(e) => this.setState({ outSidePrice: e })}
					onCheckAddOnlyCus={(e) => this.setState({ isCheckAddList: e })}
					onChangeAddList={(e) => this.setState({ isCheckAddList: e })}
					setNameHR={(e) => this.setState({ nameHR: e })}
					type_Service={(e) => {
						let a = [ { treatment: [] } ];
						this.setState(
							{
								type_service: e,
								_package: a
							},
							() => {
								this.selectPackage(0);
								this.DoctorPT.getDoctor();
								this.DoctorPT.getBooking();
							}
						);
					}}
					type_Time={(e) => this.setState({ type_time: e })}
					holiDay={moment(holiday).format('ddd')}
				/>

				{curServiceType === 'mt' ? personType === true ? (
					<Row className="mx-auto">
						<Col className="mx-auto" xs={12} lg={6}>
							<div className="mx-auto">
								<Col_left
									searchtext={(e) =>
										this.setState({ searchtext: e.target.value }, () => this.Searchtext())}
									setPackage={(index) =>
										this.setState({ newTreatment: [] }, () => this.selectPackage(index))}
									_package={_package}
									curServiceType={curServiceType}
									treatment={treatment}
									treatmentData={treatmentData}
									setNewTreatment={(e) => {
										this.setState({ treatment: e });
										this.onClick();
									}}
									settreatmentData={(e) => {
										this.setState({ treatmentData: e });
										this.onClick();
									}}
									priceAll={(e) => this.setState({ priceNewPackage: e })}
									newPackage={(e) => this.setState({ newPackage: e })}
									payType={payType}
									type_Service={type_service}
									oldPackage={[]}
									delOldpackage={(e) => console.log('e', e)}
								/>
							</div>
						</Col>
						<Col className="mx-auto" style={{ margin: 0, marginTop: 10 }} xs={12} lg={6}>
							<div className="mx-auto">
								{/* <Col_right_top
									onRef={(ref) => (this.col_right_top = ref)}
									pt={pt}
									mt={mt}
									curServiceType={curServiceType}
									BasicSchedule={() => this.setState({ BasicSchedule: true })}
								/> */}
								<Col_right
									isCustomer={isCustomer}
									personType={personType}
									GetResponseSignUp={(resData) => {
										if (!resData) return console.log('sign up error');
									}}
									onRef={(ref) => (this.colRight = ref)}
									setNameHR={(e) => this.setState({ nameHR: e })}
									setHN={(e) => this.setState({ hn: e })}
									setconfirm={(e) => this.setState({ confirm: e })}
								/>
								{unit == 3 ? (
									<div>
										<input
											placeholder="บ้านเลขที่ หมู่บ้าน ซอย หมู่ ถนน ตำบล อำเภอ จังหวัด ไปรษณีย์"
											style={{
												width: '100%',
												border: '1px solid #ced4da',
												paddingLeft: '10px',
												paddingRight: '10px',
												fontSize: 'medium'
											}}
											onChange={(e) => this.setState({ address: e.target.value })}
										/>
										<Google_map onRef={(ref) => (this.google_map = ref)} />
									</div>
								) : null}
								<Col_right_bot
									onRef={(ref) => (this.col_right_bot = ref)}
									servicePrice={servicePrice + priceNewPackage}
									outSidePrice={outSidePrice}
									totalPrice={servicePrice + outSidePrice + priceNewPackage}
									MethodClick={() => this.GetDataAllAndCreateListByFront()}
									setpayType={(e) => {
										this.setState({ payType: e }, () => {
											this.onClick();
										});
									}}
									discount={discount}
									payType={payType}
									id={this.props.match.params.id}
								/>
							</div>
						</Col>
					</Row>
				) : (
					<Row className="mx-auto">
						<Col className="mx-auto" xs={12} lg={6}>
							<Col_left
								setPackage={(index) => {
									this.setState({ index: index, newTreatment: [] }, () => this.selectPackage(index));
								}}
								_package={_package}
								curServiceType={curServiceType}
								treatment={treatment}
								treatmentData={treatmentData}
								setNewTreatment={(e) => {
									this.setState({ treatment: e });
									this.onClick();
								}}
								settreatmentData={(e) => {
									this.setState({ treatmentData: e });
									this.onClick();
								}}
								priceAll={(e) => this.setState({ priceNewPackage: e })}
								newPackage={(e) => this.setState({ newPackage: e })}
								payType={payType}
								type_Service={type_service}
								oldPackage={[]}
								delOldpackage={(e) => console.log('e', e)}
							/>
							<Col_left_bot
								obj={obj}
								setObj={(e) => this.setState({ obj: e })}
								organization_list={organization_list}
								setorganization_list={(e) => this.setState({ organization_list: e })}
								user_id={(e) => this.setState({ user_id: e })}
							/>
						</Col>
						<Col className="mx-auto" style={{ margin: 0, marginTop: 10 }} xs={12} lg={6}>
							<div className="mx-auto">
								{/* <Col_right_top
                      onRef={(ref) => (this.col_right_top = ref)}
                      pt={pt}
                      mt={mt}
                      curServiceType={curServiceType}
                    /> */}
								<Col_right
									isCustomer={isCustomer}
									personType={personType}
									unit={unit}
									index={index}
									objs={obj}
									obj={(e) => this.setState({ obj: e })}
									organization_list={(e) => this.setState({ organization_list: e })}
									organization={organization}
									GetResponseSignUp={(resData) => {
										if (!resData) return console.log('sign up error');
									}}
									onRef={(ref) => (this.colRight = ref)}
									setNameHR={(e) => this.setState({ nameHR: e })}
									setHN={(e) => this.setState({ hn: e })}
									setconfirm={(e) => this.setState({ confirm: e })}
								/>
								{unit == 3 ? (
									<div>
										<input
											placeholder="บ้านเลขที่ หมู่บ้าน ซอย หมู่ ถนน ตำบล อำเภอ จังหวัด ไปรษณีย์"
											style={{
												width: '100%',
												border: '1px solid #ced4da',
												paddingLeft: '10px',
												paddingRight: '10px',
												fontSize: 'medium'
											}}
											onChange={(e) => this.setState({ address: e.target.value })}
										/>
										<Google_map onRef={(ref) => (this.google_map = ref)} />
									</div>
								) : null}
								<Col_right_bot
									onRef={(ref) => (this.col_right_bot = ref)}
									servicePrice={servicePrice + priceNewPackage}
									outSidePrice={outSidePrice}
									totalPrice={servicePrice + outSidePrice + priceNewPackage}
									MethodClick={() => this.GetDataAllAndCreateListByFront()}
									setpayType={(e) => {
										this.setState({ payType: e }, () => {
											this.onClick();
										});
									}}
									discount={discount}
									payType={payType}
									id={this.props.match.params.id}
								/>
							</div>
						</Col>
					</Row>
				) : curServiceType === 'pt' ? (
					<div>
						<Row className="mx-auto">
							<Col className="mx-auto" lg={6}>
								{/* <div className="mt-3" /> */}
								<Col_right
									isCustomer={isCustomer}
									personType={personType}
									unit={unit}
									index={index}
									objs={obj}
									obj={(e) => this.setState({ obj: e })}
									organization_list={(e) => this.setState({ organization_list: e })}
									organization={organization}
									GetResponseSignUp={(resData) => {
										if (!resData) return console.log('sign up error');
									}}
									onRef={(ref) => (this.colRight = ref)}
									setNameHR={(e) => this.setState({ nameHR: e })}
									setHN={(e) => this.setState({ hn: e })}
									setconfirm={(e) => this.setState({ confirm: e })}
								/>

								{/* <InProgress /> */}
								{/* <div className="pb-5">
									<ChooseBedBox
										onRef={(ref) => (this._ChooseBedBox = ref)}
										defaultBed={bedSelectedFromHome}
									/>
									<InProgress /> 
								</div> */}
							</Col>
							<Col className="mx-auto" style={{ margin: 0 }} lg={6}>
								<Observe onRef={(ref) => (this.observe = ref)} />
								<PT_body
									onRef={(ref) => (this._ptBody = ref)}
									pt={pt}
									type_role="front"
									list_id=""
									card={false}
								/>
								{/* {type_service == '2' || type_service == '3' ? (
									<Col_left
										setPackage={(index) => {
											this.selectPackage(index);
											this.setState({ index: index });
										}}
										_package={_package}
										curServiceType={curServiceType}
										treatment={treatment}
										treatmentData={treatmentData}
										setNewTreatment={(e) => {
											this.setState({ treatment: e });
											this.onClick();
										}}
										settreatmentData={(e) => {
											this.setState({ treatmentData: e });
											this.onClick();
										}}
										priceAll={(e) => this.setState({ priceNewPackage: e })}
										newPackage={(e) => this.setState({ newPackage: e })}
										payType={payType}
									/>
								) : null} */}
								{/* {type_service == 1 || type_service == 2 || type_service == 3 ? ( */}
								<DoctorPT
									onRef={(ref) => (this.DoctorPT = ref)}
									type_Time={type_time}
									type_Service={type_service}
									holiDay={(e) => this.setState({ holiday: e })}
									Date_tmpt={(e) => ''}
									Time_tmpt={(e) => ''}
								/>
								{/* ) : null} */}
								{/* <Col_right_top
									onRef={(ref) => (this.col_right_top = ref)}
									pt={pt}
									mt={mt}
									curServiceType={curServiceType}
									BasicSchedule={() => this.setState({ BasicSchedule: true })}
								/> */}
								<br />
								{unit == 3 ? (
									<div>
										<input
											placeholder="บ้านเลขที่ หมู่บ้าน ซอย หมู่ ถนน ตำบล อำเภอ จังหวัด ไปรษณีย์"
											style={{
												width: '100%',
												border: '1px solid #ced4da',
												paddingLeft: '10px',
												paddingRight: '10px',
												fontSize: 'medium'
											}}
											onChange={(e) => this.setState({ address: e.target.value })}
										/>
										<Google_map onRef={(ref) => (this.google_map = ref)} />
									</div>
								) : null}
								<Col_right_bot
									isDiableBtn={true}
									onRef={(ref) => (this.col_right_bot = ref)}
									servicePrice={type_service == 1 ? tm[0].cost : servicePrice + priceNewPackage}
									outSidePrice={outSidePrice}
									totalPrice={
										type_service == 1 ? tm[0].cost : servicePrice + outSidePrice + priceNewPackage
									}
									MethodClick={() => this.GetDataAllAndCreateListByFront()}
									setpayType={(e) => {
										this.setState({ payType: e }, () => {
											this.onClick();
										});
									}}
									discount={
										type_service == 1 ? payType == 3 ? tm[0].cost : tm[0].disburseable : discount
									}
									payType={payType}
									id={this.props.match.params.id}
								/>
								{/* <InProgress /> */}
							</Col>
						</Row>
						<FixedBotButton text={'บันทึก'} Method={this.onSavePT} />
					</div>
				) : (
					<div className="py-4 text-center">--- Not expected type! ---</div>
				)}
			</div>
		) : (
			// Only Add Customer
			<div id="addcustomer" className="mb-5">
				<div id="addcustomer-top" className="d-flex mx-auto mt-3 px-4 w-100">
					<input
						checked={isCheckAddList}
						onChange={(e) => this.setState({ isCheckAddList: e.target.checked })}
						type="checkbox"
						id="only-cus"
					/>
					<Label htmlFor="only-cus" className="top-label mx-2">
						เพิ่มรายการตรวจ
					</Label>
				</div>
				<Row className="mx-auto">
					<div className="col-lg-6 mx-auto">
						{isCustomer === -1 ? (
							<Col_right
								isCustomer={isCustomer}
								personType={personType}
								GetResponseSignUp={(resData) => {
									if (!resData) return console.log('sign up error');
									// console.log('resData', resData)
								}}
								onRef={(ref) => (this.colRight = ref)}
								setNameHR={(e) => this.setState({ nameHR: e })}
								setHN={(e) => this.setState({ hn: e })}
								setconfirm={(e) => this.setState({ confirm: e })}
							/>
						) : (
							<CardData
								isCustomer={isCustomer}
								GetResponseSignUp={(resData) => {
									if (!resData) return console.log('sign up error');
									// console.log('resData', resData)
								}}
								onRef={(ref) => (this.colRight = ref)}
								setNameHR={(e) => this.setState({ nameHR: e })}
								setHN={(e) => this.setState({ hn: e })}
							/>
						)}
					</div>
					<div className="col-sm-6" />
				</Row>
			</div>
		);
	}
}

export default AddCustomer;
