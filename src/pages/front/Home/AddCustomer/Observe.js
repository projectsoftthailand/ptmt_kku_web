import React, { Component } from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
export default class Observe extends Component {
	constructor(props) {
		super(props);

		this.state = {
			weight: 0,
			height: 0,
			blood_pressure: 0,
			heart_rate: 0
		};
	}

	componentWillMount() {
		this.props.onRef(this);
	}

	returnState() {
		let { weight, heart_rate, height, blood_pressure } = this.state;
		return {
			weight,
			heart_rate,
			height,
			blood_pressure
		};
	}
	render() {
		return (
			<div style={{ backgroundColor: '#fff', borderRadius: '0.2rem', padding: '1rem' }}>
				<Row>
					<Col xs={12} sm={6} xl={3}>
						<FormGroup>
							<Label>น้ำหนัก</Label>
							<Col>
								<Input type="number" onChange={(e) => this.setState({ weight: e.target.value })} />
							</Col>
						</FormGroup>
					</Col>
					<Col xs={12} sm={6} xl={3}>
						<FormGroup>
							<Label>ส่วนสูง</Label>
							<Col>
								<Input type="number" onChange={(e) => this.setState({ height: e.target.value })} />
							</Col>
						</FormGroup>
					</Col>
					<Col xs={12} sm={6} xl={3}>
						<FormGroup>
							<Label>ความดันโลหิต</Label>
							<Col>
								<Input
									type="number"
									onChange={(e) => this.setState({ blood_pressure: e.target.value })}
								/>
							</Col>
						</FormGroup>
					</Col>
					<Col xs={12} sm={6} xl={3}>
						<FormGroup>
							<Label>อัตราการเต้นของหัวใจ</Label>
							<Col>
								<Input type="number" onChange={(e) => this.setState({ heart_rate: e.target.value })} />
							</Col>
						</FormGroup>
					</Col>
				</Row>
			</div>
		);
	}
}
