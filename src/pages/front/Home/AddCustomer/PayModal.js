import React, { Component } from 'react';
import Clock from 'react-live-clock';
import moment from 'moment';
import Modal from 'react-modal';
import { Input, Label } from 'reactstrap';
import { IoMdClose } from 'react-icons/io';
import Color from '../../../../components/Color';
import { Animated } from 'react-animated-css';
import ReactLoading from 'react-loading';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

Modal.setAppElement(document.getElementById('root'));

export default class PayModal extends Component {
	render() {
		let {
			isOpenModal,
			CloseModal,
			cashIn,
			SetCashIn,
			servicePrice,
			outSidePrice,
			OnSubmit,
			loadingSave
		} = this.props;
		return (
			<div>
				<Modal isOpen={loadingSave} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Modal isOpen={isOpenModal} onRequestClose={CloseModal} style={customStyles}>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<div
							style={{
								width: 400,
								borderRadius: 10,
								backgroundColor: '#fff',
								paddingTop: 20,
								paddingBottom: 20,
								fontSize: 12,
								position: 'relative'
							}}
							className="container mx-auto text-center"
						>
							<div
								style={{
									fontSize: 11,
									position: 'absolute',
									display: 'flex',
									top: 10,
									left: 10,
									color: Color.Blue
								}}
							>
								<div className="mr-2">
									{moment().format('DD/MM/') + (+moment().format('YYYY') + 543)}
								</div>
								<Clock format={'HH:mm:ss'} ticking={true} timezone={'US/Pacific'} />
							</div>

							<div
								className="btn-exit-hover"
								onClick={() => CloseModal()}
								style={{
									position: 'absolute',
									backgroundColor: Color.Blue,
									display: 'flex',
									justifyContent: 'center',
									alignItems: 'center',
									borderRadius: '100%',
									width: 30,
									height: 30,
									cursor: 'pointer',
									top: 20,
									right: 20,
									marginTop: -10,
									marginRight: -10
								}}
							>
								<IoMdClose color="white" size={18} />
							</div>
							<div style={{ width: '90%' }} className="pt-5 pb-4 mx-auto">
								<div className="row mx-auto my-3">
									<div className="col text-left">ยอดชำระ</div>
									<div className="col-5">{(servicePrice + outSidePrice).toLocaleString()}</div>
									<div className="col text-right">บาท</div>
								</div>
								<div className="row mx-auto my-3">
									<div className="col text-left mt-1">รับมา</div>
									<div className="col-5">
										<Input
											placeholder="0.00"
											onChange={(e) => SetCashIn(e.target.value)}
											style={{ fontSize: 12, textAlign: 'center' }}
											type="number"
										/>
									</div>
									<div className="col text-right mt-1">บาท</div>
								</div>
								<div className="row mx-auto my-3">
									<div className="col text-left">ทอน</div>
									<div className="col-5">
										{(cashIn - (servicePrice + outSidePrice)).toLocaleString()}
									</div>
									<div className="col text-right">บาท</div>
								</div>
							</div>
							<button onClick={() => OnSubmit()} className="btn-form">
								ยืนยันการชำระ
							</button>
						</div>
					</Animated>
				</Modal>
			</div>
		);
	}
}
