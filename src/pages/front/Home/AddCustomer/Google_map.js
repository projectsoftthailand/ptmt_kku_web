import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker } from 'react-google-maps';
import { withRouter } from 'react-router-dom';
import Geocode from 'react-geocode';

Geocode.setApiKey('AIzaSyDZSZWJ0NKckLq5_AOvsUGogDgxsGbQIew');
Geocode.enableDebug();

var lat = 16.439644;
var lng = 102.828584;
var AsyncMap = withScriptjs(
	withGoogleMap((props) => (
		<GoogleMap defaultZoom={12} defaultCenter={{ lat: lat, lng: lng }}>
			<Marker
				name={'Dolores park'}
				onDragEnd={onMarkerDragEnd}
				draggable={true}
				position={{ lat: lat, lng: lng }}
			/>
			<Marker />
		</GoogleMap>
	))
);
function onMarkerDragEnd(event) {
	let newLat = event.latLng.lat(),
		newLng = event.latLng.lng();
	lat = newLat;
	lng = newLng;
}

@withRouter
class Google_map extends Component {
	constructor(props) {
		super(props);

		this.state = {
			lats: 0,
			lngs: 0
		};
	}
	componentWillMount = () => {
		this.props.onRef(this);
	};

	returnState() {
		let lats = lat;
		let lngs = lng;
		return { lats, lngs };
	}
	render() {
		let map;
		map = (
			<div>
				<AsyncMap
					googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZSZWJ0NKckLq5_AOvsUGogDgxsGbQIew&libraries=places&callback=initMap"
					loadingElement={<div style={{ height: `100%` }} />}
					containerElement={<div style={{ height: `400px` }} />}
					mapElement={<div style={{ height: `100%` }} />}
					lat={10}
				/>
			</div>
		);
		return map;
	}
}
export default Google_map;
