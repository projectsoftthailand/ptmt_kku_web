import React, { Component } from 'react';
import { GET, GET_CLAIM, GET_USERS, LAST_CLAIM, POST } from '../../../../service/service';
import { Input } from 'reactstrap';
import FixedBotButton from '../../../../components/FixedBotButton';
import './style/Col_right.css';

const official_map = [ { id: 1, name: 'ลูกจ้างประจำ' }, { id: 2, name: 'บำนาญ' } ];
const right = [ { id: 1, name: 'ตนเอง' }, { id: 2, name: 'ครอบครัว' } ];
const right_me = [ 'บิดา', 'มารดา', 'คู่สมรส', 'บุตร' ];

export default class Col_right_bot extends Component {
	constructor(props) {
		super(props);

		this.state = {
			claim: [],
			payTypes: -1,
			right_id: 1,
			relation: 'บิดา',
			fullname: [],
			right_user: '',
			clickT: false,
			official: 1
		};
	}
	componentWillMount() {
		this.props.onRef(this);
		this.GetClaim();
		this.lastClaim();
	}
	returnState() {
		let { payTypes, right_id, relation, right_user, official } = this.state;
		return {
			payType: payTypes,
			right_id: right_id,
			relation: relation,
			right_user: right_user,
			official: official
		};
	}
	lastClaim = async () => {
		try {
			let id = this.props.id;
			let res = await POST(LAST_CLAIM, { id });
			if (res.success) {
				this.setState({
					payTypes: res.result.service_claim,
					right_id: res.result.right_id || 1,
					right_user: res.result.right_user || '',
					relation: res.result.relation || 'บิดา',
					official: res.result.official || 1
				});
			} else
				this.setState({
					right_id: 1,
					relation: 'บิดา',
					right_user: '',
					official: 1
				});
		} catch (error) {}
	};
	async GetClaim() {
		try {
			let res = await GET(GET_CLAIM('all'));
			let user = await GET(GET_USERS('all'));
			this.setState({
				claim: res.result,
				payTypes: res.result[0].service_claim_id,
				fullname: user.result.map((e) => {
					let prefix =
						e.th_prefix === 'น.ส.'
							? 'นางสาว'
							: e.th_prefix === 'ด.ช.' ? 'เด็กชาย' : e.th_prefix === 'ด.ญ.' ? 'เด็กหญิง' : e.th_prefix;
					return prefix + e.th_name + ' ' + e.th_lastname;
				}),
				datafullname: user.result.map((e) => {
					let prefix =
						e.th_prefix === 'น.ส.'
							? 'นางสาว'
							: e.th_prefix === 'ด.ช.' ? 'เด็กชาย' : e.th_prefix === 'ด.ญ.' ? 'เด็กหญิง' : e.th_prefix;
					return prefix + e.th_name + ' ' + e.th_lastname;
				})
			});
		} catch (error) {
			// console.log('error GetClaim : ', error);
		}
	}

	async searchFullname(e) {
		let texts = e.target.value;
		let { datafullname } = this.state;
		let data = datafullname.filter((e) => String(e).toLowerCase().indexOf(String(texts).toLowerCase()) > -1);
		this.setState({ right_user: texts, clickT: true, fullname: !texts ? [] : data });
	}
	render() {
		let { claim, payTypes, right_id, right_user, fullname, clickT, relation, official } = this.state;
		let {
			MethodClick,
			servicePrice,
			totalPrice,
			outSidePrice,
			isDiableBtn,
			setpayType,
			discount,
			payType
		} = this.props;
		return (
			<div className="pl-3 pr-5 py-5">
				<div className="row">
					<div className="col fix-col d-flex">
						<label>ใช้สิทธิ์</label>
						<div className="ml-2">
							{claim.map((e, index) => (
								<div key={'pay' + index}>
									<input
										onChange={(e) => {
											this.setState({ payTypes: e.target.value });
											setpayType(e.target.value);
										}}
										checked={Number(payType) === Number(e.service_claim_id)}
										value={e.service_claim_id}
										className="ml-4 mr-3"
										name="pay-type"
										type="radio"
										id={'pay' + e.service_claim_id}
									/>
									<label
										style={{ fontWeight: 100, fontSize: '1.2rem' }}
										htmlFor={'pay' + e.service_claim_id}
									>
										{e.claim_name}
									</label>
									<br />
								</div>
							))}
							{Number(payType) === 3 && (
								<div style={{ paddingLeft: '0.5rem' }}>
									{official_map.map((e, index) => (
										<div className="col-sm-6" key={'right' + index}>
											<input
												onChange={(e) => {
													this.setState({ official: e.target.value });
												}}
												checked={Number(official) === Number(e.id)}
												value={e.id}
												className="ml-4 mr-3"
												name="official"
												type="radio"
												id={'official_' + e.id}
											/>
											<label
												style={{ fontWeight: 100, fontSize: '1.2rem' }}
												htmlFor={'official_' + e.id}
											>
												{e.name}
											</label>
										</div>
									))}
								</div>
							)}
							{Number(payType) === 3 && (
								<div style={{ paddingLeft: '0.5rem' }}>
									{right.map((e, index) => (
										<div className="col-sm-6" key={'right' + index}>
											<input
												onChange={(e) => {
													this.setState({ right_id: e.target.value });
												}}
												checked={Number(right_id) === Number(e.id)}
												value={e.id}
												className="ml-4 mr-3"
												name="right_id"
												type="radio"
												id={'right_' + e.id}
											/>
											<label
												style={{ fontWeight: 100, fontSize: '1.2rem' }}
												htmlFor={'right_' + e.id}
											>
												{e.name}
											</label>
										</div>
									))}
								</div>
							)}
							{Number(payType) === 3 &&
							Number(right_id) === 2 && (
								<div style={{ paddingLeft: '0.5rem' }}>
									<div className="search-select col-sm-6">
										<Input
											value={right_user}
											type="text"
											className="input-search-select"
											placeholder="ค้นหาชื่อนามสกุล..."
											onChange={(e) => this.searchFullname(e)}
										/>
										<div className="search-select-content">
											{clickT &&
												fullname.length > 0 &&
												fullname.map((e, i) => (
													<button
														onClick={() => {
															this.setState({ right_user: e, clickT: false });
														}}
														className="selections"
													>
														{e}
													</button>
												))}
										</div>
									</div>
									<strong className="col-sm-2 pt-2">สำหรับ</strong>
									<div className="col-sm-4">
										<Input
											type="select"
											onChange={(e) => this.setState({ relation: e.target.value })}
											value={relation}
										>
											{right_me.map((e, i) => (
												<option key={i} value={e}>
													{e}
												</option>
											))}
										</Input>
									</div>
								</div>
							)}
						</div>
					</div>
					<div className="col fix-col text-right">
						<p style={{ fontSize: '1.2rem' }}>
							ค่าบริการ<span className="mx-4">{totalPrice.toLocaleString()}</span>บาท
						</p>
						<p style={{ fontSize: '1.2rem' }}>
							ค่าตรวจนอกสถานที่<span className="mx-4">{outSidePrice.toLocaleString()}</span>บาท
						</p>
						<p style={{ fontSize: '1.2rem' }}>
							อัตราค่าบริการที่เบิกได้<span className="mx-4">
								{Number(payType) === 1 ? 0 : discount.toLocaleString()}
							</span>บาท
						</p>
						<p style={{ fontSize: '1.2rem' }}>
							อัตราค่าบริการรวม<span className="mx-4">{totalPrice.toLocaleString()}</span>บาท
						</p>
					</div>
				</div>
				{!isDiableBtn && (
					<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
						<button onClick={() => MethodClick()} className="btn-form">
							ชำระเงิน
						</button>
					</div>
				)}
			</div>
		);
	}
}
