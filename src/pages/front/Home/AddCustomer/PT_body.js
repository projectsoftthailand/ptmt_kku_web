import React, { Component } from 'react';
import {
	Input,
	Col,
	Row,
	Button,
	InputGroup,
	InputGroupAddon,
	Label,
	Card,
	CardHeader,
	CardBody,
	CardText,
	Collapse
} from 'reactstrap';
import { FaRegEdit, FaPrint } from 'react-icons/fa';
import { IoIosArrowDropdownCircle, IoIosArrowDropupCircle } from 'react-icons/io';
import SketchFieldes from '../../../../function/SketchFieldes';
import Color from '../../../../components/Color';
import './style/Time_docterPT.css';
import {
	GET_PICTURE_SYMPTOMS,
	ip,
	GET_LIST_BY_ID,
	GET,
	LIST_CUSTOMER,
	POST,
	GET_LIST,
	UPDATE_SYMPTOM
} from '../../../../service/service';
import { Treatmentrecord } from '../../../../function/PDF';
import Responsive from 'react-responsive';
import moment from 'moment';
import swal from 'sweetalert';
let symptom_path = require('../../../../assets/image/antomy.png');
let Max = (props) => <Responsive {...props} maxWidth={1099} />;
let Min = (props) => <Responsive {...props} minWidth={1100} />;

class PT_body extends Component {
	//dont remove this constructor!!!
	constructor(props) {
		super(props);

		this.state = {
			edit: 0,
			problem: '',
			diagnosis: '',
			list_data: {},
			symptom_id: 0,
			chief_complaint: '',
			physiotherapist: '',
			so_examination: '',
			goal_of_treatment: '',
			treatment: '',
			progress_note: [],
			progress: '',
			isOpen: false,
			data_update: {}
		};
	}
	handleInputChange = (e) => {
		const value = e.target.value;
		const name = e.target.name;
		this.setState({
			[name]: value
		});
	};
	async componentWillMount() {
		this.props.onRef(this);
		if (this.props.type_role == 'pt') {
			await setTimeout(async () => {
				let state = await JSON.parse(localStorage.getItem('statePT_body'));
				if (state) {
					if (state.list_id == this.props.list_id && this.props.save == false) {
						await this.setState(state);
					} else {
						await this.getListdata();
					}
				} else {
					await this.getListdata();
					this.state['list_id'] = this.props.list_id;
					localStorage.setItem('statePT_body', JSON.stringify(this.state));
				}
			}, 0.01);
		}
	}
	componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		localStorage.setItem('statePT_body', JSON.stringify(this.state));
	}
	scrollToBottom = () => {
		setTimeout(() => {
			this.messagesEnd.scrollIntoView({ block: 'end' });
		}, 0.1);
	};
	getListdata = async () => {
		let { list_id, id, obj } = this.props;
		// let text_treatment =
		// 	obj.service_treatment &&
		// 	obj.service_treatment.map((e) => e.treatment_name + ' ' + (e.detail ? `(${e.detail})` : '')).toString();
		let res = await GET(GET_LIST_BY_ID(list_id));
		let list = res.result[0];
		let ress = await POST(LIST_CUSTOMER, { list_id, id });
		let last = ress.result[0];
		let Listall = await GET(GET_LIST('pt', 'all'));
		let chat = await Listall.result
			.filter((el) => el.user_id == id && el.service_status == 5)
			.sort((a, b) => moment(b.service_at) - moment(a.service_at))
			.slice(0, 10);
		let some = chat.some((e) => e.list_id == list.list_id);
		if (!some) {
			chat.push(list);
		}
		this.setState({
			list_data: list,
			symptom_id: list.symptom_id,
			problem: list.problem || last.problem,
			diagnosis: list.diagnosis || last.diagnosis,
			chief_complaint: list.chief_complaint || last.chief_complaint,
			physiotherapist: list.physiotherapist || last.physiotherapist,
			so_examination: list.so_examination || last.so_examination,
			goal_of_treatment: list.goal_of_treatment || last.goal_of_treatment,
			treatment: list.treatment || last.treatment,
			progress: list.progress || last.progress,
			progress_note: chat,
			symptom_picture: last.symptom_picture
		});
	};
	getData = () => {
		let { type_role } = this.props;
		let {
			diagnosis,
			problem,
			chief_complaint,
			physiotherapist,
			so_examination,
			goal_of_treatment,
			treatment,
			progress,
			edit,
			symptom_picture,
			list_data,
			data_update
		} = this.state;
		if (edit) {
			return {
				diagnosis: diagnosis || '',
				physiotherapist: physiotherapist || '',
				chief_complaint: chief_complaint || '',
				problem: problem || '',
				so_examination: so_examination || '',
				goal_of_treatment: goal_of_treatment || '',
				treatment: treatment || '',
				progress: progress || '',
				base64: this.sketch.reTurn().base64
			};
		} else {
			let path = ip + GET_PICTURE_SYMPTOMS(symptom_picture);
			let xhr = new XMLHttpRequest();
			xhr.open('GET', path, true);
			xhr.responseType = 'blob';
			let self = this.props;
			xhr.onload = function(e) {
				let reader = new FileReader();
				let { updateSymptomTwo } = self;
				reader.onload = (event) => {
					let res = event.target.result;
					let obj = {
						diagnosis: diagnosis || '',
						physiotherapist: physiotherapist || '',
						chief_complaint: chief_complaint || '',
						problem: problem || '',
						so_examination: so_examination || '',
						goal_of_treatment: goal_of_treatment || '',
						treatment: treatment || '',
						progress: progress || '',
						base64: list_data.symptom_picture ? '' : res.split('data:image/jpeg;base64,')[1]
					};
					updateSymptomTwo(obj);
				};
				let file = this.response;
				reader.readAsDataURL(file);
			};
			xhr.send();
		}
	};
	updateSymptom = async () => {
		let { diagnosis, problem, symptom_id } = this.state;
		// let { list_id } = this.props;
		let obj = {
			diagnosis,
			problem,
			chief_complaint: '',
			so_examination: '',
			symptom_picture: this.sketch.reTurn().base64
		};
	};
	reCord = () => {
		let { list_data } = this.state;
		let sym = !list_data.symptom_picture ? false : true;
		let path = !list_data.symptom_picture
			? symptom_path
			: ip + GET_PICTURE_SYMPTOMS(list_data.symptom_picture || 'null');
		let xhr = new XMLHttpRequest();
		xhr.open('GET', path, true);
		xhr.responseType = 'blob';
		xhr.onload = function(e) {
			let reader = new FileReader();
			reader.onload = (event) => {
				let res = event.target.result;
				// console.log('res', res);
				Treatmentrecord(res, list_data, sym);
			};
			let file = this.response;
			reader.readAsDataURL(file);
		};
		xhr.send();
	};
	updateProgress = async () => {
		let { list_id } = this.props;
		let { progress } = this.state;
		let obj = {
			progress
		};
		if (progress) {
			let res = await POST(UPDATE_SYMPTOM(list_id), obj);
			if (res.success) {
				swal('บันทึกสำเร็จ', '', 'success').then(() => this.componentDidMount());
			} else {
				swal('บันทึกไม่สำเร็จ', '', 'error');
			}
		} else swal('กรุณากรอกข้อความก่อนกดบันทึก', '', 'warning');
	};
	render() {
		let { pt, type_role, checkedit, list_id, save, savestate } = this.props;
		let {
			edit,
			problem,
			diagnosis,
			list_data,
			physiotherapist,
			chief_complaint,
			so_examination,
			goal_of_treatment,
			treatment,
			progress,
			progress_note,
			isOpen,
			symptom_picture
		} = this.state;
		return (
			<div style={{ marginTop: -10 }} className="bg-white rounded">
				<div className="px-4 pt-2">
					{type_role == 'pt' && (
						<center>
							<h5>แบบบันทึกการรักษาทางกายภาพ</h5>
							<h6>โครงการกายภาพบำบัดและพัฒนาสุขภาพ</h6>
						</center>
					)}
					<Row className="my-2">
						<Col xs={12}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="treatment">
										Progress Note:{' '}
										{/* {isOpen ? (
											<IoIosArrowDropupCircle
												style={{ cursor: 'pointer' }}
												onClick={() => {
													this.setState({ isOpen: false });
													setTimeout(() => {
														this.scrollToBottom();
													}, 0.1);
												}}
											/>
										) : (
											<IoIosArrowDropdownCircle
												style={{ cursor: 'pointer' }}
												onClick={() => {
													this.setState({ isOpen: true });
													setTimeout(() => {
														this.scrollToBottom();
													}, 0.1);
												}}
											/>
										)} */}
									</label>
									{/* <Collapse isOpen={isOpen}> */}
									<div className="progress-chat mb-5">
										<div className="progress-chat-body">
											{progress_note.length > 0 ? (
												progress_note
													.sort((a, b) => moment(a.service_at) - moment(b.service_at))
													.map((e) => (
														<Card className="m-3" key={e.list_id}>
															<CardHeader
																style={{
																	backgroundColor: '#5a88cd',
																	color: '#fff'
																}}
															>
																{`${moment(e.service_at).format(
																	'LLLL'
																)} , รายการที่ ${e.list_id}`}
															</CardHeader>
															<CardBody tag="h3">
																<CardText>{e.progress}</CardText>
															</CardBody>
														</Card>
													))
											) : (
												<Card className="m-3">
													<CardHeader style={{ backgroundColor: '#5a88cd', color: '#fff' }}>
														--- ยังไม่บันทึกรายการ Progress Note. ---
													</CardHeader>
												</Card>
											)}
											<div
												style={{ float: 'left', clear: 'both' }}
												ref={(el) => {
													this.messagesEnd = el;
												}}
											/>
										</div>
										<InputGroup>
											<Input
												// disabled={edit == 0 ? true : false}
												value={progress}
												onChange={this.handleInputChange}
												id="progress"
												name="progress"
												type="textarea"
												style={{ fontSize: 'large' }}
												placeholder="กรอกข้อความ..."
											/>
											<InputGroupAddon addonType="append">
												<Button color="success" onClick={() => this.updateProgress()}>
													<Label size="lg" style={{ cursor: 'pointer' }}>
														บันทึก
													</Label>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</div>
									{/* </Collapse> */}
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div className="font-input-textarea">
									<label htmlFor="diagnosis">Diagnosis:</label>
									<Input
										// disabled={edit == 0}
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={diagnosis}
										onChange={this.handleInputChange}
										id="diagnosis"
										name="diagnosis"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="physiotherapist">Physiotherpist:</label>
									<Input
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={physiotherapist}
										onChange={this.handleInputChange}
										id="physiotherapist"
										name="physiotherapist"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="diagnochief_complaintsis">Chief complaint:</label>
									<Input
										// disabled={edit == 0}
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={chief_complaint}
										onChange={this.handleInputChange}
										id="chief_complaint"
										name="chief_complaint"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="so_examination">Subjective and Objective examination:</label>
									<Input
										// disabled={edit == 0}
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={so_examination}
										onChange={this.handleInputChange}
										id="so_examination"
										name="so_examination"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="problem">Problem:</label>
									<Input
										// disabled={edit == 0}
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={problem}
										onChange={this.handleInputChange}
										id="problem"
										name="problem"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12} sm={6}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="goal_of_treatment">Goal of treatment:</label>
									<Input
										// disabled={edit == 0}
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={goal_of_treatment}
										onChange={this.handleInputChange}
										id="goal_of_treatment"
										name="goal_of_treatment"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>
						<Col xs={12}>
							{type_role == 'pt' && (
								<div>
									<label htmlFor="treatment">Treatment:</label>
									<Input
										disabled={type_role == 'pt' && edit == 0 ? true : false}
										value={treatment}
										onChange={this.handleInputChange}
										id="treatment"
										name="treatment"
										type="textarea"
										style={{ fontSize: 'large' }}
									/>
								</div>
							)}
						</Col>

						<Col xs={12}>
							{type_role == 'pt' &&
							edit == 0 && (
								<div className="d-flex justify-content-center mt-5 mb-5">
									{list_data && !list_data.symptom_picture && !symptom_picture ? (
										<div style={{ marginTop: 40 }}>---ไม่มีรูปภาพประกอบ---</div>
									) : (
										<div>
											<Min>
												<img
													style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
													src={
														ip +
														GET_PICTURE_SYMPTOMS(
															list_data.symptom_picture || symptom_picture
														)
													}
													alt="avatar"
												/>
											</Min>
											<Max>
												<img
													style={{
														boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)',
														width: '100%',
														height: '100%'
													}}
													src={
														ip +
														GET_PICTURE_SYMPTOMS(
															list_data.symptom_picture || symptom_picture
														)
													}
													alt="avatar"
												/>
											</Max>
										</div>
									)}
								</div>
							)}
							{type_role == 'pt' &&
							edit == 1 && (
								<SketchFieldes
									onRef={(ref) => (this.sketch = ref)}
									symptom_picture={list_data.symptom_picture}
									list_id={list_id}
									save={save}
								/>
							)}
						</Col>
						<Col xs={12}>
							<div>
								{type_role == 'pt' && edit == 0 ? (
									<div className="d-flex justify-content-start">
										<div
											className="btn-blue-subject"
											style={{ backgroundColor: Color.Green }}
											onClick={() => {
												checkedit(1);
												savestate(false);
												this.setState({ edit: 1 });
											}}
										>
											<FaRegEdit className="ic-btn-blue-subject mr-2" />
											<div>แก้ไขรายการ</div>
										</div>
										<FaPrint
											style={{
												marginLeft: '0.7rem',
												width: '2rem',
												height: 'auto',
												cursor: 'pointer'
											}}
											onClick={this.reCord}
										/>
									</div>
								) : type_role == 'pt' && edit == 1 ? (
									<div className="d-flex justify-content-start">
										<div
											className="cancal"
											onClick={() => {
												checkedit(0);
												this.setState({ edit: 0, problem: '', diagnosis: '' }, () =>
													this.getListdata()
												);
											}}
										>
											<FaRegEdit className="ic-btn-blue-subject mr-2" />
											<div>ยกเลิก</div>
										</div>
										{/* <div id="submit" onClick={this.updateSymptom}>
											<FaRegFileAlt className="ic-btn-blue-subject mr-2" />
											<div>บันทึก</div>
										</div> */}
									</div>
								) : null}
							</div>
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}

export default PT_body;
