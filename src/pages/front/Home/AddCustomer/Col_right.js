import React, { Component } from 'react';
import { Col, Row, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import Responsive from 'react-responsive';
import moment from 'moment';
import Color from '../../../../components/Color';
import {
	POST,
	CREATE_USER,
	ip,
	GET_PICTURE,
	GET,
	GET_USER_DETAIL,
	COMFIRM_CUSTOMER,
	GET_USERS,
	UPDATE_USER_PROFILE
} from '../../../../service/service';
import swal from 'sweetalert';
import Modal from 'react-modal';
import { Link, withRouter } from 'react-router-dom';
import ReactLoading from 'react-loading';
import socketIOClient from 'socket.io-client';
import Button_Excel from './Button_Excel';
import Barcode from 'react-barcode';
import './style/Col_right.css';
import '../../../admin/AddMember/style/AddMember.css';

var cardReaderIP = 'http://localhost:3099';

const Mobile = (props) => <Responsive {...props} maxWidth={767} />;
const Default = (props) => <Responsive {...props} minWidth={768} />;

let inputData = [];
let dumpData = [];
let dumpList = [];
var uploadImage = new Image();
let activateCustomer = 0;
let special = /[$^[\]{};'"\\|<>@#]/;
let isActivated = false;
let cardStatus = '';
// @withRouter
class Col_right extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isSignedUp: false,

			// if signedUp will get user_id
			user_id: null,

			loading: true,
			reading_card: false,
			obj: [],
			member: false,
			activeID: 99,
			password: '',
			cardID: '',
			nameTh: '',
			firstnameTh: '',
			lastnameTh: '',
			nameEn: '',
			firstnameEn: '',
			lastnameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'ชาย',
			address: '',
			congenital_disease: '', // โรคประจำตัว
			allergy: '', // แพ้ยา
			fileData: null, // for send picture to formData
			file: null, // for display picture
			foreigner: 0,
			foreignerID: '',
			nationality: '',
			religion: '',
			prefixTh: '',
			prefixEn: '',

			new_cardID: '',
			new_nameTh: '',
			new_firstnameTh: '',
			new_lastnameTh: '',
			new_nameEn: '',
			new_firstnameEn: '',
			new_lastnameEn: '',
			new_phone: '',
			new_dateBirth: '',
			new_dateIssue: '',
			new_dateExpiry: '',
			new_sex: 'ชาย',
			new_address: '',
			new_fileData: null, // for send picture to formData
			new_file: null, // for display picture

			change_data: false,
			dumpCardData: [],
			organization_list: [],
			hn: '',
			wait_loading: false
		};
	}

	componentWillMount() {
		let { onRef, isCustomer } = this.props;
		if (onRef) onRef(this);
		if (isCustomer !== -1) {
			this.setState({ isSignedUp: true, user_id: isCustomer });
			this.getCustomerData(isCustomer);
		}
		this.response();
		this.GetUsers().then(this.setState({ loading: false }));
		// }
	}

	response() {
		let { isCustomer } = this.props;
		// console.log(this.props);
		socketIOClient(cardReaderIP).on('card', (res) => {
			// console.log(isCustomer, res, res.data ? res.data : 'Hi');
			cardStatus = res.message;
			if (isCustomer !== -1) {
				res.message === 'commplete' && activateCustomer !== 1
					? this.sortArray(res.data)
					: console.log('Waiting...');
			} else {
				res.message === 'commplete'
					? this.sortArray(res.data)
					: res.message === 'reading fail'
						? swal('คำเตือน', 'เครื่องนี้อ่านเฉพาะบัตรประจำตัวประชาชนเท่านั้น', 'warning', {
								buttons: false,
								timer: 3000
							})
						: res.message === 'reading...'
							? this.setState({ reading_card: true })
							: res.message === 'card remove'
								? swal('', 'นำบัตรประชาชนออกเรียบร้อย', 'success', {
										buttons: false,
										timer: 2000
									}).then(
										this.setState({
											reading_card: false,
											cardID: '',
											foreignerID: '',
											password: '',
											prefixTh: '',
											prefixEn: '',
											nameTh: '',
											nameEn: '',
											phone: '',
											dateBirth: '',
											dateIssue: '',
											dateExpiry: '',
											sex: 'male',
											address: '',
											file: null,
											fileData: null,
											foreigner: 0,
											nationality: '',
											religion: '',
											dumpCardData: []
										})
									)
								: res.message !== 'commplete'
									? this.setState({
											cardID: '',
											foreignerID: '',
											password: '',
											prefixTh: '',
											prefixEn: '',
											nameTh: '',
											nameEn: '',
											phone: '',
											dateBirth: '',
											dateIssue: '',
											dateExpiry: '',
											sex: 'male',
											address: '',
											file: null,
											fileData: null,
											foreigner: 0,
											nationality: '',
											religion: '',
											dumpCardData: []
										})
									: console.log(res, 'Waiting...');
			}
		});
	}

	GetUsers = async () => {
		try {
			let res = await GET(GET_USERS('customer'));
			if (res.success) {
				dumpList = res.result;
			}
		} catch (error) {}
	};

	componentWillUnmount() {
		this._isMounted = false;
		socketIOClient(cardReaderIP).disconnect();
	}

	reRender() {
		console.log('render...');
	}

	sortArray = (e) => {
		let { isCustomer } = this.props;
		inputData.length === 0 ? inputData.push(e) : console.log('Waiting for data');
		dumpData = inputData[0];
		uploadImage = this.dataURLtoFile(dumpData.photo, 'profile.jpg');
		inputData.pop();
		this.setState({ reading_card: false });

		if (isCustomer !== -1) {
			this.mapping();
		} else {
			this.searchData(dumpData.citizenId);
		}
	};

	searchData = (e) => {
		// console.log(dumpList, e);
		// isActivated = true;
		// let event = e.target.value;
		// let searchText = event.trim().toLowerCase();
		let res = dumpList.filter((el) => {
			return el.citizen_id === e;
			// && el.confirm === 1;
		});
		if (res.map((el) => el.active) == 1) {
			swal('ยืนยันแล้ว', 'มีการยืนยันตัวตนผู้ใช้นี้ไปแล้ว', 'warning', {
				buttons: false,
				timer: 3000
			});
		} else if (res.length !== 0 && res.map((el) => el.active) == 0) {
			let goto = res.map((el) => el.user_id);
			// console.log(res, res.map(el => el.active)==1, goto, goto.active)
			swal('ยืนยันตัวตน', 'ต้องการยืนยันตัวตนใช่หรือไม่', 'warning', {
				buttons: {
					cancel: 'ยกเลิก',
					confirm: {
						text: 'ยืนยัน',
						value: 'confirm'
					}
				}
			}).then((value) => {
				switch (value) {
					case 'confirm':
						// console.log(res.user_id)
						this.props.history.push(`/home/addnew/${goto}`);
						// this.context.router.push('/home')
						break;

					default:
						swal('', 'ยังไม่ได้ยืนยันตัวตน', 'warning', {
							buttons: false,
							timer: 2000
						});
				}
			});
		} else {
			this.mapping();
		}
	};

	mapping = () => {
		let { isCustomer } = this.props;
		let {
			cardID,
			nameTh,
			nameEn,
			file,
			address,
			dateIssue,
			dateExpiry,
			dateBirth,
			sex,
			prefixTh,
			prefixEn
		} = this.state;

		let strGender = `${sex === 'male' ? 'ผู้ชาย' : sex === 'female' ? 'ผู้หญิง' : 'ไม่ระบุ'}`;
		let new_strGender = `${dumpData.gender === 'male'
			? 'ผู้ชาย'
			: dumpData.gender === 'female' ? 'ผู้หญิง' : 'ไม่ระบุ'}`;

		let new_photoUser = dumpData.photo !== file ? '\nรูปประจำตัว ' : '';
		let new_cardCitizenId =
			dumpData.citizenId !== cardID
				? '\nหมายเลขบัตรประจำตัวประชาชนจาก ' + cardID + ' เป็น ' + dumpData.citizenId
				: '';
		let new_prefixTh =
			dumpData.titleTH !== prefixTh ? '\nคำนำหน้า (TH) จาก ' + prefixTh + ' เป็น ' + dumpData.titleTH : '';
		let new_prefixEn =
			dumpData.titleEN !== prefixEn ? '\nคำนำหน้า (EN) จาก ' + prefixTh + ' เป็น ' + dumpData.titleTH : '';
		let new_fullnameTh =
			dumpData.firstNameTH + ' ' + dumpData.lastNameTH !== nameTh
				? '\nชื่อ-นามสกุล ภาษาไทยจาก ' + nameTh + ' เป็น ' + dumpData.firstNameTH + ' ' + dumpData.lastNameTH
				: '';
		let new_fullnameEn =
			dumpData.firstNameEN + ' ' + dumpData.lastNameEN !== nameEn
				? '\nชื่อ-นามสกุล ภาษาอังกฤษจาก ' + nameEn + ' เป็น ' + dumpData.firstNameEN + ' ' + dumpData.lastNameEN
				: '';
		let new_birthDate =
			dumpData.birthday !== dateBirth ? '\nวันเกิดจาก ' + dateBirth + ' เป็น ' + dumpData.birthday : '';
		let new_gender = dumpData.gender !== sex ? '\nเพศจาก ' + strGender + ' เป็น ' + new_strGender : '';
		let new_issue = dumpData.issue !== dateIssue ? '\nวันออกบัตรจาก ' + dateIssue + ' เป็น ' + dumpData.issue : '';
		let new_expire =
			dumpData.expire !== dateExpiry ? '\nวันหมดอายุบัตรจาก ' + dateExpiry + ' เป็น ' + dumpData.expire : '';
		let new_newaddress =
			dumpData.address !== address ? '\nที่อยู่จาก ' + address + ' เป็น ' + dumpData.address : '';

		if (isCustomer !== -1) {
			if (new_fullnameTh === nameTh) {
				// console.log('True', dumpData.firstNameTH + ' ' + dumpData.lastNameTH, nameTh)
			} else if (dumpData.firstNameTH + ' ' + dumpData.lastNameTH !== nameTh) {
				// console.log('False')
			}
			if (
				dumpData.citizenId !== cardID ||
				dumpData.prefixTh !== prefixTh ||
				dumpData.prefixEn !== prefixEn ||
				dumpData.firstNameTH + ' ' + dumpData.lastNameTH !== nameTh ||
				dumpData.firstNameEN + ' ' + dumpData.lastNameEN !== nameEn ||
				dumpData.birthday !== dateBirth ||
				dumpData.gender !== sex ||
				dumpData.issue !== dateIssue ||
				dumpData.expire !== dateExpiry ||
				dumpData.address !== address ||
				dumpData.photo !== file
			) {
				swal(
					'ข้อมูลมีการเปลี่ยนแปลง',
					'ยันยันการเปลี่ยนแปลงข้อมูล ดังนี้ ' +
						new_photoUser +
						new_cardCitizenId +
						new_prefixTh +
						new_fullnameTh +
						new_prefixEn +
						new_fullnameEn +
						new_birthDate +
						new_gender +
						new_issue +
						new_expire +
						new_newaddress +
						'หรือไม่ ?',
					'warning',
					{
						buttons: {
							cancel: 'ยกเลิก',
							confirm: {
								text: 'ยืนยัน',
								value: 'confirm'
							}
						}
					}
				).then((value) => {
					switch (value) {
						case 'confirm':
							this.setState({
								cardID: dumpData.citizenId,
								nameTh: dumpData.firstNameTH + ' ' + dumpData.lastNameTH,
								prefixTh: dumpData.titleTH,
								prefixEn: dumpData.titleEN,
								firstnameTh: dumpData.firstNameTH,
								lastnameTh: dumpData.lastNameTH,
								nameEn: dumpData.firstNameEN + ' ' + dumpData.lastNameEN,
								firstnameEn: dumpData.firstNameEN,
								lastnameEn: dumpData.lastNameEN,
								dateBirth: dumpData.birthday,
								dateIssue: dumpData.issue,
								dateExpiry: dumpData.expire,
								sex: dumpData.gender,
								address: dumpData.address,
								file: dumpData.photo,
								fileData: uploadImage
							});
							this.UpdateProfile();
							break;

						default:
							swal('', 'ไม่มีการเปลี่ยนแปลงข้อมูล', 'warning', {
								buttons: false,
								timer: 2000
							});
					}
				});
			} else {
				swal('ยืนยันตัวตน', 'ต้องการยืนยันตัวตนใช่หรือไม่', 'warning', {
					buttons: {
						cancel: 'ยกเลิก',
						confirm: {
							text: 'ยืนยัน',
							value: 'confirm'
						}
					}
				}).then((value) => {
					switch (value) {
						case 'confirm':
							this.setState({
								cardID: dumpData.citizenId,
								nameTh: dumpData.firstNameTH + ' ' + dumpData.lastNameTH,
								prefixTh: dumpData.titleTH,
								prefixEn: dumpData.titleEN,
								firstnameTh: dumpData.firstNameTH,
								lastnameTh: dumpData.lastNameTH,
								nameEn: dumpData.firstNameEN + ' ' + dumpData.lastNameEN,
								firstnameEn: dumpData.firstNameEN,
								lastnameEn: dumpData.lastNameEN,
								dateBirth: dumpData.birthday,
								dateIssue: dumpData.issue,
								dateExpiry: dumpData.expire,
								sex: dumpData.gender,
								address: dumpData.address,
								file: dumpData.photo,
								fileData: uploadImage
							});
							this.UpdateProfile();
							break;

						default:
							swal('', 'ยังไม่ได้ยืนยันตัวตน', 'warning', {
								buttons: false,
								timer: 2000
							});
					}
				});
			}
		} else {
			// console.log('test')
			if (dumpData.citizenId) {
				this.setState({ foreigner: 0 });
			}
			this.setState({
				cardID: dumpData.citizenId,
				prefixTh: dumpData.titleTH,
				prefixEn: dumpData.titleEN,
				nameTh: dumpData.firstNameTH + ' ' + dumpData.lastNameTH,
				firstnameTh: dumpData.firstNameTH,
				lastnameTh: dumpData.lastNameTH,
				nameEn: dumpData.firstNameEN + ' ' + dumpData.lastNameEN,
				firstnameEn: dumpData.firstNameEN,
				lastnameEn: dumpData.lastNameEN,
				dateBirth: dumpData.birthday,
				dateIssue: dumpData.issue,
				dateExpiry: dumpData.expire,
				sex: dumpData.gender,
				address: dumpData.address,
				file: dumpData.photo,
				fileData: uploadImage
			});
		}
	};

	dataURLtoFile(dataurl, filename) {
		var arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}
		return new File([ u8arr ], filename, { type: mime });
	}

	async UpdateProfile() {
		let { isCustomer } = this.props;
		let { password, cardID, nameTh, nameEn, phone, congenital_disease, allergy } = this.state;

		if (!phone) {
			swal({
				text: 'กรุณาระบุเบอร์โทรศัพท์ เช่น "0999999999"',
				content: 'input',
				closeOnEsc: false,
				closeOnClickOutside: false,
				buttons: {
					cancel: 'ยกเลิก',
					confirm: {
						text: 'ยืนยัน',
						// value: "confirm",
						closeModal: false
					}
				}
			}).then(async (phone) => {
				this.setState({ phone });
				if (!congenital_disease) {
					swal({
						text: 'กรุณาระบุโรคประจำตัว หากไม่มีระบุ "-"',
						content: 'input',
						closeOnEsc: false,
						closeOnClickOutside: false,
						buttons: {
							cancel: 'ยกเลิก',
							confirm: {
								text: 'ยืนยัน',
								closeModal: false
							}
						}
					}).then((disease) => {
						this.setState({ congenital_disease: disease });
						if (!allergy) {
							swal({
								text: 'กรุณาระบุยาที่แพ้ หากไม่มีระบุ "-"',
								content: 'input',
								closeOnEsc: false,
								closeOnClickOutside: false,
								buttons: {
									cancel: 'ยกเลิก',
									confirm: {
										text: 'ยืนยัน',
										closeModal: false
									}
								}
							}).then((allergy) => {
								this.setState({ allergy });
								this.UpdateCustomer();
							});
						}
					});
				} else if (!allergy) {
					swal({
						text: 'กรุณาระบุยาที่แพ้ หากไม่มีระบุ "-"',
						content: 'input',
						closeOnEsc: false,
						closeOnClickOutside: false,
						buttons: {
							cancel: 'ยกเลิก',
							confirm: {
								text: 'ยืนยัน',
								closeModal: false
							}
						}
					}).then((allergy) => {
						this.setState({ allergy });
						this.UpdateCustomer();
					});
				} else {
					this.UpdateCustomer();
				}
			});
		} else if (!congenital_disease) {
			swal({
				text: 'กรุณาระบุโรคประจำตัว หากไม่มีระบุ "-"',
				content: 'input',
				closeOnEsc: false,
				closeOnClickOutside: false,
				buttons: {
					cancel: 'ยกเลิก',
					confirm: {
						text: 'ยืนยัน',
						closeModal: false
					}
				}
			}).then((disease) => {
				this.setState({ congenital_disease: disease });
				if (!allergy) {
					swal({
						text: 'กรุณาระบุยาที่แพ้ หากไม่มีระบุ "-"',
						content: 'input',
						closeOnEsc: false,
						closeOnClickOutside: false,
						buttons: {
							cancel: 'ยกเลิก',
							confirm: {
								text: 'ยืนยัน',
								closeModal: false
							}
						}
					}).then((allergy) => {
						this.setState({ allergy });
						this.UpdateCustomer();
					});
				} else {
					this.UpdateCustomer();
				}
			});
		} else if (!allergy) {
			swal({
				text: 'กรุณาระบุยาที่แพ้ หากไม่มีระบุ "-"',
				content: 'input',
				closeOnEsc: false,
				closeOnClickOutside: false,
				buttons: {
					cancel: 'ยกเลิก',
					confirm: {
						text: 'ยืนยัน',
						closeModal: false
					}
				}
			}).then((allergy) => {
				this.setState({ allergy });
				this.UpdateCustomer();
			});
		} else {
			this.UpdateCustomer();
		}
	}

	async UpdateCustomer() {
		let { isCustomer } = this.props;
		let {
			password,
			cardID,
			nameTh,
			nameEn,
			phone,
			dateBirth,
			dateIssue,
			dateExpiry,
			sex,
			address,
			congenital_disease,
			allergy,
			fileData,
			firstnameTh,
			lastnameTh,
			firstnameEn,
			lastnameEn,
			change_data,
			foreigner,
			foreignerID,
			nationality,
			religion,
			prefixTh,
			prefixEn
		} = this.state;

		try {
			let formdata = new FormData();
			formdata.append('user_id', isCustomer);
			formdata.append('password', password);
			formdata.append('role_id', '6');
			formdata.append('foreigner', foreigner);
			formdata.append('en_prefix', prefixEn);
			formdata.append('en_name', firstnameEn);
			formdata.append('en_lastname', lastnameEn);
			formdata.append('th_prefix', prefixTh);
			formdata.append('th_name', firstnameTh);
			formdata.append('th_lastname', lastnameTh);
			if (foreigner !== 1) {
				formdata.append('citizen_id', cardID);
			} else {
				formdata.append('citizen_id', foreignerID);
			}
			formdata.append('birthday', dateBirth);
			formdata.append('sex', sex);
			formdata.append('phone', phone);
			formdata.append('religion', religion);
			formdata.append('nationality', nationality);
			formdata.append('allergy', allergy);
			formdata.append('congenital_disease', congenital_disease);
			formdata.append('issue', dateIssue);
			formdata.append('expire', dateExpiry);
			formdata.append('address', address);
			formdata.append('fileData', fileData);

			// Display the values
			// for (var value of formdata.values()) {
			//     console.log(value);
			// }

			let res = await POST(UPDATE_USER_PROFILE, formdata, true);
			// console.log('res update : ', res)

			if (res.success) {
				swal('สำเร็จ!', 'แก้ไขข้อมูลผู้รับบริการเรียบร้อยแล้ว', 'success', {
					buttons: false,
					timer: 2000
				});
				if (change_data) {
					this.setState({ change_data: false });
				} else {
					this.ConfirmCustomer();
				}
			} else {
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (error) {
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	}

	async ConfirmCustomer() {
		let { isCustomer } = this.props;
		try {
			let res = await GET(COMFIRM_CUSTOMER(isCustomer));
			// console.log('res update : ', res)

			if (res.success) {
				swal('สำเร็จ!', 'ยืนยันตัวตนแล้ว', 'success', {
					buttons: false,
					timer: 2000
				}).then(window.location.reload());
			} else {
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (error) {
			// console.log('error update : ', error)
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	}

	async getCustomerData(id) {
		try {
			let res = await GET(GET_USER_DETAIL(id));
			// console.log('res getCustomerData : ', res);
			let d = res.result[0];
			activateCustomer = d.confirm;
			this.props.setconfirm(d.confirm);
			this.setState(
				{
					hn: d.hn || '',
					member: true,
					activeID: d.confirm,
					cardID: d.citizen_id || '',
					foreigner: d.foreigner,
					prefixTh: d.th_prefix,
					prefixEn: d.en_prefix,
					nameTh: d.th_name + ' ' + d.th_lastname,
					nameEn: d.en_name + ' ' + d.en_lastname,
					phone: d.phone || '',
					dateBirth: d.birthday ? moment(d.birthday.split('T')[0]).add(1, 'd').format('YYYY-MM-DD') : '',
					religion: d.religion,
					nationality: d.nationality,
					dateIssue: d.issue ? d.issue.split('T')[0] : '',
					dateExpiry: d.expire ? d.expire.split('T')[0] : '',
					sex: d.sex === 'หญิง' || d.sex === 'female' ? 'female' : 'male' || '',
					address: d.address || '',
					congenital_disease: d.congenital_disease || '',
					allergy: d.allergy || '',
					file: ip + GET_PICTURE(d.picture || 'null')
				},
				() => {
					let name = this.state.prefixTh + ' ' + this.state.nameTh;
					let hn = this.state.hn;
					this.props.setNameHR(name);
					this.props.setHN(hn);
					// console.log('hn', hn);
					// console.log('file', this.state.file);
				}
			);
			// console.log('pic : ', d.picture)
		} catch (error) {
			// console.log('error getCustomerData : ', error)
		}
	}

	returnState() {
		let { user_id, dateBirth } = this.state;
		return {
			user_id: user_id,
			dateBirth: dateBirth
		};
	}

	CheckLanThaiInput(text) {
		let alpTh = ' กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮะาิีึืุูเะเแะแโะโำไใเาฤฤๅฦฦๅฯั่้๊๋์';
		if (alpTh.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckLanEngInput(text) {
		let alpEng = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if (alpEng.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckForeignerIDInput(text) {
		let alpForeigner = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		if (alpForeigner.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckBothLanInput(text) {
		let alpLan =
			' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZกขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮะาิีึืุูเะเแะแโะโำไใเาฤฤๅฦฦๅ่้๊๋์ฯํ็';
		if (alpLan.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckInput(d) {
		let { foreigner } = this.state;
		let { isCustomer } = this.props;
		// console.log('data signup is : ', d)
		if (foreigner === 0 && d.cardID === '') {
			swal('ผิดพลาด!', 'กรุณากรอกเลขบัตรประจำตัวประชาชน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner === 0 && d.cardID && d.cardID.length != 13) {
			swal('ผิดพลาด!', 'เลขบัตรประจำตัวประชาชนต้องมี 13 หลักเท่านั้น', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner === 1 && d.foreignerID === '') {
			swal('ผิดพลาด!', 'กรุณากรอกเลขหนังสือเดินทาง', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (isCustomer == -1 && (!d.password || d.password === '')) {
			swal('ผิดพลาด!', 'กรุณากรอก password', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (isCustomer == -1 && d.password.length < 6) {
			swal('ผิดพลาด!', 'password ต้องมี 6 ตัวอักษรขึ้นไป', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner !== 1 && (!d.prefixTh || d.prefixTh === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกคำนำหน้าชื่อภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.prefixEn || d.prefixEn === '') {
			swal('ผิดพลาด!', 'กรุณากรอกคำนำหน้าชื่อภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner !== 1 && (!d.nameTh || d.nameTh === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner !== 1 && (d.nameTh.split(' ')[1] === undefined || d.nameTh.split(' ')[1] === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.nameEn || d.nameEn === '') {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.nameEn.split(' ')[1] === undefined || d.nameEn.split(' ')[1] === '') {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.phone || d.phone === '') {
			swal('ผิดพลาด!', 'กรุณากรอกหมายเลขโทรศัพท์', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.dateBirth || d.dateBirth === '') {
			swal('ผิดพลาด!', 'กรุณาเลือกวันเกิด', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.sex || d.sex === '') {
			swal('ผิดพลาด!', 'กรุณาเลือกเพศ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner !== 0 && (!d.nationality || d.nationality === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกสัญชาติ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.address || d.address === '') {
			swal('ผิดพลาด!', 'กรุณากรอกที่อยู่', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.address.length < 10) {
			swal('ผิดพลาด!', 'ที่อยู่สั้นเกินไป กรุณากรอกให้มากกว่า 10 ตัวอักษร', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		return true;
	}

	async SignUp(response) {
		// let organization = this.props.organization;
		let personType = this.props.personType;
		if (personType === true) {
			let {
				cardID,
				password,
				nameTh,
				nameEn,
				phone,
				dateBirth,
				dateIssue,
				dateExpiry,
				sex,
				address,
				congenital_disease,
				allergy,
				fileData,
				firstnameTh,
				lastnameTh,
				firstnameEn,
				lastnameEn,
				foreigner,
				foreignerID,
				nationality,
				religion,
				prefixTh,
				prefixEn
			} = this.state;
			// let status = this.CheckInput({
			// 	cardID,
			// 	foreignerID,
			// 	password,
			// 	nameTh,
			// 	nameEn,
			// 	phone,
			// 	dateBirth,
			// 	dateIssue,
			// 	dateExpiry,
			// 	sex,
			// 	address,
			// 	congenital_disease,
			// 	allergy,
			// 	nationality,
			// 	religion,
			// 	foreigner,
			// 	prefixTh,
			// 	prefixEn
			// });

			// if (!status) return;

			try {
				this.setState({ wait_loading: true });
				let formdata = new FormData();

				if (foreigner !== 1) {
					formdata.append('citizen_id', cardID);
					formdata.append('th_prefix', prefixTh);
					formdata.append('th_name', nameTh.split(' ')[0]);
					formdata.append('th_lastname', nameTh.split(' ')[1]);
					formdata.append('en_prefix', prefixEn);
					formdata.append('en_name', nameEn.split(' ')[0]);
					formdata.append('en_lastname', nameEn.split(' ')[1]);
				} else {
					formdata.append('citizen_id', foreignerID);
					formdata.append('th_prefix', prefixEn);
					formdata.append('th_name', nameEn.split(' ')[0]);
					formdata.append('th_lastname', nameEn.split(' ')[1]);
					formdata.append('en_prefix', prefixEn);
					formdata.append('en_name', nameEn.split(' ')[0]);
					formdata.append('en_lastname', nameEn.split(' ')[1]);
				}

				formdata.append('foreigner', foreigner);
				formdata.append('password', password);
				formdata.append('role_id', '6');
				formdata.append('birthday', dateBirth);
				formdata.append('sex', sex);
				formdata.append('phone', phone);
				formdata.append('allergy', allergy);
				formdata.append('congenital_disease', congenital_disease);
				formdata.append('issue', dateIssue);
				formdata.append('expire', dateExpiry);
				formdata.append('address', address);
				formdata.append('fileData', fileData);
				formdata.append('nationality', nationality);
				formdata.append('religion', religion);

				// for (var value of formdata.values()) {
				// 	console.log(value);
				// }

				let res = await POST(CREATE_USER, formdata, true);

				if (res.success) {
					this.setState({ wait_loading: false });
					this.setState({ isSignedUp: true });
					this.setValueAfterSignUp(res.result);
					swal('สำเร็จ!', 'สมัครสมาชิกสำเร็จ', 'success', {
						buttons: false,
						timer: 2000
					});
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 3000
					});
				}
				response(res);
			} catch (error) {
				this.setState({ wait_loading: false });
				response(null);
				// console.log('error signUp : ', error)
				swal('ผิดพลาด!', 'Network Error', 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} else {
			// alert('asdsad');
			this.setState({ obj: this.props.objs });
			let {
				cardID,
				password,
				nameTh,
				nameEn,
				phone,
				dateBirth,
				dateIssue,
				dateExpiry,
				sex,
				address,
				congenital_disease,
				allergy,
				fileData,
				firstnameTh,
				lastnameTh,
				firstnameEn,
				lastnameEn,
				foreigner,
				foreignerID,
				nationality,
				religion,
				prefixTh,
				prefixEn
			} = this.state;

			let status = this.CheckInput({
				cardID,
				foreignerID,
				password,
				nameTh,
				nameEn,
				phone,
				dateBirth,
				dateIssue,
				dateExpiry,
				sex,
				address,
				congenital_disease,
				allergy,
				nationality,
				religion,
				foreigner,
				prefixTh,
				prefixEn
			});
			if (!status) return;

			if (foreigner !== 1) {
				let index = this.props.index;

				let arry = this.state.organization_list;
				let objs = {
					citizen_id: cardID,
					th_prefix: prefixTh,
					th_name: nameTh.split(' ')[0],
					th_lastname: nameTh.split(' ')[1],
					en_prefix: prefixEn,
					en_name: nameEn.split(' ')[0],
					en_lastname: nameEn.split(' ')[1],
					foreigner: foreigner,
					password: password,
					role_id: 6,
					birthday: dateBirth,
					sex: sex,
					phone: phone,
					allergy: allergy,
					congenital_disease: congenital_disease,
					issue: dateIssue,
					expire: dateExpiry,
					address: address,
					// fileData: fileData,
					nationality: nationality,
					religion: religion
				};
				arry.push(objs);
				this.setState(
					{
						organization_list: arry
					},
					() => {
						this.props.organization_list(this.state.organization_list);
						// console.log('this.state.organization_list', this.state.organization_list);
					}
				);

				// console.log(index);
				let unit = this.props.unit;
				let date = moment().format('DD-MM-YYYY');
				let time = moment().format('HH:mm:ss');
				let frist = nameTh.split(' ')[0];
				let last = nameTh.split(' ')[1];
				let age = moment().diff(dateBirth, 'years');
				let AAA = {
					date: date,
					time: time,
					frist: frist,
					last: last,
					age: age,
					unit: unit,
					index: index,
					key: prefixTh
				};
				let obj = this.state.obj;
				obj.push(AAA);
				this.setState({ obj: obj });
				// console.log('objjjjjjjjjjjjj', obj);
				this.props.obj(this.state.obj);
				this.setState(
					{
						cardID: '',
						password: '',
						nameTh: '',
						nameEn: '',
						phone: '',
						dateBirth: '',
						dateIssue: '',
						dateExpiry: '',
						sex: 'ชาย',
						address: '',
						congenital_disease: '',
						allergy: '',
						fileData: null,
						firstnameTh: '',
						lastnameTh: '',
						firstnameEn: '',
						lastnameEn: '',
						foreigner: 0,
						foreignerID: '',
						nationality: '',
						religion: '',
						prefixTh: '',
						prefixEn: ''
					},
					() => {
						this.resetThai();
					}
				);
			} else {
				let index = this.props.index;

				let arry = this.state.organization_list;
				let objs = {
					citizen_id: foreignerID,
					th_prefix: prefixEn,
					th_name: nameEn.split(' ')[0],
					th_lastname: nameEn.split(' ')[1],
					en_prefix: prefixEn,
					en_name: nameEn.split(' ')[0],
					en_lastname: nameEn.split(' ')[1],
					foreigner: foreigner,
					password: password,
					role_id: 6,
					birthday: dateBirth,
					sex: sex,
					phone: phone,
					allergy: allergy,
					congenital_disease: congenital_disease,
					issue: dateIssue,
					expire: dateExpiry,
					address: address,
					// fileData: fileData,
					nationality: nationality,
					religion: religion
				};
				arry.push(objs);
				this.setState(
					{
						organization_list: arry
					},
					() => {
						this.props.organization_list(this.state.organization_list);
					}
				);

				let unit = this.props.unit;
				let date = moment().format('DD-MM-YYYY');
				let time = moment().format('HH:mm:ss');
				let frist = nameEn.split(' ')[0];
				let last = nameEn.split(' ')[1];
				let age = moment().diff(dateBirth, 'years');
				let AAA = {
					date: date,
					time: time,
					frist: frist,
					last: last,
					age: age,
					unit: unit,
					index: index,
					key: prefixEn
				};
				let obj = this.state.obj;
				obj.push(AAA);
				this.setState({ obj: obj });
				// console.log('objjjjjjjjjjjjj', obj);
				this.props.obj(this.state.obj);
				this.setState(
					{
						cardID: '',
						password: '',
						nameTh: '',
						nameEn: '',
						phone: '',
						dateBirth: '',
						dateIssue: '',
						dateExpiry: '',
						sex: 'ชาย',
						address: '',
						congenital_disease: '',
						allergy: '',
						fileData: null,
						firstnameTh: '',
						lastnameTh: '',
						firstnameEn: '',
						lastnameEn: '',
						foreigner: 0,
						foreignerID: '',
						nationality: '',
						religion: '',
						prefixTh: '',
						prefixEn: ''
					},
					() => {
						this.resetThai();
					}
				);
			}
		}
	}

	setValueAfterSignUp(v) {
		// console.log('v is ', GET_PICTURE(v.picture || null));
		this.props.setconfirm(v.confirm);
		this.setState(
			{
				hn: v.hn,
				user_id: v.user_id,
				// display things
				cardID: v.citizen_id,
				prefixTh: v.th_prefix,
				prefixEn: v.en_prefix,
				nameTh: v.th_name + ' ' + v.th_lastname,
				nameEn: v.en_name + ' ' + v.en_lastname,
				phone: v.phone,
				dateBirth: v.birthday,
				dateIssue: v.issue,
				dateExpiry: v.expire,
				sex: v.sex,
				address: v.address,
				congenital_disease: v.congenital_disease,
				allergy: v.allergy,
				file: ip + GET_PICTURE(v.picture || 'null')
			},
			() => {
				let name = v.th_prefix + ' ' + v.th_name + ' ' + v.th_lastname;
				let hn = v.hn;
				this.props.setNameHR(name);
				this.props.setHN(hn);
			}
		);
	}

	handleChange = (event) => {
		// uplaod pic
		let { fileData, file } = this.state;
		// console.log(event.target.files[0])
		if ((fileData !== null || file !== null) && event.target.files[0] === undefined) {
			this.setState({
				fileData: null,
				file: null
			});
		} else if ((fileData !== null || file !== null) && event.target.files[0] !== undefined) {
			this.setState({
				fileData: event.target.files[0],
				file: URL.createObjectURL(event.target.files[0])
			});
		} else {
			this.setState({
				fileData: event.target.files[0],
				file: URL.createObjectURL(event.target.files[0])
			});
		}
	};

	Upfirst = (str = '', i = 0) => {
		if (str.charAt(i) != ' ' && i == 0) {
			return this.Upfirst(str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
		} else if (str.charAt(i - 1) == ' ') {
			if (str.charAt(i) == ' ') {
				return this.Upfirst(str.slice(0, i), i);
			} else {
				return this.Upfirst(str.slice(0, i) + str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
			}
		} else if (str.length > i) {
			return this.Upfirst(str, i + 1);
		} else {
			return str;
		}
	};

	_onChange = (e) => {
		if (this.CheckLanEngInput(e.target.value.charAt(e.target.value.length - 1))) {
			this.setState({ nameEn: this.Upfirst(e.target.value) });
		}
	};

	resetThai = () => {
		this.setState({
			foreigner: 0,
			cardID: '',
			foreignerID: '',
			password: '',
			prefixTh: '',
			prefixEn: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'male',
			address: '',
			file: null,
			fileData: null,
			nationality: '',
			religion: '',
			dumpCardData: [],
			isSignedUp: false,
			isCustomer: -1,
			congenital_disease: '',
			allergy: ''
		});
	};

	resetForeigner = () => {
		this.setState({
			foreigner: 1,
			cardID: '',
			foreignerID: '',
			password: '',
			prefixTh: '',
			prefixEn: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'male',
			address: '',
			file: null,
			fileData: null,
			nationality: '',
			religion: '',
			dumpCardData: [],
			isSignedUp: false
		});
	};

	render() {
		let {
			activeID,
			member,
			cardID,
			nameTh,
			nameEn,
			phone,
			password,
			file,
			isSignedUp,
			address,
			allergy,
			congenital_disease,
			dateIssue,
			dateExpiry,
			dateBirth,
			sex,
			loading,
			reading_card,
			change_data,
			foreigner,
			foreignerID,
			nationality,
			religion,
			prefixTh,
			prefixEn,
			organization_list,
			hn,
			wait_loading
		} = this.state;
		let { GetResponseSignUp, isCustomer, personType } = this.props;
		// console.log('Or:', organization_list);
		// console.log('isSignedUp', isSignedUp);
		return (
			<div>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : reading_card ? (
					<div
						style={{
							width: '100%',
							height: '50vh',
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
							justifyContent: 'center'
						}}
					>
						<div className="label-back">กำลังอ่านข้อมูลจากบัตรประจำตัวประชาชน</div>
						<ReactLoading type={'bars'} color={Color.Blue} height={'auto'} width={'5vw'} />
					</div>
				) : (
					<div className="col-md mx-auto py-4 bg-white rounded">
						<div style={{ display: 'flex', justifyContent: 'space-between' }}>
							<div>
								<h4 style={{ color: Color.Blue }}>
									{member ? 'ข้อมูลผู้รับบริการ' : 'เพิ่มผู้รับบริการ'}
								</h4>
								<h5 style={{ color: activeID === 1 ? Color.Green : activeID === 0 ? 'red' : null }}>
									{activeID === 1 ? '* ยืนยันตัวตนแล้ว' : activeID === 0 ? '* รอยืนยันตัวตน' : null}
								</h5>
							</div>
							{hn !== '' && (
								<Barcode
									value={`HN${hn}`}
									width={0.8}
									height={25}
									fontSize={12}
									background={'transparent'}
								/>
							)}
						</div>
						<Form>
							<Default>
								<Row>
									{isCustomer == -1 &&
									!isSignedUp && (
										<Col>
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<label check htmlFor="cardID" className="label-user-detail">
															<input
																type="radio"
																name="radio2"
																className="mr-2"
																value={foreigner}
																checked={foreigner !== 1 ? true : false}
																onChange={this.resetThai}
																// this.handleChange}
															/>
															เลขประจำตัวประชาชน
														</label>
														{/* {foreigner !== 1 ? (
															<Label
																htmlFor="foreigner"
																style={{ color: 'red', marginLeft: '0.25vw' }}
															>
																*
															</Label>
														) : null} */}
													</div>
													<Input
														id="cardID"
														type="number"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															if (e.target.value.length <= 13)
																this.setState({ cardID: '' + e.target.value });
														}}
														value={!cardID ? '' : cardID}
														disabled={foreigner !== 1 ? false : true}
													/>
												</FormGroup>
											</Row>
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div>
														<input
															type="radio"
															name="radio2"
															className="mr-2"
															value={foreigner}
															checked={foreigner === 1 ? true : false}
															onChange={this.resetForeigner}
															// this.handleChange}
														/>
														<label htmlFor="foreignerID" className="label-user-detail">
															เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
															{/* {foreigner !== 0 ? (
																<Label style={{ color: 'red', marginLeft: '0.25vw' }}>
																	*
																</Label>
															) : null} */}
														</label>
													</div>
													<Input
														id="foreignerID"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															if (
																this.CheckForeignerIDInput(
																	e.target.value.charAt(e.target.value.length - 1)
																)
															) {
																this.setState({
																	foreignerID: e.target.value.toUpperCase()
																});
															}
														}}
														readOnly={foreigner !== 1 ? true : false}
														value={!foreignerID ? '' : foreignerID}
													/>
												</FormGroup>
											</Row>

											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label htmlFor="password" className="label-user-detail">
															รหัสผ่าน
														</Label>
														{/* <Label
															htmlFor="password"
															style={{ color: 'red', marginLeft: '0.25vw' }}
														>
															*
														</Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>Password</FormText>
													<Input
														id="password"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															this.setState({ password: e.target.value });
														}}
														readOnly={activeID !== 0 ? isSignedUp : false}
														value={!password ? '' : password}
													/>
												</FormGroup>
											</Row>
										</Col>
									)}
									{isCustomer > 0 &&
									!isSignedUp && (
										<Col>
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<label check htmlFor="cardID" className="label-user-detail">
															<input
																type="radio"
																name="radio2"
																className="mr-2"
																value={foreigner}
																checked={foreigner !== 1 ? true : false}
																onChange={this.resetThai}
																// this.handleChange}
															/>
															เลขประจำตัวประชาชน
														</label>
														{/* {foreigner !== 1 ? (
															<Label
																htmlFor="foreigner"
																style={{ color: 'red', marginLeft: '0.25vw' }}
															>
																*
															</Label>
														) : null} */}
													</div>
													{/* <FormText style={{ marginTop: -10 }}>Identification Number</FormText> */}
													{/* <Label htmlFor="cardID">เลขประจำตัวประชาชน</Label> */}
													<Input
														id="cardID"
														type="number"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															if (e.target.value.length <= 13)
																this.setState({ cardID: '' + e.target.value });
														}}
														value={!cardID ? '' : cardID}
														disabled={foreigner !== 1 ? false : true}
													/>
												</FormGroup>
											</Row>
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div>
														<input
															type="radio"
															name="radio2"
															className="mr-2"
															value={foreigner}
															checked={foreigner === 1 ? true : false}
															onChange={this.resetForeigner}
															// this.handleChange}
														/>
														<label htmlFor="foreignerID" className="label-user-detail">
															เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
															{/* {foreigner !== 0 ? (
																<Label style={{ color: 'red', marginLeft: '0.25vw' }}>
																	*
																</Label>
															) : null} */}
														</label>
													</div>
													<Input
														id="foreignerID"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															if (
																this.CheckForeignerIDInput(
																	e.target.value.charAt(e.target.value.length - 1)
																)
															) {
																this.setState({
																	foreignerID: e.target.value.toUpperCase()
																});
															}
														}}
														readOnly={foreigner !== 1 ? true : false}
														value={!foreignerID ? '' : foreignerID}
													/>
												</FormGroup>
											</Row>

											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label htmlFor="password" className="label-user-detail">
															รหัสผ่าน
														</Label>
														{/* <Label
															htmlFor="password"
															style={{ color: 'red', marginLeft: '0.25vw' }}
														>
															*
														</Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>Password</FormText>
													<Input
														id="password"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															this.setState({ password: e.target.value });
														}}
														readOnly={activeID !== 0 ? isSignedUp : false}
														value={!password ? '' : password}
													/>
												</FormGroup>
											</Row>
										</Col>
									)}
									<Col
										className={
											isCustomer == -1 && !isSignedUp ? (
												'text-right'
											) : isSignedUp == false ? (
												'text-right'
											) : (
												'text-center'
											)
										}
									>
										{/* upload pic */}
										<Label
											style={{
												cursor: 'pointer',
												position: 'relative',
												height: '14rem',
												width: '14rem'
											}}
											htmlFor="fileupload"
										>
											<Input
												style={{ position: 'absolute', display: 'none', top: 0, left: 0 }}
												id="fileupload"
												type="file"
												accept=".png, .jpg, .jpeg"
												onChange={(event) => this.handleChange(event)}
												disabled={activeID !== 0 ? isSignedUp : false}
											/>
											{file !== '' && file ? (
												<div
													style={{
														cursor: 'pointer',
														width: '100%',
														height: '100%',
														backgroundImage: `url(${file})`,
														backgroundRepeat: 'no-repeat',
														backgroundPosition: 'center',
														borderRadius: 10,
														backgroundSize: '100% auto'
													}}
												/>
											) : (
												<div
													style={{
														width: '100%',
														color: '#fff',
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
														height: '100%',
														backgroundColor: '#ccc',
														borderRadius: 10
													}}
												>
													ไม่มีรูป
												</div>
											)}
										</Label>
									</Col>
								</Row>

								{isCustomer == -1 &&
								isSignedUp &&
								foreigner !== 1 && (
									<Row>
										<Col className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="cardID" className="label-user-detail">
														เลขประจำตัวประชาชน
													</Label>
													{/* <Label
														htmlFor="cardID"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Identification Number</FormText>
												<Input
													id="cardID"
													type="number"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (e.target.value.length <= 13)
															this.setState({ cardID: '' + e.target.value });
													}}
													value={!cardID ? '' : cardID}
													disabled={foreigner !== 1 ? false : true}
												/>
											</FormGroup>
										</Col>
										<Col className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="password" className="label-user-detail">
														รหัสผ่าน
													</Label>
													{/* <Label
														htmlFor="password"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="password"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ password: e.target.value });
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}
								{isCustomer == -1 &&
								isSignedUp &&
								foreigner !== 0 && (
									<Row>
										<Col className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="foreignerID" className="label-user-detail">
														เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
													</Label>
													{/* <Label
														htmlFor="foreignerID"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Passport Number</FormText>
												<Input
													id="foreignerID"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckForeignerIDInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({
																foreignerID: e.target.value.toUpperCase()
															});
														}
													}}
													readOnly={foreigner !== 1 ? true : false}
													value={!foreignerID ? '' : foreignerID}
												/>
											</FormGroup>
										</Col>
										<Col className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="password" className="label-user-detail">
														รหัสผ่าน
													</Label>
													{/* <Label
														htmlFor="password"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="password"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ password: e.target.value });
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}

								{foreigner !== 1 ? (
									<Row>
										<Col sm={3}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixTh" className="label-user-detail">
														คำนำหน้า
													</Label>
													{/* <Label
														htmlFor="prefixTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
												<Input
													id="prefixTh"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixTh: e.target.value });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixTh ? '' : prefixTh}
												/>
											</FormGroup>
										</Col>

										<Col sm={9}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nameTh" className="label-user-detail">
														ชื่อ - นามสกุล
													</Label>
													{/* <Label
														htmlFor="nameTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Th)</FormText>
												<Input
													id="nameTh"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameTh: e.target.value });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameTh ? '' : nameTh}
												/>
											</FormGroup>
										</Col>
									</Row>
								) : (
									<Row>
										<Col sm={3}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixEn" className="label-user-detail">
														Prefix
													</Label>
													{/* <Label
														htmlFor="prefixEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
												<Input
													id="prefixEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixEn ? '' : prefixEn}
												/>
											</FormGroup>
										</Col>

										<Col sm={9}>
											<FormGroup>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label
														className="text-muted"
														htmlFor="nameEn"
														className="label-user-detail"
													>
														Name - Last name
													</Label>
													{/* <Label
														htmlFor="nameEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
												<Input
													id="nameEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameEn ? '' : nameEn}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}

								{foreigner !== 1 ? (
									<Row>
										<Col sm={3}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixEn" className="label-user-detail">
														Prefix
													</Label>
													{/* <Label
														htmlFor="prefixEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
												<Input
													id="prefixEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixEn ? '' : prefixEn}
												/>
											</FormGroup>
										</Col>

										<Col sm={9}>
											<FormGroup>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label
														className="text-muted"
														htmlFor="nameEn"
														className="label-user-detail"
													>
														Name - Last name
													</Label>
													{/* <Label
														htmlFor="nameEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
												<Input
													id="nameEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={this._onChange}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameEn ? '' : nameEn}
												/>
											</FormGroup>
										</Col>
									</Row>
								) : null}
							</Default>
							<Mobile>
								<Row className="text-center">
									{/* upload pic mobile */}
									<Col>
										<Label
											className="text-center"
											style={{
												cursor: 'pointer',
												position: 'relative',
												height: '14rem',
												width: '14rem'
											}}
											htmlFor="fileupload"
										>
											<Input
												style={{ position: 'absolute', display: 'none', top: 0, left: 0 }}
												id="fileupload"
												type="file"
												accept=".png, .jpg, .jpeg"
												onChange={(event) => this.handleChange(event)}
												disabled={activeID !== 0 ? isSignedUp : false}
											/>
											{file && file !== '' ? (
												<div
													style={{
														width: '100%',
														height: '100%',
														backgroundImage: `url(${file})`,
														backgroundRepeat: 'no-repeat',
														backgroundPosition: 'center',
														borderRadius: 10,
														backgroundSize: '100% auto'
													}}
												/>
											) : (
												<div
													style={{
														color: '#fff',
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
														width: '100%',
														height: '100%',
														backgroundColor: '#ccc',
														borderRadius: 10
													}}
												>
													ไม่มีรูป
												</div>
											)}
										</Label>
									</Col>
								</Row>
								{isCustomer == -1 &&
								!isSignedUp && (
									<div>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<label check htmlFor="cardID" className="label-user-detail">
														<input
															type="radio"
															name="radio2"
															className="mr-2"
															value={foreigner}
															checked={foreigner !== 1 ? true : false}
															onChange={this.resetThai}
															// this.handleChange}
														/>
														เลขประจำตัวประชาชน
													</label>
													{/* {foreigner !== 1 ? (
														<Label
															htmlFor="foreigner"
															style={{ color: 'red', marginLeft: '0.5vw' }}
														>
															*
														</Label>
													) : null} */}
												</div>
												<Input
													id="cardID"
													type="number"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (e.target.value.length <= 13)
															this.setState({ cardID: '' + e.target.value });
													}}
													value={!cardID ? '' : cardID}
													disabled={foreigner !== 1 ? false : true}
												/>
											</FormGroup>
										</Row>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<label check htmlFor="cardID" className="label-user-detail">
														<input
															type="radio"
															name="radio2"
															className="mr-2"
															value={foreigner}
															checked={foreigner === 1 ? true : false}
															onChange={this.resetForeigner}
															// this.handleChange}
														/>
														เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
													</label>
													{/* {foreigner !== 0 ? (
														<Label
															htmlFor="foreigner"
															style={{ color: 'red', marginLeft: '0.25vw' }}
														>
															*
														</Label>
													) : null} */}
												</div>
												<Input
													id="foreignerID"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckForeignerIDInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({
																foreignerID: e.target.value.toUpperCase()
															});
														}
													}}
													readOnly={foreigner !== 1 ? true : false}
													value={!foreignerID ? '' : foreignerID}
												/>
											</FormGroup>
										</Row>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="password" className="label-user-detail">
														รหัสผ่าน
													</Label>
													{/* <Label
														htmlFor="password"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="password"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ password: e.target.value });
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Row>
									</div>
								)}
								{foreigner !== 1 ? (
									<div>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixTh" className="label-user-detail">
														คำนำหน้า
													</Label>
													{/* <Label
														htmlFor="prefixTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
												<Input
													id="prefixTh"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixTh: e.target.value });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixTh ? '' : prefixTh}
												/>
											</FormGroup>
										</Row>

										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nameTh" className="label-user-detail">
														ชื่อ - นามสกุล
													</Label>
													{/* <Label
														htmlFor="nameTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Th)</FormText>
												<Input
													id="nameTh"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameTh: e.target.value });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameTh ? '' : nameTh}
												/>
											</FormGroup>
										</Row>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixEn" className="label-user-detail">
														Prefix
													</Label>
													{/* <Label
														htmlFor="prefixEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
												<Input
													id="prefixEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixEn ? '' : prefixEn}
												/>
											</FormGroup>
										</Row>

										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label
														className="text-muted"
														htmlFor="nameEn"
														className="label-user-detail"
													>
														Name - Last name
													</Label>
													{/* <Label
														htmlFor="nameEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
												<Input
													id="nameEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameEn ? '' : nameEn}
												/>
											</FormGroup>
										</Row>
									</div>
								) : (
									<div>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixEn" className="label-user-detail">
														Prefix
													</Label>
													{/* <Label
														htmlFor="prefixEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
												<Input
													id="prefixEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!prefixEn ? '' : prefixEn}
												/>
											</FormGroup>
										</Row>
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label
														className="text-muted"
														htmlFor="nameEn"
														className="label-user-detail"
													>
														Name - Last name
													</Label>
													{/* <Label
														htmlFor="nameEn"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
												<Input
													id="nameEn"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameEn: this.Upfirst(e.target.value) });
														}
													}}
													readOnly={activeID !== 0 ? isSignedUp : false}
													value={!nameEn ? '' : nameEn}
												/>
											</FormGroup>
										</Row>
									</div>
								)}
							</Mobile>
							{/* -------- bottom -------- */}
							<Row>
								{isCustomer !== -1 &&
								isSignedUp && (
									<Col sm>
										<FormGroup style={{ width: '100%' }}>
											<Label htmlFor="cardID" className="label-user-detail">
												เลขประจำตัวประชาชน
											</Label>
											<FormText style={{ marginTop: -10 }}>Identification Number</FormText>
											<Input
												id="cardID"
												type="number"
												className="input-user-detail"
												style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
												onChange={(e) => {
													if (e.target.value.length <= 13)
														this.setState({ cardID: '' + e.target.value });
												}}
												readOnly={activeID !== 0 ? isSignedUp : false}
												value={!cardID ? '' : cardID}
											/>
										</FormGroup>
									</Col>
								)}

								<Col sm>
									<FormGroup>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="phone" className="label-user-detail">
												เบอร์โทรศัพท์
											</Label>
											{/* <Label htmlFor="phone" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Phone Number</FormText>
										<Input
											id="phone"
											type="number"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
											onChange={(e) => {
												if (e.target.value.length <= 10)
													this.setState({ phone: '' + e.target.value });
											}}
											readOnly={activeID !== 0 && change_data === false ? isSignedUp : false}
											// readOnly={activeID !== 0 ? isSignedUp : change_data ? false : false}
											value={!phone ? '' : phone}
										/>
									</FormGroup>
								</Col>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="dateBirth" className="label-user-detail">
												เกิดวันที่
											</Label>
											{/* <Label htmlFor="dateBirth" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Date of birth</FormText>
										<Input
											type="date"
											id="dateBirth"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ dateBirth: e.target.value });
											}}
											readOnly={activeID !== 0 ? isSignedUp : false}
											value={!dateBirth ? '' : dateBirth}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="sex" className="label-user-detail">
												เพศ
											</Label>
											{/* <Label htmlFor="sex" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Sex</FormText>
										<Input
											id="sex"
											type="select"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ sex: e.target.value });
											}}
											disabled={activeID !== 0 ? isSignedUp : false}
											value={!sex ? '' : sex}
										>
											<option value="male">ชาย</option>
											<option value="female">หญิง</option>
										</Input>
									</FormGroup>
								</Col>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										{foreigner !== 0 ? (
											<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nationality" className="label-user-detail">
														สัญชาติ
													</Label>
													{/* <Label
														htmlFor="nationality"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Nationality</FormText>
											</div>
										) : (
											<div>
												<Label htmlFor="nationality" className="label-user-detail">
													สัญชาติ
												</Label>
												<FormText style={{ marginTop: -10 }}>Nationality</FormText>
											</div>
										)}
										<Input
											id="nationality"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
											onChange={(e) => {
												if (
													this.CheckLanEngInput(
														e.target.value.charAt(e.target.value.length - 1)
													)
												) {
													this.setState({ nationality: this.Upfirst(e.target.value) });
												}
											}}
											readOnly={activeID !== 0 ? isSignedUp : false}
											value={foreigner !== 1 ? 'Thai' : !nationality ? '' : nationality}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<Label htmlFor="religion" className="label-user-detail">
											ศาสนา
										</Label>
										<FormText style={{ marginTop: -10 }}>Religion</FormText>
										<Input
											id="religion"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
											onChange={(e) => {
												if (
													this.CheckBothLanInput(
														e.target.value.charAt(e.target.value.length - 1)
													)
												) {
													this.setState({ religion: this.Upfirst(e.target.value) });
												}
											}}
											readOnly={activeID !== 0 ? isSignedUp : false}
											value={!religion ? '' : religion}
										/>
									</FormGroup>
								</Col>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										<Label htmlFor="dateIssue" className="label-user-detail">
											วันออกบัตร
										</Label>
										<FormText style={{ marginTop: -10 }}>Date of issue</FormText>
										<Input
											type="date"
											id="dateIssue"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ dateIssue: e.target.value });
											}}
											readOnly={activeID !== 0 ? isSignedUp : false}
											value={!dateIssue ? '' : dateIssue}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<Label htmlFor="dateExpiry" className="label-user-detail">
											วันหมดอายุบัตร
										</Label>
										<FormText style={{ marginTop: -10 }}>Date of Expiry</FormText>
										<Input
											type="date"
											id="dateExpiry"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ dateExpiry: e.target.value });
											}}
											readOnly={activeID !== 0 ? isSignedUp : false}
											value={!dateExpiry ? '' : dateExpiry}
										/>
									</FormGroup>
								</Col>
							</Row>
							<FormGroup
								style={{
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									marginBottom: '1rem'
								}}
							>
								<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
									<div style={{ display: 'flex', flexDirection: 'row' }}>
										<Label htmlFor="address" className="label-user-detail">
											ที่อยู่
										</Label>
										{/* <Label htmlFor="address" style={{ color: 'red', marginLeft: '0.25vw' }}>
											*
										</Label> */}
									</div>
									<FormText style={{ marginTop: -10 }}>Address</FormText>
								</div>
								<Col style={{ width: '70%', paddingRight: 0 }}>
									<Input
										type="textarea"
										id="address"
										className="input-user-detail"
										style={{ fontSize: '1.3rem' }}
										onChange={(e) => {
											this.setState({ address: this.Upfirst(e.target.value) });
										}}
										readOnly={activeID !== 0 && change_data === false ? isSignedUp : false}
										value={!address ? '' : address}
									/>
								</Col>
							</FormGroup>
							<FormGroup
								style={{
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									marginBottom: '1rem'
								}}
							>
								<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
									<Label htmlFor="congenital" className="label-user-detail">
										โรคประจำตัว
									</Label>
									<FormText style={{ marginTop: -10 }}>Congenital Disease</FormText>
								</div>
								<Col style={{ width: '70%', paddingRight: 0 }}>
									<Input
										type="textarea"
										id="congenital"
										className="input-user-detail"
										style={{ fontSize: '1.3rem' }}
										onChange={(e) => {
											this.setState({ congenital_disease: e.target.value });
										}}
										readOnly={activeID !== 0 && change_data === false ? isSignedUp : false}
										value={!congenital_disease ? '' : congenital_disease}
									/>
								</Col>
							</FormGroup>
							<FormGroup
								style={{
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									marginBottom: '1rem'
								}}
							>
								<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
									<Label htmlFor="allergy" className="label-user-detail">
										ยาที่แพ้
									</Label>
									<FormText style={{ marginTop: -10 }}>Drug Allergy</FormText>
								</div>
								<Col style={{ width: '70%', paddingRight: 0 }}>
									<Input
										type="textarea"
										id="allergy"
										className="input-user-detail"
										style={{ fontSize: '1.3rem' }}
										onChange={(e) => {
											this.setState({ allergy: e.target.value });
										}}
										readOnly={activeID !== 0 && change_data === false ? isSignedUp : false}
										value={!allergy ? '' : allergy}
									/>
								</Col>
							</FormGroup>
							{!isSignedUp && (
								<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
									{personType === true ? null : (
										<Button_Excel
											unit={this.props.unit}
											obj={(e) => this.props.obj(e)}
											organization_list={(e) => this.props.organization_list(e)}
										/>
									)}
									<button
										type="button"
										onClick={() => this.SignUp(GetResponseSignUp)}
										className="btn-form"
									>
										+ เพิ่มผู้รับบริการ
									</button>
								</div>
							)}

							{isSignedUp && (
								<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
									{/* {personType === true ? ( */}
									<button type="button" onClick={this.resetThai} className="btn-form">
										ล้างข้อมูล
									</button>
									{/* ) : (
										<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
											<Button_Excel
												unit={this.props.unit}
												obj={(e) => this.props.obj(e)}
												organization_list={(e) => this.props.organization_list(e)}
											/>
											<button
												type="button"
												onClick={() => this.SignUp(GetResponseSignUp)}
												className="btn-form"
											>
												+ เพิ่มผู้รับบริการ
											</button>
										</div>
									)} */}
								</div>
							)}
						</Form>
					</div>
				)}
				<Modal
					isOpen={wait_loading}
					// onRequestClose={() => this.setState({ wait_loading: false })}
					style={customStyles}
				>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
export default withRouter(Col_right);
