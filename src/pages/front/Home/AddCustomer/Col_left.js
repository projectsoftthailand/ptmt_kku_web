import React, { Component } from 'react';
import Table_1 from '../../../../components/Table_1';
import { Table, Input } from 'reactstrap';
import swal from 'sweetalert';
import fn from '../../../../function/function';
import Responsive from 'react-responsive';
import './style/Addcustomer.css';

let Min = (props) => <Responsive {...props} minWidth={768} />;
let Max = (props) => <Responsive {...props} maxWidth={767} />;
let InputMin = (props) => <Responsive {...props} minWidth={550} />;
let InputMax = (props) => <Responsive {...props} maxWidth={549} />;

export default class Col_left extends Component {
	constructor(props) {
		super(props);

		this.state = {
			package: [],
			t_name: '',
			price_t: '',
			code_t: '',
			newPackage: [],
			priceAll: 0,
			i: 0,
			role: '',
			amount: 1
		};
	}
	async componentWillMount() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		let { _package } = this.props;
		this.setState({
			role: USER.role
		});

		await setTimeout(async () => {
			let state = await JSON.parse(localStorage.getItem('stateCol_left'));
			if (state) {
				if (state.list_id == this.props.list_id && this.props.save == false) {
					await this.setState(state);
				} else {
					if (_package.length > 1) {
						// console.log('_package', _package);
						let found = false;
						_package.forEach((el) => {
							if (el.name === 'กำหนดเอง') {
								found = true;
							}
						});
						if (!found) {
							_package.unshift({ id: 0, name: 'กำหนดเอง', treatment: [], type: 'mt' });
						}
						this.setState({ package: _package });
					}
					if (_package.length == 0) {
						// console.log('_package', _package);
					}
				}
			} else {
				if (_package.length > 1) {
					// console.log('_package', _package);
					let found = false;
					_package.forEach((el) => {
						if (el.name === 'กำหนดเอง') {
							found = true;
						}
					});
					if (!found) {
						_package.unshift({ id: 0, name: 'กำหนดเอง', treatment: [], type: 'mt' });
					}
					this.setState({ package: _package });
				}
				if (_package.length == 0) {
					// console.log('_package', _package);
				}
				this.state['list_id'] = this.props.list_id;
				localStorage.setItem('stateCol_left', JSON.stringify(this.state));
			}
		}, 0.01);
	}
	componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		localStorage.setItem('stateCol_left', JSON.stringify(this.state));
	}
	del_objNewPackage = (i) => {
		let { newPackage } = this.state;
		newPackage.splice(i, 1);
		this.setState({ newPackage: newPackage }, () => {
			let { newPackage } = this.state;
			let priceAll = newPackage.reduce(fn.add('cost'), 0);
			this.props.priceAll(priceAll);
			this.props.newPackage(newPackage);
		});
	};

	setNewTreatment = (e) => {
		this.props.setNewTreatment(e);
		// console.log('setNewTreatment', e);
	};
	settreatmentData = (e, index) => {
		this.props.settreatmentData(e);
		// console.log('settreatmentData', e);
	};
	render() {
		let { newPackage, i, role, code_t, amount } = this.state;
		let {
			treatment,
			setPackage,
			curServiceType,
			treatmentData,
			payType,
			searchtext,
			oldPackage,
			delOldpackage
		} = this.props;
		return (
			<div
				className="bg-white rounded mb-4"
				style={curServiceType == 'mt' ? { fontSize: 12, marginTop: 10 } : { fontSize: 12 }}
			>
				<div className="row mx-4 pt-3 pb-2">
					{curServiceType == 'mt' ? (
						this.state.package.map(
							(e, index) =>
								e.type === curServiceType && (
									<div key={'' + (index + 1)} className="fix-col">
										<label htmlFor={'i' + (index + 1)} key={'' + (index + 1)} className="mr-3">
											<input
												onClick={() => {
													setPackage(index);
													this.setState({ i: index });
												}}
												name="p-type"
												id={'i' + (index + 1)}
												className="mr-2"
												type="radio"
												checked={i == index}
											/>
											{e.name}
										</label>
									</div>
								)
						)
					) : (
						<div style={{ color: 'rgb(70, 122, 200)', fontSize: 'medium' }}>รายการรักษา</div>
					)}
				</div>
				<div className="col-xs-12 mb-2">
					<Input type="text" placeholder="ค้นหารายการตรวจ..." onChange={(e) => searchtext(e)} />
				</div>
				<Table_1
					rows={treatment}
					setNewTreatment={(e) => this.setNewTreatment(e)}
					treatmentData={treatmentData}
					settreatmentData={(e) => this.settreatmentData(e)}
					payTypes={payType}
					role={role}
				/>
				<Table responsive>
					<thead>
						<tr>
							<th style={{ borderBottomWidth: 0, backgroundColor: 'white', whiteSpace: 'pre' }}>
								อื่น ๆ
							</th>
							<th style={{ borderBottomWidth: 0, backgroundColor: 'white', whiteSpace: 'pre' }}>{''}</th>
						</tr>
					</thead>
					{(role == 'mt' || role == 'frontMT') && (
						<tbody>
							{oldPackage.length > 0 &&
								oldPackage.map((e, index) => {
									return (
										<tr>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0
												}}
											>
												{e.treatment_name}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													textAlign: 'center'
												}}
											>
												{e.cgd_code}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '17%',
													textAlign: 'right'
												}}
											>
												{Number(e.cost).toLocaleString()}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '9%'
												}}
											>
												<button
													onClick={() => delOldpackage(index)}
													style={{
														color: 'white',
														borderRadius: '3px',
														backgroundColor: 'red',
														borderColor: 'red'
													}}
												>
													ลบ
												</button>
											</td>
										</tr>
									);
								})}
							{newPackage.length > 0 &&
								newPackage.map((e, index) => {
									return (
										<tr>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0
												}}
											>
												{e.treatment_name}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													textAlign: 'center'
												}}
											>
												{e.cgd_code}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '17%',
													textAlign: 'right'
												}}
											>
												{Number(e.cost).toLocaleString()}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '9%'
												}}
											>
												<button
													onClick={() => this.del_objNewPackage(index)}
													style={{
														color: 'white',
														borderRadius: '3px',
														backgroundColor: 'red',
														borderColor: 'red'
													}}
												>
													ลบ
												</button>
											</td>
										</tr>
									);
								})}
							<tr>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0
									}}
								>
									<input
										value={this.state.t_name}
										type="text"
										placeholder="ชื่อรายการ..."
										style={{
											width: '100%',
											paddingLeft: '5px',
											borderRadius: '3px',
											borderWidth: '0.5px',
											outline: 'none'
										}}
										onChange={(e) => this.setState({ t_name: e.target.value })}
									/>
								</td>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0
									}}
								>
									<input
										value={code_t}
										type="number"
										placeholder="เลขที่กรมบัญชีกลาง..."
										style={{
											width: '100%',
											paddingLeft: '5px',
											borderRadius: '3px',
											borderWidth: '0.5px',
											outline: 'none'
										}}
										onChange={(e) => this.setState({ code_t: e.target.value })}
									/>
								</td>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0,
										width: '17%'
									}}
								>
									<input
										value={this.state.price_t}
										type="number"
										placeholder="ราคา..."
										style={{
											width: '100%',
											borderRadius: '3px',
											borderWidth: '0.5px',
											outline: 'none',
											textAlign: 'right',
											paddingRight: '5px'
										}}
										onChange={(e) => this.setState({ price_t: e.target.value })}
									/>
								</td>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0,
										width: '9%'
									}}
								>
									<button
										style={{
											color: 'white',
											borderRadius: '3px',
											backgroundColor: '#467ac8',
											borderColor: '#467ac8'
										}}
										onClick={this.AddMT}
									>
										เพิ่ม
									</button>
								</td>
							</tr>
						</tbody>
					)}
					{(role == 'pt' || role == 'frontPT') && (
						<tbody>
							{oldPackage.length > 0 &&
								oldPackage.map((e, index) => {
									return (
										<tr>
											<td>{e.treatment_name}</td>
											<td
												style={{
													textAlign: 'center'
												}}
											>
												{e.cgd_code}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '17%'
												}}
											>
												<div className="disable-input d-flex">
													<div>
														<Input
															value={e.cost / e.amount}
															type="number"
															placeholder="ราคา"
															disabled
															style={{
																width: '5rem',
																// borderRadius: '3px',
																// borderWidth: '0.5px',
																// outline: 'none',
																textAlign: 'right'
															}}
														/>
													</div>
													<div className="d-flex">
														<button className="btn-minus" style={{ color: '#ced4da' }}>
															-
														</button>
														<input
															value={e.amount}
															type="number"
															disabled
															className="width-input-amount"
														/>
														<button className="btn-plus" style={{ color: '#ced4da' }}>
															+
														</button>
													</div>
													<div>
														<Input
															value={e.cost}
															type="number"
															disabled
															style={{
																width: '5rem',
																// borderRadius: '3px',
																// borderWidth: '0.5px',
																// outline: 'none',
																textAlign: 'right'
															}}
															className="width-input"
														/>
													</div>
												</div>
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '9%'
												}}
											>
												<button
													onClick={() => delOldpackage(index)}
													style={{
														color: 'white',
														borderRadius: '3px',
														backgroundColor: 'red',
														borderColor: 'red'
													}}
												>
													<Min>ลบ</Min>
													<Max>x</Max>
												</button>
											</td>
										</tr>
									);
								})}
							{newPackage.length > 0 &&
								newPackage.map((e, index) => {
									return (
										<tr>
											<td>{e.treatment_name}</td>
											<td
												style={{
													textAlign: 'center'
												}}
											>
												{e.cgd_code}
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '17%'
												}}
											>
												<div className="disable-input d-flex">
													<div>
														<Input
															value={e.cost / e.amount}
															type="number"
															placeholder="ราคา"
															disabled
															style={{
																width: '5rem',
																// borderRadius: '3px',
																// borderWidth: '0.5px',
																// outline: 'none',
																textAlign: 'right'
															}}
														/>
													</div>
													<div className="d-flex">
														<button className="btn-minus" style={{ color: '#ced4da' }}>
															-
														</button>
														<input
															value={e.amount}
															type="number"
															disabled
															className="width-input-amount"
														/>
														<button className="btn-plus" style={{ color: '#ced4da' }}>
															+
														</button>
													</div>
													<div>
														<Input
															value={e.cost}
															type="number"
															disabled
															style={{
																width: '5rem',
																// borderRadius: '3px',
																// borderWidth: '0.5px',
																// outline: 'none',
																textAlign: 'right'
															}}
															className="width-input"
														/>
													</div>
												</div>
											</td>
											<td
												style={{
													backgroundColor: 'white',
													borderLeftWidth: 0,
													borderRightWidth: 0,
													width: '9%'
												}}
											>
												<button
													onClick={() => this.del_objNewPackage(index)}
													style={{
														color: 'white',
														borderRadius: '3px',
														backgroundColor: 'red',
														borderColor: 'red'
													}}
												>
													<Min>ลบ</Min>
													<Max>x</Max>
												</button>
											</td>
										</tr>
									);
								})}
							<tr>
								<td>
									<InputMin>
										<input
											value={this.state.t_name}
											type="text"
											placeholder="ชื่อรายการ..."
											style={{
												width: '100%',
												paddingLeft: '5px',
												borderRadius: '3px',
												borderWidth: '0.5px',
												outline: 'none'
											}}
											onChange={(e) => this.setState({ t_name: e.target.value })}
										/>
									</InputMin>
									<InputMax>
										<input
											value={this.state.t_name}
											type="text"
											placeholder="ชื่อ..."
											style={{
												width: '100px',
												paddingLeft: '5px',
												borderRadius: '3px',
												borderWidth: '0.5px',
												outline: 'none'
											}}
											onChange={(e) => this.setState({ t_name: e.target.value })}
										/>
									</InputMax>
								</td>
								<td>
									<InputMin>
										<input
											value={code_t}
											type="number"
											placeholder="เลขกรมบัญชีกลาง..."
											style={{
												width: '100%',
												paddingLeft: '5px',
												borderRadius: '3px',
												borderWidth: '0.5px',
												outline: 'none'
											}}
											onChange={(e) => this.setState({ code_t: e.target.value })}
										/>
									</InputMin>
									<InputMax>
										<input
											value={code_t}
											type="number"
											placeholder="เลขกรม..."
											style={{
												width: '100px',
												paddingLeft: '5px',
												borderRadius: '3px',
												borderWidth: '0.5px',
												outline: 'none'
											}}
											onChange={(e) => this.setState({ code_t: e.target.value })}
										/>
									</InputMax>
								</td>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0,
										width: '17%'
									}}
								>
									<div className="disable-input d-flex">
										<div>
											<input
												value={this.state.price_t}
												onChange={(e) => this.setState({ price_t: e.target.value })}
												type="number"
												placeholder="ราคา"
												style={{
													width: '5rem',
													borderRadius: '3px',
													borderWidth: '0.5px',
													outline: 'none',
													textAlign: 'right'
												}}
											/>
										</div>
										<div className="d-flex">
											<button className="btn-minus" onClick={() => this.mutiple('minus')}>
												-
											</button>
											<input
												value={amount}
												type="number"
												disabled
												className="width-input-amount"
											/>
											<button className="btn-plus" onClick={() => this.mutiple('plus')}>
												+
											</button>
										</div>
										<div>
											<Input
												value={this.state.price_t * amount}
												type="number"
												disabled
												style={{
													width: '5rem',
													// borderRadius: '3px',
													// borderWidth: '0.5px',
													// outline: 'none',
													textAlign: 'right'
												}}
												className="width-input"
											/>
										</div>
									</div>
								</td>
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0,
										width: '9%'
									}}
								>
									<button
										style={{
											color: 'white',
											borderRadius: '3px',
											backgroundColor: '#467ac8',
											borderColor: '#467ac8'
										}}
										onClick={this.AddPT}
									>
										<Min>เพิ่ม</Min>
										<Max>+</Max>
									</button>
								</td>
							</tr>
						</tbody>
					)}
				</Table>
			</div>
		);
	}

	AddMT = () => {
		let { t_name, price_t, code_t } = this.state;
		let { type_Service, curServiceType } = this.props;
		if (!t_name || !price_t) {
			swal('ผิดพลาด!', 'กรุณากรอกรายการและราคาให้ครบถ่วน', 'error', {
				buttons: false,
				timer: 2000
			});
		} else {
			let type = curServiceType === 'mt' ? 'mt' : type_Service == 1 ? 'tm' : 'pt';
			let obj = {
				type,
				treatment_name: t_name,
				cost: Number(price_t),
				cgd_code: code_t,
				detail: null,
				disburseable: 0
			};
			let { newPackage } = this.state;
			newPackage.push(obj);
			this.setState(
				{
					newPackage: newPackage,
					t_name: '',
					price_t: '',
					code_t: ''
				},
				() => {
					let { newPackage } = this.state;
					let priceAll = newPackage.reduce(fn.add('cost'), 0);
					this.props.priceAll(Number(priceAll));
					this.props.newPackage(newPackage);
					// this.setState({ newPackage: [] });
				}
			);
		}
	};
	AddPT = () => {
		let { t_name, price_t, code_t, amount } = this.state;
		let { type_Service, curServiceType } = this.props;
		if (!t_name || !price_t) {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อรายการและราคาให้ครบถ่วน', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			let type = curServiceType === 'mt' ? 'mt' : type_Service == 1 ? 'tm' : 'pt';
			let obj = {
				cost: Number(price_t) * Number(amount),
				type,
				amount: Number(amount),
				detail: null,
				percent: 0,
				cgd_code: code_t,
				pay_price: 0,
				disburseable: 0,
				treatment_name: t_name
			};
			let { newPackage } = this.state;
			newPackage.push(obj);
			this.setState(
				{
					newPackage: newPackage,
					t_name: '',
					price_t: '',
					code_t: '',
					amount: 1
				},
				() => {
					let { newPackage } = this.state;
					let priceAll = newPackage.reduce(fn.add('cost'), 0);
					this.props.priceAll(Number(priceAll));
					this.props.newPackage(newPackage);
					// this.setState({ newPackage: [] });
				}
			);
		}
	};
	mutiple = (e) => {
		let { amount } = this.state;
		if (e === 'plus') {
			this.setState((pre) => ({ amount: pre.amount + 1 }));
		}
		if (e === 'minus' && amount > 1) {
			this.setState((pre) => ({ amount: pre.amount - 1 }));
		}
	};
}
