import React, { Component } from 'react';
import { ip, GET, GET_USERS, GET_PICTURE } from '../../../../../service/service';
import { FaRegFileAlt, FaRegEdit } from 'react-icons/fa';
import './Styled/style.css';
import Barcode from 'react-barcode';
import default_picture from '../../../../../assets/image/man-user.png';
import moment from 'moment';
import Color from '../../../../../components/Color';
import { Grid } from '../../CustomerDetail/views/Styled/styled';
import ReactLoading from 'react-loading';
import swal from 'sweetalert';
import { sevenDigits } from '../../../../../function/function';

let new_id = '';

export default class CardTableDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			detailTH: [],
			detailEN: [],
			user_id: '',
			picture_id: ''
		};
	}

	GetHistory = async () => {
		let { id } = this.props;
		new_id = sevenDigits(id);

		try {
			let res = await GET(GET_USERS(id));
			if (res.success) {
				this.setState({
					user_id: new_id,
					picture_id: res.result.map((el) => el.picture),
					detailTH: res.result.map((el, i) => [
						{
							TH_Pre: 'ชื่อ-นามสกุล',
							EN_Pre: 'Name-Last name',
							TH_Data:
								el.th_prefix && el.th_name && el.th_lastname
									? el.th_prefix + ' ' + el.th_name + ' ' + el.th_lastname
									: '-',
							EN_Data:
								el.en_prefix && el.en_name && el.en_lastname
									? el.en_prefix + ' ' + el.en_name + ' ' + el.en_lastname
									: '-'
						},
						{
							TH_Pre: 'เบอร์โทรศัพท์',
							EN_Pre: '',
							TH_Data: el.phone ? el.phone.slice(0, 3) + '-' + el.phone.slice(3) : '-',
							EN_Data: ''
						},
						{
							TH_Pre: 'เกิดวันที่',
							EN_Pre: 'Date of birth',
							TH_Data: el.birthday
								? moment(el.birthday).locale('th').format('D MMM') +
									' ' +
									(Number(moment(el.birthday).format('YYYY')) + 543).toString()
								: '-',
							EN_Data: el.birthday ? moment(el.birthday).locale('en').format('D MMM YYYY') : '-'
						},
						{
							TH_Pre: 'เพศ',
							EN_Pre: 'Sex',
							TH_Data: el.sex ? el.sex : 'ไม่ระบุ',
							EN_Data: el.sex === 'ผู้หญิง' ? 'Female' : el.sex === 'ผู้ชาย' ? 'Male' : 'Unknown'
						},
						{
							TH_Pre: 'เลขประจำตัวประชาชน',
							EN_Pre: 'Identification Number',
							TH_Data: el.citizen_id
								? el.citizen_id.slice(0, 1) +
									'-' +
									el.citizen_id.slice(1, 5) +
									'-' +
									el.citizen_id.slice(5, 7) +
									'-' +
									el.citizen_id.slice(7, 10) +
									'-' +
									el.citizen_id.slice(10, 12) +
									'-' +
									el.citizen_id.slice(12)
								: '-',
							EN_Data: ''
						},
						{
							TH_Pre: 'วันออกบัตร',
							EN_Pre: 'Date of Issue',
							TH_Data: el.issue
								? moment(el.issue).locale('th').format('D MMM') +
									' ' +
									(Number(moment(el.issue).format('YYYY')) + 543).toString()
								: '-',
							EN_Data: ''
						},
						{
							TH_Pre: 'วันหมดอายุ',
							EN_Pre: 'Date of Expiry',
							TH_Data: el.expire
								? moment(el.expire).locale('th').format('D MMM') +
									' ' +
									(Number(moment(el.expire).format('YYYY')) + 543).toString()
								: '-',
							EN_Data: ''
						},
						{
							TH_Pre: 'ที่อยู่',
							EN_Pre: '',
							TH_Data: el.address ? el.address : '-',
							EN_Data: ''
						},
						{
							TH_Pre: 'หมายเหตุ',
							EN_Pre: '',
							TH_Data: el.allergy ? el.allergy : '-',
							EN_Data: ''
						}
					]),
					loading: false
				});
			} else {
				swal('ผิดพลาด!', res.message, 'warning', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (e) {}
	};

	componentWillMount() {
		this.GetHistory();
	}

	render() {
		let { detailTH, user_id, picture_id, loading } = this.state;
		// console.log(user_id)
		return (
			<div>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : (
					<div className="div-table-customer-detail">
						<div className="div-table-card-detail">
							<div className="div-column-detail">
								<div className="label-detail">ข้อมูลผู้ป่วย</div>
								<Grid column={2}>
									{detailTH.map((el, index) => {
										if (index < 1) {
											return el.map((e, i) => {
												if (i < 4) {
													return (
														<div key={i} className="card-detail-data">
															<div className="card-detail-sub-data">
																<div className="card-detail-sub-data-font">
																	{e.TH_Pre}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw' }}
																>
																	{e.EN_Pre}
																</div>
															</div>
															<div className="card-detail-sub-data">
																<div className="card-detail-sub-data-font">
																	{e.TH_Data}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw' }}
																>
																	{e.EN_Data}
																</div>
															</div>
														</div>
													);
												}
											});
										}
									})}
								</Grid>
								<Grid column={3}>
									{detailTH.map((el, index) => {
										if (index < 1) {
											return el.map((e, i) => {
												if (i > 3 && i < 7) {
													return (
														<div ckey={i} lassName="card-detail-sub-data">
															<div className="card-detail-sub-data-font">{e.TH_Pre}</div>
															<div
																className="card-detail-sub-data-font"
																style={{ fontSize: '0.7vw' }}
															>
																{e.EN_Pre}
															</div>
															<div className="card-detail-sub-data-font">{e.TH_Data}</div>
															<div
																className="card-detail-sub-data-font"
																style={{ fontSize: '0.7vw' }}
															>
																{e.EN_Data}
															</div>
														</div>
													);
												}
											});
										}
									})}
								</Grid>
								<Grid column={1}>
									{detailTH.map((el, index) => {
										if (index < 1) {
											return el.map((e, i) => {
												if (i === 7) {
													return (
														<div key={i} className="card-detail-data">
															<div className="card-detail-sub-data">
																<div className="card-detail-sub-data-font">
																	{e.TH_Pre}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw' }}
																>
																	{e.EN_Pre}
																</div>
															</div>
															<div className="card-detail-sub-data">
																<div className="card-detail-sub-data-font">
																	{e.TH_Data}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw' }}
																>
																	{e.EN_Data}
																</div>
															</div>
														</div>
													);
												} else if (i === 8) {
													return (
														<div key={i} className="card-detail-data">
															<div className="card-detail-sub-data">
																<div
																	className="card-detail-sub-data-font"
																	style={{ color: Color.Orange }}
																>
																	{e.TH_Pre}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw', color: Color.Orange }}
																>
																	{e.EN_Pre}
																</div>
															</div>
															<div className="card-detail-sub-data">
																<div
																	className="card-detail-sub-data-font"
																	style={{ color: Color.Orange }}
																>
																	{e.TH_Data}
																</div>
																<div
																	className="card-detail-sub-data-font"
																	style={{ fontSize: '0.7vw', color: Color.Orange }}
																>
																	{e.EN_Data}
																</div>
															</div>
														</div>
													);
												}
											});
										}
									})}
								</Grid>
							</div>

							<div className="div-column-detail" style={{ width: '25%' }}>
								<div>
									<Barcode value={user_id} width={0.9} height={25} />
								</div>
								<img
									src={ip + GET_PICTURE(picture_id || 'null')}
									className="avatar-card-detail-table"
									alt="avatar"
								/>
							</div>
						</div>
						<div className="div-row-space">
							<div className="btn-blue-subject" style={{ margin: '2vh 0', backgroundColor: Color.Green }}>
								<FaRegEdit className="ic-btn-blue-subject" />
								แก้ไขหมายเหตุ
							</div>

							<div className="btn-blue-subject" style={{ margin: '2vh 0' }}>
								<FaRegFileAlt className="ic-btn-blue-subject" />
								ประวัติการรักษา
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}
