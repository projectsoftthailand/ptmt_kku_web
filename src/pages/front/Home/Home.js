import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Today from '../../../components/Today/Today';
import IPD from '../../../components/IPD/IPD';
import socketIOClient from 'socket.io-client';
import { _ip } from '../../../service/service';
// import firebase from "firebase";
import './style.css';

@observer
class Home extends Component {
	componentWillMount() {
		// this.response();
		// this.handle();
	}

	response() {
		// socketIOClient(_ip).on('rerender', (res) => {
		// 	this.reRender();
		// });
	}

	// handle() {
	//   firebase.initializeApp({
	//     messagingSenderId: "635245093746" // troque pelo seu sender id
	//   });
	//   const messaging = firebase.messaging();
	//   messaging.onMessage(payload => {
	//     console.log(JSON.parse(payload.data.extra));
	//   });
	// }

	componentWillUnmount() {
		// socketIOClient(_ip).disconnect();
	}

	reRender() {
		this.reRenderToday.reRender();
		this.reRenderIPD.reRender();
	}

	render() {
		return (
			<div style={{ fontSize: 11 }}>
				<div className="row mx-auto">
					<div className="col-lg-8">
						<Today onRef={(ref) => (this.reRenderToday = ref)} Today={true} />
					</div>
					<div className="col-lg-4">
						<IPD onRef={(ref) => (this.reRenderIPD = ref)} />
					</div>
				</div>
			</div>
		);
	}
}

export default Home;
