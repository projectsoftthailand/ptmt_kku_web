import React, { Component } from 'react'
import Color from '../../../components/Color';

export default class BedItem extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       isActive: false
    }
  }
  
  render() {
    let {status, statusTypes, number} = this.props
    let index = statusTypes.findIndex(x => x.id===status)
    let typeString = 'ว่าง'
    let color
    if(status===-2) {
      color = '#ccc'
    }else if(status===-1) {
      color = Color.EmptyGray
    }else if(status===1) {
      color = Color.WaitComfirm
    }else if(status===2) {
      color = Color.InQueue
    }else if(status===3) {
      color = Color.Servicing
    }else if(status===4) {
      color = Color.Done
    }
    if(index!==-1) typeString = statusTypes[index].name 
    
    // if(this.state) {

    // }
    return (
      <div onClick={() => {
          this.setState({isActive: !this.state.isActive})
        }} 
        style={{borderRadius: 6, backgroundColor: color}} className="border px-3 text-center text-white">
        <p style={{whiteSpace: 'pre', margin: 0, padding: 0}}>เตียง {number}</p>
        <p style={{whiteSpace: 'pre', margin: 0, padding: 0}}>{typeString}</p>
      </div>
    )
    
  }
}
