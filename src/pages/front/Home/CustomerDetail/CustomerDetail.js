import React, { Component } from "react";
import "./Styled/style.css";
import CardDetail from "./views/CardDetail";
import CardHistory from "./views/CardHistory";
import "./views/Styled/style.css";

export default class CustomerDetail extends Component {
  render() {
    let { id } = this.props.match.params;
    return (
      <div
        style={{ marginBottom: "3vh" }}
        id="customerdetail"
        className="pb-3"
        // className="col-md-12"
      >
        <div className="btn-back label-back pb-2" onClick={() => this.props.history.goBack()}>
          {"< กลับ"}
        </div>

        <CardDetail id={id} />
        <CardHistory id={id} />
      </div>
    );
  }
}
