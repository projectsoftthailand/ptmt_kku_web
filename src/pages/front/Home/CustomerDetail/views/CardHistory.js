import React, { Component } from 'react';
import { Input, Table, Label } from 'reactstrap';
import { FaSearch, FaMapMarkerAlt } from 'react-icons/fa';
import './Styled/style.css';
import ReactLoading from 'react-loading';
import Color from '../../../../../components/Color';
import { GET, GET_USERS, GET_CUSTOMER_HISTORY, GET_UNIT } from '../../../../../service/service';
import swal from 'sweetalert';
import Responsive from 'react-responsive';
import User from '../../../../../mobx/user/user';
import Pagination from '../../../../../components/Pagination';

let Desktop = (props) => <Responsive {...props} minWidth={992} />;
let NotDesktop = (props) => <Responsive {...props} maxWidth={991} />;

let head = [ 'ว/ด/ป', 'เวลา', 'รหัส', 'ชื่อ', 'นามสกุล', 'หน่วยตรวจ', 'โปรแกรมการตรวจ', '', '' ];
let numberPage = [ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
let special = /[$^[\]{};'"\\|<>@#]/;

export default class CardHistory extends Component {
	constructor(props) {
		super(props);

		this.state = {
			historyData: [],
			dumpHistoryData: [],
			loading: true,
			currentPage: 1,
			pageSize: 5,
			searchText: ''
		};
	}

	GetHistory = async () => {
		let { id } = this.props;
		try {
			let res = await GET(GET_CUSTOMER_HISTORY(id, 'all', 'all'));
			let resName = await GET(GET_USERS(id));
			let firstname = resName.result[0].th_name;
			let lastname = resName.result[0].th_lastname;

			let resUnit = await GET(GET_UNIT);

			let dumpData = User.role === 'frontPT' ? res.result.pt : res.result.mt;
			let rows = [];
			let dumpRows = [];
			dumpData.forEach((e) => {
				let date = e.service_at.split('T')[0];
				date = date.split('-');
				date = date[2] + '/' + date[1] + '/' + date[0];
				let time = e.service_at.split('T')[1].split('.')[0];
				let unit = resUnit.result.filter((el) => {
					return el.id === Number(e.service_unit);
				});

				rows.push([ date, time, e.customer_id, firstname, lastname, unit[0].name, e.type_name, '@', '@', '@' ]);
				// console.log(date, time)

				// dumpData.map(el => {
				dumpRows.push({
					date: date,
					time: time,
					customer_id: e.customer_id,
					firstname: firstname,
					lastname: lastname,
					unit: unit[0].name,
					type_name: e.type_name
				});
				//     date: date, time: time, customer_id: el.customer_id, firstname: firstname, lastname: lastname, unit: unit[0].name, type_name: el.type_name
				// })
			});
			// console.log("dumpData: ", dumpData, "rows: ", rows, "dumpRows: ", dumpRows)
			this.setState({ historyData: rows, dumpHistoryData: dumpRows });
			// dumpData = dumpRows;
		} catch (error) {}
	};

	componentWillMount() {
		this.GetHistory().then(this.setState({ loading: false }));
		// console.log(User.role)
	}

	searchData = (e) => {
		let event = e.target.value;
		let searchText = event.trim().toLowerCase();
		let { dumpHistoryData } = this.state;
		this.setState({ searchText: searchText });
		// console.log(searchText, dumpData)
		if (special.test(searchText)) {
			swal('ตัวอักษรพิเศษ!', 'ไม่สามารถกรอกตัวอักษรพิเศษได้', 'warning', {
				buttons: false,
				timer: 2000
			}).then(this.setState({ searchText: '' }), (searchText = ''));
		} else {
			let res = dumpHistoryData.filter((el) => {
				// return el.type_name.match(searchText);
				return el.type_name.toLowerCase().match(searchText);
			});
			// console.log(res)
			this.mapData(res);
		}
	};

	mapData(res) {
		let rows = [];
		res.forEach((e) => {
			rows.push([ e.date, e.time, e.customer_id, e.firstname, e.lastname, e.unit, e.type_name, '@', '@', '@' ]);
		});
		this.setState({ historyData: rows });
	}

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { historyData, loading, currentPage, pageSize, searchText } = this.state;

		return (
			<div className="col-md-12 div-row-space">
				<div className="card-detail-sub-data">
					<div className="search">
						<Input name="searchText" onChange={this.searchData} type="text" value={searchText} />
						<FaSearch className="ic" />
					</div>

					{loading ? (
						<div className="loading d-flex flex-column">
							<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>
								กำลังโหลดข้อมูล
							</Label>
						</div>
					) : (
						<div>
							<div className="label-table-history">
								{User.role === 'frontPT' ? 'ประวัติการทำกายภาพบำบัด' : 'ประวัติการตรวจแลป'}
							</div>
							{historyData.length !== 0 ? (
								<div>
									<Table style={{ whiteSpace: 'pre' }} responsive>
										{head && (
											<thead>
												<tr>{head.map((el, index) => <th key={'a' + index}>{el}</th>)}</tr>
											</thead>
										)}
										<tbody style={{ backgroundColor: '#fff' }}>
											{historyData
												.slice((currentPage - 1) * pageSize, currentPage * pageSize)
												.map((row, index) => (
													<tr key={'b' + index} className="card-history-table-hover">
														{row.map((d, index) => {
															if (index < 7) {
																return (
																	<td
																		key={'c' + index}
																		style={{
																			backgroundColor: 'transparent',
																			paddingTop: '0.6rem'
																		}}
																	>
																		{d}
																	</td>
																);
															} else if (index === 7) {
																return (
																	<td
																		key={'c' + index}
																		className="card-history-contain-icon"
																	>
																		<img
																			alt="printer"
																			className="card-history-icon"
																			src={require('../../../../../assets/icon/printer.png')}
																		/>
																	</td>
																);
															} else if (index === 8) {
																return (
																	<td
																		key={'c' + index}
																		className="card-history-contain-icon"
																	>
																		<img
																			alt="pdf"
																			className="card-history-icon"
																			src={require('../../../../../assets/icon/pdf.png')}
																		/>
																	</td>
																);
															} else {
																return (
																	<td
																		key={'c' + index}
																		className="card-history-contain-icon"
																	>
																		<img
																			alt="excel"
																			className="card-history-icon"
																			src={require('../../../../../assets/icon/excel.png')}
																		/>
																	</td>
																);
															}
														})}
													</tr>
												))}
										</tbody>
									</Table>

									<div className="d-flex justify-content-between">
										<select
											className="select-page-size"
											name="pageSize"
											onChange={async (e) => {
												await this.setState({ [e.target.name]: +e.target.value });
											}}
										>
											{numberPage.map((el, i) => (
												<option key={i} value={el}>
													{el}
												</option>
											))}
										</select>
										<Pagination
											totalRecords={historyData.length}
											pageLimit={pageSize}
											pageNeighbours={2}
											onPageChanged={this.onPageChanged}
										/>
									</div>
								</div>
							) : (
								<div className="empty-list-history">---ไม่พบข้อมูล---</div>
							)}
						</div>
					)}
				</div>
			</div>
		);
	}
}
