import styled from 'styled-components';

export const Grid = styled.div`
    width: 100%;
    display: grid;
    background-color: transparent;
    margin-top: 2vh;
    word-wrap: break-word;
    grid-column-gap: 2vw;
    grid-template-columns: repeat(${props => props.column}, 1fr);
    grid-gap: 1.4vw;
`