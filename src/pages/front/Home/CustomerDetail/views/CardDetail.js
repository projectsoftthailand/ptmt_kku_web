import React, { Component } from "react";
import { ip, GET, GET_USERS, GET_PICTURE } from "../../../../../service/service";
import { FaFileAlt, FaPrint, FaFileExport } from "react-icons/fa";
import "./Styled/style.css";
import Barcode from "react-barcode";
import default_picture from "../../../../../assets/image/man-user.png";
import moment from "moment";
import { Grid } from "./Styled/styled";
import Color from "../../../../../components/Color";
import { sevenDigits } from "../../../../../function/function";

export default class CardDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      detailTH: [],
      detailEN: [],
      user_id: "",
      picture_id: ""
    };
  }

  GetHistory = async () => {
    let { id } = this.props;
    // console.log(this.props)
    try {
      let res = await GET(GET_USERS(id));
      let new_id = sevenDigits(id);
      this.setState({
        user_id: new_id,
        picture_id: res.result.map(el => el.picture),
        detailTH: res.result.map((el, i) => [
          {
            TH_Pre: "ชื่อ-นามสกุล",
            EN_Pre: "Name-Last name",
            TH_Data:
              el.th_prefix && el.th_name && el.th_lastname
                ? el.th_prefix + " " + el.th_name + " " + el.th_lastname
                : "-",
            EN_Data:
              el.en_prefix && el.en_name && el.en_lastname
                ? el.en_prefix + " " + el.en_name + " " + el.en_lastname
                : "-"
          },
          {
            TH_Pre: "เกิดวันที่",
            EN_Pre: "Date of birth",
            TH_Data: el.birthday
              ? moment(el.birthday)
                  .locale("th")
                  .format("D MMM") +
                " " +
                (Number(moment(el.birthday).format("YYYY")) + 543).toString()
              : "-",
            EN_Data: el.birthday
              ? moment(el.birthday)
                  .locale("en")
                  .format("D MMM YYYY")
              : "-"
          },
          {
            TH_Pre: "เพศ",
            EN_Pre: "Sex",
            TH_Data: el.sex === "male" ? "ผู้ชาย" : el.sex === "female" ? "ผู้หญิง" : "ไม่ระบุ",
            EN_Data: el.sex === "male" ? "Male" : el.sex === "female" ? "Female" : "Unknown"
          },
          {
            TH_Pre: "เบอร์โทรศัพท์",
            EN_Pre: "",
            TH_Data: el.phone ? el.phone.slice(0, 3) + "-" + el.phone.slice(3) : "-",
            EN_Data: ""
          },
          {
            TH_Pre: "เลขประจำตัวประชาชน",
            EN_Pre: "Identification Number",
            TH_Data: el.citizen_id
              ? el.citizen_id.slice(0, 1) +
                "-" +
                el.citizen_id.slice(1, 5) +
                "-" +
                el.citizen_id.slice(5, 7) +
                "-" +
                el.citizen_id.slice(7, 10) +
                "-" +
                el.citizen_id.slice(10, 12) +
                "-" +
                el.citizen_id.slice(12)
              : "-",
            EN_Data: ""
          },
          {
            TH_Pre: "วันออกบัตร",
            EN_Pre: "Date of Issue",
            TH_Data: el.issue
              ? moment(el.issue)
                  .locale("th")
                  .format("D MMM") +
                " " +
                (Number(moment(el.issue).format("YYYY")) + 543).toString()
              : "-",
            EN_Data: ""
          },
          {
            TH_Pre: "วันหมดอายุ",
            EN_Pre: "Date of Expiry",
            TH_Data: el.expire
              ? moment(el.expire)
                  .locale("th")
                  .format("D MMM") +
                " " +
                (Number(moment(el.expire).format("YYYY")) + 543).toString()
              : "-",
            EN_Data: ""
          },
          {
            TH_Pre: "ที่อยู่",
            EN_Pre: "",
            TH_Data: el.address ? el.address : "-",
            EN_Data: ""
          },
          {
            TH_Pre: "หมายเหตุ",
            EN_Pre: "",
            TH_Data: el.allergy ? el.allergy : "-",
            EN_Data: ""
          }
        ])
      });
      // console.log(this.state.detailTH)
    } catch (e) {}
  };

  componentWillMount() {
    this.GetHistory().then(this.setState({ loading: false }));
  }

  render() {
    let { detailTH, user_id, picture_id } = this.state;
    // let data = detailTH[0];
    // console.log(detailTH[0], picture_id)

    return (
      <div className="col-md-12 div-customer-detail">
        <div className="div-row-space">
          <div className="div-row-subject">
            <FaFileAlt className="ic-subject" />
            <div className="label-subject">ประวัติการรักษา</div>
          </div>
          <div className="div-row-subject">
            <div className="btn-blue-subject">
              <FaPrint className="ic-btn-blue-subject" />
              พิมพ์ผลการตรวจทั้งหมด
            </div>
            <div className="btn-blue-subject">
              <FaFileExport className="ic-btn-blue-subject" />
              export to csv
            </div>
          </div>
        </div>

        <div className="div-card-detail">
          <div className="div-column-detail">
            <div className="label-detail">ข้อมูลผู้รับบริการ</div>

            <Grid column={4}>
              {detailTH.map((el, index) => {
                if (index < 1) {
                  return el.map((e, i) => {
                    if (i < 4) {
                      return (
                        <div key={i} className="card-detail-data">
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font">{e.TH_Pre}</div>
                            <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                              {e.EN_Pre}
                            </div>
                          </div>
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font">{e.TH_Data}</div>
                            <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                              {e.EN_Data}
                            </div>
                          </div>
                        </div>
                      );
                    }
                  });
                }
              })}
            </Grid>
            <Grid column={5}>
              {detailTH.map((el, index) => {
                if (index < 1) {
                  return el.map((e, i) => {
                    if (i > 3 && i < 7) {
                      return (
                        <div className="card-detail-sub-data">
                          <div className="card-detail-sub-data-font">{e.TH_Pre}</div>
                          <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                            {e.EN_Pre}
                          </div>
                          <div className="card-detail-sub-data-font">{e.TH_Data}</div>
                          <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                            {e.EN_Data}
                          </div>
                        </div>
                      );
                    } else if (i === 7) {
                      return (
                        <div key={i} className="card-detail-data">
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font">{e.TH_Pre}</div>
                            <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                              {e.EN_Pre}
                            </div>
                          </div>
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font">{e.TH_Data}</div>
                            <div className="card-detail-sub-data-font" style={{ fontSize: "0.7vw" }}>
                              {e.EN_Data}
                            </div>
                          </div>
                        </div>
                      );
                    } else if (i === 8) {
                      return (
                        <div key={i} className="card-detail-data">
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font" style={{ color: Color.Orange }}>
                              {e.TH_Pre}
                            </div>
                            <div
                              className="card-detail-sub-data-font"
                              style={{ fontSize: "0.7vw", color: Color.Orange }}
                            >
                              {e.EN_Pre}
                            </div>
                          </div>
                          <div className="card-detail-sub-data">
                            <div className="card-detail-sub-data-font" style={{ color: Color.Orange }}>
                              {e.TH_Data}
                            </div>
                            <div
                              className="card-detail-sub-data-font"
                              style={{ fontSize: "0.7vw", color: Color.Orange }}
                            >
                              {e.EN_Data}
                            </div>
                          </div>
                        </div>
                      );
                    }
                  });
                }
              })}
            </Grid>
          </div>

          <div className="div-column-detail" style={{ width: "10%" }}>
            <div>
              <Barcode value={user_id} width={1.3} height={40} />
            </div>
            <img
              src={ip + GET_PICTURE(picture_id || 'null')}
              className="avatar-card-detail"
              alt="avatar"
            />
          </div>
        </div>
      </div>
    );
  }
}
