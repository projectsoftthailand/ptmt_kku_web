import React, { Component } from 'react';
import Color from '../../../../../components/Color';
import InProgress from '../../../../../components/InProgress/InProgress';
import user from '../../../../../mobx/user/user';

class Home extends Component {
    render() {
        return (
            <div  >
                <div style={{ fontSize: 11 }} >
                    <div className="row mx-auto">
                        <div className="col-md-12">
                            {/* <Today onRef={ref => (this.reRenderToday = ref)} Today={true} AllTable={true} /> */}
                            <div className={"d-flex justify-content-between align-items-center my-2"}>
                                <div className="LabelTodayTable">ตารางงาน</div>
                            </div>
                            <InProgress />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;
