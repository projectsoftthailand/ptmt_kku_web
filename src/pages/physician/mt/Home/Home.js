import React, { Component } from 'react';
import Today from '../../../../components/Today/Today';
import { Route, withRouter } from 'react-router-dom';

class Home extends Component {
	render() {
		return (
			<div className="col-md-12" style={{ fontSize: '1.3rem' }}>
				<Today onRef={(ref) => (this.reRenderToday = ref)} Today={true} />
			</div>
		);
	}
}

export default Home;
