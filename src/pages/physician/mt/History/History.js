import React, { Component } from 'react';
import Today from '../../../../components/Today/Today';

class History extends Component {
	render() {
		return (
			<div className="col-md-12" style={{ fontSize: '1.3rem' }}>
				<Today onRef={(ref) => (this.reRenderToday = ref)} History={true} Path="history" id={this.props.match.params.id} />
			</div>
		);
	}
}

export default History;
