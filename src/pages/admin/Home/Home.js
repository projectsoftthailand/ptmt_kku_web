import React, { Component } from 'react';
import { observer } from 'mobx-react';
import IPD from './IPD/IPD';

import './style.css';
import moment from 'moment';
let th = require('moment/locale/th');
moment.updateLocale('th', th);

@observer
class Home extends Component {
	render() {
		return (
			<div className="row mx-auto" style={{ fontSize: '1.4rem' }}>
				<div className="col-md-12">
					<IPD user={'all'} onRef={(ref) => (this.reRenderIPD = ref)} />
				</div>
			</div>
		);
	}
}

export default Home;
