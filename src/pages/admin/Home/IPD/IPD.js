import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Row, Col, Table, Label } from 'reactstrap';
import { GET, GET_USERS, GET_ROLE } from '../../../../service/service';
import ReactLoading from 'react-loading';
import Color from '../../../../components/Color';
import SelectTypeUser from '../../../../components/SelectTypeUser/SelectTypeUser';
import InputWithText from '../../../../components/InputWithText/InputWithText';
import Pagination from '../../../../components/Pagination';
import user from '../../../../mobx/user/user';
import { sevenDigits } from '../../../../function/function';
import moment from 'moment';

let dumpList = [];
let dumpListRole = [];
let nameOfRole = '';
let numberPage = [ 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
let head = [ 'รหัส', 'ชื่อ', 'นามสกุล', 'เบอร์โทร', 'ประเภทผู้ใช้งาน', 'สถานะ' ];

function getRole(role) {
	return role === 'admin'
		? 'แอดมิน'
		: role === 'frontPT'
			? 'ฟ้อนท์กายภาพ'
			: role === 'frontMT'
				? 'ฟ้อนท์เทคนิคการแพทย์'
				: role === 'pt'
					? 'นักกายภาพบำบัด'
					: role === 'mt'
						? 'นักเทคนิคการแพทย์'
						: role === 'marketing'
							? 'การตลาด'
							: role === 'customer' ? 'ลูกค้า' : role === 'tm' ? 'เจ้าหน้าที่นวดไทย' : role;
}

function getDisable(n) {
	return n ? 'ปิดใช้งาน' : 'ใช้งานได้';
}

@withRouter
class IPD extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ListUserShow: [],

			listRole: [],
			role: [],

			type: [],
			loading: true,

			searchType: '',
			searchTextUser: '',

			currentPage: 1,
			pageSize: 10
		};
	}
	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.props.onRef(this);
		await this.GetRows();
		await this.GetUsers();
		await this.setState({ loading: false });
	}

	GetUsers = async () => {
		try {
			let res = await GET(GET_USERS('all'));
			let result = res.result.filter((el) => el.user_id !== user.user_id);
			let newData = result.map((e) => {
				let phone = e.phone.slice(0, 3) + '-' + e.phone.slice(3);
				let id = sevenDigits(e.user_id);
				let role_n =
					e.sub_role.length > 0 ? (e.sub_role[0] === 'ThaiMassage' ? 'tm' : e.role_name) : e.role_name;
				let roles = getRole(role_n);
				return [ e.user_id, id, e.th_name, e.th_lastname, phone, roles, getDisable(e.isDisable) ];
			});
			this.setState({ ListUserShow: newData });
			dumpList = result;
		} catch (error) {}
	};

	GetRows = async () => {
		try {
			let res = await GET(GET_ROLE);
			// console.log('GetRows', res);
			if (res.success) {
				let dump = res.result;
				dump.unshift({ role_id: 0, role_name: 'ประเภทผู้ใช้งานทั้งหมด' });
				dump.push({
					role_id: 8,
					role_name: 'tm'
				});
				this.setState({ listRole: dump });
				dumpListRole = dump;
			}
		} catch (error) {}
	};

	searchRole = (e) => {
		let res = dumpListRole.filter((el) => el.role_id === e);
		let nRole = getRole(res.map((el) => el.role_name)[0]);
		return nRole;
	};

	searchData = (e) => {
		let { searchType } = this.state;
		let target = e.target;
		let name = target.name;
		let event = e.target.value;
		let searchText = event.trim().toLowerCase();
		let res = [];
		this.setState({ [name]: event });
		if (searchType !== '0' && searchType !== '') {
			res = dumpList.filter((el) => {
				return (
					el.role_id === Number(searchType) &&
					(el.th_name.toLowerCase().match(searchText) ||
						el.th_lastname.toLowerCase().match(searchText) ||
						el.phone.toLowerCase().match(searchText))
				);
			});
		} else {
			res = dumpList.filter((el) => {
				return (
					el.th_name.toLowerCase().match(searchText) ||
					el.th_lastname.toLowerCase().match(searchText) ||
					el.phone.toLowerCase().match(searchText)
				);
			});
		}

		this.mapData(res);
	};

	searchSelectType = (e) => {
		let name = e.target.name;
		let event = e.target.value;
		let searchText = event.trim().toLowerCase();
		this.setState({ [name]: event });
		if (event !== '0') {
			let res = dumpList.filter((el) => {
				return el.role_id === Number(searchText);
			});
			this.mapData(res);
		} else {
			this.mapData(dumpList);
		}
	};

	async mapData(res) {
		let newData = res.map((e) => {
			let phone = e.phone.slice(0, 3) + '-' + e.phone.slice(3);
			let id = sevenDigits(e.user_id);
			let role_n = e.sub_role.length > 0 ? (e.sub_role[0] === 'ThaiMassage' ? 'tm' : e.role_name) : e.role_name;
			let roles = getRole(role_n);
			// this.searchRole(e.sub_role[0] === 'ThaiMassage' ? 8 : e.role_id);
			// rows.push([ e.user_id, id, e.th_name, e.th_lastname, phone, nameOfRole, getDisable(e.isDisable) ]);
			return [ e.user_id, id, e.th_name, e.th_lastname, phone, roles, getDisable(e.isDisable) ];
		});
		this.setState({ ListUserShow: newData });
	}

	reRender() {
		this.componentWillMount();
	}

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { ListUserShow, loading, currentPage, pageSize, listRole, searchType, searchTextUser } = this.state;
		return (
			<div>
				<div className="d-flex justify-content-between align-items-center my-2">
					<div className="LabelTodayTable">รายชื่อผู้ใช้งานในระบบ</div>
					<Link
						to={user.role === 'admin' ? '/add-member' : '/home-admin/add-member'}
						className="btn-all-table"
					>
						เพิ่มบุคคลากร
					</Link>
				</div>
				<Row>
					<Col xs={4}>
						<SelectTypeUser
							name="searchType"
							text="ประเภทผู้ใช้"
							options={listRole}
							getType={this.searchSelectType}
							value={searchType}
						/>
					</Col>
					<Col xs={4} />
					<Col xs={4}>
						<InputWithText
							name="searchTextUser"
							text="ค้นหา"
							onChange={this.searchData}
							value={searchTextUser}
						/>
					</Col>
				</Row>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : ListUserShow.length !== 0 ? (
					<div>
						<Table style={{ whiteSpace: 'pre' }} responsive striped>
							<thead>{head && <tr>{head.map((el, index) => <th key={'a' + index}>{el}</th>)}</tr>}</thead>
							<tbody>
								{ListUserShow.slice(
									(currentPage - 1) * pageSize,
									currentPage * pageSize
								).map((row, index) => (
									<tr
										key={'b' + index}
										className="card-history-table-hover"
										onClick={() =>
											this.props.history.push(
												user.role === 'admin'
													? '/home/member/' + row[0]
													: '/home-admin/member/' + row[0]
											)}
									>
										{row.map((d, index) => {
											if (index > 0) {
												return (
													<td
														key={'c' + index}
														style={
															index === 6 ? (
																{
																	color: d === 'ปิดใช้งาน' ? 'red' : 'green'
																}
															) : (
																{}
															)
														}
													>
														{d}
													</td>
												);
											}
										})}
									</tr>
								))}
							</tbody>
						</Table>
						<div className="d-flex justify-content-between">
							<select
								className="select-page-size"
								name="pageSize"
								onChange={async (e) => {
									await this.setState({ [e.target.name]: +e.target.value });
								}}
							>
								{numberPage.map((el, i) => (
									<option key={i} value={el}>
										{el}
									</option>
								))}
							</select>
							<Pagination
								totalRecords={ListUserShow.length}
								pageLimit={pageSize}
								pageNeighbours={2}
								onPageChanged={this.onPageChanged}
							/>
						</div>
					</div>
				) : (
					<div className="EmptyListShow">---ไม่พบข้อมูล---</div>
				)}
			</div>
		);
	}
}

export default IPD;
