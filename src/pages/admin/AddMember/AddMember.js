import React, { Component } from 'react';
import { Col, Row, Form, FormText, FormGroup, Label, CustomInput, Input } from 'reactstrap';
import Responsive from 'react-responsive';
import Color from '../../../components/Color';
import { GET, GET_ROLE, POST, CREATE_USER, GET_USERS } from '../../../service/service';
import swal from 'sweetalert';
import ReactLoading from 'react-loading';
import socketIOClient from 'socket.io-client';
import { subRole } from '../../../const/subRole';
import functions from '../../../function/function';
import moment from 'moment';
import Modal from 'react-modal';
import User from '../../../mobx/user/user';
import './style/AddMember.css';
var cardReaderIP = 'http://localhost:3099';
let th = require('moment/locale/th');
moment.updateLocale('th', th);

const Mobile = (props) => <Responsive {...props} maxWidth={767} />;
const Default = (props) => <Responsive {...props} minWidth={768} />;

let inputData = [];
let dumpData = [];
let dumpList = [];
var uploadImage = new Image();

export default class AddMember extends Component {
	constructor(props) {
		super(props);

		this.state = {
			roleData: [],
			userData: [],

			// if signedUp will get user_id
			user_id: null,
			role: 1,
			username: '',
			password: '',
			cardID: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'ชาย',
			address: '',
			congenital_disease: '', // โรคประจำตัว
			allergy: '',
			fileData: null, // for send picture to formData
			file: null, // for display picture
			foreigner: 0,
			foreignerID: '',
			nationality: '',
			religion: '',
			prefixTh: '',
			prefixEn: '',
			sub_role: [],
			loading: true,
			reading_card: false,

			dumpCardData: [],
			wait_loading: false
		};
	}

	componentDidMount = async () => {
		await GET(GET_ROLE).then((res) => {
			let data = res.result;
			let moxdata = {
				role_id: 8,
				role_name: 'tm'
			};
			data.push(moxdata);
			this.setState({ roleData: data });
		});
	};

	componentWillMount() {
		let {
			onRef
			// , isCustomer
		} = this.props;
		if (onRef) onRef(this);
		this.GetUsers();
		this.response();
		this.setState({ loading: false });
	}

	GetUsers = async () => {
		try {
			let res = await GET(GET_USERS('all'));
			dumpList = res.result;
		} catch (error) {}
	};

	response() {
		socketIOClient(cardReaderIP).on('card', (res) => {
			// console.log(res, res.data ? res.data : "Hi");
			res.message === 'commplete'
				? this.sortArray(res.data)
				: res.message === 'reading fail'
					? swal('คำเตือน', 'เครื่องนี้อ่านเฉพาะบัตรประจำตัวประชาชนเท่านั้น', 'warning', {
							buttons: false,
							timer: 3000
						})
					: res.message === 'reading...'
						? this.setState({ reading_card: true })
						: res.message === 'card remove'
							? swal('', 'นำบัตรประชาชนออกเรียบร้อย', 'success', {
									buttons: false,
									timer: 2000
								}).then(
									this.setState({
										cardID: '',
										prefixTh: '',
										prefixEn: '',
										nameTh: '',
										nameEn: '',
										phone: '',
										dateBirth: '',
										dateIssue: '',
										dateExpiry: '',
										sex: 'male',
										address: '',
										file: null,
										fileData: null,
										foreigner: 0,
										nationality: '',
										religion: '',
										dumpCardData: []
									})
								)
							: res.message !== 'commplete'
								? this.setState({
										cardID: '',
										prefixTh: '',
										prefixEn: '',
										nameTh: '',
										nameEn: '',
										phone: '',
										dateBirth: '',
										dateIssue: '',
										dateExpiry: '',
										sex: 'male',
										address: '',
										file: null,
										fileData: null,
										foreigner: 0,
										nationality: '',
										religion: '',
										dumpCardData: []
									})
								: console.log('Waiting...');
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
		socketIOClient(cardReaderIP).disconnect();
	}

	reRender() {
		console.log('render...');
	}

	sortArray = (e) => {
		inputData.length === 0 ? inputData.push(e) : console.log('Waiting for data');
		dumpData = inputData[0];
		uploadImage = this.dataURLtoFile(dumpData.photo, 'profile.jpg');
		inputData.pop();
		this.setState({ reading_card: false });

		this.searchData(dumpData.citizenId);
	};

	searchData = (e) => {
		let res = dumpList.filter((el) => {
			return el.citizen_id === e;
		});
		if (res.length !== 0) {
			swal('คำเตือน', 'มีผู้ใช้นี้ในระบบแล้ว', 'warning', {
				buttons: false,
				timer: 3000
			});
		} else {
			this.setState({
				cardID: dumpData.citizenId,
				prefixTh: dumpData.titleTH,
				prefixEn: dumpData.titleEN,
				nameTh: dumpData.firstNameTH + ' ' + dumpData.lastNameTH,
				nameEn: dumpData.firstNameEN + ' ' + dumpData.lastNameEN,
				dateBirth: dumpData.birthday,
				dateIssue: dumpData.issue,
				dateExpiry: dumpData.expire,
				sex: dumpData.gender,
				address: dumpData.address,
				file: dumpData.photo,
				fileData: uploadImage,
				role: this.state.role
			});
		}
	};

	dataURLtoFile(dataurl, filename) {
		var arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);
		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}
		return new File([ u8arr ], filename, { type: mime });
	}

	CheckLanThaiInput(text) {
		let alpTh = ' กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮะาิีึืุูเะเแะแโะโำไใเาฤฤๅฦฦๅ่้๊๋์ฯํ็';
		if (alpTh.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckLanEngInput(text) {
		let alpEng = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if (alpEng.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckInput(d) {
		let { role, foreigner } = this.state;
		if (role !== 6 && (!d.username || d.username === '')) {
			swal('ผิดพลาด!', 'กรุณากรอก username ก่อนเพิ่มผู้รับบริการ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (role !== 6 && d.username.length < 6) {
			swal('ผิดพลาด!', 'username ต้องมี 6 ตัวอักษรขึ้นไป', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (role === 6 && foreigner == 0 && d.cardID === '') {
			swal('ผิดพลาด!', 'กรุณากรอกเลขบัตรประจำตัวประชาชน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (role === 6 && foreigner == 0 && d.cardID && d.cardID.length != 13) {
			swal('ผิดพลาด!', 'เลขบัตรประจำตัวประชาชนต้องมี 13 หลักเท่านั้น', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner == 1 && d.foreignerID === '') {
			swal('ผิดพลาด!', 'กรุณากรอกเลขหนังสือเดินทาง', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.password || d.password === '') {
			swal('ผิดพลาด!', 'กรุณากรอก password', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.password.length < 6) {
			swal('ผิดพลาด!', 'password ต้องมี 6 ตัวอักษรขึ้นไป', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner == 0 && (!d.prefixTh || d.prefixTh === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกคำนำหน้าชื่อภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner == 0 && (!d.nameTh || d.nameTh === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner == 0 && (d.nameTh.split(' ')[1] === undefined || d.nameTh.split(' ')[1] === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.prefixEn || d.prefixEn === '') {
			swal('ผิดพลาด!', 'กรุณากรอกคำนำหน้าชื่อภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.nameEn || d.nameEn === '') {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.nameEn.split(' ')[1] === undefined || d.nameEn.split(' ')[1] === '') {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (role !== 6 && d.cardID === '') {
			swal('ผิดพลาด!', 'กรุณากรอกเลขบัตรประจำตัวประชาชน', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (role !== 6 && d.cardID && d.cardID.length != 13) {
			swal('ผิดพลาด!', 'เลขบัตรประจำตัวประชาชนต้องมี 13 หลักเท่านั้น', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.phone || d.phone === '') {
			swal('ผิดพลาด!', 'กรุณากรอกหมายเลขโทรศัพท์', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.dateBirth || d.dateBirth === '') {
			swal('ผิดพลาด!', 'กรุณาเลือกวันเกิด', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.sex || d.sex === '') {
			swal('ผิดพลาด!', 'กรุณาเลือกเพศ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (foreigner !== 0 && (!d.nationality || d.nationality === '')) {
			swal('ผิดพลาด!', 'กรุณากรอกสัญชาติ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.address || d.address === '') {
			swal('ผิดพลาด!', 'กรุณากรอกที่อยู่', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.address.length < 10) {
			swal('ผิดพลาด!', 'ที่อยู่สั้นเกินไป กรุณากรอกให้มากกว่า 10 ตัวอักษร', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (!d.role || d.role === '') {
			swal('ผิดพลาด!', 'กรุณากรอกประเภท', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		// else if (d.hn.length < 3) {
		//   swal('ผิดพลาด!', 'กรอกหมายเลข HN สามหลักเท่านั้น', 'error', {
		//     buttons: false,
		//     timer: 2000
		//   });
		//   return false;
		// }
		return true;
	}

	async SignUp() {
		let {
			cardID,
			password,
			nameTh,
			nameEn,
			phone,
			dateBirth,
			dateIssue,
			dateExpiry,
			sex,
			address,
			congenital_disease,
			allergy,
			fileData,
			foreigner,
			foreignerID,
			nationality,
			religion,
			prefixTh,
			prefixEn,
			role,
			username,
			sub_role
		} = this.state;

		// let status = this.CheckInput({
		//   role,
		//   username,
		//   cardID,
		//   foreignerID,
		//   password,
		//   nameTh,
		//   nameEn,
		//   phone,
		//   dateBirth,
		//   dateIssue,
		//   dateExpiry,
		//   sex,
		//   address,
		//   congenital_disease,
		//   allergy,
		//   nationality,
		//   religion,
		//   foreigner,
		//   prefixTh,
		//   prefixEn,
		//   sub_role
		// });
		// if (!status) return;

		try {
			this.setState({ wait_loading: true });
			let formdata = new FormData();
			if (role !== 6) {
				formdata.append('username', username);
				formdata.append('citizen_id', cardID);
				formdata.append('th_prefix', prefixTh);
				formdata.append('th_name', nameTh.split(' ')[0]);
				formdata.append('th_lastname', nameTh.split(' ')[1]);
				if (role === 2 || role === 8) {
					formdata.append(
						'sub_role',
						JSON.stringify(role === 8 ? [ 'ThaiMassage' ] : role === 2 ? sub_role : [])
					);
				}
			} else {
				if (foreigner !== 1) {
					formdata.append('citizen_id', cardID);
					formdata.append('th_prefix', prefixTh);
					formdata.append('th_name', nameTh.split(' ')[0]);
					formdata.append('th_lastname', nameTh.split(' ')[1]);
				} else {
					formdata.append('citizen_id', foreignerID);
					formdata.append('th_prefix', prefixEn);
					formdata.append('th_name', nameEn.split(' ')[0]);
					formdata.append('th_lastname', nameEn.split(' ')[1]);
				}
			}
			formdata.append('en_prefix', prefixEn);
			formdata.append('en_name', nameEn.split(' ')[0]);
			formdata.append('en_lastname', nameEn.split(' ')[1]);
			formdata.append('foreigner', foreigner);
			formdata.append('password', password);
			formdata.append('role_id', role === 8 ? 2 : role);
			formdata.append('birthday', dateBirth);
			formdata.append('sex', sex);
			formdata.append('phone', phone);
			formdata.append('allergy', allergy);
			formdata.append('congenital_disease', congenital_disease);
			formdata.append('issue', dateIssue);
			formdata.append('expire', dateExpiry);
			formdata.append('address', address);
			formdata.append('fileData', fileData);
			formdata.append('nationality', nationality);
			formdata.append('religion', religion);

			let res = await POST(CREATE_USER, formdata, true);
			if (res.success) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', 'สมัครสมาชิกสำเร็จ', 'success', {
					buttons: false,
					timer: 2000
				}).then(() => this.props.history.push(User.role === 'admin' ? '/home' : '/home-admin'));
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	}

	handleChange = (event) => {
		let { fileData, file } = this.state;
		// console.log(event.target.files[0])
		if ((fileData !== null || file !== null) && event.target.files[0] === undefined) {
			this.setState({
				fileData: null,
				file: null
			});
		} else if ((fileData !== null || file !== null) && event.target.files[0] !== undefined) {
			this.setState({
				fileData: event.target.files[0],
				file: URL.createObjectURL(event.target.files[0])
			});
		} else {
			this.setState({
				fileData: event.target.files[0],
				file: URL.createObjectURL(event.target.files[0])
			});
		}
	};

	handleSelect = (event) => {
		let id = event.target.id;
		let checked = event.target.checked;
		let { sub_role } = this.state;
		if (checked) {
			sub_role.push(id);
		} else {
			var index = sub_role.indexOf(id);
			sub_role.splice(index, 1);
		}
		this.setState({
			sub_role: sub_role
		});
	};

	Upfirst = (str = '', i = 0) => {
		if (str.charAt(i) != ' ' && i == 0) {
			return this.Upfirst(str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
		} else if (str.charAt(i - 1) == ' ') {
			if (str.charAt(i) == ' ') {
				return this.Upfirst(str.slice(0, i), i);
			} else {
				return this.Upfirst(str.slice(0, i) + str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
			}
		} else if (str.length > i) {
			return this.Upfirst(str, i + 1);
		} else {
			return str;
		}
	};

	resetThai = () => {
		this.setState({
			foreigner: 0,
			cardID: '',
			foreignerID: '',
			password: '',
			prefixTh: '',
			prefixEn: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'male',
			address: '',
			file: null,
			fileData: null,
			nationality: '',
			religion: '',
			dumpCardData: [],
			isSignedUp: false
		});
	};

	resetForeigner = () => {
		this.setState({
			foreigner: 1,
			cardID: '',
			foreignerID: '',
			password: '',
			prefixTh: '',
			prefixEn: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'male',
			address: '',
			file: null,
			fileData: null,
			nationality: '',
			religion: '',
			dumpCardData: [],
			isSignedUp: false
		});
	};

	render() {
		let {
			roleData,
			cardID,
			nameTh,
			nameEn,
			phone,
			username,
			password,
			file,
			address,
			dateIssue,
			dateExpiry,
			dateBirth,
			sex,
			loading,
			role,
			foreigner,
			foreignerID,
			prefixEn,
			prefixTh,
			congenital_disease,
			allergy,
			nationality,
			religion,
			hn,
			reading_card,
			wait_loading
		} = this.state;
		// console.log('role',roleData)
		return (
			<div id="adduser-top" className="px-4 pb-4">
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<div className="d-flex mx-auto mt-3">
					{/* <Label className="btn-back top-label" onClick={() => this.props.history.goBack()}>
            {'< กลับ'}
          </Label> */}
				</div>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : reading_card ? (
					<div
						style={{
							width: '100%',
							height: '50vh',
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
							justifyContent: 'center'
						}}
					>
						<div className="label-back">กำลังอ่านข้อมูลจากบัตรประจำตัวประชาชน</div>
						<ReactLoading type={'bars'} color={Color.Blue} height={'auto'} width={'5vw'} />
					</div>
				) : (
					<Row>
						<Col sm={12} lg={{ size: 6, offset: 3 }} className="box-input-user">
							<Row className="px-4 pb-2 w-100">
								<div className="label-add-user">เพิ่มบุคคลากร</div>
							</Row>
							<Row className="px-4 pb-2 m-0 w-100">
								<Form className="w-100">
									<Default>
										<Row className="w-100">
											<Col md={7}>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label for="role" className="label-user-detail">
																ประเภท
															</Label>
															{/* <Label htmlFor="role" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Role of User</FormText>
														<Input
															id="role"
															type="select"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																this.setState({ role: Number(e.target.value) });
															}}
														>
															{roleData.map((el) => (
																<option value={el.role_id}>
																	{functions.getRole(el.role_name)}
																</option>
															))}
														</Input>
													</FormGroup>
													{role === 2 && (
														<FormGroup>
															<Label for="exampleCheckbox">ประเภทกายภาพ</Label>
															<Row>
																{subRole.map((el, i) => (
																	<Col xs={6}>
																		<CustomInput
																			onChange={this.handleSelect}
																			type="checkbox"
																			id={el.en_name}
																			label={el.name}
																		/>
																	</Col>
																))}
															</Row>
														</FormGroup>
													)}
												</Row>
												{role !== 6 ? (
													<Row className="mx-auto">
														<FormGroup style={{ width: '100%' }}>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label for="username" className="label-user-detail">
																	ชื่อผู้ใช้งาน
																</Label>
																{/* <Label htmlFor="username" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>Username</FormText>
															<Input
																id="username"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	this.setState({ username: e.target.value });
																}}
																value={!username ? '' : username}
															/>
														</FormGroup>
													</Row>
												) : (
													<div>
														<Row className="mx-auto">
															<FormGroup style={{ width: '100%' }}>
																<div style={{ display: 'flex', flexDirection: 'row' }}>
																	<label
																		check
																		htmlFor="cardID"
																		className="label-user-detail"
																	>
																		<input
																			type="radio"
																			name="radio2"
																			value={foreigner}
																			checked={foreigner !== 1 ? true : false}
																			onChange={this.resetThai}
																		/>
																		เลขประจำตัวประชาชน
																	</label>
																	{/* {foreigner !== 1 ? (
                                    <Label htmlFor="foreigner" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                      *
                                    </Label>
                                  ) : null} */}
																</div>
																<Input
																	id="cardID"
																	type="number"
																	className="input-user-detail"
																	style={{
																		fontSize: '1.3rem',
																		height: 'auto',
																		marginTop: 3
																	}}
																	onChange={(e) => {
																		if (e.target.value.length <= 13)
																			this.setState({
																				cardID: '' + e.target.value
																			});
																	}}
																	value={!cardID ? '' : cardID}
																	disabled={foreigner !== 1 ? false : true}
																/>
															</FormGroup>
														</Row>
														<Row className="mx-auto">
															<FormGroup style={{ width: '100%' }}>
																<div style={{ display: 'flex', flexDirection: 'row' }}>
																	<label
																		check
																		htmlFor="cardID"
																		className="label-user-detail"
																	>
																		<input
																			type="radio"
																			name="radio2"
																			value={foreigner}
																			checked={foreigner === 1 ? true : false}
																			onChange={this.resetForeigner}
																			// this.handleChange}
																		/>
																		เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
																	</label>
																	{/* {foreigner !== 0 ? (
                                    <Label htmlFor="foreigner" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                      *
                                    </Label>
                                  ) : null} */}
																</div>
																<Input
																	id="foreignerID"
																	className="input-user-detail"
																	style={{
																		fontSize: '1.3rem',
																		height: 'auto',
																		marginTop: 3
																	}}
																	onChange={(e) => {
																		if (
																			this.CheckForeignerIDInput(
																				e.target.value.charAt(
																					e.target.value.length - 1
																				)
																			)
																		) {
																			this.setState({
																				foreignerID: e.target.value.toUpperCase()
																			});
																		}
																	}}
																	readOnly={foreigner !== 1 ? true : false}
																	value={!foreignerID ? '' : foreignerID}
																/>
															</FormGroup>
														</Row>
													</div>
												)}
												{role != 6 && (
													<Row className="mx-auto">
														<FormGroup style={{ width: '100%' }}>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label htmlFor="password" className="label-user-detail">
																	รหัสผ่าน
																</Label>
																{/* <Label htmlFor="password" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>Password</FormText>
															<Input
																id="password"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	this.setState({ password: e.target.value });
																}}
																value={!password ? '' : password}
															/>
														</FormGroup>
													</Row>
												)}
											</Col>
											<Col md={5} className="text-center">
												<Label
													style={{
														cursor: 'pointer',
														position: 'relative',
														paddingTop: '5px',
														height: '14rem',
														width: '14rem'
													}}
													for="fileupload"
												>
													<Input
														style={{
															position: 'absolute',
															display: 'none',
															top: 0,
															left: 0
														}}
														id="fileupload"
														type="file"
														accept=".png, .jpg, .jpeg"
														onChange={(event) => this.handleChange(event)}
													/>
													{file !== '' && file ? (
														<div
															style={{
																cursor: 'pointer',
																width: '100%',
																height: '100%',
																backgroundImage: `url(${file})`,
																backgroundRepeat: 'no-repeat',
																backgroundPosition: 'center',
																borderRadius: 10,
																backgroundSize: '100% auto'
															}}
														/>
													) : (
														<div
															style={{
																width: '100%',
																color: '#fff',
																display: 'flex',
																justifyContent: 'center',
																alignItems: 'center',
																height: '100%',
																backgroundColor: '#ccc',
																borderRadius: 10
															}}
														>
															ไม่มีรูป
														</div>
													)}
												</Label>
											</Col>
										</Row>
										{role == 6 && (
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label htmlFor="password" className="label-user-detail">
															รหัสผ่าน
														</Label>
														{/* <Label htmlFor="password" style={{ color: 'red', marginLeft: '0.25vw' }}>
                              *
                            </Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>Password</FormText>
													<Input
														id="password"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															this.setState({ password: e.target.value });
														}}
														value={!password ? '' : password}
													/>
												</FormGroup>
											</Row>
										)}
										{role == 6 && foreigner == 1 ? (
											<Row>
												<Col sm={3}>
													<FormGroup>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label htmlFor="prefixEn" className="label-user-detail">
																Prefix
															</Label>
															{/* <Label htmlFor="prefixEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
														<Input
															id="prefixEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		prefixEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!prefixEn ? '' : prefixEn}
														/>
													</FormGroup>
												</Col>
												<Col sm={9}>
													<FormGroup>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label
																className="text-muted"
																htmlFor="nameEn"
																className="label-user-detail"
															>
																Name - Last name
															</Label>
															{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>
															Name - Last name (Eng)
														</FormText>
														<Input
															id="nameEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		nameEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!nameEn ? '' : nameEn}
														/>
													</FormGroup>
												</Col>
											</Row>
										) : (
											<div>
												<Row>
													<Col sm={3}>
														<FormGroup style={{ width: '100%' }}>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label htmlFor="prefixTh" className="label-user-detail">
																	คำนำหน้า
																</Label>
																{/* <Label htmlFor="prefixTh" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
															<Input
																id="prefixTh"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	if (
																		this.CheckLanThaiInput(
																			e.target.value.charAt(
																				e.target.value.length - 1
																			)
																		)
																	) {
																		this.setState({ prefixTh: e.target.value });
																	}
																}}
																value={!prefixTh ? '' : prefixTh}
															/>
														</FormGroup>
													</Col>
													<Col sm={9}>
														<FormGroup style={{ width: '100%' }}>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label for="nameTh" className="label-user-detail">
																	ชื่อ-นามสกุล
																</Label>
																{/* <Label htmlFor="nameTh" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>
																Name - Last name (Th)
															</FormText>
															<Input
																id="nameTh"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	if (
																		this.CheckLanThaiInput(
																			e.target.value.charAt(
																				e.target.value.length - 1
																			)
																		)
																	) {
																		this.setState({ nameTh: e.target.value });
																	}
																}}
																value={!nameTh ? '' : nameTh}
															/>
														</FormGroup>
													</Col>
												</Row>
												<Row>
													<Col sm={3}>
														<FormGroup>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label htmlFor="prefixEn" className="label-user-detail">
																	Prefix
																</Label>
																{/* <Label htmlFor="prefixEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
															<Input
																id="prefixEn"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	if (
																		this.CheckLanEngInput(
																			e.target.value.charAt(
																				e.target.value.length - 1
																			)
																		)
																	) {
																		this.setState({
																			prefixEn: this.Upfirst(e.target.value)
																		});
																	}
																}}
																value={!prefixEn ? '' : prefixEn}
															/>
														</FormGroup>
													</Col>
													<Col sm={9}>
														<FormGroup>
															<div style={{ display: 'flex', flexDirection: 'row' }}>
																<Label
																	className="text-muted"
																	htmlFor="nameEn"
																	className="label-user-detail"
																>
																	Name - Last name
																</Label>
																{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label> */}
															</div>
															<FormText style={{ marginTop: -10 }}>
																Name - Last name (Eng)
															</FormText>
															<Input
																id="nameEn"
																className="input-user-detail"
																style={{
																	fontSize: '1.3rem',
																	height: 'auto',
																	marginTop: 3
																}}
																onChange={(e) => {
																	if (
																		this.CheckLanEngInput(
																			e.target.value.charAt(
																				e.target.value.length - 1
																			)
																		)
																	) {
																		this.setState({
																			nameEn: this.Upfirst(e.target.value)
																		});
																	}
																}}
																value={!nameEn ? '' : nameEn}
															/>
														</FormGroup>
													</Col>
												</Row>
											</div>
										)}
									</Default>
									<Mobile>
										<Row className="text-center">
											{/* upload pic mobile */}
											<Col>
												<Label
													className="text-center"
													style={{
														cursor: 'pointer',
														position: 'relative',
														height: '14rem',
														width: '14rem'
													}}
													for="fileupload"
												>
													<Input
														style={{
															position: 'absolute',
															display: 'none',
															top: 0,
															left: 0
														}}
														id="fileupload"
														type="file"
														accept=".png, .jpg, .jpeg"
														onChange={(event) => this.handleChange(event)}
													/>
													{file && file !== '' ? (
														<div
															style={{
																width: '100%',
																height: '100%',
																backgroundImage: `url(${file})`,
																backgroundRepeat: 'no-repeat',
																backgroundPosition: 'center',
																borderRadius: 10,
																backgroundSize: '100% auto'
															}}
														/>
													) : (
														<div
															style={{
																color: '#fff',
																display: 'flex',
																justifyContent: 'center',
																alignItems: 'center',
																width: '100%',
																height: '100%',
																backgroundColor: '#ccc',
																borderRadius: 10
															}}
														>
															ไม่มีรูป
														</div>
													)}
												</Label>
											</Col>
										</Row>
										{/* <div> */}
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label for="role" className="label-user-detail">
														ประเภท
													</Label>
													{/* <Label htmlFor="role" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Role of User</FormText>
												<Input
													id="role"
													type="select"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ role: Number(e.target.value) });
													}}
												>
													{roleData.map((el) => (
														<option value={el.role_id}>{el.role_name}</option>
													))}
												</Input>
											</FormGroup>
											{role === 2 && (
												<FormGroup>
													<Label for="exampleCheckbox">ประเภทกายภาพ</Label>
													<Row>
														{subRole.map((el, i) => (
															<Col xs={6}>
																<CustomInput
																	onChange={this.handleSelect}
																	type="checkbox"
																	id={el.en_name}
																	label={el.name}
																/>
															</Col>
														))}
													</Row>
												</FormGroup>
											)}
										</Row>
										{role !== 6 ? (
											<Row className="mx-auto">
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label for="username" className="label-user-detail">
															ชื่อผู้ใช้งาน
														</Label>
														{/* <Label htmlFor="username" style={{ color: 'red', marginLeft: '0.25vw' }}>
                              *
                            </Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>Username</FormText>
													<Input
														id="username"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															this.setState({ username: e.target.value });
														}}
														value={!username ? '' : username}
													/>
												</FormGroup>
											</Row>
										) : (
											<div>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<label check htmlFor="cardID" className="label-user-detail">
																<input
																	type="radio"
																	name="radio2"
																	value={foreigner}
																	checked={foreigner !== 1 ? true : false}
																	onChange={this.resetThai}
																	// this.handleChange}
																/>
																เลขประจำตัวประชาชน
															</label>
															{/* {foreigner !== 1 ? (
                                <Label htmlFor="foreigner" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label>
                              ) : null} */}
														</div>
														<Input
															id="cardID"
															type="number"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (e.target.value.length <= 13)
																	this.setState({ cardID: '' + e.target.value });
															}}
															value={!cardID ? '' : cardID}
															disabled={foreigner !== 1 ? false : true}
														/>
													</FormGroup>
												</Row>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<label check htmlFor="cardID" className="label-user-detail">
																<input
																	type="radio"
																	name="radio2"
																	value={foreigner}
																	checked={foreigner === 1 ? true : false}
																	onChange={this.resetForeigner}
																	// this.handleChange}
																/>
																เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
															</label>
															{/* {foreigner !== 0 ? (
                                <Label htmlFor="foreigner" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                  *
                                </Label>
                              ) : null} */}
														</div>
														<Input
															id="foreignerID"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckForeignerIDInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		foreignerID: e.target.value.toUpperCase()
																	});
																}
															}}
															readOnly={foreigner !== 1 ? true : false}
															value={!foreignerID ? '' : foreignerID}
														/>
													</FormGroup>
												</Row>
											</div>
										)}
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="password" className="label-user-detail">
														รหัสผ่าน
													</Label>
													{/* <Label htmlFor="password" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="password"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ password: e.target.value });
													}}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Row>
										{/* </div> */}
										{role == 6 && foreigner == 1 ? (
											<div>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label htmlFor="prefixEn" className="label-user-detail">
																Prefix
															</Label>
															{/* <Label htmlFor="prefixEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
														<Input
															id="prefixEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		prefixEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!prefixEn ? '' : prefixEn}
														/>
													</FormGroup>
												</Row>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label
																className="text-muted"
																htmlFor="nameEn"
																className="label-user-detail"
															>
																Name - Last name
															</Label>
															{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>
															Name - Last name (Eng)
														</FormText>
														<Input
															id="nameEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		nameEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!nameEn ? '' : nameEn}
														/>
													</FormGroup>
												</Row>
											</div>
										) : (
											<div>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label htmlFor="prefixTh" className="label-user-detail">
																คำนำหน้า
															</Label>
															{/* <Label htmlFor="prefixTh" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
														<Input
															id="prefixTh"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanThaiInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({ prefixTh: e.target.value });
																}
															}}
															value={!prefixTh ? '' : prefixTh}
														/>
													</FormGroup>
												</Row>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label for="nameTh" className="label-user-detail">
																ชื่อ-นามสกุล
															</Label>
															{/* <Label htmlFor="nameTh" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>
															Name - Last name (Th)
														</FormText>
														<Input
															id="nameTh"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanThaiInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({ nameTh: e.target.value });
																}
															}}
															value={!nameTh ? '' : nameTh}
														/>
													</FormGroup>
												</Row>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label htmlFor="prefixEn" className="label-user-detail">
																Prefix
															</Label>
															{/* <Label htmlFor="prefixEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
														<Input
															id="prefixEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		prefixEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!prefixEn ? '' : prefixEn}
														/>
													</FormGroup>
												</Row>
												<Row className="mx-auto">
													<FormGroup style={{ width: '100%' }}>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label
																className="text-muted"
																htmlFor="nameEn"
																className="label-user-detail"
															>
																Name - Last name
															</Label>
															{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>
															Name - Last name (Eng)
														</FormText>
														<Input
															id="nameEn"
															className="input-user-detail"
															style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
															onChange={(e) => {
																if (
																	this.CheckLanEngInput(
																		e.target.value.charAt(e.target.value.length - 1)
																	)
																) {
																	this.setState({
																		nameEn: this.Upfirst(e.target.value)
																	});
																}
															}}
															value={!nameEn ? '' : nameEn}
														/>
													</FormGroup>
												</Row>
											</div>
										)}
									</Mobile>

									{/* -------- bottom -------- */}

									<Row>
										{role !== 6 && (
											<Col sm>
												<FormGroup style={{ width: '100%' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label htmlFor="cardID" className="label-user-detail">
															เลขประจำตัวประชาชน
														</Label>
														{/* <Label htmlFor="cardID" style={{ color: 'red', marginLeft: '0.25vw' }}>
                              *
                            </Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>
														Identification Number
													</FormText>
													<Input
														id="cardID"
														type="number"
														className="input-user-detail"
														style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
														onChange={(e) => {
															if (e.target.value.length <= 13)
																this.setState({ cardID: '' + e.target.value });
														}}
														value={!cardID ? '' : cardID}
													/>
												</FormGroup>
											</Col>
										)}

										<Col sm>
											<FormGroup>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="phone" className="label-user-detail">
														เบอร์โทรศัพท์
													</Label>
													{/* <Label htmlFor="phone" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Phone Number</FormText>
												<Input
													id="phone"
													type="number"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (e.target.value.length <= 10)
															this.setState({ phone: '' + e.target.value });
													}}
													value={!phone ? '' : phone}
												/>
											</FormGroup>
										</Col>
									</Row>
									<Row>
										<Col sm>
											<FormGroup>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="dateBirth" className="label-user-detail">
														เกิดวันที่
													</Label>
													{/* <Label htmlFor="dateBirth" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Date of birth</FormText>
												<Input
													type="date"
													id="dateBirth"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ dateBirth: e.target.value });
													}}
													value={!dateBirth ? '' : dateBirth}
												/>
											</FormGroup>
										</Col>
										<Col sm>
											<FormGroup>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="sex" className="label-user-detail">
														เพศ
													</Label>
													{/* <Label htmlFor="sex" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Sex</FormText>
												<Input
													id="sex"
													type="select"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ sex: e.target.value });
													}}
													value={!sex ? '' : sex}
												>
													<option value="male">ชาย</option>
													<option value="female">หญิง</option>
												</Input>
											</FormGroup>
										</Col>
									</Row>
									<Row>
										<Col sm>
											<FormGroup>
												{foreigner !== 0 ? (
													<div
														style={{
															width: '30%',
															display: 'flex',
															flexDirection: 'column'
														}}
													>
														<div style={{ display: 'flex', flexDirection: 'row' }}>
															<Label htmlFor="nationality" className="label-user-detail">
																สัญชาติ
															</Label>
															{/* <Label htmlFor="nationality" style={{ color: 'red', marginLeft: '0.25vw' }}>
                                *
                              </Label> */}
														</div>
														<FormText style={{ marginTop: -10 }}>Nationality</FormText>
													</div>
												) : (
													<div>
														<Label htmlFor="nationality" className="label-user-detail">
															สัญชาติ
														</Label>
														<FormText style={{ marginTop: -10 }}>Nationality</FormText>
													</div>
												)}
												<Input
													id="nationality"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														if (
															this.CheckLanEngInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({
																nationality: this.Upfirst(e.target.value)
															});
														}
													}}
													value={foreigner !== 1 ? 'Thai' : !nationality ? '' : nationality}
												/>
											</FormGroup>
										</Col>
										<Col sm>
											<FormGroup>
												<Label htmlFor="religion" className="label-user-detail">
													ศาสนา
												</Label>
												<FormText style={{ marginTop: -10 }}>Religion</FormText>
												<Input
													id="religion"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														// if (this.CheckBothLanInput(e.target.value.charAt(e.target.value.length - 1))) {
														this.setState({ religion: this.Upfirst(e.target.value) });
														// }
													}}
													value={!religion ? '' : religion}
												/>
											</FormGroup>
										</Col>
									</Row>
									<Row>
										<Col sm>
											<FormGroup>
												<Label htmlFor="dateIssue" className="label-user-detail">
													วันออกบัตร
												</Label>
												<FormText style={{ marginTop: -10 }}>Date of issue</FormText>
												<Input
													type="date"
													id="dateIssue"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ dateIssue: e.target.value });
													}}
													value={!dateIssue ? '' : dateIssue}
												/>
											</FormGroup>
										</Col>
										<Col sm>
											<FormGroup>
												<Label htmlFor="dateExpiry" className="label-user-detail">
													วันหมดอายุบัตร
												</Label>
												<FormText style={{ marginTop: -10 }}>Date of Expiry</FormText>
												<Input
													type="date"
													id="dateExpiry"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: '3rem', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ dateExpiry: e.target.value });
													}}
													value={!dateExpiry ? '' : dateExpiry}
												/>
											</FormGroup>
										</Col>
									</Row>

									{role !== 6 ? (
										<Row className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="address" className="label-user-detail">
														ที่อยู่
													</Label>
													{/* <Label htmlFor="address" style={{ color: 'red', marginLeft: '0.25vw' }}>
                            *
                          </Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Address</FormText>
												<Input
													type="textarea"
													id="address"
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={(e) => {
														this.setState({ address: this.Upfirst(e.target.value) });
													}}
													value={!address ? '' : address}
												/>
											</FormGroup>
										</Row>
									) : (
										<div>
											<FormGroup
												style={{
													display: 'flex',
													flexDirection: 'row',
													justifyContent: 'space-between',
													alignItems: 'center',
													marginBottom: '1rem'
												}}
											>
												<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
													<div style={{ display: 'flex', flexDirection: 'row' }}>
														<Label htmlFor="address" className="label-user-detail">
															ที่อยู่
														</Label>
														{/* <Label htmlFor="address" style={{ color: 'red', marginLeft: '0.25vw' }}>
                              *
                            </Label> */}
													</div>
													<FormText style={{ marginTop: -10 }}>Address</FormText>
												</div>
												<Col style={{ width: '70%', paddingRight: 0 }}>
													<Input
														type="textarea"
														id="address"
														className="input-user-detail"
														style={{ fontSize: '1.3rem' }}
														onChange={(e) => {
															this.setState({ address: this.Upfirst(e.target.value) });
														}}
														value={!address ? '' : address}
													/>
												</Col>
											</FormGroup>
											<FormGroup
												style={{
													display: 'flex',
													flexDirection: 'row',
													justifyContent: 'space-between',
													alignItems: 'center',
													marginBottom: '1rem'
												}}
											>
												<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
													<Label htmlFor="congenital" className="label-user-detail">
														โรคประจำตัว
													</Label>
													<FormText style={{ marginTop: -10 }}>Congenital Disease</FormText>
												</div>
												<Col style={{ width: '70%', paddingRight: 0 }}>
													<Input
														type="textarea"
														id="congenital"
														className="input-user-detail"
														style={{ fontSize: '1.3rem' }}
														onChange={(e) => {
															this.setState({ congenital_disease: e.target.value });
														}}
														value={!congenital_disease ? '' : congenital_disease}
													/>
												</Col>
											</FormGroup>
											<FormGroup
												style={{
													display: 'flex',
													flexDirection: 'row',
													justifyContent: 'space-between',
													alignItems: 'center',
													marginBottom: '1rem'
												}}
											>
												<div style={{ width: '30%', display: 'flex', flexDirection: 'column' }}>
													<Label htmlFor="allergy" className="label-user-detail">
														ยาที่แพ้
													</Label>
													<FormText style={{ marginTop: -10 }}>Drug Allergy</FormText>
												</div>
												<Col style={{ width: '70%', paddingRight: 0 }}>
													<Input
														type="textarea"
														id="allergy"
														className="input-user-detail"
														style={{ fontSize: '1.3rem' }}
														onChange={(e) => {
															this.setState({ allergy: e.target.value });
														}}
														value={!allergy ? '' : allergy}
													/>
												</Col>
											</FormGroup>
										</div>
									)}
									<div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '2rem' }}>
										<button
											type="button"
											onClick={() => this.SignUp()}
											className="btn-form btn-add-user"
										>
											+ เพิ่มบุคคลากร
										</button>
									</div>
								</Form>
							</Row>
						</Col>
					</Row>
				)}
			</div>
		);
	}
}
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
