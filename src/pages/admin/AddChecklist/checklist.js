import React, { Component } from 'react';
import moment from 'moment';
import ReactLoading from 'react-loading';
import { withRouter, Link } from 'react-router-dom';
import { Row, Col, Table, Form, FormGroup, Label, Input } from 'reactstrap';
import { GET, GET_USERS, GET_ROLE, GET_TREATMENT } from '../../../service/service';
import Color from '../../../components/Color';
import InputWithText from '../../../components/InputWithText/InputWithText';
import Pagination from '../../../components/Pagination';
import Modal from 'react-modal';
import { IoMdClose } from 'react-icons/io';
import swal from 'sweetalert';
import { Animated } from 'react-animated-css';

let numberPage = [ 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
let head = [
	'เลขกรมบัญชีกลาง',
	'ชื่อรายการตรวจ',
	'รายละเอียด',
	'ประเภท',
	'ราคา (บาท)',
	'ราคาที่เบิกได้ (บาท)',
	'ค่าตอบแทน Neuro(%)',
	'ค่าตอบแทน Ortho(%)'
];

export default class checklist extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ListUserShow: [],

			role: [],

			type: [],
			loading: true,

			searchTextUser: '',

			currentPage: 1,
			pageSize: 10,
			isOpenModal: false
		};
	}
	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		// this.props.onRef(this);
		await this.GetUsers();
		await this.setState({ loading: false });
	}
	GetUsers = async () => {
		let USER = JSON.parse(localStorage.getItem('USER'));
		let id =
			USER.role == 'frontPT' || USER.role == 'pt'
				? 'pt'
				: USER.role == 'frontMT' || USER.role == 'mt' ? 'mt' : 'all';
		try {
			let res = await GET(GET_TREATMENT(id));
			// console.log('object', res);
			this.setState({ ListUserShow: res.result });
		} catch (error) {
			// console.log(error);
		}
	};

	searchData = async (e) => {
		try {
			let res = await GET(GET_TREATMENT('all'));
			// console.log('res', res);
			let texts = e.toLowerCase();
			let data = res.result
				.map((e) => ({
					...e,
					types:
						e.type === 'mt' ? 'บริการตรวจสุขภาพ' : e.type === 'pt' ? 'บริการกายภาพบำบัด' : 'บริการนวดไทย',
					dis: e.type === 'mt' ? e.disburseable : e.type === 'pt' ? 0 : e.disburseable
				}))
				.filter(
					(e, i) =>
						String(e.cgd_code).toLowerCase().indexOf(texts) > -1 ||
						String(e.treatment_name).toLowerCase().indexOf(texts) > -1 ||
						String(e.detail).toLowerCase().indexOf(texts) > -1 ||
						String(e.cost).toLowerCase().indexOf(texts) > -1 ||
						String(e.types).toLowerCase().indexOf(texts) > -1 ||
						String(e.dis).toLowerCase().indexOf(texts) > -1
				);
			// console.log('data', data);
			this.setState({ ListUserShow: data, currentPage: 1 });
		} catch (error) {
			// console.log(error);
		}
	};

	reRender() {
		this.componentWillMount();
	}

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { ListUserShow, loading, currentPage, pageSize, searchTextUser, isOpenModal } = this.state;
		return (
			<div className="row mx-auto" style={{ fontSize: '1.4rem' }}>
				<div className="col-md-12">
					<div>
						<div>
							<div className="d-flex justify-content-between align-items-center my-2">
								<div className="LabelTodayTable">รายการตรวจ</div>
								<div
									className="btn-all-table"
									onClick={() =>
										this.props.history.push({
											pathname: '/checklist/addchecklist',
											state: { id: false }
										})}
								>
									เพิ่มรายการตรวจ
								</div>
							</div>
							<Row>
								<Col xs={4} />
								<Col xs={4} />
								<Col xs={4}>
									<InputWithText
										name="searchTextUser"
										text="ค้นหา"
										onChange={(e) => {
											this.searchData(e.target.value);
											this.setState({ searchTextUser: e.target.value });
										}}
										value={searchTextUser}
									/>
								</Col>
							</Row>
							{loading ? (
								<div className="loading d-flex flex-column">
									<ReactLoading
										type={'spinningBubbles'}
										color={Color.Blue}
										height={'auto'}
										width={'5vw'}
									/>
									<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>
										กำลังโหลดข้อมูล
									</Label>
								</div>
							) : ListUserShow.length !== 0 ? (
								<div>
									<Table style={{ whiteSpace: 'pre' }} responsive striped>
										<thead>
											{head && (
												<tr>{head.map((el, index) => <th key={'a' + index}>{el}</th>)}</tr>
											)}
										</thead>
										<tbody>
											{ListUserShow.slice(
												(currentPage - 1) * pageSize,
												currentPage * pageSize
											).map((el, index) => (
												<tr
													key={'b' + index}
													className="card-history-table-hover"
													onClick={() =>
														this.props.history.push({
															pathname: '/checklist/addchecklist',
															state: { id: el.id }
														})}
												>
													<td>{!el.cgd_code ? '-' : el.cgd_code}</td>
													<td>{!el.treatment_name ? '-' : el.treatment_name}</td>
													<td>{!el.detail ? '-' : el.detail}</td>
													<td>
														{el.type === 'mt' ? (
															'บริการตรวจสุขภาพ'
														) : el.type === 'pt' ? (
															'บริการกายภาพบำบัด'
														) : (
															'บริการนวดไทย'
														)}
													</td>
													<td>{el.cost.toLocaleString()}</td>
													<td>{el.disburseable.toLocaleString()}</td>
													<td>{!el.pay_neuro ? '-' : el.pay_neuro}</td>
													<td>{!el.pay_ortho ? '-' : el.pay_ortho}</td>
												</tr>
											))}
										</tbody>
									</Table>
									<div className="d-flex justify-content-between">
										<select
											className="select-page-size"
											name="pageSize"
											onChange={async (e) => {
												await this.setState({ [e.target.name]: +e.target.value });
											}}
										>
											{numberPage.map((el, i) => (
												<option key={i} value={el}>
													{el}
												</option>
											))}
										</select>
										<Pagination
											totalRecords={ListUserShow.length}
											pageLimit={pageSize}
											pageNeighbours={2}
											onPageChanged={this.onPageChanged}
										/>
									</div>
								</div>
							) : (
								<div className="EmptyListShow">---ไม่พบข้อมูล---</div>
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
