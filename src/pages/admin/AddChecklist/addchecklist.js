import React, { Component } from 'react';
import { Col, Row, Form, FormText, FormGroup, Label, CustomInput, Input } from 'reactstrap';
import Responsive from 'react-responsive';
import {
	GET,
	GET_ROLE,
	POST,
	CREATE_USER,
	GET_USERS,
	CREATE_TREATMENT,
	GET_TREATMENT,
	DELETE_TREATMENT,
	UPDATE_TREATMENT
} from '../../../service/service';
import swal from 'sweetalert';
import ReactLoading from 'react-loading';
import socketIOClient from 'socket.io-client';
import moment from 'moment';
import Modal from 'react-modal';
import Color from '../../../components/Color';

let inputType = [
	{
		label: 'เลขกรมบัญชีกลาง',
		id: 'เลขกรมบัญชีกลาง',
		type: 'number',
		name: 'cgd_code',
		hidden: true,
		value: 'cgd_code'
	},
	{
		label: 'ชื่อรายการตรวจ',
		id: 'ชื่อรายการตรวจ',
		type: 'text',
		name: 'treatment_name',
		hidden: true,
		value: 'treatment_name'
	},
	{
		label: 'ชื่อย่อรายการตรวจ',
		id: 'ชื่อย่อรายการตรวจ',
		type: 'text',
		name: 'treatment_name_mobile',
		hidden: false,
		value: 'treatment_name_mobile'
	},
	{
		label: 'รายละเอียด',
		id: 'รายละเอียด',
		type: 'textarea',
		name: 'detail',
		hidden: true,
		value: 'detail'
	},
	{
		label: 'ราคา (บาท)',
		id: 'ราคา',
		type: 'number',
		name: 'cost',
		hidden: true,
		value: 'cost'
	},
	{
		label: 'ราคาที่เบิกได้ (บาท)',
		id: 'ราคาที่เบิกได้',
		type: 'number',
		name: 'disburseable',
		hidden: true,
		value: 'disburseable'
	},
	{
		label: 'ค่าตอบแทน Neuro(%)',
		id: 'Neuro',
		type: 'number',
		name: 'pay_neuro',
		hidden: false,
		value: 'pay_neuro'
	},
	{
		label: 'ค่าตอบแทน Ortho(%)',
		id: 'Ortho',
		type: 'number',
		name: 'pay_ortho',
		hidden: false,
		value: 'pay_ortho'
	}
];
let customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

export default class addchecklist extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			radio: 1,
			wait_loading: false,
			news: true,
			to_edit_data: false,
			dis: true,
			cgd_code: '',
			treatment_name: '',
			treatment_name_mobile: '',
			detail: '',
			cost: '',
			disburseable: '',
			pay_neuro: '',
			pay_ortho: ''
		};
	}

	async componentDidMount() {
		await this.getData();
		await this.setState({ loading: false });
	}
	async getData() {
		let { id } = this.props.location.state;
		if (id !== false) {
			// console.log('id', id);
			try {
				this.setState({ wait_loading: true, news: false });
				let res = await GET(GET_TREATMENT(id));
				if (res.success) {
					this.setState({
						wait_loading: false,
						cgd_code: res.result[0].cgd_code,
						treatment_name: res.result[0].treatment_name,
						treatment_name_mobile: res.result[0].treatment_name_mobile,
						detail: res.result[0].detail,
						cost: res.result[0].cost,
						disburseable: res.result[0].disburseable,
						pay_neuro: res.result[0].pay_neuro,
						pay_ortho: res.result[0].pay_ortho,
						radio: res.result[0].type === 'mt' ? 1 : res.result[0].type === 'pt' ? 2 : 3
					});
				}
				// console.log('res', res);
			} catch (error) {
				this.setState({ wait_loading: false, news: false });
				// console.log(error);
			}
		}
	}
	inputText = (e) => {
		// console.log('e.target.name', e.target.name, 'e.target.value', e.target.value);
		this.setState({ [e.target.name]: e.target.value });
	};
	Submit = async () => {
		let {
			cgd_code,
			treatment_name,
			detail,
			cost,
			disburseable,
			pay_neuro,
			pay_ortho,
			radio,
			treatment_name_mobile
		} = this.state;
		let obj = {
			cgd_code,
			treatment_name,
			treatment_name_mobile,
			detail,
			cost,
			disburseable,
			pay_neuro,
			pay_ortho,
			type: Number(radio) === 1 ? 'mt' : Number(radio) === 2 ? 'pt' : 'tm'
		};
		if (cgd_code && treatment_name && detail && cost && disburseable) {
			try {
				this.setState({ wait_loading: true });
				let res = await POST(CREATE_TREATMENT, obj);
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', 'เพิ่มรายการตรวจสำเร็จ', 'success', {
						buttons: false,
						timer: 2000
					}).then(() => this.props.history.push('/checklist'));
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 3000
					});
				}
			} catch (error) {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', 'Network Error', 'error', {
					buttons: false,
					timer: 2000
				});
			}
		}
	};
	updateTreatment = async () => {
		let { id } = this.props.location.state;
		let {
			cgd_code,
			treatment_name,
			detail,
			cost,
			disburseable,
			pay_neuro,
			pay_ortho,
			radio,
			treatment_name_mobile
		} = this.state;
		let obj = {
			cgd_code,
			treatment_name,
			treatment_name_mobile,
			detail,
			cost,
			disburseable,
			pay_neuro,
			pay_ortho,
			type: Number(radio) === 1 ? 'mt' : Number(radio) === 2 ? 'pt' : 'tm'
		};
		// console.log('obj', obj);
		try {
			this.setState({ wait_loading: true });
			let res = await POST(UPDATE_TREATMENT(id), obj);
			if (res.success) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', 'แก้ไขรายการตรวจสำเร็จ', 'success', {
					buttons: false,
					timer: 2000
				}).then(() => this.props.history.push('/checklist'));
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};

	deleteTreatment = async () => {
		swal('ลบรายการตรวจ', 'ลบรายการตรวจนี้ใช่หรือไม่', 'warning', {
			buttons: {
				cancel: 'ยกเลิก',
				confirm: {
					text: 'ยืนยัน',
					value: 'confirm'
				}
			}
		}).then((value) => {
			switch (value) {
				case 'confirm':
					this.confirmDelete();
					break;
			}
		});
	};
	confirmDelete = async () => {
		let { id } = this.props.location.state;
		try {
			this.setState({ wait_loading: true });
			let res = await GET(DELETE_TREATMENT(id));
			if (res.success) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', 'ลบรายการตรวจสำเร็จ', 'success', {
					buttons: false,
					timer: 2000
				}).then(() => this.props.history.push('/checklist'));
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};

	render() {
		let { radio, wait_loading, news, to_edit_data, dis, loading } = this.state;
		let { id } = this.props.location.state;
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : (
			<div id="adduser-top" className="px-4 pb-4">
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Row>
					<Col sm={12} lg={{ size: 6, offset: 3 }} className="box-input-user">
						<Row className="px-4 pb-2 w-100">
							<div className="label-add-user">เพิ่มรายการตรวจ</div>
						</Row>
						<Row>
							<Col sm>
								<FormGroup style={{ width: '100%' }}>
									<Label className="label-user-detail">ประเภท</Label>
									<div className="label-user-detail" style={{ paddingLeft: '1rem' }}>
										<CustomInput
											type="radio"
											id="CustomRadio"
											name="radio"
											label="บริการตรวจสุขภาพ"
											checked={Number(radio) === 1}
											onChange={this.inputText}
											value={1}
											disabled={id}
										/>
										<CustomInput
											type="radio"
											id="CustomRadio2"
											name="radio"
											label="บริการกายภาพบำบัด"
											checked={Number(radio) === 2}
											onChange={this.inputText}
											value={2}
											disabled={id}
										/>
										<CustomInput
											type="radio"
											id="CustomRadio3"
											name="radio"
											label="บริการนวดไทย"
											checked={Number(radio) === 3}
											onChange={this.inputText}
											value={3}
											disabled={id}
										/>
									</div>
								</FormGroup>
							</Col>
						</Row>
						{inputType.map((e) => {
							let states = this.state;
							return (Number(radio) === 1 || Number(radio) === 3) && e.hidden === true ? (
								<Row>
									<Col sm>
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label className="label-user-detail">{e.label}</Label>
											</div>
											<Input
												name={e.name}
												id={e.id}
												type={e.type}
												className="input-user-detail"
												style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
												onChange={this.inputText}
												value={states[e.value]}
												disabled={id && dis}
											/>
										</FormGroup>
									</Col>
								</Row>
							) : (
								Number(radio) === 2 && (
									<Row>
										<Col sm>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label className="label-user-detail">{e.label}</Label>
												</div>
												<Input
													name={e.name}
													id={e.id}
													type={e.type}
													className="input-user-detail"
													style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
													onChange={this.inputText}
													value={states[e.value]}
													disabled={id && dis}
												/>
											</FormGroup>
										</Col>
									</Row>
								)
							);
						})}
						{news ? (
							<div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '2rem' }}>
								<button type="button" onClick={() => this.Submit()} className="btn-form btn-add-user">
									+ เพิ่มรายการตรวจ
								</button>
							</div>
						) : (
							<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
								<div
									style={{
										color: 'white',
										textDecoration: 'none',
										backgroundColor: Color.Red,
										marginRight: '1vw'
									}}
									className="btn-form"
									onClick={() => this.deleteTreatment()}
								>
									ลบรายการ
								</div>
								<div
									style={{ color: 'white', textDecoration: 'none' }}
									className="btn-form"
									onClick={
										to_edit_data ? (
											() => this.updateTreatment()
										) : (
											() => this.setState({ to_edit_data: true, dis: false })
										)
									}
								>
									{to_edit_data ? 'บันทึก' : 'แก้ไขข้อมูล'}
								</div>
							</div>
						)}
					</Col>
				</Row>
			</div>
		);
	}
}
