import React, { Component } from 'react';
import { Col, Row, Form, FormGroup, Label, Input, FormText, CustomInput } from 'reactstrap';
import Responsive from 'react-responsive';
import Color from '../../../components/Color';
import {
	POST,
	ip,
	GET_PICTURE,
	GET,
	GET_USER_DETAIL,
	UPDATE_USER_PROFILE,
	DELETE_USER,
	UNDELETE_USER,
	GET_ROLE
} from '../../../service/service';
import swal from 'sweetalert';
import ReactLoading from 'react-loading';
import Modal from 'react-modal';
import Barcode from 'react-barcode';
import moment from 'moment';
import User from '../../../mobx/user/user';
import { subRole } from '../../../const/subRole';
import functions from '../../../function/function';
let th = require('moment/locale/th');
moment.updateLocale('th', th);

const Mobile = (props) => <Responsive {...props} maxWidth={767} />;
const Default = (props) => <Responsive {...props} minWidth={768} />;

const Max = (props) => <Responsive {...props} maxWidth={364} />;
const Min = (props) => <Responsive {...props} minWidth={365} />;

export default class MemberDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			roleData: [],
			sub_role: [],
			isSignedUp: false,
			// if signedUp will get user_id
			user_id: null,

			username: '',
			password: '',
			cardID: '',
			nameTh: '',
			nameEn: '',
			phone: '',
			dateBirth: '',
			dateIssue: '',
			dateExpiry: '',
			sex: 'ชาย',
			address: '',
			congenital_disease: '', // โรคประจำตัว
			allergy: '', // แพ้ยา
			fileData: null, // for send picture to formData
			file: null, // for display picture
			foreigner: 0,
			foreignerID: '',
			nationality: '',
			religion: '',
			prefixTh: '',
			prefixEn: '',
			role: 0,

			to_edit_data: false,
			loading: true,
			wait_loading: false
		};
	}
	async componentWillMount() {
		let { onRef, isCustomer } = this.props;
		if (onRef) onRef(this);
		if (isCustomer !== -1) {
			await this.setState({ isSignedUp: true, user_id: isCustomer });
			await this.getCustomerData();
			await this.setState({ loading: false });
		}
		await this.getRole();
	}

	async getCustomerData() {
		let { id } = this.props.match.params;
		try {
			let res = await GET(GET_USER_DETAIL(id));
			let d = res.result[0];
			this.setState({
				username: d.username,
				role: d.sub_role[0] === 'ThaiMassage' ? 8 : d.role_id,
				sub_role: d.sub_role,
				cardID: d.citizen_id && d.citizen_id !== 'null' ? d.citizen_id : '',
				foreigner: d.foreigner,
				foreignerID: d.foreigner === 1 ? d.citizen_id : '',
				prefixTh: d.th_prefix,
				prefixEn: d.en_prefix,
				nameTh: d.th_name + ' ' + d.th_lastname,
				nameEn: d.en_name + ' ' + d.en_lastname,
				phone: d.phone && d.phone !== 'null' ? d.phone : '',
				dateBirth: d.birthday ? d.birthday.split('T')[0] : '',
				dateIssue: d.issue ? d.issue.split('T')[0] : '',
				dateExpiry: d.expire ? d.expire.split('T')[0] : '',
				sex: d.sex == 'หญิง' ? 'female' : 'male' || '',
				hn: d.hn,
				religion: d.religion,
				nationality: d.nationality,
				address: d.address && d.address !== 'null' ? d.address : '',
				congenital_disease: d.congenital_disease && d.congenital_disease !== 'null' ? d.congenital_disease : '',
				allergy: d.allergy && d.allergy !== 'null' ? d.allergy : '',
				isDisable: d.isDisable && d.isDisable !== 'null' ? d.isDisable : '',
				file: ip + GET_PICTURE(d.picture || 'null')
			});
			// console.log("pic : ", d.picture);
		} catch (error) {
			// console.log("error getCustomerData : ", error);
		}
	}

	getRole = async () => {
		await GET(GET_ROLE).then((res) => {
			let data = res.result;
			let moxdata = {
				role_id: 8,
				role_name: 'tm'
			};
			data.push(moxdata);
			this.setState({ roleData: data });
		});
	};
	returnState() {
		let { user_id } = this.state;
		return {
			user_id
		};
	}

	CheckLanThaiInput(text) {
		let alpTh = ' กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮะาิีึืุูเะเแะแโะโำไใเาฤฤๅฦฦๅ่้๊๋์ฯํ็';
		if (alpTh.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckLanEngInput(text) {
		let alpEng = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if (alpEng.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckForeignerIDInput(text) {
		let alpForeigner = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		if (alpForeigner.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckBothLanInput(text) {
		let alpLan =
			' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZกขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮะาิีึืุูเะเแะแโะโำไใเาฤฤๅฦฦๅ่้๊๋์ฯํ็';
		if (alpLan.indexOf(text) !== -1) {
			return true;
		}
		return false;
	}

	CheckInput(d) {
		if (!d.nameTh || d.nameTh === '') {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.nameTh.split(' ')[1] === undefined || d.nameTh.split(' ')[1] === '') {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาไทย', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (!d.nameEn || d.nameEn === '') {
			swal('ผิดพลาด!', 'กรุณากรอกชื่อภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.nameEn.split(' ')[1] === undefined || d.nameEn.split(' ')[1] === '') {
			swal('ผิดพลาด!', 'กรุณากรอกนามสกุลภาษาอังกฤษ', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (!d.phone || d.phone === '') {
			swal('ผิดพลาด!', 'กรุณากรอกหมายเลขโทรศัพท์', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (d.cardID && d.cardID.length != 13) {
			swal('ผิดพลาด!', 'เลขบัตรประจำตัวประชาชนต้องมี 13 หลักเท่านั้น', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		if (!d.address || d.address === '') {
			swal('ผิดพลาด!', 'กรุณากรอกที่อยู่', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		} else if (d.address.length < 10) {
			swal('ผิดพลาด!', 'ที่อยู่สั้นเกินไป กรุณากรอกให้มากกว่า 10 ตัวอักษร', 'error', {
				buttons: false,
				timer: 2000
			});
			return false;
		}
		return true;
	}

	async UpdateProfile(response) {
		let { id } = this.props.match.params;
		let {
			cardID,
			nameTh,
			nameEn,
			phone,
			dateBirth,
			dateIssue,
			dateExpiry,
			sex,
			address,
			congenital_disease,
			allergy,
			fileData,
			foreigner,
			foreignerID,
			nationality,
			religion,
			prefixTh,
			prefixEn,
			username,
			password,
			role,
			sub_role
		} = this.state;

		// let status = this.CheckInput({
		// 	nameTh,
		// 	nameEn,
		// 	phone,
		// 	dateBirth,
		// 	dateIssue,
		// 	dateExpiry,
		// 	sex,
		// 	address,
		// 	congenital_disease,
		// 	allergy,
		// 	cardID,
		// 	foreigner,
		// 	foreignerID,
		// 	nationality,
		// 	religion,
		// 	prefixTh,
		// 	prefixEn
		// });

		// if (!status) return;

		try {
			this.setState({ wait_loading: true });
			let formdata = new FormData();
			if (foreigner !== 1) {
				formdata.append('citizen_id', cardID);
				formdata.append('th_prefix', prefixTh);
				formdata.append('th_name', nameTh.split(' ')[0] || ' ');
				formdata.append('th_lastname', nameTh.split(' ')[1] || ' ');
			} else {
				formdata.append('citizen_id', foreignerID);
				formdata.append('th_prefix', prefixEn);
				formdata.append('th_name', nameEn.split(' ')[0] || ' ');
				formdata.append('th_lastname', nameEn.split(' ')[1] || ' ');
			}
			formdata.append('en_prefix', prefixEn);
			formdata.append('en_name', nameEn.split(' ')[0] || ' ');
			formdata.append('en_lastname', nameEn.split(' ')[1] || ' ');
			formdata.append('user_id', id);
			formdata.append('foreigner', foreigner);
			formdata.append('birthday', dateBirth);
			formdata.append('sex', sex);
			formdata.append('phone', phone);
			formdata.append('allergy', allergy);
			formdata.append('congenital_disease', congenital_disease);
			formdata.append('issue', dateIssue);
			formdata.append('expire', dateExpiry);
			formdata.append('address', address);
			formdata.append('fileData', fileData);
			formdata.append('nationality', nationality);
			formdata.append('religion', religion);
			formdata.append('username', username);
			formdata.append('password', password);
			formdata.append('role_id', role === 8 ? 2 : role);
			formdata.append('sub_role', JSON.stringify(role === 8 ? [ 'ThaiMassage' ] : role === 2 ? sub_role : []));

			let res = await POST(UPDATE_USER_PROFILE, formdata, true);
			// console.log('res signUp : ', res)

			if (res.success) {
				this.setState({ isSignedUp: true, to_edit_data: false, wait_loading: false });
				this.setValueAfterSignUp(res.result);
				swal('สำเร็จ!', 'แก้ไขข้อมูลสำเร็จ', 'success', {
					buttons: false,
					timer: 2000
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}

			// response(res)
		} catch (error) {
			this.setState({ wait_loading: false });
			response(null);
			// console.log("error signUp : ", error);
			swal('ผิดพลาด!', 'Network Error', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	}

	setValueAfterSignUp(v) {
		// console.log('v is ', v)
		this.setState({
			user_id: v.user_id,

			// display things
			cardID: v.citizen_id,
			nameTh: v.th_name + ' ' + v.th_lastname,
			nameEn: v.en_name + ' ' + v.en_lastname,
			phone: v.phone,
			dateBirth: v.birthday,
			dateIssue: v.issue,
			dateExpiry: v.expire,
			sex: v.sex,
			address: v.address,
			congenital_disease: v.congenital_disease,
			allergy: v.allergy,
			file: ip + GET_PICTURE(v.picture || 'null')
		});
	}

	handleChange = (event) => {
		// uplaod pic
		this.setState({
			fileData: event.target.files[0],
			file: URL.createObjectURL(event.target.files[0])
		});
	};

	deleteUser = () => {
		swal('ปิดใช้งาน', 'ต้องการปิดใช้งานผู้รับบริการผู้นี้ใช่หรือไม่', 'warning', {
			buttons: {
				cancel: 'ยกเลิก',
				confirm: {
					text: 'ยืนยัน',
					value: 'confirm'
				}
			}
		}).then((value) => {
			switch (value) {
				case 'confirm':
					this.confirmDelete();
					break;
			}
		});
	};

	undeleteUser = () => {
		swal('เปิดใช้งาน', 'ต้องการเปิดใช้งานผู้รับบริการผู้นี้ใช่หรือไม่', 'warning', {
			buttons: {
				cancel: 'ยกเลิก',
				confirm: {
					text: 'ยืนยัน',
					value: 'confirm'
				}
			}
		}).then((value) => {
			switch (value) {
				case 'confirm':
					this.confirmUndelete();
					break;
			}
		});
	};

	confirmDelete = async () => {
		let { id } = this.props.match.params;
		try {
			this.setState({ wait_loading: true });
			let res = await GET(DELETE_USER(id));
			if (res.success) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', 'ปิดใช้งานผู้รับบริการเรียบร้อยแล้ว', 'success', {
					buttons: false,
					timer: 2000
				}).then(this.props.history.push(User.role === 'admin' ? '/home' : '/home-admin'));
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (err) {
			this.setState({ wait_loading: false });
			swal('Error!', err.message, 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};

	confirmUndelete = async () => {
		let { id } = this.props.match.params;
		try {
			this.setState({ wait_loading: true });
			let res = await GET(UNDELETE_USER(id));
			if (res.success) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', 'เปิดใช้งานผู้รับบริการเรียบร้อยแล้ว', 'success', {
					buttons: false,
					timer: 2000
				}).then(this.props.history.push(User.role === 'admin' ? '/home' : '/home-admin'));
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 3000
				});
			}
		} catch (err) {
			this.setState({ wait_loading: false });
			swal('Error!', err.message, 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};

	Upfirst = (str = '', i = 0) => {
		if (str.charAt(i) != ' ' && i == 0) {
			return this.Upfirst(str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
		} else if (str.charAt(i - 1) == ' ') {
			if (str.charAt(i) == ' ') {
				return this.Upfirst(str.slice(0, i), i);
			} else {
				return this.Upfirst(str.slice(0, i) + str.charAt(i).toUpperCase() + str.slice(i + 1), i + 1);
			}
		} else if (str.length > i) {
			return this.Upfirst(str, i + 1);
		} else {
			return str;
		}
	};

	render() {
		let {
			cardID,
			nameTh,
			nameEn,
			phone,
			file,
			isSignedUp,
			address,
			allergy,
			congenital_disease,
			dateIssue,
			dateExpiry,
			dateBirth,
			sex,
			to_edit_data,
			foreigner,
			foreignerID,
			nationality,
			religion,
			prefixTh,
			prefixEn,
			loading,
			role,
			hn,
			isDisable,
			wait_loading,
			username,
			password,
			roleData,
			sub_role
		} = this.state;
		let { GetResponseSignUp, isCustomer } = this.props;
		return (
			<div
				style={{
					width: '100%',
					minWidth: '100vw',
					height: '100%',
					minHeight: '89vh',
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center'
				}}
			>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : (
					<div
						style={{
							position: 'relative',
							width: '80%',
							height: '100%',
							backgroundColor: '#fff',
							padding: '2vh',
							borderRadius: 5,
							margin: '5vh 0'
						}}
					>
						<h3 style={{ color: Color.Blue }}>ข้อมูลผู้ใช้</h3>
						{hn ? (
							<div
								style={{
									display: 'flex',
									position: 'absolute',
									top: '1rem',
									right: '1rem'
								}}
							>
								<Barcode
									value={`HN${hn}`}
									width={0.8}
									height={25}
									fontSize={12}
									background={'transparent'}
								/>
							</div>
						) : null}
						<Form>
							<Default>
								<Row>
									<Col className={isCustomer == -1 && !isSignedUp ? 'text-right' : 'text-center'}>
										{/* upload pic */}
										<Label
											style={{
												cursor: 'pointer',
												position: 'relative',
												height: '14rem',
												width: '14rem'
											}}
											for="fileupload"
										>
											<Input
												style={{
													position: 'absolute',
													display: 'none',
													top: 0,
													left: 0
												}}
												id="fileupload"
												type="file"
												accept=".png, .jpg, .jpeg"
												onChange={(event) => this.handleChange(event)}
												disabled={to_edit_data ? false : isSignedUp}
											/>
											{file !== '' && file ? (
												<div
													style={{
														cursor: 'pointer',
														width: '100%',
														height: '100%',
														backgroundImage: `url(${file})`,
														backgroundRepeat: 'no-repeat',
														backgroundPosition: 'center',
														borderRadius: 10,
														backgroundSize: 'cover',
														position: 'relative'
													}}
												/>
											) : (
												<div
													style={{
														width: '100%',
														color: '#fff',
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
														height: '100%',
														backgroundColor: '#ccc',
														borderRadius: 10
													}}
												>
													ไม่มีรูป
												</div>
											)}
										</Label>
									</Col>
								</Row>
								{(User.role === 'admin' || User.role === 'frontPT' || User.role === 'frontMT') && (
									<Row>
										<Col sm>
											<FormGroup>
												<Label htmlFor="religion" className="label-user-detail">
													Username
												</Label>
												<FormText style={{ marginTop: -10 }}>Username</FormText>
												<Input
													id="religion"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														this.setState({
															username: e.target.value
														});
													}}
													disabled={to_edit_data ? !to_edit_data : isSignedUp}
													value={!username ? '' : username}
												/>
											</FormGroup>
										</Col>
										<Col sm>
											<FormGroup>
												<Label htmlFor="religion" className="label-user-detail">
													Password
												</Label>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="religion"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														this.setState({
															password: e.target.value
														});
													}}
													disabled={to_edit_data ? !to_edit_data : isSignedUp}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}
								<Row className="mx-auto">
									<FormGroup style={{ width: '100%' }}>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label for="role" className="label-user-detail">
												ประเภท
											</Label>
											{/* <Label htmlFor="role" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Role of User</FormText>
										<Input
											value={role}
											id="role"
											type="select"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ role: Number(e.target.value) });
											}}
											disabled={to_edit_data ? !to_edit_data : isSignedUp}
										>
											{roleData.map((el) => (
												<option value={el.role_id}>{functions.getRole(el.role_name)}</option>
											))}
										</Input>
									</FormGroup>

									{role === 2 && (
										<FormGroup>
											<Label for="exampleCheckbox">ประเภทกายภาพ</Label>
											<Row>
												{subRole.map((el, i) => (
													<Col xs={6}>
														<CustomInput
															checked={sub_role.some((e) => e === el.en_name)}
															disabled={to_edit_data ? !to_edit_data : isSignedUp}
															onChange={this.handleSelect}
															type="checkbox"
															id={el.en_name}
															label={el.name}
														/>
													</Col>
												))}
											</Row>
										</FormGroup>
									)}
								</Row>

								{foreigner !== 1 ? (
									<Row sm className="mx-auto">
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label htmlFor="cardID" className="label-user-detail">
													เลขประจำตัวประชาชน
												</Label>
												{/* <Label htmlFor="cardID" style={{ color: 'red', marginLeft: '0.25vw' }}>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Identification Number</FormText>
											<Input
												id="cardID"
												type="number"
												onChange={(e) => {
													if (e.target.value.length <= 13)
														this.setState({ cardID: '' + e.target.value });
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!cardID ? '' : cardID}
											/>
										</FormGroup>
									</Row>
								) : (
									<Row sm className="mx-auto">
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label htmlFor="foreignerID" className="label-user-detail">
													เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
												</Label>
												{/* <Label
													htmlFor="foreignerID"
													style={{ color: 'red', marginLeft: '0.25vw' }}
												>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Passport Number</FormText>
											<Input
												id="foreignerID"
												className="input-user-detail"
												style={{
													fontSize: '1.3rem',
													height: 'auto',
													marginTop: 3
												}}
												onChange={(e) => {
													if (
														this.CheckForeignerIDInput(
															e.target.value.charAt(e.target.value.length - 1)
														)
													) {
														this.setState({
															foreignerID: e.target.value.toUpperCase()
														});
													}
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!foreignerID ? '' : foreignerID}
											/>
										</FormGroup>
									</Row>
								)}
								{foreigner !== 1 && (
									<Row>
										<Col sm={3}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixTh" className="label-user-detail">
														คำนำหน้า
													</Label>
													{/* <Label
														htmlFor="prefixTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
												<Input
													id="prefixTh"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixTh: e.target.value });
														}
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!prefixTh ? '' : prefixTh}
												/>
											</FormGroup>
										</Col>
										<Col sm={9}>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nameTh" className="label-user-detail">
														ชื่อ - นามสกุล
													</Label>
													{/* <Label
														htmlFor="nameTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Th)</FormText>
												<Input
													id="nameTh"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameTh: e.target.value });
														}
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!nameTh ? '' : nameTh}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}
								<Row>
									<Col sm={3}>
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label htmlFor="prefixEn" className="label-user-detail">
													Prefix
												</Label>
												{/* <Label
													htmlFor="prefixEn"
													style={{ color: 'red', marginLeft: '0.25vw' }}
												>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
											<Input
												id="prefixEn"
												className="input-user-detail"
												style={{
													fontSize: '1.3rem',
													height: 'auto',
													marginTop: 3
												}}
												onChange={(e) => {
													if (
														this.CheckLanEngInput(
															e.target.value.charAt(e.target.value.length - 1)
														)
													) {
														this.setState({
															prefixEn: this.Upfirst(e.target.value)
														});
													}
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!prefixEn ? '' : prefixEn}
											/>
										</FormGroup>
									</Col>
									<Col sm={9}>
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label
													className="text-muted"
													htmlFor="nameEn"
													className="label-user-detail"
												>
													Name - Last name
												</Label>
												{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
											<Input
												id="nameEn"
												className="input-user-detail"
												style={{
													fontSize: '1.3rem',
													height: 'auto',
													marginTop: 3
												}}
												onChange={(e) => {
													if (
														this.CheckLanEngInput(
															e.target.value.charAt(e.target.value.length - 1)
														)
													) {
														this.setState({
															nameEn: this.Upfirst(e.target.value)
														});
													}
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!nameEn ? '' : nameEn}
											/>
										</FormGroup>
									</Col>
								</Row>
							</Default>

							<Mobile>
								<Row className="text-center">
									<Col>
										<Label
											className="text-center"
											style={{
												cursor: 'pointer',
												position: 'relative',
												height: '14rem',
												width: '14rem'
											}}
											for="fileupload"
										>
											<Input
												style={{
													position: 'absolute',
													display: 'none',
													top: 0,
													left: 0
												}}
												id="fileupload"
												type="file"
												accept=".png, .jpg, .jpeg"
												onChange={(event) => this.handleChange(event)}
												disabled={to_edit_data ? to_edit_data : isSignedUp}
											/>
											{file && file !== '' ? (
												<div
													style={{
														width: '100%',
														height: '100%',
														backgroundImage: `url(${file})`,
														backgroundRepeat: 'no-repeat',
														backgroundPosition: 'center',
														borderRadius: 10,
														backgroundSize: '100% auto'
													}}
												/>
											) : (
												<div
													style={{
														color: '#fff',
														display: 'flex',
														justifyContent: 'center',
														alignItems: 'center',
														width: '100%',
														height: '100%',
														backgroundColor: '#ccc',
														borderRadius: 10
													}}
												>
													ไม่มีรูป
												</div>
											)}
										</Label>
									</Col>
								</Row>
								{(User.role === 'admin' || User.role === 'frontPT' || User.role === 'frontMT') && (
									<Row>
										<Col sm>
											<FormGroup>
												<Label htmlFor="religion" className="label-user-detail">
													Username
												</Label>
												<FormText style={{ marginTop: -10 }}>Username</FormText>
												<Input
													id="religion"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														this.setState({
															username: e.target.value
														});
													}}
													disabled={to_edit_data ? !to_edit_data : isSignedUp}
													value={!username ? '' : username}
												/>
											</FormGroup>
										</Col>
										<Col sm>
											<FormGroup>
												<Label htmlFor="religion" className="label-user-detail">
													Password
												</Label>
												<FormText style={{ marginTop: -10 }}>Password</FormText>
												<Input
													id="religion"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														this.setState({
															password: e.target.value
														});
													}}
													disabled={to_edit_data ? !to_edit_data : isSignedUp}
													value={!password ? '' : password}
												/>
											</FormGroup>
										</Col>
									</Row>
								)}
								<Row className="mx-auto">
									<FormGroup style={{ width: '100%' }}>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label for="role" className="label-user-detail">
												ประเภท
											</Label>
											{/* <Label htmlFor="role" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Role of User</FormText>
										<Input
											id="role"
											type="select"
											className="input-user-detail"
											style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
											onChange={(e) => {
												this.setState({ role: Number(e.target.value) });
											}}
											disabled={to_edit_data ? !to_edit_data : isSignedUp}
										>
											{roleData.map((el) => (
												<option value={el.role_id}>{functions.getRole(el.role_name)}</option>
											))}
										</Input>
									</FormGroup>
									{role === 2 && (
										<FormGroup>
											<Label for="exampleCheckbox">ประเภทกายภาพ</Label>
											<Row>
												{subRole.map((el, i) => (
													<Col xs={6}>
														<CustomInput
															checked={sub_role.some((e) => e === el.en_name)}
															disabled={to_edit_data ? !to_edit_data : isSignedUp}
															onChange={this.handleSelect}
															type="checkbox"
															id={el.en_name}
															label={el.name}
														/>
													</Col>
												))}
											</Row>
										</FormGroup>
									)}
								</Row>
								<Row>
									<Col md className="mx-auto">
										{foreigner !== 1 ? (
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="cardID" className="label-user-detail">
														เลขประจำตัวประชาชน
													</Label>
													{/* <Label
														htmlFor="cardID"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Identification Number</FormText>
												<Input
													id="cardID"
													type="number"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (e.target.value.length <= 13)
															this.setState({ cardID: '' + e.target.value });
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!cardID ? '' : cardID}
												/>
											</FormGroup>
										) : (
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="foreignerID" className="label-user-detail">
														เลขหนังสือเดินทาง/หนังสือเดินทางชั่วคราว
													</Label>
													{/* <Label
														htmlFor="foreignerID"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Passport Number</FormText>
												<Input
													id="foreignerID"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (
															this.CheckForeignerIDInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({
																foreignerID: e.target.value.toUpperCase()
															});
														}
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!foreignerID ? '' : foreignerID}
												/>
											</FormGroup>
										)}
									</Col>
									{foreigner !== 1 && (
										<Col md className="mx-auto">
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="prefixTh" className="label-user-detail">
														คำนำหน้า
													</Label>
													{/* <Label
														htmlFor="prefixTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Prefix (Th)</FormText>
												<Input
													id="prefixTh"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ prefixTh: e.target.value });
														}
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!prefixTh ? '' : prefixTh}
												/>
											</FormGroup>
											<FormGroup style={{ width: '100%' }}>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nameTh" className="label-user-detail">
														ชื่อ - นามสกุล
													</Label>
													{/* <Label
														htmlFor="nameTh"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Name - Last name (Th)</FormText>
												<Input
													id="nameTh"
													className="input-user-detail"
													style={{
														fontSize: '1.3rem',
														height: 'auto',
														marginTop: 3
													}}
													onChange={(e) => {
														if (
															this.CheckLanThaiInput(
																e.target.value.charAt(e.target.value.length - 1)
															)
														) {
															this.setState({ nameTh: e.target.value });
														}
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!nameTh ? '' : nameTh}
												/>
											</FormGroup>
										</Col>
									)}
									<Col md className="mx-auto">
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label htmlFor="prefixEn" className="label-user-detail">
													Prefix
												</Label>
												{/* <Label
													htmlFor="prefixEn"
													style={{ color: 'red', marginLeft: '0.25vw' }}
												>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Prefix (Eng)</FormText>
											<Input
												id="prefixEn"
												className="input-user-detail"
												style={{
													fontSize: '1.3rem',
													height: 'auto',
													marginTop: 3
												}}
												onChange={(e) => {
													if (
														this.CheckLanEngInput(
															e.target.value.charAt(e.target.value.length - 1)
														)
													) {
														this.setState({
															prefixEn: this.Upfirst(e.target.value)
														});
													}
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!prefixEn ? '' : prefixEn}
											/>
										</FormGroup>
									</Col>
									<Col md className="mx-auto">
										<FormGroup style={{ width: '100%' }}>
											<div style={{ display: 'flex', flexDirection: 'row' }}>
												<Label
													className="text-muted"
													htmlFor="nameEn"
													className="label-user-detail"
												>
													Name - Last name
												</Label>
												{/* <Label htmlFor="nameEn" style={{ color: 'red', marginLeft: '0.25vw' }}>
													*
												</Label> */}
											</div>
											<FormText style={{ marginTop: -10 }}>Name - Last name (Eng)</FormText>
											<Input
												id="nameEn"
												className="input-user-detail"
												style={{
													fontSize: '1.3rem',
													height: 'auto',
													marginTop: 3
												}}
												onChange={(e) => {
													if (
														this.CheckLanEngInput(
															e.target.value.charAt(e.target.value.length - 1)
														)
													) {
														this.setState({
															nameEn: this.Upfirst(e.target.value)
														});
													}
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!nameEn ? '' : nameEn}
											/>
										</FormGroup>
									</Col>
								</Row>
							</Mobile>

							{/* -------- bottom -------- */}

							<Row className="mx-auto">
								<FormGroup style={{ width: '100%' }}>
									<div style={{ display: 'flex', flexDirection: 'row' }}>
										<Label htmlFor="phone" className="label-user-detail">
											เบอร์โทรศัพท์
										</Label>
										{/* <Label htmlFor="phone" style={{ color: 'red', marginLeft: '0.25vw' }}>
											*
										</Label> */}
									</div>
									<FormText style={{ marginTop: -10 }}>Phone Number</FormText>
									<Input
										id="phone"
										type="number"
										className="input-user-detail"
										style={{ fontSize: '1.3rem', height: 'auto', marginTop: 3 }}
										onChange={(e) => {
											if (e.target.value.length <= 10)
												this.setState({ phone: '' + e.target.value });
										}}
										readOnly={to_edit_data ? !to_edit_data : isSignedUp}
										value={!phone ? '' : phone}
									/>
								</FormGroup>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="dateBirth" className="label-user-detail">
												เกิดวันที่
											</Label>
											{/* <Label htmlFor="dateBirth" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Date of birth</FormText>
										<Input
											type="date"
											id="dateBirth"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: '3rem',
												marginTop: 3
											}}
											onChange={(e) => {
												this.setState({ dateBirth: e.target.value });
											}}
											readOnly={to_edit_data ? !to_edit_data : isSignedUp}
											value={!dateBirth ? '' : dateBirth}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="sex" className="label-user-detail">
												เพศ
											</Label>
											{/* <Label htmlFor="sex" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Sex</FormText>
										<Input
											id="sex"
											type="select"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: '3rem',
												marginTop: 3
											}}
											onChange={(e) => {
												this.setState({ sex: e.target.value });
											}}
											disabled={to_edit_data ? !to_edit_data : isSignedUp}
											value={!sex ? '' : sex}
										>
											<option value="male">ชาย</option>
											<option value="female">หญิง</option>
										</Input>
									</FormGroup>
								</Col>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										{foreigner !== 0 ? (
											<div
												style={{
													width: '30%',
													display: 'flex',
													flexDirection: 'column'
												}}
											>
												<div style={{ display: 'flex', flexDirection: 'row' }}>
													<Label htmlFor="nationality" className="label-user-detail">
														สัญชาติ
													</Label>
													{/* <Label
														htmlFor="nationality"
														style={{ color: 'red', marginLeft: '0.25vw' }}
													>
														*
													</Label> */}
												</div>
												<FormText style={{ marginTop: -10 }}>Nationality</FormText>
											</div>
										) : (
											<div>
												<Label htmlFor="nationality" className="label-user-detail">
													สัญชาติ
												</Label>
												<FormText style={{ marginTop: -10 }}>Nationality</FormText>
											</div>
										)}
										<Input
											id="nationality"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: 'auto',
												marginTop: 3
											}}
											onChange={(e) => {
												if (
													this.CheckLanEngInput(
														e.target.value.charAt(e.target.value.length - 1)
													)
												) {
													this.setState({
														nationality: this.Upfirst(e.target.value)
													});
												}
											}}
											readOnly={to_edit_data ? !to_edit_data : isSignedUp}
											value={foreigner !== 1 ? 'Thai' : !nationality ? '' : nationality}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<Label htmlFor="religion" className="label-user-detail">
											ศาสนา
										</Label>
										<FormText style={{ marginTop: -10 }}>Religion</FormText>
										<Input
											id="religion"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: 'auto',
												marginTop: 3
											}}
											onChange={(e) => {
												if (
													this.CheckBothLanInput(
														e.target.value.charAt(e.target.value.length - 1)
													)
												) {
													this.setState({
														religion: this.Upfirst(e.target.value)
													});
												}
											}}
											disabled={to_edit_data ? !to_edit_data : isSignedUp}
											value={!religion ? '' : religion}
										>
											<option value="male">ชาย</option>
											<option value="feMale">หญิง</option>
										</Input>
									</FormGroup>
								</Col>
							</Row>
							<Row>
								<Col sm>
									<FormGroup>
										<Label for="dateIssue">วันออกบัตร</Label>
										<FormText style={{ marginTop: -10 }}>Date of issue</FormText>
										<Input
											type="date"
											id="dateIssue"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: '3rem',
												marginTop: 3
											}}
											onChange={(e) => {
												this.setState({ dateIssue: e.target.value });
											}}
											readOnly={to_edit_data ? !to_edit_data : isSignedUp}
											value={dateIssue}
										/>
									</FormGroup>
								</Col>
								<Col sm>
									<FormGroup>
										<Label for="dateExpiry">วันหมดอายุบัตร</Label>
										<FormText style={{ marginTop: -10 }}>Date of Expiry</FormText>
										<Input
											type="date"
											id="dateExpiry"
											className="input-user-detail"
											style={{
												fontSize: '1.3rem',
												height: '3rem',
												marginTop: 3
											}}
											onChange={(e) => {
												this.setState({ dateExpiry: e.target.value });
											}}
											readOnly={to_edit_data ? !to_edit_data : isSignedUp}
											value={dateExpiry}
										/>
									</FormGroup>
								</Col>
							</Row>

							<Default>
								<FormGroup row className="mx-auto">
									<div
										style={{
											width: '30%',
											display: 'flex',
											flexDirection: 'column'
										}}
									>
										<div style={{ display: 'flex', flexDirection: 'row' }}>
											<Label htmlFor="address" className="label-user-detail">
												ที่อยู่
											</Label>
											{/* <Label htmlFor="address" style={{ color: 'red', marginLeft: '0.25vw' }}>
												*
											</Label> */}
										</div>
										<FormText style={{ marginTop: -10 }}>Address</FormText>
									</div>
									<Col style={{ width: '70%', paddingRight: 0 }}>
										<Input
											type="textarea"
											id="address"
											className="input-user-detail"
											style={{ fontSize: '1.3rem' }}
											onChange={(e) => {
												this.setState({
													address: this.Upfirst(e.target.value)
												});
											}}
											readOnly={to_edit_data ? !to_edit_data : isSignedUp}
											value={!address ? '' : address}
										/>
									</Col>
								</FormGroup>
								{role == 6 && (
									<Col className="mx-auto">
										<FormGroup row>
											<div
												style={{
													width: '30%',
													display: 'flex',
													flexDirection: 'column'
												}}
											>
												<div
													style={{
														width: '30%',
														display: 'flex',
														flexDirection: 'column'
													}}
												>
													<Label htmlFor="congenital" className="label-user-detail">
														โรคประจำตัว
													</Label>
													<FormText style={{ marginTop: -10 }}>Congenital Disease</FormText>
												</div>
											</div>
											<Col style={{ width: '70%', paddingRight: 0 }}>
												<Input
													type="textarea"
													id="congenital"
													className="input-user-detail"
													style={{ fontSize: '1.3rem' }}
													onChange={(e) => {
														this.setState({
															congenital_disease: e.target.value
														});
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!congenital_disease ? '' : congenital_disease}
												/>
											</Col>
										</FormGroup>
										<FormGroup row>
											<div
												style={{
													width: '30%',
													display: 'flex',
													flexDirection: 'column'
												}}
											>
												<Label htmlFor="allergy" className="label-user-detail">
													ยาที่แพ้
												</Label>
												<FormText style={{ marginTop: -10 }}>Drug Allergy</FormText>
											</div>
											<Col style={{ width: '70%', paddingRight: 0 }}>
												<Input
													type="textarea"
													id="allergy"
													className="input-user-detail"
													style={{ fontSize: '1.3rem' }}
													onChange={(e) => {
														this.setState({ allergy: e.target.value });
													}}
													readOnly={to_edit_data ? !to_edit_data : isSignedUp}
													value={!allergy ? '' : allergy}
												/>
											</Col>
										</FormGroup>
									</Col>
								)}
							</Default>
							<Mobile>
								<FormGroup style={{ width: '100%' }}>
									<div style={{ display: 'flex', flexDirection: 'row' }}>
										<Label htmlFor="address" className="label-user-detail">
											ที่อยู่
										</Label>
										{/* <Label htmlFor="address" style={{ color: 'red', marginLeft: '0.25vw' }}>
											*
										</Label> */}
									</div>
									<FormText style={{ marginTop: -10 }}>Address</FormText>
									<Input
										type="textarea"
										id="address"
										className="input-user-detail"
										style={{ fontSize: '1.3rem', marginTop: 3 }}
										onChange={(e) => {
											this.setState({ address: this.Upfirst(e.target.value) });
										}}
										readOnly={to_edit_data ? !to_edit_data : isSignedUp}
										value={!address ? '' : address}
									/>
								</FormGroup>
								{role == 6 && (
									<div>
										<FormGroup style={{ width: '100%' }}>
											<Label htmlFor="congenital" className="label-user-detail">
												โรคประจำตัว
											</Label>
											<FormText style={{ marginTop: -10 }}>Congenital Disease</FormText>
											<Input
												type="textarea"
												id="congenital"
												className="input-user-detail"
												style={{ fontSize: '1.3rem', marginTop: 3 }}
												onChange={(e) => {
													this.setState({ congenital_disease: e.target.value });
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!congenital_disease ? '' : congenital_disease}
											/>
										</FormGroup>
										<FormGroup style={{ width: '100%' }}>
											<Label htmlFor="allergy" className="label-user-detail">
												ยาที่แพ้
											</Label>
											<FormText style={{ marginTop: -10 }}>Drug Allergy</FormText>
											<Input
												type="textarea"
												id="allergy"
												className="input-user-detail"
												style={{ fontSize: '1.3rem', marginTop: 3 }}
												onChange={(e) => {
													this.setState({ allergy: e.target.value });
												}}
												readOnly={to_edit_data ? !to_edit_data : isSignedUp}
												value={!allergy ? '' : allergy}
											/>
										</FormGroup>
									</div>
								)}
							</Mobile>
							{isCustomer !== -1 && (
								<div>
									<Min>
										<div className="d-flex justify-content-end">
											<div
												style={{
													color: 'white',
													textDecoration: 'none',
													backgroundColor: isDisable ? Color.Green : Color.Red,
													marginRight: '1vw'
												}}
												className="btn-form"
												onClick={isDisable ? this.undeleteUser : this.deleteUser}
											>
												{isDisable ? 'เปิดใช้งาน' : 'ปิดใช้งาน'}
											</div>
											<div
												style={{ color: 'white', textDecoration: 'none' }}
												className="btn-form"
												onClick={
													to_edit_data ? (
														() => this.UpdateProfile()
													) : (
														() => this.setState({ to_edit_data: true })
													)
												}
											>
												{to_edit_data ? 'บันทึก' : 'แก้ไขข้อมูล'}
											</div>
										</div>
									</Min>
									<Max>
										<div className="d-flex flex-column align-items-center">
											<div
												style={{
													color: 'white',
													textDecoration: 'none',
													backgroundColor: isDisable ? Color.Green : Color.Red,
													width: '100%'
												}}
												className="btn-form"
												onClick={isDisable ? this.undeleteUser : this.deleteUser}
											>
												{isDisable ? 'เปิดใช้งาน' : 'ปิดใช้งาน'}
											</div>
											<div
												style={{ color: 'white', textDecoration: 'none', width: '100%' }}
												className="btn-form mt-2"
												onClick={
													to_edit_data ? (
														() => this.UpdateProfile()
													) : (
														() => this.setState({ to_edit_data: true })
													)
												}
											>
												{to_edit_data ? 'บันทึก' : 'แก้ไขข้อมูล'}
											</div>
										</div>
									</Max>
								</div>
							)}
						</Form>
					</div>
				)}
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
	handleSelect = (event) => {
		let id = event.target.id;
		let checked = event.target.checked;
		let { sub_role } = this.state;
		if (checked) {
			sub_role.push(id);
		} else {
			var index = sub_role.indexOf(id);
			sub_role.splice(index, 1);
		}
		this.setState({
			sub_role: sub_role
		});
	};
}
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
