import _profile from "../components/Profile/Profile";

import _fHome from "./front/Home/Home";
import _fLogin from "./Login/Login";
import _fGraph from "../components/Graph/Graph";
import _fAllWork from "../components/AllWorkTable/AllWorkTable";
import _fCustomerDetail from "./front/Home/CustomerDetail/CustomerDetail";
import _fAddCustomer from "./front/Home/AddCustomer/AddCustomer";
// import _fCustomer from "./front/Home/Customer/Customer";

import _mHome from "./marketing/Home";
import _mAddBanner from "./marketing/AddBanner";

import _aHome from "./admin/Home/Home";
import _aAddMember from "./admin/AddMember/AddMember";
import _aMemberDetail from "./admin/MemberDetail/MemberDetail";

import _ptHome from "./physician/pt/Home/Home/Home";
import _ptAllWork from "../components/AllWorkTable/AllWorkTable";
import _ptGraph from "../components/Graph/Graph";

import _mtHome from "./physician/mt/Home/Home";
import _mtAllWork from "../components/AllWorkTable/AllWorkTable";
import _mtHistory from "./physician/mt/History/History";

import TableDetail from "../components/TableDetail/TableDetail";

// all
export const profile = _profile;

// admin
export const aHome = _aHome;
export const aAddMember = _aAddMember;
export const aMemberDetail = _aMemberDetail;

// front
export const fHome = _fHome;
export const fLogin = _fLogin;
export const fGraph = _fGraph;
export const fAllWork = _fAllWork;
export const fTableDetail = TableDetail;
export const fAddCustomer = _fAddCustomer;
export const fCustomerDetail = _fCustomerDetail;

// marketing
export const mHome = _mHome;
export const mAddBanner = _mAddBanner;

//pt
export const ptHome = _ptHome;
export const ptAllWork = _ptAllWork;
export const ptGraph = _ptGraph;

//mt
export const mtHome = _mtHome;
export const mtAllWork = _mtAllWork;
export const mtHistory = _mtHistory;
export const mtTableDetail = TableDetail;
