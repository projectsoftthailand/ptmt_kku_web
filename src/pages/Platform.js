import React, { Component } from 'react';
import { PLATFORM, POST, GET } from '../service/service';

export default class Platform extends Component {
	componentDidMount = async () => {
		let device = navigator.platform;
		// console.log('device', device);
		let res = await POST(PLATFORM, { device });
		if (res.success) {
			window.location.replace(res.result);
		}
	};

	render() {
		return <div />;
	}
}
