import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ImageGallery from 'react-image-gallery';
import { GET, GET_BANNER, GET_BANNER_PIC } from '../../service/service';
import ReactLoading from 'react-loading';
import Color from '../../components/Color';
import { Label } from 'reactstrap';
import './style.css';
import 'react-image-gallery/styles/css/image-gallery.css';
import user from '../../mobx/user/user';

export default class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			bannerData: [],
			loading: true
		};
	}
	async componentWillMount() {
		// alert('market');
		if (!user.auth) {
			this.props.history.push('/');
		} else {
			await this.fetch();
			await this.setState({ loading: false });
		}
	}

	async fetch() {
		try {
			let res = await GET(GET_BANNER('all'));
			if (res.success) {
				this.setState({
					bannerData: res.result.map((el) => ({
						original: GET_BANNER_PIC(el.banner_picture),
						originalClass: 'ImgBanner',
						originalAlt: 'Banner'
					}))
				});
			}
		} catch (error) {}
	}

	render() {
		let { bannerData, loading } = this.state;
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : (
			<div className="main text-center">
				<div className="d-flex justify-content-between align-items-center my-2">
					<h3 style={{ color: Color.Blue }}>แบนเนอร์ที่แสดงอยู่ในขณะนี้</h3>
					<Link
						to={user.role === 'marketing' ? '/home/add-banner' : '/home-marketing/add-banner'}
						className="btn-all-table"
					>
						{bannerData.length !== 0 ? 'แก้ไข' : 'เพิ่ม'}
					</Link>
				</div>
				{bannerData.length !== 0 ? (
					<div className="d-flex justify-content-center" style={{ width: '100%' }}>
						<div className="col-sm-8 text-center">
							<ImageGallery
								items={bannerData}
								showNav={true}
								autoPlay={true}
								showIndex={true}
								showPlayButton={false}
								showThumbnails={false}
								showFullscreenButton={false}
								slideDuration={500}
							/>
						</div>
					</div>
				) : (
					<div style={{ marginTop: '10rem' }}>---ไม่มีแบรนเนอร์---</div>
				)}
			</div>
		);
	}
}
