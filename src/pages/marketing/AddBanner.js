import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ImageGallery from 'react-image-gallery';
import swal from 'sweetalert';
import { Input, Label } from 'reactstrap';
import {
	GET,
	POST,
	GET_BANNER,
	GET_BANNER_PIC,
	EDIT_BANNER,
	REMOVE_BANNER,
	CREATE_BANNER
} from '../../service/service';
import Color from '../../components/Color';
import './style.css';
import 'react-image-gallery/styles/css/image-gallery.css';
import ReactLoading from 'react-loading';
import Modal from 'react-modal';
import moment from 'moment';
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
export default class AddBanner extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			currentIndex: 0,
			addBanner: false,
			createBanner: null,
			bannerData: [],
			bannerImg: [],
			createBannerName: null,
			createBannerStartDate: null,
			createBannerExpireDate: null,
			createBannerDetail: null,
			createBannerLink: null,
			createBannerPicture: null,
			createBannerPictureShow: null,
			wait_loading: false,
			createBannertime_start: null,
			createBannertime_expire: null
		};
	}

	componentDidMount = async () => {
		await this.fetch();
		await this.setState({ loading: false });
	};

	fetch = async () => {
		await GET(GET_BANNER('edit')).then((res) => {
			if (res.success) {
				this.setState({
					bannerData: res.result.map((el) => ({
						banner_id: el.banner_id,
						banner_name: el.banner_name,
						banner_picture: el.banner_picture,
						banner_link: el.banner_link,
						start_date: moment(el.start_date).format('YYYY-MM-DD'),
						time_start: moment(el.start_date).format('HH:mm'),
						expire_date: moment(el.expire_date).format('YYYY-MM-DD'),
						time_expire: moment(el.expire_date).format('HH:mm'),
						banner_detail: el.banner_detail
					})),

					bannerImg: res.result.map((el) => ({
						original: GET_BANNER_PIC(el.banner_picture),
						originalAlt: 'Banner',
						originalClass: 'ImgBanner'
					}))
				});
			} else {
				this.setState({ addBanner: true });
			}
		});
	};

	handleInput = (e) => {
		let { bannerData, currentIndex } = this.state;
		bannerData[currentIndex][e.target.name] = e.target.value;
		this.setState({ bannerData });
	};

	handleInputNew = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleFile = (e) => {
		let { bannerData, currentIndex, bannerImg } = this.state;
		bannerData[currentIndex][e.target.name] = e.target.files[0];
		bannerImg[currentIndex].original = URL.createObjectURL(e.target.files[0]);
		this.setState({ bannerData, bannerImg });
	};

	handleFileNew = (e) => {
		this.setState({
			createBannerPicture: e.target.files[0],
			createBannerPictureShow: URL.createObjectURL(e.target.files[0])
		});
	};

	editBanner = async () => {
		let { bannerData, currentIndex } = this.state;
		let data = new FormData();
		data.append('fileData', bannerData[currentIndex].banner_picture);
		data.append('banner_id', bannerData[currentIndex].banner_id);
		data.append('banner_name', bannerData[currentIndex].banner_name);
		data.append('start_date', bannerData[currentIndex].start_date + ' ' + bannerData[currentIndex].time_start);
		data.append('expire_date', bannerData[currentIndex].expire_date + ' ' + bannerData[currentIndex].time_expire);
		data.append('banner_detail', bannerData[currentIndex].banner_detail);
		data.append('banner_link', bannerData[currentIndex].banner_link);

		try {
			this.setState({ wait_loading: true });
			await POST(EDIT_BANNER, data, true).then((res) => {
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: true
					}).then((res) => window.location.reload());
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			});
		} catch (error) {
			this.setState({ wait_loading: false });
			// console.log(error);
		}
	};

	createBanner = async () => {
		let {
			createBannerName,
			createBannerStartDate,
			createBannerExpireDate,
			createBannerDetail,
			createBannerLink,
			createBannerPicture,
			createBannertime_start,
			createBannertime_expire
		} = this.state;
		let data = new FormData();
		data.append('fileData', createBannerPicture);
		data.append('banner_name', createBannerName);
		data.append('start_date', createBannerStartDate + ' ' + createBannertime_start);
		data.append('expire_date', createBannerExpireDate + ' ' + createBannertime_expire);
		data.append('banner_detail', createBannerDetail);
		data.append('banner_link', createBannerLink);
		try {
			this.setState({ wait_loading: true });
			await POST(CREATE_BANNER, data, true).then((res) => {
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: true
					}).then((res) => window.location.reload());
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			});
		} catch (error) {
			this.setState({ wait_loading: false });
			// console.log(error);
		}
	};

	removeBanner = async () => {
		try {
			this.setState({ wait_loading: true });
			let { bannerData, currentIndex } = this.state;
			let id = bannerData[currentIndex].banner_id;
			await GET(REMOVE_BANNER(id)).then((res) => {
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: true
					}).then((res) => window.location.reload());
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			});
		} catch (error) {
			this.setState({ wait_loading: false });
			// console.log(error);
		}
	};

	toggleAddBanner = () => {
		this.setState({
			addBanner: !this.state.addBanner
		});
	};

	inputEdit = (name, store, type, accept) => {
		let { bannerData, currentIndex } = this.state;
		return (
			<div className="row m-2">
				<h5 className="col-4 m-0 p-0">{name} :</h5>
				{type == 'file' ? (
					<label class="custom-file-upload">
						<Input
							className="col-8"
							type={type}
							name={store}
							value={accept ? null : bannerData[currentIndex] ? bannerData[currentIndex][store] : null}
							accept={accept}
							onChange={accept ? this.handleFile : this.handleInput}
						/>
						Custom Upload
					</label>
				) : (
					<Input
						className="col-8"
						type={type}
						name={store}
						value={accept ? null : bannerData[currentIndex] ? bannerData[currentIndex][store] : null}
						accept={accept}
						onChange={accept ? this.handleFile : this.handleInput}
					/>
				)}
				{name == 'อัพโหลดภาพ' && <div style={{ color: 'red' }}>**ขนาดรูปภาพต้องเป็น 1200 x 800 เท่านั้น</div>}
			</div>
		);
	};

	inputCreate = (name, store, type, accept) => {
		return (
			<div className="row m-2">
				<h5 className="col-4 m-0 p-0">{name} :</h5>
				{type == 'file' ? (
					<label class="custom-file-upload">
						<Input
							className="col-8"
							type={type}
							name={store}
							value={accept ? null : this.state[store]}
							accept={accept}
							onChange={accept ? this.handleFileNew : this.handleInputNew}
						/>
						Custom Upload
					</label>
				) : (
					<Input
						className="col-8"
						type={type}
						name={store}
						value={accept ? null : this.state[store]}
						accept={accept}
						onChange={accept ? this.handleFileNew : this.handleInputNew}
					/>
				)}
				{name == 'อัพโหลดภาพ' && <p style={{ color: 'red' }}>**ขนาดรูปภาพต้องเป็น 1200 x 800 เท่านั้น</p>}
			</div>
		);
	};

	render() {
		let { addBanner, bannerImg, createBannerPictureShow, wait_loading, loading } = this.state;
		// console.log('bannerData', this.state.bannerData);
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : !addBanner ? (
			<div className="main">
				<div className="d-flex justify-content-between align-items-center my-2">
					<h3 style={{ color: Color.Blue }}>ส่วนจัดการโปรโมตให้ข้อมูล</h3>
					<div className="row m-0">
						<button className="btn-all-table mx-2" onClick={() => this.toggleAddBanner()}>
							เพิ่ม
						</button>
						<Link to={'/home-marketing'} className="btn-delete mx-2">
							กลับ
						</Link>
					</div>
				</div>
				<div className="row mx-2">
					<div className="col-sm-7">
						<ImageGallery
							items={bannerImg || null}
							showNav={true}
							showIndex={true}
							showThumbnails={false}
							showPlayButton={false}
							showFullscreenButton={false}
							onSlide={(res) =>
								this.setState({
									currentIndex: res
								})}
						/>
					</div>
					<div className="col-sm-5">
						<div style={{ paddingTop: 15, paddingBottom: 15 }}>
							{this.inputEdit('ชื่อแบนเนอร์', 'banner_name', 'text')}
							{this.inputEdit('แสดงวันที่', 'start_date', 'date')}
							{this.inputEdit('แสดงเวลาที่', 'time_start', 'time')}
							{this.inputEdit('สิ้นสุดวันที่', 'expire_date', 'date')}
							{this.inputEdit('สิ้นสุดเวลาที่', 'time_expire', 'time')}
							{this.inputEdit('คำบรรยาย', 'banner_detail', 'text')}
							{this.inputEdit('เชื่อมโยงไปยัง', 'banner_link', 'text')}
							{this.inputEdit('อัพโหลดภาพ', 'banner_picture', 'file', 'image/*')}
						</div>
						<div className="row justify-content-between">
							<button className="btn-all-table" onClick={this.editBanner}>
								บันทึก
							</button>
							<button className="btn-delete" onClick={this.removeBanner}>
								ลบ
							</button>
						</div>
					</div>
				</div>
			</div>
		) : (
			<div className="main">
				<div className="d-flex justify-content-between align-items-center my-2">
					<h3 style={{ color: Color.Blue }}>ส่วนจัดการโปรโมตให้ข้อมูล</h3>
					<div className="row m-0">
						<button className="btn-all-table mx-2" onClick={() => this.toggleAddBanner()}>
							แก้ไข
						</button>
						<Link to={'/home-marketing'} className="btn-delete mx-2">
							กลับ
						</Link>
					</div>
				</div>
				<div className="row mx-2">
					<div className="col-sm-7">
						{createBannerPictureShow ? (
							<img src={createBannerPictureShow} style={{ width: '100%' }} alt="Banner" />
						) : (
							<div
								style={{
									minHeight: '40vh',
									width: '100%',
									display: 'flex',
									justifyContent: 'center',
									alignItems: 'center'
								}}
							>
								-- ไม่มีรูปภาพ --
							</div>
						)}
					</div>
					<div className="col-sm-5" style={{ paddingTop: 15, paddingBottom: 15 }}>
						{this.inputCreate('ชื่อแบนเนอร์', 'createBannerName', 'text')}
						{this.inputCreate('แสดงวันที่', 'createBannerStartDate', 'date')}
						{this.inputCreate('แสดงเวลาที่', 'createBannertime_start', 'time')}
						{this.inputCreate('สิ้นสุดวันที่', 'createBannerExpireDate', 'date')}
						{this.inputCreate('สิ้นสุดเวลาที่', 'createBannertime_expire', 'time')}
						{this.inputCreate('คำบรรยาย', 'createBannerDetail', 'text')}
						{this.inputCreate('เชื่อมโยงไปยัง', 'createBannerLink', 'text')}
						{this.inputCreate('อัพโหลดภาพ', 'createBannerPicture', 'file', 'image/*')}
						<div className="row justify-content-between">
							<button className="btn-all-table" onClick={this.createBanner}>
								บันทึก
							</button>
						</div>
					</div>
				</div>
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}
