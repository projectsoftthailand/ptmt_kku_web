import styled from 'styled-components';
import color from '../../../components/Color';

export const InputFormIC = styled.input`
background: white url(${props => props.url}) no-repeat left 1.2vw center;
background-size: 1vw 1vw;
width: 25vw;
border-radius: 2px;
padding: 0.4vw 2.5vw 0.4vw 1.5vw;
font-family: 'SUKHUMVITSET';
font-size: 1vw; 
border: 1px solid ${color.graydark}
&:focus {
    // border: 1px solid ${color.hoverHeaderCus};
    box-shadow: 0 0px 5px ${color.hoverHeaderCus};
    outline: none;
    border: 0;
}

@media screen and (max-width: 991px) {
    background: white url(${props => props.url}) no-repeat right 1.2vw center;
    background-size: 1.3vh 1.3vh;
    width: 100%;
    border-radius: 25px;
    padding: 0.5vh 5vw 0.5vh 2vw;
    font-size: 1.3vh; 
    border: 1px solid ${color.graydark}
    &:focus {
        // border: 1px solid ${color.hoverHeaderCus};
        box-shadow: 0 0px 5px ${color.hoverHeaderCus};
        outline: none;
        border: 0;
    }
}

@media screen and (max-width: 1024px) and (min-width: 992px) {
    background: white url(${props => props.url}) no-repeat right 1.2vw center;
    background-size: 1.3vh 1.3vh;
    width: 100%;
    border-radius: 25px;
    padding: 0.5vh 5vw 0.5vh 2vw;
    font-size: 2vw; 
    border: 1px solid ${color.graydark};
    // margin: 2vh 0 0 0;
    &:focus {
        // border: 1px solid ${color.hoverHeaderCus};
        box-shadow: 0 0px 5px ${color.hoverHeaderCus};
        outline: none;
        border: 0;
    }

    ${props => props.allsale && `
        width: 70%;
    `}
}
`