import React, { Component } from 'react';
import Clock from 'react-live-clock';
import moment from 'moment-timezone';
import './Styled/style.css';
import { Input, Label } from 'reactstrap';
import { FaUserAlt, FaKey } from 'react-icons/fa';
import windowSize from 'react-window-size';
import swal from 'sweetalert';
import { POST, LOGIN } from '../../service/service';
import User from '../../mobx/user/user';
// import { askForPermissioToReceiveNotifications } from '../../push-notification';
import ReactLoading from 'react-loading';
import Modal from 'react-modal';
import { Animated } from 'react-animated-css';
import { IoMdClose } from 'react-icons/io';
import Color from '../../components/Color';
import Responsive from 'react-responsive';
import { Button } from 'react-bootstrap';
import { messaging } from '../../push-notification';

const Desktop = (props) => <Responsive {...props} minWidth={700} />;
const Mobile = (props) => <Responsive {...props} maxWidth={699} />;

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: '',
			username: '',
			password: '',
			show: false,
			block_data: '',
			wait_loading: false,
			role: '',
			sub_role: [],
			token: ''
		};
	}
	async componentWillMount() {
		if (!messaging) return;
		try {
			await messaging.requestPermission();
			const token = await messaging.getToken();
			this.setState({ token: token });
		} catch (e) {
			// console.log('e', e);
		}
	}
	componentDidMount() {
		if (User.auth) {
			this.props.history.push('/home');
		} else {
			this.setDateTime();
		}
	}

	setDateTime() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		let year = +moment().format('YYYY') + 543;
		let dateString = moment().format('dddd Do MMMM') + ' ' + year;
		this.setState({ date: dateString });
		let en = require('moment/locale/en-au');
		moment.updateLocale('en-au', en);
	}

	handleKeyPress = (event) => {
		if (event.key === 'Enter') {
			this.Login();
		}
	};

	Login = async () => {
		let { username, password, token } = this.state;
		if (username && password) {
			try {
				this.setState({ wait_loading: true });
				let noti_token = token;
				// let noti_token = await askForPermissioToReceiveNotifications();
				// console.log('noti_token', noti_token);
				let res = await POST(LOGIN, { username, password, noti_token });
				if (res.success) {
					this.setState({ wait_loading: false, role: res.result.role, sub_role: res.result.sub_role });
					// if (res.result.role === 'mt' || res.result.role === 'pt') {
					// 	this.setState({ show: true, block_data: res.result });
					// } else {
					swal('สำเร็จ!', 'ลงชื่อเข้าใช้สำเร็จ', 'success', {
						buttons: false,
						timer: 2000
					});
					let obj = res.result;
					User.login(obj);
					this.props.history.push('/home');
					// }
				} else {
					this.setState({ wait_loading: false });
					swal('เข้าสู่ระบบไม่สำเร็จ!', 'Username หรือ Password ไม่ถูกต้องหรือถูกปิดใช้งาน', 'error');
				}
			} catch (err) {
				this.setState({ wait_loading: false });
				// console.log(err);
			}
		} else {
			swal('ไม่ครบ!', 'กรุณากรอก Username และ Password', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};
	ChangeRole = (e) => {
		if (e === 'doctor') {
			swal('สำเร็จ!', 'ลงชื่อเข้าใช้สำเร็จ', 'success', {
				buttons: false,
				timer: 2000
			});
			let { block_data } = this.state;
			let obj = block_data;
			User.login(obj);
			this.props.history.push('/home');
		} else {
			swal('สำเร็จ!', 'ลงชื่อเข้าใช้สำเร็จ', 'success', {
				buttons: false,
				timer: 2000
			});
			let { block_data } = this.state;
			if (block_data.role == 'mt') {
				block_data.role = 'frontMT';
				let obj = block_data;
				User.login(obj);
				this.props.history.push('/home');
			} else {
				block_data.role = 'frontPT';
				let obj = block_data;
				User.login(obj);
				this.props.history.push('/home');
			}
		}
	};
	render() {
		let { date, show, wait_loading, role, sub_role } = this.state;
		let isMobile;
		this.props.windowWidth <= 767 ? (isMobile = true) : (isMobile = false);
		return (
			<div id="section-login" className="Divbody">
				<Desktop>
					<div style={{ height: '100vh', paddingTop: isMobile ? 100 : 200 }} className="container">
						<div className="row">
							<div className="col-sm">
								<div className={isMobile ? 'text-center' : 'text-right'}>
									<p className="date">{date}</p>
									<Clock className="timer" format={'HH:mm น.'} ticking={true} />
								</div>
							</div>
							<div className="col-sm">
								<div
									style={{ width: '25rem' }}
									className={
										isMobile ? 'mt-5 text-left mx-auto text-white' : 'ml-5 mr-auto text-left'
									}
								>
									<div className="input-with-icon">
										<Input
											onChange={(e) => this.setState({ username: e.target.value })}
											bsSize="lg"
											type="text"
											placeholder="Username"
											onKeyPress={this.handleKeyPress}
										/>
										<FaUserAlt className="ic-login" />
									</div>
									<div className="input-with-icon">
										<Input
											onChange={(e) => this.setState({ password: e.target.value })}
											bsSize="lg"
											type="password"
											placeholder="Password"
											onKeyPress={this.handleKeyPress}
										/>
										<FaKey className="ic-login" />
									</div>
									<div
										onClick={this.Login}
										style={{ marginTop: '2vh', width: '100%', zIndex: 0 }}
										className="button-blue mr-auto btn-block"
									>
										เข้าสู่ระบบ
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="ocean">
						<div className="wave" />
						<div className="wave" />
					</div>
				</Desktop>
				<Mobile>
					<div style={{ height: '100vh', paddingTop: isMobile ? 100 : 200 }} className="container">
						<div className="row">
							<div className="col-sm">
								<div className={isMobile ? 'text-center' : 'text-right'}>
									<p className="date">{date}</p>
									<Clock className="timer" format={'HH:mm น.'} ticking={true} />
								</div>
							</div>
							<div className="col-sm">
								<div
									style={{ width: '25rem' }}
									className={
										isMobile ? 'mt-5 text-left mx-auto text-white' : 'ml-5 mr-auto text-left'
									}
								>
									<div className="input-with-icon">
										<Input
											onChange={(e) => this.setState({ username: e.target.value })}
											bsSize="lg"
											type="text"
											placeholder="Username"
											onKeyPress={this.handleKeyPress}
										/>
										<FaUserAlt className="ic-login" />
									</div>
									<div className="input-with-icon">
										<Input
											onChange={(e) => this.setState({ password: e.target.value })}
											bsSize="lg"
											type="password"
											placeholder="Password"
											onKeyPress={this.handleKeyPress}
										/>
										<FaKey className="ic-login" />
									</div>
									<div
										onClick={this.Login}
										style={{ marginTop: '2vh', width: '100%', zIndex: 0 }}
										className="button-blue mr-auto btn-block"
									>
										เข้าสู่ระบบ
									</div>
								</div>
							</div>
						</div>
					</div>
				</Mobile>
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Modal isOpen={show} onRequestClose={() => this.setState({ show: false })} style={customStyles}>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<div
							style={{
								width: 400,
								borderRadius: 10,
								backgroundColor: '#fff',
								paddingTop: 20,
								paddingBottom: 20,
								fontSize: 12,
								position: 'relative'
							}}
							className="container mx-auto text-center"
						>
							<div
								className="btn-exit-hover"
								onClick={() => this.setState({ show: false })}
								style={{
									position: 'absolute',
									backgroundColor: Color.Blue,
									display: 'flex',
									justifyContent: 'center',
									alignItems: 'center',
									borderRadius: '100%',
									width: 15,
									height: 15,
									cursor: 'pointer',
									top: 20,
									right: 20,
									marginTop: -10,
									marginRight: -10
								}}
							>
								<IoMdClose color="white" size={12} />
							</div>
							<div>
								<h2 style={{ fontWeight: 'bold' }}>กรุณาเลือกหน้าที่</h2>
							</div>
							<div
								style={{
									display: 'flex',
									alignItems: 'center',
									justifyContent: 'space-evenly',
									marginTop: '30px'
								}}
							>
								<Button
									bsStyle="info"
									onClick={() => this.ChangeRole('doctor')}
									style={{ width: '18rem' }}
								>
									<h3>
										{role === 'mt' ? (
											'นักเทคนิคการแพทย์'
										) : sub_role && sub_role[0] === 'ThaiMassage' ? (
											'เจ้าที่หน้านวดไทย'
										) : (
											'นักกายภาพบำบัด'
										)}
									</h3>
								</Button>
								<Button
									bsStyle="success"
									onClick={() => this.ChangeRole('front')}
									style={{ width: '18rem' }}
								>
									<h3>หน้าเคาท์เตอร์</h3>
								</Button>
							</div>
						</div>
					</Animated>
				</Modal>
			</div>
		);
	}
}

export default windowSize(Login);
