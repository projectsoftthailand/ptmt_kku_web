import React, { Component } from 'react';
import { createBrowserHistory } from 'history';
import MyNav from './components/Header/MyNav';
import { medium } from './components/Font';
import User from './mobx/user/user';
import './App.css';
import Responsive from 'react-responsive';
import Error from './assets/image/Error.png';

const Desktop = (props) => <Responsive {...props} minWidth={700} />;
const Mobile = (props) => <Responsive {...props} maxWidth={699} />;

export default class WrapperComponent extends Component {
	componentDidMount() {
		if (!User.auth) {
			createBrowserHistory().push('/');
		}
	}

	render() {
		return (
			<div style={{ fontFamily: medium }} className={'App'}>
				<Desktop>
					{/* HEADER */}
					<MyNav />
					{/* BODY */}
					<div style={{ margin: '0.5rem' }}>{this.props.children}</div>
				</Desktop>
				<Mobile>
					{/* HEADER */}
					<MyNav />
					<div
						style={{
							display: 'flex',
							justifyContent: 'center',
							background: 'linear-gradient(to right, #467ac8, #b8358d)',
							border: '0.2px solid #b2d0ef',
							width: '100%',
							padding: '0.3rem 0rem 0.1rem 0rem'
						}}
					>
						<img
							src={Error}
							style={{ height: '1rem', width: 'auto', marginRight: '0.3rem', marginTop: '0.2rem' }}
						/>
						<div style={{ fontSize: '1rem', color: '#fff' }}>แนะนำให้ใช้ขนาดหน้าจอ Tablet ขึ้นไป</div>
					</div>
					{/* BODY */}
					<div style={{ margin: '0.5rem' }}>{this.props.children}</div>
				</Mobile>
			</div>
		);
	}
}
