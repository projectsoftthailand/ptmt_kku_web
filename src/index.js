import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Root from './routes/Root';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import 'bootstrap/dist/css/bootstrap.min.css';

// import { initializeFirebase } from './push-notification';

// ReactDOM.render(<Root />, document.getElementById('root'));
// serviceWorker.register();
// initializeFirebase();
if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register('./firebase-messaging-sw.js')
		.then(function(registration) {
			console.log('Registration successful, scope is:', registration.scope);
		})
		.catch(function(err) {
			console.log('Service worker registration failed, error:', err);
		});
}

ReactDOM.render(<Root />, document.getElementById('root'));
serviceWorker.unregister();
