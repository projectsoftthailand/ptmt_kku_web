exports.sevenDigits = (n) => {
	var string = '0000000';
	var length = 7 - n.toString().length;
	var trimmedString = string.substring(0, length);
	return trimmedString + n.toString();
};

exports.fiveDigits = (n) => {
	var string = '0000000';
	var length = 5 - n.toString().length;
	var trimmedString = string.substring(0, length);
	return trimmedString + n.toString();
};

exports.getListID = (type, id) => {
	return `${type.toUpperCase()}${this.sevenDigits(id)}`;
};

exports.getRoleName = (role) => {
	return role === 'pt'
		? 'บริการกายภาพบำบัด'
		: role === 'tm'
			? 'บริการนวดไทย'
			: role === 'mt'
				? 'บริการตรวจสุขภาพ'
				: role === 'Ortho'
					? 'บริการกายภาพบำบัดกระดูกและกล้ามเนื้อ'
					: role === 'Neuro'
						? 'บริการกายภาพบำบัดระบบประสาท'
						: role === 'Chest'
							? 'บริการกายภาพบำบัดระบบทรวงอก'
							: role === 'Child' ? 'บริการกายภาพบำบัดระบบเด็ก' : null;
};

exports.getOntimeName = (time) => {
	return time ? 'ในเวลา' : 'นอกเวลา';
};

exports.convert_status = (status) => {
	return status === 1
		? 'รอการยืนยัน'
		: status === 2
			? 'อยู่ในคิว'
			: status === 3
				? 'กำลังตรวจ'
				: status === 4 ? 'รอชำระเงิน' : status === 5 ? 'เสร็จสิ้น' : status === 99 ? 'ยกเลิกบริการ' : status;
};

exports.getType = (role) => {
	return role === 'frontMT' ? 'mt' : 'frontPT' ? 'pt' : role;
};

exports.getRole = (role) => {
	return role === 'admin'
		? 'แอดมิน'
		: role === 'frontPT'
			? 'ฟ้อนท์กายภาพ'
			: role === 'frontMT'
				? 'ฟ้อนท์เทคนิคการแพทย์'
				: role === 'pt'
					? 'นักกายภาพบำบัด'
					: role === 'mt'
						? 'นักเทคนิคการแพทย์'
						: role === 'marketing'
							? 'การตลาด'
							: role === 'customer' ? 'ลูกค้า' : role === 'tm' ? 'เจ้าหน้าที่นวดไทย' : role;
};
// exports.compare = (arr1, arr2) => {
// 	if (!arr1 || !arr2) return;
// 	let result;
// 	arr1.forEach((e1, i) =>
// 		arr2.forEach((e2) => {
// 			if (e1.length > 1 && e2.length) {
// 				result = compare(e1, e2);
// 			} else if (e1 !== e2) {
// 				result = false;
// 			} else {
// 				result = true;
// 			}
// 		})
// 	);
// 	return result;
// };
exports.add = (key) => {
	return (a, b) => a + b[key];
};
exports.ArabicNumberToText = (Number) => {
	var Number = CheckNumber(Number);
	var NumberArray = new Array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
	var DigitArray = new Array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
	var BahtText = '';
	if (isNaN(Number)) {
		return 'ข้อมูลนำเข้าไม่ถูกต้อง';
	} else {
		if (parseInt(Number) === 0) {
			return 'ศูนย์บาทถ้วน';
		}
		if (parseInt(Number) === 1) {
			return 'หนึ่งบาทถ้วน';
		}
		if (Number - 0 > 9999999.9999) {
			return 'ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้';
		} else {
			Number = Number.split('.');
			if (Number[1].length > 0) {
				Number[1] = Number[1].substring(0, 2);
			}
			var NumberLen = Number[0].length - 0;
			for (var i = 0; i < NumberLen; i++) {
				var tmp = Number[0].substring(i, i + 1) - 0;
				if (tmp != 0) {
					if (i == NumberLen - 1 && tmp == 1) {
						BahtText += 'เอ็ด';
					} else if (i == NumberLen - 2 && tmp == 2) {
						BahtText += 'ยี่';
					} else if (i == NumberLen - 2 && tmp == 1) {
						BahtText += '';
					} else {
						BahtText += NumberArray[tmp];
					}
					BahtText += DigitArray[NumberLen - i - 1];
				}
			}
			BahtText += 'บาท';
			if (Number[1] == '0' || Number[1] == '00') {
				BahtText += 'ถ้วน';
			} else {
				// DecimalLen = Number[1].length - 0;
				// for (var i = 0; i < DecimalLen; i++) {
				// 	var tmp = Number[1].substring(i, i + 1) - 0;
				// 	if (tmp != 0) {
				// 		if (i == DecimalLen - 1 && tmp == 1) {
				// 			BahtText += 'เอ็ด';
				// 		} else if (i == DecimalLen - 2 && tmp == 2) {
				// 			BahtText += 'ยี่';
				// 		} else if (i == DecimalLen - 2 && tmp == 1) {
				// 			BahtText += '';
				// 		} else {
				// 			BahtText += NumberArray[tmp];
				// 		}
				// 		BahtText += DigitArray[DecimalLen - i - 1];
				// 	}
				// }
				// BahtText += 'สตางค์';
			}
			return BahtText;
		}
	}
};
function CheckNumber(Number) {
	var decimal = false;
	Number = Number.toString();
	Number = Number.replace(/ |,|บาท|฿/gi, '');
	for (var i = 0; i < Number.length; i++) {
		if (Number[i] == '.') {
			decimal = true;
		}
	}
	if (decimal == false) {
		Number = Number + '.00';
	}
	return Number;
}
