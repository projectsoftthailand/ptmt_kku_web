import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from '../libs/pdfmake/build/vfs_fonts';
import moment from 'moment';
import imgFile64 from '../components/Datafile64';
import file64 from '../components/Datafile64';
import { sevenDigits } from './function';
import User from '../mobx/user/user';
import fn from './function';
import { arrList, arrYearly } from '../const/subRole';

let th = require('moment/locale/th');
moment.updateLocale('th', th);
pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.fonts = {
	THSarabunNew: {
		normal: 'THSarabunNew.ttf',
		bold: 'THSarabunNew-Bold.ttf',
		italics: 'THSarabunNew-Italic.ttf',
		bolditalics: 'THSarabunNew-BoldItalic.ttf'
	},
	Roboto: {
		normal: 'Roboto-Regular.ttf',
		bold: 'Roboto-Medium.ttf',
		italics: 'Roboto-Italic.ttf',
		bolditalics: 'Roboto-MediumItalic.ttf'
	}
};

export const pdfResult = (result_pdf, reported_date, received_date, sampling_date) => {
	// console.log('result_pdf', result_pdf);
	// console.log('reported_date', reported_date, 'received_date', received_date, 'sampling_date', sampling_date);
	let d1 = reported_date !== null ? reported_date : '-';
	let d2 = received_date !== null ? received_date : '-';
	let d3 = sampling_date !== null ? sampling_date : '-';
	var docDefinition = {
		pageMargins: [ 35, 230, 35, 150 ],
		//--------------------------------------------------------------Header-------------------------------------------------------------------------//
		header: function(currentPage, pageCount, pageSize) {
			return [
				{
					margin: [ 35, 30, 35, 0 ],
					columns: [
						{
							width: '74%',
							text: 'ใบรายงานผลการตรวจวิเคราะห์ทางห้องปฏิบัติการ',
							style: 'header',
							alignment: 'right'
						},
						[
							{
								margin: [ 0, 2.5, 5, 0 ],
								text: 'ใบรายงานผล FR-QP-CL-022-03',
								alignment: 'right',
								fontSize: 8
							},
							{
								margin: [ 0, 0, 5, 0 ],
								text: 'วันที่ประกาศใช้ 27 ธันวาคม 2554 ฉบับที่ 2',
								alignment: 'right',
								fontSize: 8
							}
						]
					]
				},
				{
					margin: [ 35, 5, 35, 0 ],
					table: {
						widths: [ '50%', '50%' ],
						body: [
							[
								{
									border: [ true, true, true, false ],
									columns: [
										{
											width: 60,
											image: file64.img_result1,
											margin: [ 0, 15, 0, 0 ]
										},
										[
											{
												text: 'โครงการห้องปฏิบัติการชุมชน',
												fontSize: 14,
												bold: true,
												margin: [ 2, 0, 0, 0 ]
											},
											{ text: 'โครงการห้องปฏิบัติการชุมชน', margin: [ 2, 0, 0, 0 ] },
											{
												text:
													'คณะเทคนิคการแพทย์ มหาลัยขอนแก่น 123/2003 หมู่ 16 ถนนมิตรภาพ ต.ในเมือง อ.เมือง จ.ขอนแก่น 40002',
												margin: [ 2, 0, 0, 0 ]
											},
											{
												text: 'Tel. 043-202081 (Office), 043-203585 (Lab)',
												margin: [ 2, 0, 0, 0 ]
											},
											{
												text: 'http://amsalumni.kku.ac.th/clinicnew/index.php',
												margin: [ 2, 0, 0, 0 ]
											}
										]
									]
								},
								{
									margin: [ 0, 20, 0, 0 ],
									border: [ false, true, true, false ],
									columns: [
										[
											{
												columns: [
													{
														text:
															'ชื่อ : ' +
															result_pdf.OTitle +
															result_pdf.OFname +
															' ' +
															result_pdf.OLname
													},
													{
														text: 'อายุ : ' + result_pdf.AgeSYear + ' ปี'
													}
												]
											},
											{
												columns: [
													{
														text: 'HN : ' + result_pdf.HN
													},
													{
														text: 'LN : ' + result_pdf.LabNo
													}
												]
											},
											{
												text: 'วันที่เก็บสิ่งส่งตรวจ : ' + d3
											},
											{
												text: 'วันที่รับสิ่งส่งตรวจ : ' + d2
											},
											{
												text: 'วันที่รายงาน : ' + d1
											}
										]
									]
								}
							],
							[
								{
									border: [ true, true, true, true ],
									columns: [
										{
											width: 110,
											columns: [
												{
													width: 50,
													image: file64.img_result2
												},
												{
													margin: [ 8, 0, 0, 0 ],
													width: 50,
													image: file64.img_result3
												}
											]
										},
										[
											{
												text: 'ห้องปฏิบัติการได้รับการรับรองมาตรฐาน',
												alignment: 'center',
												margin: [ 0, 10, 0, 0 ]
											},
											{ text: 'ISO 15189:2012', alignment: 'center' }
										]
									]
								},
								{
									border: [ false, false, true, true ],
									columns: [
										[
											{
												columns: [
													{
														text: 'น้ำหนัก  ' + result_pdf.Weight + '  kg.'
													},
													{
														text: 'ส่วนสูง  ' + result_pdf.Height + '  cm.'
													},
													{
														text: 'BMI  ' + result_pdf.BMI
													}
												]
											},
											{
												columns: [
													{
														text: 'ความดัน  ' + result_pdf.Systolic + '  มม.ปรอท'
													},
													{
														text: 'ชีพจร  ' + result_pdf.HeartRate + '  ครั้ง/นาที'
													}
												]
											}
										]
									]
								}
							]
						]
					}
				}
			];
		},
		//---------------------------------------------------------------footer------------------------------------------------------------------------//
		footer: function(currentPage, pageCount) {
			return [
				{
					columns: [
						[
							{
								text: 'ผู้รับรองผลการตรวจวิเคราะห์ : ' + result_pdf['ValidateBy'],
								alignment: 'center',
								margin: [ 0, 0, 0, 5 ],
								fontSize: 14,
								bold: true
							},
							{
								columns: [
									{
										text: '* หมายถึง รายการตรวจวิเคราะห์ที่อยู่นอกขอบข่ายการขอรับรอง 15189',
										alignment: 'center'
									},
									{
										margin: [ 0, 0, 30, 0 ],
										text: 'CV หมายถึง ค่าที่อยู่ในช่วงค่าวิกฤต',
										alignment: 'right'
									}
								]
							},
							{
								columns: [
									{
										text: '** หมายถึง รายการตรวจวิเคราะห์ที่รายงานโดยห้องปฏิบัติการรับตรวจต่อ',
										alignment: 'center'
									},
									{
										margin: [ 0, 0, 30, 0 ],
										text: 'L หมายถึง ค่าที่ต่ำกว่าค่าอ้างอิง',
										alignment: 'right'
									}
								]
							},
							{
								margin: [ 0, 0, 30, 0 ],
								columns: [ '', { text: 'H หมายถึง ค่าที่สูงกว่าค่าอ้างอิง', alignment: 'right' } ]
							},
							{
								text: 'หน้าที่ ' + currentPage.toString() + ' / ' + pageCount,
								alignment: 'center'
							}
						]
					]
				}
			];
		},
		content: [
			//-----------------------------------------------------------BIOCHEMISTRY--------------------------------------------------------------//
			(result_pdf['105_Hemocysteine'] ||
				result_pdf['001_Glucose'] ||
				result_pdf['120_GGT'] ||
				result_pdf['002_BUN'] ||
				result_pdf['003_Creatinine'] ||
				result_pdf['004_Uric Acid'] ||
				result_pdf['005_Cholesterol'] ||
				result_pdf['006_Triglyceride'] ||
				result_pdf['008_HDL-C'] ||
				result_pdf['009_LDL'] ||
				result_pdf['010_AST'] ||
				result_pdf['012_ALT'] ||
				result_pdf['013_Alkaline Phos'] ||
				result_pdf['014_HBsAg'] ||
				result_pdf['015_HBsAb'] ||
				result_pdf['017_HBcAb'] ||
				result_pdf['018_AntiHCV'] ||
				result_pdf['076_Total Bil'] ||
				result_pdf['077_Direct Bili'] ||
				result_pdf['079_Indirect Bili'] ||
				result_pdf['088_Albumin'] ||
				result_pdf['089_Globulin'] ||
				result_pdf['091_Calcium'] ||
				result_pdf['092_Inor Phos'] ||
				result_pdf['093_Mg++'] ||
				result_pdf['128_OTH_CH'] ||
				result_pdf['MA'] ||
				result_pdf['Cre(Urine)']) && {
				alignment: 'center',
				text: [
					{
						fontSize: 18,
						bold: true,
						text: 'BIOCHEMISTRY '
					},
					{
						text:
							result_pdf['ChemApproveBy'] == null
								? 'ผู้ตรวจวิเคราะห์ : -'
								: 'ผู้ตรวจวิเคราะห์ : ' + result_pdf['ChemApproveBy']
					}
				]
			},
			(result_pdf['105_Hemocysteine'] ||
				result_pdf['001_Glucose'] ||
				result_pdf['120_GGT'] ||
				result_pdf['002_BUN'] ||
				result_pdf['003_Creatinine'] ||
				result_pdf['004_Uric Acid'] ||
				result_pdf['005_Cholesterol'] ||
				result_pdf['006_Triglyceride'] ||
				result_pdf['008_HDL-C'] ||
				result_pdf['009_LDL'] ||
				result_pdf['010_AST'] ||
				result_pdf['012_ALT'] ||
				result_pdf['013_Alkaline Phos'] ||
				result_pdf['014_HBsAg'] ||
				result_pdf['015_HBsAb'] ||
				result_pdf['017_HBcAb'] ||
				result_pdf['018_AntiHCV'] ||
				result_pdf['076_Total Bil'] ||
				result_pdf['077_Direct Bili'] ||
				result_pdf['079_Indirect Bili'] ||
				result_pdf['088_Albumin'] ||
				result_pdf['089_Globulin'] ||
				result_pdf['091_Calcium'] ||
				result_pdf['092_Inor Phos'] ||
				result_pdf['093_Mg++'] ||
				result_pdf['128_OTH_CH'] ||
				result_pdf['MA'] ||
				result_pdf['Cre(Urine)']) && {
				columns: [
					{
						width: '59%',
						alignment: 'right',
						text: 'คุณภาพ/ปริมาณสิ่งส่งตรวจ ผ่าน'
					},
					[
						{
							width: 15,
							height: 15,
							image: imgFile64.img_filebase64_result
						}
					]
				]
			},
			(result_pdf['105_Hemocysteine'] ||
				result_pdf['001_Glucose'] ||
				result_pdf['120_GGT'] ||
				result_pdf['002_BUN'] ||
				result_pdf['003_Creatinine'] ||
				result_pdf['004_Uric Acid'] ||
				result_pdf['005_Cholesterol'] ||
				result_pdf['006_Triglyceride'] ||
				result_pdf['008_HDL-C'] ||
				result_pdf['009_LDL'] ||
				result_pdf['010_AST'] ||
				result_pdf['012_ALT'] ||
				result_pdf['013_Alkaline Phos'] ||
				result_pdf['014_HBsAg'] ||
				result_pdf['015_HBsAb'] ||
				result_pdf['017_HBcAb'] ||
				result_pdf['018_AntiHCV'] ||
				result_pdf['076_Total Bil'] ||
				result_pdf['077_Direct Bili'] ||
				result_pdf['079_Indirect Bili'] ||
				result_pdf['088_Albumin'] ||
				result_pdf['089_Globulin'] ||
				result_pdf['091_Calcium'] ||
				result_pdf['092_Inor Phos'] ||
				result_pdf['093_Mg++'] ||
				result_pdf['128_OTH_CH'] ||
				result_pdf['MA'] ||
				result_pdf['Cre(Urine)']) && {
				fontSize: 16,
				bold: true,
				text: 'Chemistry'
			},
			(result_pdf['105_Hemocysteine'] ||
				result_pdf['001_Glucose'] ||
				result_pdf['120_GGT'] ||
				result_pdf['002_BUN'] ||
				result_pdf['003_Creatinine'] ||
				result_pdf['004_Uric Acid'] ||
				result_pdf['005_Cholesterol'] ||
				result_pdf['006_Triglyceride'] ||
				result_pdf['008_HDL-C'] ||
				result_pdf['009_LDL'] ||
				result_pdf['010_AST'] ||
				result_pdf['012_ALT'] ||
				result_pdf['013_Alkaline Phos'] ||
				result_pdf['014_HBsAg'] ||
				result_pdf['015_HBsAb'] ||
				result_pdf['017_HBcAb'] ||
				result_pdf['018_AntiHCV'] ||
				result_pdf['076_Total Bil'] ||
				result_pdf['077_Direct Bili'] ||
				result_pdf['079_Indirect Bili'] ||
				result_pdf['088_Albumin'] ||
				result_pdf['089_Globulin'] ||
				result_pdf['091_Calcium'] ||
				result_pdf['092_Inor Phos'] ||
				result_pdf['093_Mg++'] ||
				result_pdf['128_OTH_CH'] ||
				result_pdf['MA'] ||
				result_pdf['Cre(Urine)']) && {
				margin: [ 0, 0, 0, 5 ],
				columns: [
					[
						{
							columns: [
								{ text: '', width: '37%' },
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: 'Reference Value', width: '30%', fontSize: 14, decoration: 'underline' }
							]
						},
						result_pdf['105_Hemocysteine'] && {
							margin: [ 0, 0, 0, 5 ],
							columns: [
								{ text: 'Hemocysteine', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['105_Hemocysteine'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['001_Glucose'] && {
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'NaF', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['001_Glucose'] && {
							margin: [ 0, 0, 0, 5 ],
							columns: [
								{ text: 'Glucose', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['001_Glucose'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( 70 - 110 )', width: '30%' }
							]
						},
						(result_pdf['105_Hemocysteine'] ||
							result_pdf['001_Glucose'] ||
							result_pdf['120_GGT'] ||
							result_pdf['002_BUN'] ||
							result_pdf['003_Creatinine'] ||
							result_pdf['004_Uric Acid'] ||
							result_pdf['005_Cholesterol'] ||
							result_pdf['006_Triglyceride'] ||
							result_pdf['008_HDL-C'] ||
							result_pdf['009_LDL'] ||
							result_pdf['010_AST'] ||
							result_pdf['012_ALT'] ||
							result_pdf['013_Alkaline Phos'] ||
							result_pdf['014_HBsAg'] ||
							result_pdf['015_HBsAb'] ||
							result_pdf['017_HBcAb'] ||
							result_pdf['018_AntiHCV'] ||
							result_pdf['076_Total Bil'] ||
							result_pdf['077_Direct Bili'] ||
							result_pdf['079_Indirect Bili'] ||
							result_pdf['088_Albumin'] ||
							result_pdf['089_Globulin'] ||
							result_pdf['091_Calcium'] ||
							result_pdf['092_Inor Phos'] ||
							result_pdf['093_Mg++'] ||
							result_pdf['128_OTH_CH']) && {
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Serum', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['120_GGT'] && {
							columns: [
								{ text: 'Gamma GT **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['120_GGT'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( 7-50 )', width: '30%' }
							]
						},
						result_pdf['002_BUN'] && {
							columns: [
								{ text: 'BUN', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['002_BUN'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( 5.8 - 19.1 )', width: '30%' }
							]
						},
						result_pdf['003_Creatinine'] && {
							columns: [
								{ text: 'Creatinine', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['003_Creatinine'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( 0.5 - 1.5 )', width: '30%' }
							]
						},
						result_pdf['004_Uric Acid'] && {
							columns: [
								{ text: 'Uric Acid', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['004_Uric Acid'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( 2.7 - 7.0 )', width: '30%' }
							]
						},
						result_pdf['005_Cholesterol'] && {
							columns: [
								{ text: 'Total Cholesterol', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['005_Cholesterol'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( < 200 )', width: '30%' }
							]
						},
						result_pdf['006_Triglyceride'] && {
							columns: [
								{ text: 'Trigiyceride', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['006_Triglyceride'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( < 150 )', width: '30%' }
							]
						},
						result_pdf['008_HDL-C'] && {
							columns: [
								{ text: 'HDL-C', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['008_HDL-C'], width: '15%' },
								{ text: 'L', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( > 40 )', width: '30%' }
							]
						},
						result_pdf['009_LDL'] && {
							columns: [
								{ text: 'LDL-C (Calculated) *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['009_LDL'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( < 100 )', width: '30%' }
							]
						},
						result_pdf['010_AST'] && {
							columns: [
								{ text: 'AST(SGOT)', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['010_AST'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( 12 - 32 )', width: '30%' }
							]
						},
						result_pdf['012_ALT'] && {
							columns: [
								{ text: 'ALT(SGPT)', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['012_ALT'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( 4 - 36 )', width: '30%' }
							]
						},
						result_pdf['013_Alkaline Phos'] && {
							columns: [
								{ text: 'Alkaline Phosphatase', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['013_Alkaline Phos'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( Adult ( 37 - 147 ),Children ( < 390) )', width: '30%' }
							]
						},
						result_pdf['014_HBsAg'] && {
							columns: [
								{ text: 'HBsAg *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['014_HBsAg'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['015_HBsAb'] && {
							columns: [
								{ text: 'HBsAb *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['015_HBsAb'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mIU/mL', width: '15%' },
								{ text: 'Less than 10 mIU/mL', width: '30%' }
							]
						},
						result_pdf['017_HBcAb'] && {
							columns: [
								{ text: 'HBcAb *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['017_HBcAb'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['018_AntiHCV'] && {
							columns: [
								{ text: 'Anti HCV *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['018_AntiHCV'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['076_Total Bil'] && {
							columns: [
								{ text: 'Total Bilirubin *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['076_Total Bil'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '( 0.50 - 1.20)', width: '30%' }
							]
						},
						result_pdf['077_Direct Bili'] && {
							columns: [
								{ text: 'Direct Bilirubin *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['077_Direct Bili'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '(0.10 - 0.50)', width: '30%' }
							]
						},
						result_pdf['079_Indirect Bili'] && {
							columns: [
								{ text: 'Indirect Bilirubin *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['079_Indirect Bili'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '(0.10 - 0.80)', width: '30%' }
							]
						},
						result_pdf['088_Albumin'] && {
							columns: [
								{ text: 'Albumin **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['088_Albumin'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/dL', width: '15%' },
								{ text: '(3.5 - 5.0)', width: '30%' }
							]
						},
						result_pdf['089_Globulin'] && {
							columns: [
								{ text: 'Globulin *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['089_Globulin'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/dL', width: '15%' },
								{ text: '(2.5 - 3.5)', width: '30%' }
							]
						},
						result_pdf['091_Calcium'] && {
							columns: [
								{ text: 'Total Calcium **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['091_Calcium'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '(8.4 - 10.2)', width: '30%' }
							]
						},
						result_pdf['092_Inor Phos'] && {
							columns: [
								{ text: 'Inorganic Phosphate *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['092_Inor Phos'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '(2.7 - 4.5)', width: '30%' }
							]
						},
						result_pdf['093_Mg++'] && {
							columns: [
								{ text: 'Magnesium *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['093_Mg++'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dL', width: '15%' },
								{ text: '(1.6 - 2.3)', width: '30%' }
							]
						},
						result_pdf['128_OTH_CH'] && {
							columns: [
								{ text: 'Remark', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['128_OTH_CH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						(result_pdf['MA'] || result_pdf['Cre(Urine)']) && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Urine', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['MA'] && {
							columns: [
								{ text: 'Urine microalbumin', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['MA'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dl', width: '15%' },
								{ text: '(0.2 - 30.0)', width: '30%' }
							]
						},
						result_pdf['Cre(Urine)'] && {
							columns: [
								{ text: 'Urine creatinine(Random)', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['Cre(Urine)'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/dl', width: '15%' },
								{ text: '', width: '30%' }
							]
						}
					]
				]
			},
			///------------------------------------------------------------HEMATOLOGY--------------------------------------------------------------//
			(result_pdf['109_Rh'] ||
				result_pdf['019_WBC'] ||
				result_pdf['020_NEUT%'] ||
				result_pdf['021_LYMPH%'] ||
				result_pdf['022_MONO%'] ||
				result_pdf['023_EO%'] ||
				result_pdf['024_BASO%'] ||
				result_pdf['031_PLT'] ||
				result_pdf['032_MPV'] ||
				result_pdf['033_RBC'] ||
				result_pdf['034_HGB'] ||
				result_pdf['035_HCT'] ||
				result_pdf['036_MCV'] ||
				result_pdf['037_MCH'] ||
				result_pdf['038_MCHC'] ||
				result_pdf['039_RDW-SD'] ||
				result_pdf['039_RDW-CV'] ||
				result_pdf['040_BF Platelet'] ||
				result_pdf['041_RBC Morp'] ||
				result_pdf['030_Other'] ||
				result_pdf['129_OTH_HM'] ||
				result_pdf['042_Blood Grp']) && {
				pageBreak: 'before',
				alignment: 'center',
				text: [
					{
						fontSize: 18,
						bold: true,
						text: 'HEMATOLOGY '
					},
					{
						text:
							result_pdf['CBCApproveBy'] == null
								? 'ผู้ตรวจวิเคราะห์ : -'
								: 'ผู้ตรวจวิเคราะห์ : ' + result_pdf['CBCApproveBy']
					}
				]
			},
			(result_pdf['109_Rh'] ||
				result_pdf['019_WBC'] ||
				result_pdf['020_NEUT%'] ||
				result_pdf['021_LYMPH%'] ||
				result_pdf['022_MONO%'] ||
				result_pdf['023_EO%'] ||
				result_pdf['024_BASO%'] ||
				result_pdf['031_PLT'] ||
				result_pdf['032_MPV'] ||
				result_pdf['033_RBC'] ||
				result_pdf['034_HGB'] ||
				result_pdf['035_HCT'] ||
				result_pdf['036_MCV'] ||
				result_pdf['037_MCH'] ||
				result_pdf['038_MCHC'] ||
				result_pdf['039_RDW-SD'] ||
				result_pdf['039_RDW-CV'] ||
				result_pdf['040_BF Platelet'] ||
				result_pdf['041_RBC Morp'] ||
				result_pdf['030_Other'] ||
				result_pdf['129_OTH_HM'] ||
				result_pdf['042_Blood Grp']) && {
				columns: [
					{
						width: '59%',
						alignment: 'right',
						text: 'คุณภาพ/ปริมาณสิ่งส่งตรวจ ผ่าน'
					},
					[
						{
							width: 15,
							height: 15,
							image: imgFile64.img_filebase64_result
						}
					]
				]
			},
			result_pdf['109_Rh'] && {
				columns: [
					[
						{
							columns: [
								{
									text: '',
									width: '37%',
									margin: [ 15, 0, 0, 0 ]
								},
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{
									text: 'Reference Value',
									width: '30%',
									fontSize: 14,
									decoration: 'underline'
								}
							]
						},
						{
							columns: [
								{ text: 'Rh group*', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['109_Rh'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						}
					]
				]
			},
			(result_pdf['109_Rh'] ||
				result_pdf['019_WBC'] ||
				result_pdf['020_NEUT%'] ||
				result_pdf['021_LYMPH%'] ||
				result_pdf['022_MONO%'] ||
				result_pdf['023_EO%'] ||
				result_pdf['024_BASO%'] ||
				result_pdf['031_PLT'] ||
				result_pdf['032_MPV'] ||
				result_pdf['033_RBC'] ||
				result_pdf['034_HGB'] ||
				result_pdf['035_HCT'] ||
				result_pdf['036_MCV'] ||
				result_pdf['037_MCH'] ||
				result_pdf['038_MCHC'] ||
				result_pdf['039_RDW-SD'] ||
				result_pdf['039_RDW-CV'] ||
				result_pdf['040_BF Platelet'] ||
				result_pdf['041_RBC Morp'] ||
				result_pdf['030_Other'] ||
				result_pdf['129_OTH_HM'] ||
				result_pdf['042_Blood Grp']) && {
				fontSize: 16,
				bold: true,
				text: 'CBC'
			},
			(result_pdf['109_Rh'] ||
				result_pdf['019_WBC'] ||
				result_pdf['020_NEUT%'] ||
				result_pdf['021_LYMPH%'] ||
				result_pdf['022_MONO%'] ||
				result_pdf['023_EO%'] ||
				result_pdf['024_BASO%'] ||
				result_pdf['031_PLT'] ||
				result_pdf['032_MPV'] ||
				result_pdf['033_RBC'] ||
				result_pdf['034_HGB'] ||
				result_pdf['035_HCT'] ||
				result_pdf['036_MCV'] ||
				result_pdf['037_MCH'] ||
				result_pdf['038_MCHC'] ||
				result_pdf['039_RDW-SD'] ||
				result_pdf['039_RDW-CV'] ||
				result_pdf['040_BF Platelet'] ||
				result_pdf['041_RBC Morp'] ||
				result_pdf['030_Other'] ||
				result_pdf['129_OTH_HM'] ||
				result_pdf['042_Blood Grp']) && {
				columns: [
					[
						{
							columns: [
								{
									text: 'White blood cell',
									width: '37%',
									fontSize: 14,
									bold: true,
									margin: [ 15, 0, 0, 0 ]
								},
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{
									text: 'Reference Value',
									width: '30%',
									fontSize: 14,
									decoration: 'underline'
								}
							]
						},
						(result_pdf['109_Rh'] ||
							result_pdf['019_WBC'] ||
							result_pdf['020_NEUT%'] ||
							result_pdf['021_LYMPH%'] ||
							result_pdf['022_MONO%'] ||
							result_pdf['023_EO%'] ||
							result_pdf['024_BASO%'] ||
							result_pdf['031_PLT'] ||
							result_pdf['032_MPV'] ||
							result_pdf['033_RBC'] ||
							result_pdf['034_HGB'] ||
							result_pdf['035_HCT'] ||
							result_pdf['036_MCV'] ||
							result_pdf['037_MCH'] ||
							result_pdf['038_MCHC'] ||
							result_pdf['039_RDW-SD'] ||
							result_pdf['039_RDW-CV'] ||
							result_pdf['040_BF Platelet'] ||
							result_pdf['041_RBC Morp'] ||
							result_pdf['030_Other'] ||
							result_pdf['129_OTH_HM']) && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'EDTA', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['019_WBC'] && {
							columns: [
								{ text: 'Wbc Count', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['019_WBC'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'x 10^9/L', width: '15%' },
								{ text: '( 4.0 - 10.0 )', width: '30%' }
							]
						},
						result_pdf['020_NEUT%'] && {
							columns: [
								{ text: 'Neutrophil', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['020_NEUT%'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 34 - 71 )', width: '30%' }
							]
						},
						result_pdf['021_LYMPH%'] && {
							columns: [
								{ text: 'Lymphocyte', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['021_LYMPH%'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 19 - 53 )', width: '30%' }
							]
						},
						result_pdf['022_MONO%'] && {
							columns: [
								{ text: 'Monocyte', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['022_MONO%'], width: '15%' },
								{ text: 'L', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 5 - 13 )', width: '30%' }
							]
						},
						result_pdf['023_EO%'] && {
							columns: [
								{ text: 'Eosinophil', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['023_EO%'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 1 - 7 )', width: '30%' }
							]
						},
						result_pdf['024_BASO%'] && {
							columns: [
								{ text: 'Basophil', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['024_BASO%'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 0 - 1 )', width: '30%' }
							]
						},
						result_pdf['031_PLT'] && {
							columns: [
								{ text: 'Platelet count', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['031_PLT'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'x 10^9/L', width: '15%' },
								{ text: '( 140 - 400 )', width: '30%' }
							]
						},
						result_pdf['032_MPV'] && {
							columns: [
								{ text: 'MPV', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['032_MPV'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'fL', width: '15%' },
								{ text: '( 7.0 - 10.8 )', width: '30%' }
							]
						},
						result_pdf['033_RBC'] && {
							columns: [
								{ text: 'Rbc count', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['033_RBC'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'x 10^12/L', width: '15%' },
								{ text: '( Female 4.0 - 5.2, Male 4.6 - 6.1 )', width: '30%' }
							]
						},
						result_pdf['034_HGB'] && {
							columns: [
								{ text: 'Hemoglobin', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['034_HGB'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/dL', width: '15%' },
								{ text: '( Female 11.2 - 15.7, Male 13.7 - 17.5 )', width: '30%' }
							]
						},
						result_pdf['035_HCT'] && {
							columns: [
								{ text: 'Hct (Hematocrit)', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['035_HCT'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( Female 34 - 45, Male 40 - 51 )', width: '30%' }
							]
						},
						result_pdf['036_MCV'] && {
							columns: [
								{ text: 'MCV', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['036_MCV'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'fL', width: '15%' },
								{ text: '( Female 79.4 - 94.8, Male 79.0 - 92.2 )', width: '30%' }
							]
						},
						result_pdf['037_MCH'] && {
							columns: [
								{ text: 'MCH', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['037_MCH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'pg', width: '15%' },
								{ text: '( 25.6 - 32.2 )', width: '30%' }
							]
						},
						result_pdf['038_MCHC'] && {
							columns: [
								{ text: 'MCHC', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['038_MCHC'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/dL', width: '15%' },
								{ text: '( Female 32.2 - 35.5, Male 32.3 - 36.5 )', width: '30%' }
							]
						},
						result_pdf['039_RDW-SD'] && {
							columns: [
								{ text: 'RDW-SD', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['039_RDW-SD'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'fL', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['039_RDW-CV'] && {
							columns: [
								{ text: 'RDW', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['039_RDW-CV'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 11.5 - 14.0 )', width: '30%' }
							]
						},
						result_pdf['040_BF Platelet'] && {
							columns: [
								{ text: 'Blood Film Platelet estimation', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['040_BF Platelet'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['041_RBC Morp'] && {
							columns: [
								{ text: 'RBC Morphology', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['041_RBC Morp'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'pg', width: '15%' },
								{ text: '( 25.6 - 32.2 )', width: '30%' }
							]
						},
						result_pdf['030_Other'] && {
							columns: [
								{ text: 'Other', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['030_Other'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['129_OTH_HM'] && {
							columns: [
								{ text: 'Remark', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['129_OTH_HM'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['042_Blood Grp'] && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'EDTA1', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['042_Blood Grp'] && {
							columns: [
								{ text: 'ABO Blood gr.', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['042_Blood Grp'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						}
					]
				]
			},
			//-------------------------------------------------------------ส่งตรวจภายนอก------------------------------------------------------------//
			(result_pdf['132_DCIP'] ||
				result_pdf['130_Toluene'] ||
				result_pdf['117_insulin'] ||
				result_pdf['114_BEN'] ||
				result_pdf['107_HCG'] ||
				result_pdf['116_FT3'] ||
				result_pdf['112_ferritin'] ||
				result_pdf['115_XYL'] ||
				result_pdf['108_TPHA'] ||
				result_pdf['113_mel'] ||
				result_pdf['119_Occult'] ||
				result_pdf['106_Pb'] ||
				result_pdf['070_Xray'] ||
				result_pdf['102_CA19-9'] ||
				result_pdf['139_OTH_OUT'] ||
				result_pdf['098_RF'] ||
				result_pdf['071_HbA1c'] ||
				result_pdf['105_ESR'] ||
				result_pdf['074_FT4'] ||
				result_pdf['083_Na+'] ||
				result_pdf['084_K++'] ||
				result_pdf['085_Chloride'] ||
				result_pdf['086_Total CO2'] ||
				result_pdf['090_Fructosamine'] ||
				result_pdf['094_HIV'] ||
				result_pdf['095_VDRL'] ||
				result_pdf['096_RubAb'] ||
				result_pdf['097_Cortisol'] ||
				result_pdf['100_CA125'] ||
				result_pdf['101_CA15-3'] ||
				result_pdf['104_PSA'] ||
				result_pdf['122_CPK'] ||
				result_pdf['134_Anti-TPO'] ||
				result_pdf['125_FSH'] ||
				result_pdf['124_ANA'] ||
				result_pdf['123_CK-MB'] ||
				result_pdf['121_LDH'] ||
				result_pdf['126_LH'] ||
				result_pdf['099_CEA'] ||
				result_pdf['103_AFP'] ||
				result_pdf['087_Total Protein']) && {
				pageBreak: 'before',
				alignment: 'center',
				text: [
					{
						fontSize: 18,
						bold: true,
						text: 'ส่งตรวจภายนอก '
					},
					{
						text: 'ผู้ตรวจวิเคราะห์ : -'
					}
				]
			},
			(result_pdf['132_DCIP'] ||
				result_pdf['130_Toluene'] ||
				result_pdf['117_insulin'] ||
				result_pdf['114_BEN'] ||
				result_pdf['107_HCG'] ||
				result_pdf['116_FT3'] ||
				result_pdf['112_ferritin'] ||
				result_pdf['115_XYL'] ||
				result_pdf['108_TPHA'] ||
				result_pdf['113_mel'] ||
				result_pdf['119_Occult'] ||
				result_pdf['106_Pb'] ||
				result_pdf['070_Xray'] ||
				result_pdf['102_CA19-9'] ||
				result_pdf['139_OTH_OUT'] ||
				result_pdf['098_RF'] ||
				result_pdf['071_HbA1c'] ||
				result_pdf['105_ESR'] ||
				result_pdf['074_FT4'] ||
				result_pdf['083_Na+'] ||
				result_pdf['084_K++'] ||
				result_pdf['085_Chloride'] ||
				result_pdf['086_Total CO2'] ||
				result_pdf['090_Fructosamine'] ||
				result_pdf['094_HIV'] ||
				result_pdf['095_VDRL'] ||
				result_pdf['096_RubAb'] ||
				result_pdf['097_Cortisol'] ||
				result_pdf['100_CA125'] ||
				result_pdf['101_CA15-3'] ||
				result_pdf['104_PSA'] ||
				result_pdf['122_CPK'] ||
				result_pdf['134_Anti-TPO'] ||
				result_pdf['125_FSH'] ||
				result_pdf['124_ANA'] ||
				result_pdf['123_CK-MB'] ||
				result_pdf['121_LDH'] ||
				result_pdf['126_LH'] ||
				result_pdf['099_CEA'] ||
				result_pdf['103_AFP'] ||
				result_pdf['087_Total Protein']) && {
				columns: [
					{
						width: '59%',
						alignment: 'right',
						text: 'คุณภาพ/ปริมาณสิ่งส่งตรวจ ผ่าน'
					},
					[
						{
							width: 15,
							height: 15,
							image: imgFile64.img_filebase64_result
						}
					]
				]
			},
			(result_pdf['132_DCIP'] ||
				result_pdf['130_Toluene'] ||
				result_pdf['117_insulin'] ||
				result_pdf['114_BEN'] ||
				result_pdf['107_HCG'] ||
				result_pdf['116_FT3'] ||
				result_pdf['112_ferritin'] ||
				result_pdf['115_XYL'] ||
				result_pdf['108_TPHA'] ||
				result_pdf['113_mel'] ||
				result_pdf['119_Occult'] ||
				result_pdf['106_Pb'] ||
				result_pdf['070_Xray'] ||
				result_pdf['102_CA19-9'] ||
				result_pdf['139_OTH_OUT'] ||
				result_pdf['098_RF'] ||
				result_pdf['071_HbA1c'] ||
				result_pdf['105_ESR'] ||
				result_pdf['074_FT4'] ||
				result_pdf['083_Na+'] ||
				result_pdf['084_K++'] ||
				result_pdf['085_Chloride'] ||
				result_pdf['086_Total CO2'] ||
				result_pdf['090_Fructosamine'] ||
				result_pdf['094_HIV'] ||
				result_pdf['095_VDRL'] ||
				result_pdf['096_RubAb'] ||
				result_pdf['097_Cortisol'] ||
				result_pdf['100_CA125'] ||
				result_pdf['101_CA15-3'] ||
				result_pdf['104_PSA']) && {
				columns: [
					[
						{
							columns: [
								{
									text: '',
									width: '37%',
									margin: [ 15, 0, 0, 0 ]
								},
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{
									text: 'Reference Value',
									width: '30%',
									fontSize: 14,
									decoration: 'underline'
								}
							]
						},
						result_pdf['132_DCIP'] && {
							columns: [
								{ text: 'DCIP', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['132_DCIP'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['130_Toluene'] && {
							columns: [
								{ text: 'Toluene', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['130_Toluene'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/g cr.', width: '15%' },
								{ text: 'บุคคลทั่วไป < 1.5 , บุคคลที่สัมผัส < 2.5', width: '30%' }
							]
						},
						result_pdf['117_insulin'] && {
							columns: [
								{ text: 'Insulin **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['117_insulin'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['114_BEN'] && {
							columns: [
								{ text: 'Benzene **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['114_BEN'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mg/L', width: '15%' },
								{ text: 'บุคคลทั่วไป < 20 , บุคคลที่สัมผัส < 50', width: '30%' }
							]
						},
						result_pdf['107_HCG'] && {
							columns: [
								{ text: 'HCG **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['107_HCG'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mlU/ml', width: '15%' },
								{ text: '0 - 5.3', width: '30%' }
							]
						},
						result_pdf['116_FT3'] && {
							columns: [
								{ text: 'FT3 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['116_FT3'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'pg/ml', width: '15%' },
								{ text: '2.0 - 4.4', width: '30%' }
							]
						},
						result_pdf['112_ferritin'] && {
							columns: [
								{ text: 'Serum ferrintin **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['112_ferritin'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ng/ml', width: '15%' },
								{ text: '12 - 300', width: '30%' }
							]
						},
						result_pdf['115_XYL'] && {
							columns: [
								{ text: 'Xylene **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['115_XYL'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/g creatinine', width: '15%' },
								{ text: '< 1.5 g/g Xylene', width: '30%' }
							]
						},
						result_pdf['108_TPHA'] && {
							columns: [
								{ text: 'TPHA **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['108_TPHA'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['113_mel'] && {
							columns: [
								{ text: 'Melioidosis Ab', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['113_mel'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '< 160', width: '30%' }
							]
						},
						result_pdf['119_Occult'] && {
							columns: [
								{ text: 'Occult Blood **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['119_Occult'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['106_Pb'] && {
							columns: [
								{ text: 'Lead in whole blood **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['106_Pb'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ug/dl', width: '15%' },
								{ text: '< 30 ug/dl', width: '30%' }
							]
						},
						result_pdf['070_Xray'] && {
							columns: [
								{ text: 'X-RAY **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['070_Xray'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['102_CA19-9'] && {
							columns: [
								{ text: 'CA 19 - 9 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['102_CA19-9'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/mL', width: '15%' },
								{ text: '( 0.00 - 39.00)', width: '30%' }
							]
						},
						result_pdf['139_OTH_OUT'] && {
							columns: [
								{ text: 'Remark', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['139_OTH_OUT'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['098_RF'] && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Blood Clot', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['098_RF'] && {
							columns: [
								{ text: 'Rheumatoid factor test - Latex **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['098_RF'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						(result_pdf['071_HbA1c'] || result_pdf['105_ESR']) && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'EDTA', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['071_HbA1c'] && {
							columns: [
								{ text: 'HbA1c **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['071_HbA1c'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '%', width: '15%' },
								{ text: '( 4.6 - 6.2 )', width: '30%' }
							]
						},
						result_pdf['105_ESR'] && {
							columns: [
								{
									text: 'ESR(Erythrocyte Sedimentation Rate) **',
									width: '37%',
									margin: [ 15, 0, 0, 0 ]
								},
								{ text: result_pdf['105_ESR'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mm/hr.', width: '15%' },
								{ text: '( Female: 0-20., Male 0-15 , Child 0-15 )', width: '30%' }
							]
						},
						(result_pdf['074_FT4'] ||
							result_pdf['083_Na+'] ||
							result_pdf['084_K++'] ||
							result_pdf['085_Chloride'] ||
							result_pdf['086_Total CO2'] ||
							result_pdf['090_Fructosamine'] ||
							result_pdf['094_HIV'] ||
							result_pdf['095_VDRL'] ||
							result_pdf['096_RubAb'] ||
							result_pdf['097_Cortisol'] ||
							result_pdf['100_CA125'] ||
							result_pdf['101_CA15-3'] ||
							result_pdf['104_PSA']) && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Serum', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['074_FT4'] && {
							columns: [
								{ text: 'FT4 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['074_FT4'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ng/dL', width: '15%' },
								{ text: '( 0.93-1.7 )', width: '30%' }
							]
						},
						result_pdf['083_Na+'] && {
							columns: [
								{ text: 'Sodium **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['083_Na+'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mmol/L', width: '15%' },
								{ text: '( 130 - 147 )', width: '30%' }
							]
						},
						result_pdf['084_K++'] && {
							columns: [
								{ text: 'Potassium **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['084_K++'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mmol/L', width: '15%' },
								{ text: '( 3.4 - 4.7 )', width: '30%' }
							]
						},
						result_pdf['085_Chloride'] && {
							columns: [
								{ text: 'Chloride **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['085_Chloride'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mmol/L', width: '15%' },
								{ text: '( 97 - 107 )', width: '30%' }
							]
						},
						result_pdf['086_Total CO2'] && {
							columns: [
								{ text: 'Total CO2 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['086_Total CO2'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mmol/L', width: '15%' },
								{ text: '( 22 - 30 )', width: '30%' }
							]
						},
						result_pdf['090_Fructosamine'] && {
							columns: [
								{ text: 'Fructosamine **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['090_Fructosamine'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['094_HIV'] && {
							columns: [
								{ text: 'Anti HIV - Ab (screening) **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['094_HIV'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['095_VDRL'] && {
							columns: [
								{ text: 'VDRL **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['095_VDRL'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['096_RubAb'] && {
							columns: [
								{ text: 'Rubella Ab **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['096_RubAb'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'IU/ml', width: '15%' },
								{ text: '0.00 - 10.00', width: '30%' }
							]
						},
						result_pdf['097_Cortisol'] && {
							columns: [
								{ text: 'Cortisol **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['097_Cortisol'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ug/dL', width: '15%' },
								{
									text: '( Morning hours 7-10 am: 6.2 - 19.4 , Afternoon hours 4-8 pm: 2.3 - 11.9 )',
									width: '30%'
								}
							]
						},
						result_pdf['100_CA125'] && {
							columns: [
								{ text: 'CA 125 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['100_CA125'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/mL', width: '15%' },
								{ text: '( 0.00 - 35.00)', width: '30%' }
							]
						},
						result_pdf['101_CA15-3'] && {
							columns: [
								{ text: 'CA 15 - 3 **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['101_CA15-3'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/mL', width: '15%' },
								{ text: '( 0.00 - 25.00)', width: '30%' }
							]
						},
						result_pdf['104_PSA'] && {
							columns: [
								{ text: 'PSA **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['104_PSA'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ng/mL', width: '15%' },
								{ text: '( 0.00 - 4.000)', width: '30%' }
							]
						}
					]
				]
			},
			(result_pdf['122_CPK'] ||
				result_pdf['134_Anti-TPO'] ||
				result_pdf['125_FSH'] ||
				result_pdf['124_ANA'] ||
				result_pdf['123_CK-MB'] ||
				result_pdf['121_LDH'] ||
				result_pdf['126_LH'] ||
				result_pdf['099_CEA'] ||
				result_pdf['103_AFP'] ||
				result_pdf['087_Total Protein']) && {
				fontSize: 16,
				bold: true,
				text: 'Chemistry'
			},
			(result_pdf['122_CPK'] ||
				result_pdf['134_Anti-TPO'] ||
				result_pdf['125_FSH'] ||
				result_pdf['124_ANA'] ||
				result_pdf['123_CK-MB'] ||
				result_pdf['121_LDH'] ||
				result_pdf['126_LH'] ||
				result_pdf['099_CEA'] ||
				result_pdf['103_AFP'] ||
				result_pdf['087_Total Protein']) && {
				margin: [ 0, 0, 0, 5 ],
				columns: [
					[
						{
							columns: [
								{ text: '', width: '37%' },
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: 'Reference Value', width: '30%', fontSize: 14, decoration: 'underline' }
							]
						},
						result_pdf['122_CPK'] && {
							columns: [
								{ text: 'CPK **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['122_CPK'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '(Male = 0-190),(Female = 0-170)', width: '30%' }
							]
						},
						result_pdf['134_Anti-TPO'] && {
							columns: [
								{ text: 'Anti-TPO **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['134_Anti-TPO'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['125_FSH'] && {
							columns: [
								{ text: 'FSH **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['125_FSH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'mlU/ml', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['124_ANA'] && {
							columns: [
								{ text: 'ANA **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['124_ANA'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['123_CK-MB'] && {
							columns: [
								{ text: 'CK-MB **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['123_CK-MB'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( 0 - 25 )', width: '30%' }
							]
						},
						result_pdf['121_LDH'] && {
							columns: [
								{ text: 'LDH **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['121_LDH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'U/L', width: '15%' },
								{ text: '( 89 - 221 )', width: '30%' }
							]
						},
						result_pdf['126_LH'] && {
							columns: [
								{ text: 'LH **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['126_LH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						(result_pdf['099_CEA'] || result_pdf['103_AFP'] || result_pdf['087_Total Protein']) && {
							margin: [ 0, 5, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Serum', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['099_CEA'] && {
							columns: [
								{ text: 'CEA **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['099_CEA'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ng/mL', width: '15%' },
								{ text: '( 0 - 4.7 )', width: '30%' }
							]
						},
						result_pdf['103_AFP'] && {
							columns: [
								{ text: 'AFP **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['103_AFP'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ng/mL', width: '15%' },
								{ text: '( 0.00 - 7.00 )', width: '30%' }
							]
						},
						result_pdf['087_Total Protein'] && {
							columns: [
								{ text: 'Total Protein **', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['087_Total Protein'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'g/dL', width: '15%' },
								{ text: '( 6.5 - 8.8 )', width: '30%' }
							]
						}
					]
				]
			},
			//-------------------------------------------------------------STOOL EXAMINATION-------------------------------------------------------//
			(result_pdf['043_Parasite'] || result_pdf['138_OTH_PR']) && {
				pageBreak: 'before',
				alignment: 'center',
				text: [
					{
						fontSize: 18,
						bold: true,
						text: 'STOOL EXAMINATION '
					},
					{
						text:
							result_pdf['StoolApproveBy'] == null
								? 'ผู้ตรวจวิเคราะห์ : -'
								: 'ผู้ตรวจวิเคราะห์ : ' + result_pdf['StoolApproveBy']
					}
				]
			},
			(result_pdf['043_Parasite'] || result_pdf['138_OTH_PR']) && {
				columns: [
					{
						width: '59%',
						alignment: 'right',
						text: 'คุณภาพ/ปริมาณสิ่งส่งตรวจ ผ่าน'
					},
					[
						{
							width: 15,
							height: 15,
							image: imgFile64.img_filebase64_result
						}
					]
				]
			},
			(result_pdf['043_Parasite'] || result_pdf['138_OTH_PR']) && {
				columns: [
					[
						{
							columns: [
								{
									text: '',
									width: '37%'
								},
								{
									text: 'Result',
									width: '15%',
									fontSize: 15,
									bold: true,
									decoration: 'underline'
								},
								{ text: '', width: '3%' },
								{
									text: 'Unit',
									width: '15%',
									fontSize: 15,
									bold: true,
									decoration: 'underline'
								},
								{
									text: 'Reference Value',
									width: '30%',
									fontSize: 15,
									bold: true,
									decoration: 'underline'
								}
							]
						},
						(result_pdf['043_Parasite'] || result_pdf['138_OTH_PR']) && {
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Stool', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['043_Parasite'] && {
							columns: [
								{ text: 'Stool Examination *', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['043_Parasite'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['138_OTH_PR'] && {
							columns: [
								{ text: 'Remark', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['138_OTH_PR'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						}
					]
				]
			},
			//-------------------------------------------------------------URINALYSIS-------------------------------------------------------------//
			(result_pdf['110_Amphetamine'] ||
				result_pdf['044_Preg'] ||
				result_pdf['045_Centrifuged'] ||
				result_pdf['046_uColor'] ||
				result_pdf['047_uClarity'] ||
				result_pdf['049_Sp_gr'] ||
				result_pdf['048_pH'] ||
				result_pdf['050_uProtein'] ||
				result_pdf['051_uGlucose'] ||
				result_pdf['052_uKetone'] ||
				result_pdf['053_uOccBld'] ||
				result_pdf['058_uWBC'] ||
				result_pdf['059_uRBC'] ||
				result_pdf['060_Squam'] ||
				result_pdf['061_Transit'] ||
				result_pdf['062_Renal'] ||
				result_pdf['063_Crystal'] ||
				result_pdf['064_Bacteria'] ||
				result_pdf['065_Hyaline'] ||
				result_pdf['066_Granular'] ||
				result_pdf['067_uMucous'] ||
				result_pdf['068_uAmorph'] ||
				result_pdf['069_Urine Other'] ||
				result_pdf['137_OTH_U']) && {
				pageBreak: 'before',
				alignment: 'center',
				text: [
					{
						fontSize: 18,
						bold: true,
						text: 'URINALYSIS '
					},
					{
						text:
							result_pdf['UrineApproveBy'] == null
								? 'ผู้ตรวจวิเคราะห์ : -'
								: 'ผู้ตรวจวิเคราะห์ : ' + result_pdf['UrineApproveBy']
					}
				]
			},
			(result_pdf['110_Amphetamine'] ||
				result_pdf['044_Preg'] ||
				result_pdf['045_Centrifuged'] ||
				result_pdf['046_uColor'] ||
				result_pdf['047_uClarity'] ||
				result_pdf['049_Sp_gr'] ||
				result_pdf['048_pH'] ||
				result_pdf['050_uProtein'] ||
				result_pdf['051_uGlucose'] ||
				result_pdf['052_uKetone'] ||
				result_pdf['053_uOccBld'] ||
				result_pdf['058_uWBC'] ||
				result_pdf['059_uRBC'] ||
				result_pdf['060_Squam'] ||
				result_pdf['061_Transit'] ||
				result_pdf['062_Renal'] ||
				result_pdf['063_Crystal'] ||
				result_pdf['064_Bacteria'] ||
				result_pdf['065_Hyaline'] ||
				result_pdf['066_Granular'] ||
				result_pdf['067_uMucous'] ||
				result_pdf['068_uAmorph'] ||
				result_pdf['069_Urine Other'] ||
				result_pdf['137_OTH_U']) && {
				columns: [
					{
						width: '59%',
						alignment: 'right',
						text: 'คุณภาพ/ปริมาณสิ่งส่งตรวจ ผ่าน'
					},
					[
						{
							width: 15,
							height: 15,
							image: imgFile64.img_filebase64_result
						}
					]
				]
			},
			(result_pdf['110_Amphetamine'] ||
				result_pdf['044_Preg'] ||
				result_pdf['045_Centrifuged'] ||
				result_pdf['046_uColor'] ||
				result_pdf['047_uClarity'] ||
				result_pdf['049_Sp_gr'] ||
				result_pdf['048_pH'] ||
				result_pdf['050_uProtein'] ||
				result_pdf['051_uGlucose'] ||
				result_pdf['052_uKetone'] ||
				result_pdf['053_uOccBld'] ||
				result_pdf['058_uWBC'] ||
				result_pdf['059_uRBC'] ||
				result_pdf['060_Squam'] ||
				result_pdf['061_Transit'] ||
				result_pdf['062_Renal'] ||
				result_pdf['063_Crystal'] ||
				result_pdf['064_Bacteria'] ||
				result_pdf['065_Hyaline'] ||
				result_pdf['066_Granular'] ||
				result_pdf['067_uMucous'] ||
				result_pdf['068_uAmorph'] ||
				result_pdf['069_Urine Other'] ||
				result_pdf['137_OTH_U']) && {
				fontSize: 16,
				bold: true,
				text: 'Routine Urinalysis'
			},
			(result_pdf['110_Amphetamine'] ||
				result_pdf['044_Preg'] ||
				result_pdf['045_Centrifuged'] ||
				result_pdf['046_uColor'] ||
				result_pdf['047_uClarity'] ||
				result_pdf['049_Sp_gr'] ||
				result_pdf['048_pH'] ||
				result_pdf['050_uProtein'] ||
				result_pdf['051_uGlucose'] ||
				result_pdf['052_uKetone'] ||
				result_pdf['053_uOccBld'] ||
				result_pdf['058_uWBC'] ||
				result_pdf['059_uRBC'] ||
				result_pdf['060_Squam'] ||
				result_pdf['061_Transit'] ||
				result_pdf['062_Renal'] ||
				result_pdf['063_Crystal'] ||
				result_pdf['064_Bacteria'] ||
				result_pdf['065_Hyaline'] ||
				result_pdf['066_Granular'] ||
				result_pdf['067_uMucous'] ||
				result_pdf['068_uAmorph'] ||
				result_pdf['069_Urine Other'] ||
				result_pdf['137_OTH_U']) && {
				columns: [
					[
						result_pdf['110_Amphetamine'] && {
							columns: [
								{ text: 'Urine amphetamine', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['110_Amphetamine'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: 'Negative', width: '30%' }
							]
						},
						(result_pdf['044_Preg'] ||
							result_pdf['045_Centrifuged'] ||
							result_pdf['046_uColor'] ||
							result_pdf['047_uClarity'] ||
							result_pdf['049_Sp_gr'] ||
							result_pdf['048_pH'] ||
							result_pdf['050_uProtein'] ||
							result_pdf['051_uGlucose'] ||
							result_pdf['052_uKetone'] ||
							result_pdf['053_uOccBld'] ||
							result_pdf['058_uWBC'] ||
							result_pdf['059_uRBC'] ||
							result_pdf['060_Squam'] ||
							result_pdf['061_Transit'] ||
							result_pdf['062_Renal'] ||
							result_pdf['063_Crystal'] ||
							result_pdf['064_Bacteria'] ||
							result_pdf['065_Hyaline'] ||
							result_pdf['066_Granular'] ||
							result_pdf['067_uMucous'] ||
							result_pdf['068_uAmorph'] ||
							result_pdf['069_Urine Other'] ||
							result_pdf['137_OTH_U']) && {
							columns: [
								{
									text: 'Physical & Chemical exam',
									width: '37%',
									margin: [ 15, 0, 0, 0 ],
									fontSize: 14,
									bold: true
								},
								{ text: 'Result', width: '15%', fontSize: 14, decoration: 'underline' },
								{ text: '', width: '3%' },
								{ text: 'Unit', width: '15%', fontSize: 14, decoration: 'underline' },
								{
									text: 'Reference Value',
									width: '30%',
									fontSize: 14,
									decoration: 'underline'
								}
							]
						},
						(result_pdf['044_Preg'] ||
							result_pdf['045_Centrifuged'] ||
							result_pdf['046_uColor'] ||
							result_pdf['047_uClarity'] ||
							result_pdf['049_Sp_gr'] ||
							result_pdf['048_pH'] ||
							result_pdf['050_uProtein'] ||
							result_pdf['051_uGlucose'] ||
							result_pdf['052_uKetone'] ||
							result_pdf['053_uOccBld'] ||
							result_pdf['058_uWBC'] ||
							result_pdf['059_uRBC'] ||
							result_pdf['060_Squam'] ||
							result_pdf['061_Transit'] ||
							result_pdf['062_Renal'] ||
							result_pdf['063_Crystal'] ||
							result_pdf['064_Bacteria'] ||
							result_pdf['065_Hyaline'] ||
							result_pdf['066_Granular'] ||
							result_pdf['067_uMucous'] ||
							result_pdf['068_uAmorph'] ||
							result_pdf['069_Urine Other'] ||
							result_pdf['137_OTH_U']) && {
							margin: [ 0, 10, 0, 0 ],
							columns: [
								{
									columns: [ 'ชนิดสิ่งส่งตรวจ', 'Urine', '' ],
									width: '37%'
								},
								{ text: '', width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['044_Preg'] && {
							columns: [
								{ text: 'Pregnancy test', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['044_Preg'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['045_Centrifuged'] && {
							columns: [
								{ text: 'Centrifuged', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['045_Centrifuged'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: 'ml', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['046_uColor'] && {
							columns: [
								{ text: 'Color', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['046_uColor'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Yellow )', width: '30%' }
							]
						},
						result_pdf['047_uClarity'] && {
							columns: [
								{ text: 'Clarity', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['047_uClarity'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Clear )', width: '30%' }
							]
						},
						result_pdf['049_Sp_gr'] && {
							columns: [
								{ text: 'Sp.gr', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['049_Sp_gr'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( 1.003 - 1.030 )', width: '30%' }
							]
						},
						result_pdf['048_pH'] && {
							columns: [
								{ text: 'pH', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['048_pH'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( 4.5 - 8.0 )', width: '30%' }
							]
						},
						result_pdf['050_uProtein'] && {
							columns: [
								{ text: 'Protein', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['050_uProtein'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Negative )', width: '30%' }
							]
						},
						result_pdf['051_uGlucose'] && {
							columns: [
								{ text: 'Glucose', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['051_uGlucose'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Negative )', width: '30%' }
							]
						},
						result_pdf['052_uKetone'] && {
							columns: [
								{ text: 'Kentone', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['052_uKetone'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Negative )', width: '30%' }
							]
						},
						result_pdf['053_uOccBld'] && {
							columns: [
								{ text: 'Blood', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['053_uOccBld'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '( Negative )', width: '30%' }
							]
						},
						result_pdf['058_uWBC'] && {
							columns: [
								{ text: 'WBC', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['058_uWBC'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( 0 - 5 )', width: '30%' }
							]
						},
						result_pdf['059_uRBC'] && {
							columns: [
								{ text: 'RBC', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['059_uRBC'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( 0 - 5 )', width: '30%' }
							]
						},
						result_pdf['060_Squam'] && {
							columns: [
								{ text: 'Squamous epithelial cell', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['060_Squam'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( 0 - 5 )', width: '30%' }
							]
						},
						result_pdf['061_Transit'] && {
							columns: [
								{ text: 'Transitional epithelial cell', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['061_Transit'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( 0 - 5 )', width: '30%' }
							]
						},
						result_pdf['062_Renal'] && {
							columns: [
								{ text: 'Renal epithelial cell', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['062_Renal'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( 0 - 5 )', width: '30%' }
							]
						},
						result_pdf['063_Crystal'] && {
							columns: [
								{ text: 'Crystal', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['063_Crystal'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( No abnormal crystal )', width: '30%' }
							]
						},
						result_pdf['064_Bacteria'] && {
							columns: [
								{ text: 'Bacteria', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['064_Bacteria'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/HP', width: '15%' },
								{ text: '( Negative )', width: '30%' }
							]
						},
						result_pdf['065_Hyaline'] && {
							columns: [
								{ text: 'Hyaline cast', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['065_Hyaline'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/LP', width: '15%' },
								{ text: '( 0 - 3 )', width: '30%' }
							]
						},
						result_pdf['066_Granular'] && {
							columns: [
								{ text: 'Granular cast', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['066_Granular'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '/LP', width: '15%' },
								{ text: '( 0 - 1 )', width: '30%' }
							]
						},
						result_pdf['067_uMucous'] && {
							columns: [
								{ text: 'Mucous', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['067_uMucous'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['068_uAmorph'] && {
							columns: [
								{ text: 'Amorph', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['068_uAmorph'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['069_Urine Other'] && {
							columns: [
								{ text: 'Other', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['069_Urine Other'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						},
						result_pdf['137_OTH_U'] && {
							columns: [
								{ text: 'Remark', width: '37%', margin: [ 15, 0, 0, 0 ] },
								{ text: result_pdf['137_OTH_U'], width: '15%' },
								{ text: '', width: '3%' },
								{ text: '', width: '15%' },
								{ text: '', width: '30%' }
							]
						}
					]
				]
			}
		],
		styles: {
			header: {
				fontSize: 18,
				bold: true
			}
		},
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
//---------------------------------------------------------------------- LIST -----------------------------------------------------------------------------//
export const pdfList = (
	ln,
	nameHR,
	hn,
	personType,
	datas_pdf,
	payType,
	priceNewPackage,
	newPackage,
	discount,
	outSidePrice,
	servicePrice,
	f_name,
	l_name
) => {
	// console.log('datas_pdf', datas_pdf);
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				margin: [ 0, 0, 0, 0 ],
				text: 'ใบแจ้งค่าตรวจสุขภาพ',
				alignment: 'center',
				fontSize: 25,
				bold: true
			},
			{
				margin: [ 0, 0, 0, 15 ],
				alignment: 'justify',
				columns: [
					[
						{
							width: '50%',
							columns: [
								{
									width: 70,
									height: 70,
									image: file64.img_filebase64
								}
							]
						}
					],
					[
						{
							text: ln == null ? 'เลขที่เอกสาร / Document No: -' : 'เลขที่เอกสาร / Document No: ' + ln, //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						},
						{
							text:
								'วันที่ / Date: ' +
								moment(new Date()).format('DD MMMM') +
								' ' +
								(Number(moment(new Date()).format('YYYY')) + 543), //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						},
						{
							text: 'ชื่อ / Patient Name: ' + nameHR, //ใส่ค่าที่ต้องการ
							alignment: 'right',
							fontSize: 15
						},
						personType === true
							? {
									text: `เลขประจำตัวผู้ป่วย / HN: ${sevenDigits(hn)}`, //ใส่ค่าที่ต้องการ
									alignment: 'right',
									fontSize: 15
								}
							: {
									text: `เลขประจำตัวผู้ป่วย / HN: ${nameHR}`, //ใส่ค่าที่ต้องการ
									alignment: 'right',
									fontSize: 15
								}
					]
				]
			},
			{
				margin: [ 0, 0, 0, 0 ],
				table: {
					widths: [ '8%', '45%', '26%', '13%', '8%' ],
					heights: [ 15, 15, 15, 15, 15 ],
					body: [
						[
							{
								margin: [ 5, 0, 0, 0 ],
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ที่',
								fontSize: 17
							},
							{
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'รายการตรวจ',
								fontSize: 17
							},
							{
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'เลขกรมบัญชีกลาง',
								fontSize: 17,
								alignment: 'right'
							},
							{
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ราคาที่เบิกได้',
								fontSize: 17,
								alignment: 'right'
							},
							{
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ราคา',
								fontSize: 17,
								alignment: 'right'
							}
						]
					]
				}
			},
			datas_pdf.map((el, i) => {
				return {
					columns: [
						{
							margin: [ 10, 0, 0, 0 ],
							width: '8%',
							text: i + 1,
							fontSize: 16
						},
						{
							margin: [ 10, 0, 0, 0 ],
							width: '45%',
							text: el.detail == null ? el.treatment_name : el.treatment_name + ' (' + el.detail + ')',
							fontSize: 16
						},
						{
							margin: [ 0, 0, 10, 0 ],
							width: '26%',
							text: el.cgd_code,
							fontSize: 16,
							alignment: 'right'
						},
						{
							margin: [ 0, 0, 10, 0 ],
							width: '13%',
							text: el.disburseable.toLocaleString(),
							fontSize: 16,
							alignment: 'right'
						},
						{
							margin: [ 0, 0, 5, 0 ],
							width: '8%',
							text: el.cost.toLocaleString(),
							fontSize: 16,
							alignment: 'right'
						}
					]
				};
			}),
			priceNewPackage === 0
				? null
				: {
						margin: [ 0, 5, 0, 0 ],
						text: 'รายการตรวจเพิ่มเติม',
						fontSize: 16,
						decoration: 'underline'
					},
			newPackage.map((el, i) => {
				return {
					columns: [
						{
							margin: [ 15, 0, 0, 0 ],
							width: '10%',
							text: i + 1,
							fontSize: 16
						},
						{
							margin: [ 15, 0, 0, 0 ],
							width: '50%',
							text: el.treatment_name,
							fontSize: 16
						},
						{
							margin: [ 0, 0, 5, 0 ],
							width: '40%',
							text: el.cost.toLocaleString(),
							fontSize: 16,
							alignment: 'right'
						}
					]
				};
			}),
			{
				margin: [ 0, 0, 0, 10 ],
				table: {
					widths: [ '50%', '50%' ],
					heights: [ 15, 15, 15, 15 ],
					body: [
						[
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ค่าบริการ',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: datas_pdf.reduce(fn.add('cost'), 0).toLocaleString(),
								fontSize: 17,
								alignment: 'right'
							}
						],
						[
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: 'ค่าตรวจนอกสถานที่',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, false, false, false ],
								fillColor: '#e6f7ff',
								text: outSidePrice,
								fontSize: 17,
								alignment: 'right'
							}
						],
						[
							{
								// width: ,
								border: [ false, false, false, true ],
								fillColor: '#e6f7ff',
								text: 'ค่าบริการที่เบิกได้',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, false, false, true ],
								fillColor: '#e6f7ff',
								text:
									Number(payType) === 1
										? 0
										: datas_pdf.reduce(fn.add('disburseable'), 0).toLocaleString(),
								fontSize: 17,
								alignment: 'right'
							}
						],
						[
							{
								// width: ,
								border: [ false, false, false, true ],
								fillColor: '#e6f7ff',
								text: 'รวมมูลค่าทั้งหมด',
								fontSize: 17
							},
							{
								// width: ,
								border: [ false, true, false, true ],
								fillColor: '#e6f7ff',
								text: (servicePrice + outSidePrice + priceNewPackage).toLocaleString(),
								fontSize: 17,
								alignment: 'right'
							}
						]
					]
				}
			},
			{
				text: 'เจ้าหน้าที่ :   ' + f_name + ' ' + l_name,
				fontSize: 17,
				alignment: 'right'
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
//--------------------------------------------------------------------- PDF PT ----------------------------------------------------------------------------//
export const billPT = (dis, data, frontPTTM, namelast) => {
	let arr = data.service_treatment.map((e) => ({
		id: e.cgd_code,
		name: !e.treatment_name ? '' : e.treatment_name,
		detail: !e.detail ? '' : e.detail,
		num: e.cost
	}));
	let thaibath = fn.ArabicNumberToText(dis);
	let prefix = data.th_prefix === 'นาย' ? 'นาย' : data.th_prefix === 'นาง' ? 'นาง' : 'นางสาว';
	let fullname = prefix + data.th_name + ' ' + data.th_lastname;
	var docDefinition = {
		// footer: function(currentPage, pageCount) {
		// 	return [
		// 		{
		// 			text: currentPage.toString() + ' of ' + pageCount,
		// 			alignment: 'right',
		// 			margin: [ 0, 0, 40, 0 ]
		// 		}
		// 	];
		// },
		content: [
			{ alignment: 'right', fontSize: 14, text: 'ต้นฉบับ' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 12, 0, 0 ],
						width: 60,
						height: 90,
						image: file64.logo_kku
					},
					[
						{ alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' },
						{
							alignment: 'center',
							fontSize: 19,
							bold: true,
							text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
						},
						{
							alignment: 'center',
							fontSize: 14,
							text: 'สถานพยาบาลของทางราชการตามนัยมาตรา 4 แห่งพระราชกฤษฎีกา'
						},
						{ alignment: 'center', fontSize: 14, text: 'เงินสวัสดิการเกี่ยวกับการรักษาพยาบาล พ.ศ. 2523' },
						{
							alignment: 'center',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043)202080'
						}
					]
				]
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				alignment: 'right',
				text: [
					'วันที่ ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: moment(new Date()).add(543, 'year').format('LL')
					}
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'ลำดับ'
							},
							{ bold: true, fontSize: 14, text: 'รายการ' },
							{
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								alignment: 'center',
								fontSize: 14,
								text: e.id
							},
							{
								vLineStyle: 4,
								hLineStyle: 4,
								border: [ true, false, true, true ],
								fontSize: 14,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								colSpan: 2,
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'รวมทั้งสิน'
							},
							{
								border: [ true, false, true, true ],
								text: ''
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 14,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			//---------------------------------------------------------------------------- 2 -----------------------------------------------//
			{ pageBreak: 'before', alignment: 'right', fontSize: 14, text: 'สำเนา' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 12, 0, 0 ],
						width: 60,
						height: 90,
						image: file64.logo_kku
					},
					[
						{ alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' },
						{
							alignment: 'center',
							fontSize: 19,
							bold: true,
							text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
						},
						{
							alignment: 'center',
							fontSize: 14,
							text: 'สถานพยาบาลของทางราชการตามนัยมาตรา 4 แห่งพระราชกฤษฎีกา'
						},
						{ alignment: 'center', fontSize: 14, text: 'เงินสวัสดิการเกี่ยวกับการรักษาพยาบาล พ.ศ. 2523' },
						{
							alignment: 'center',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043)202080'
						}
					]
				]
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				alignment: 'right',
				text: [
					'วันที่ ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: moment(new Date()).add(543, 'year').format('LL')
					}
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'ลำดับ'
							},
							{ bold: true, fontSize: 14, text: 'รายการ' },
							{
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								alignment: 'center',
								fontSize: 14,
								text: e.id
							},
							{
								vLineStyle: 4,
								hLineStyle: 4,
								border: [ true, false, true, true ],
								fontSize: 14,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								colSpan: 2,
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'รวมทั้งสิน'
							},
							{
								border: [ true, false, true, true ],
								text: ''
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 14,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			//-------------------------------------------------------------------------- 3 ------------------------------------------------------//
			{ pageBreak: 'before', alignment: 'right', fontSize: 14, text: 'สำเนา' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 12, 0, 0 ],
						width: 60,
						height: 90,
						image: file64.logo_kku
					},
					[
						{ alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' },
						{
							alignment: 'center',
							fontSize: 19,
							bold: true,
							text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
						},
						{
							alignment: 'center',
							fontSize: 14,
							text: 'สถานพยาบาลของทางราชการตามนัยมาตรา 4 แห่งพระราชกฤษฎีกา'
						},
						{ alignment: 'center', fontSize: 14, text: 'เงินสวัสดิการเกี่ยวกับการรักษาพยาบาล พ.ศ. 2523' },
						{
							alignment: 'center',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043)202080'
						}
					]
				]
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				alignment: 'right',
				text: [
					'วันที่ ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: moment(new Date()).add(543, 'year').format('LL')
					}
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'ลำดับ'
							},
							{ bold: true, fontSize: 14, text: 'รายการ' },
							{
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								alignment: 'center',
								fontSize: 14,
								text: e.id
							},
							{
								vLineStyle: 4,
								hLineStyle: 4,
								border: [ true, false, true, true ],
								fontSize: 14,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '10%', '70%', '20%' ],
					body: [
						[
							{
								colSpan: 2,
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'center',
								fontSize: 14,
								text: 'รวมทั้งสิน'
							},
							{
								border: [ true, false, true, true ],
								text: ''
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 14,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const billTM = (dis, data, frontPTTM, namelast) => {
	let arr = data.service_treatment.map((e) => ({
		id: e.cgd_code,
		name: !e.treatment_name ? '' : e.treatment_name,
		detail: !e.detail ? '' : e.detail,
		num: data.service_claim > 1 ? e.cost - e.disburseable : e.cost
	}));
	let thaibath = fn.ArabicNumberToText(dis);
	let prefix = data.th_prefix === 'นาย' ? 'นาย' : data.th_prefix === 'นาง' ? 'นาง' : 'นางสาว';
	let fullname = prefix + data.th_name + ' ' + data.th_lastname;
	var docDefinition = {
		// footer: function(currentPage, pageCount) {
		// 	return [
		// 		{
		// 			text: currentPage.toString() + ' of ' + pageCount,
		// 			alignment: 'right',
		// 			margin: [ 0, 0, 40, 0 ]
		// 		}
		// 	];
		// },
		content: [
			{ alignment: 'right', fontSize: 14, text: 'ต้นฉบับ' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[ { alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' } ],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				alignment: 'center',
				fontSize: 19,
				bold: true,
				text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
			},
			{
				columns: [
					[
						{
							margin: [ 0, 0, 0, 0 ],
							width: 55,
							height: 75,
							image: file64.logo_kku
						},
						{
							margin: [ 20, 0, 0, 0 ],
							fontSize: 16,
							bold: true,
							text: '(ท)'
						}
					],
					[
						{
							margin: [ 0, 10, 0, 0 ],
							alignment: 'right',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: 'โทรศัพท์/โทรสาร (043)202081'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: [
								'วันที่ ',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: moment(new Date()).add(543, 'year').format('LL')
								}
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{ alignment: 'center', bold: true, fontSize: 16, text: 'รายการ' },
							{
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน (บาท)'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'right',
								fontSize: 16,
								text: 'รวม'
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 10, 0, 0 ],
				fontSize: 16,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 16,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				margin: [ 0, 3, 0, 0 ],
				alignment: 'center',
				fontSize: 14,
				text:
					'................................................................................................................................................................................................................................'
			},
			{ margin: [ 0, 3, 0, 0 ], alignment: 'right', fontSize: 14, text: 'สำเนา' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[ { alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' } ],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				alignment: 'center',
				fontSize: 19,
				bold: true,
				text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
			},
			{
				columns: [
					[
						{
							margin: [ 0, 0, 0, 0 ],
							width: 55,
							height: 75,
							image: file64.logo_kku
						},
						{
							margin: [ 20, 0, 0, 0 ],
							fontSize: 16,
							bold: true,
							text: '(ท)'
						}
					],
					[
						{
							margin: [ 0, 10, 0, 0 ],
							alignment: 'right',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: 'โทรศัพท์/โทรสาร (043)202081'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: [
								'วันที่ ',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: moment(new Date()).add(543, 'year').format('LL')
								}
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{ alignment: 'center', bold: true, fontSize: 16, text: 'รายการ' },
							{
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน (บาท)'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'right',
								fontSize: 16,
								text: 'รวม'
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 10, 0, 0 ],
				fontSize: 16,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 16,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{ pageBreak: 'before', alignment: 'right', fontSize: 14, text: 'สำเนา' },
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{ fontSize: 20, text: !data.book_number ? '' : parseInt(data.book_number) }
							]
						}
					],
					[ { alignment: 'center', fontSize: 19, bold: true, text: 'ใบเสร็จรับเงิน' } ],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{ fontSize: 20, text: !data.bill_number ? '' : parseInt(data.bill_number) }
							]
						}
					]
				]
			},
			{
				alignment: 'center',
				fontSize: 19,
				bold: true,
				text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
			},
			{
				columns: [
					[
						{
							margin: [ 0, 0, 0, 0 ],
							width: 55,
							height: 75,
							image: file64.logo_kku
						},
						{
							margin: [ 20, 0, 0, 0 ],
							fontSize: 16,
							bold: true,
							text: '(ท)'
						}
					],
					[
						{
							margin: [ 0, 10, 0, 0 ],
							alignment: 'right',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: 'โทรศัพท์/โทรสาร (043)202081'
						},
						{
							alignment: 'right',
							fontSize: 16,
							text: [
								'วันที่ ',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: moment(new Date()).add(543, 'year').format('LL')
								}
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: fullname
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 16,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{ alignment: 'center', bold: true, fontSize: 16, text: 'รายการ' },
							{
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน (บาท)'
							}
						]
					]
				}
			},
			arr.map((e) => ({
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								text: e.name + ' ' + e.detail
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								alignment: 'right',
								text: e.num.toLocaleString()
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'right',
								fontSize: 16,
								text: 'รวม'
							},
							{
								border: [ true, false, true, true ],
								fontSize: 16,
								bold: true,
								alignment: 'right',
								text: dis.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 10, 0, 0 ],
				fontSize: 16,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: !dis ? 'ศูนย์บาทถ้วน' : thaibath
					}
				]
			},
			{
				fontSize: 16,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: frontPTTM
							},
							' )'
						],
						fontSize: 16,
						alignment: 'center'
					}
				]
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const Treatmentrecord = (path, data, sym) => {
	let fullname = data.th_prefix + data.th_name + ' ' + data.th_lastname;
	let c = data.birthday && moment(new Date()).year() - moment(data.birthday).year() > 0;
	let age = c
		? moment(new Date()).year() - moment(data.birthday).year()
		: moment(new Date()).add(543, 'year').year() - moment(data.birthday).year();
	let birthday = !data.birthday
		? '-'
		: c ? moment(data.birthday).add(543, 'year').format('DD/MM/YYYY') : moment(data.birthday).format('DD/MM/YYYY');
	let hn = sevenDigits(data.customer);
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{ alignment: 'right', fontSize: 20, bold: true, text: `HN${hn}` },
			{ alignment: 'right', fontSize: 15, bold: true, text: 'กายภาพบำบัดสวัสดิการ' },
			{ alignment: 'right', fontSize: 15, bold: true, text: '(ในเวลาราชการ)\n\n' },
			{ alignment: 'center', fontSize: 18, bold: true, text: 'แบบบันทึกการรักษาทางกายภาพ' },
			{ alignment: 'center', fontSize: 16, bold: true, text: 'โครงการกายภาพบำบัดและพัฒนาสุขภาพ\n\n' },
			{
				style: 'bigger',
				text: [
					'ชื่อ-สกุล',
					{
						style: 'underline',
						text: `   ${fullname}   `
					},
					'อายุ',
					{
						style: 'underline',
						text: `   ${age > 0 ? age : '-'}   `
					},
					'ปี วัน/เดือน/ปีเกิด',
					{
						style: 'underline',
						text: `   ${birthday}   `
					},
					'อาชีพ',
					{
						style: 'underline',
						text: '         -          '
					},
					'ที่อยู่',
					{
						style: 'underline',
						text: `   ${data.address}   `
					},
					'เบอร์โทรศัพท์',
					{
						style: 'underline',
						text: `   ${data.phone}   `
					},
					'Diagnosis',
					{
						style: 'underline',
						text: `   ${!data.diagnosis ? '-' : data.diagnosis}   `
					},
					'Physiotherpist',
					{
						style: 'underline',
						text: `   ${!data.physiotherapist ? '-' : data.physiotherapist}   `
					},
					'Chief complaint',
					{
						style: 'underline',
						text: `   ${!data.chief_complaint ? '-' : data.chief_complaint}   `
					}
				]
			},
			{
				margin: [ 0, 10, 0, 0 ],
				columns: [
					{
						style: 'bigger',
						text: 'Subjective and Objective examination'
					},
					{
						style: 'bigger',
						text: 'Problems'
					}
				]
			},
			{
				columns: [
					{
						margin: [ 15, 0, 0, 0 ],
						text: data.so_examination
					},
					{
						margin: [ 15, 0, 0, 0 ],
						text: data.problem
					}
				]
			},
			{
				margin: [ 0, 10, 0, 0 ],
				columns: [
					{
						style: 'bigger',
						text: 'Goal of treatment'
					},
					{
						style: 'bigger',
						text: 'Treatment'
					}
				]
			},
			{
				columns: [
					{
						margin: [ 15, 0, 0, 0 ],
						text: data.goal_of_treatment
					},
					{
						margin: [ 15, 0, 0, 0 ],
						text: data.treatment
					}
				]
			},
			{
				style: 'bigger',
				text: 'Area of symptoms'
			},
			{
				width: sym ? 300 : 225,
				height: sym ? 300 : 200,
				image: path
			}
		],
		styles: {
			bigger: {
				alignment: 'justify',
				fontSize: 15,
				bold: true
			},
			underline: {
				decoration: 'underline',
				decorationStyle: 'dotted'
			}
		},
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const Pays = (data) => {
	let startDate = moment(data.startDate).add(543, 'year').format('DD/MM/YYYY');
	let endDate = moment(data.endDate).add(543, 'year').format('DD/MM/YYYY');
	let ontime = Number(data.ontime) ? 'ในเวลา' : 'นอกเวลา';
	let types = data.Service_type === 'tm' ? 'นวดไทย' : 'กายภาพ';
	let unique_date = [ ...new Set(data.data_mapAll.map((e) => moment(e.service_at).format('YYYY-MM-DD'))) ];
	let datas = [];
	unique_date.forEach((el) => {
		let details = data.data_mapAll.filter((e) => moment(e.service_at).format('L') === moment(el).format('L'));
		let obj = {
			name: el,
			details
		};
		datas.push(obj);
	});
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				alignment: 'center',
				fontSize: 16,
				bold: true,
				text: `ตารางเบิกค่าตอบแทนโครงงานบริการกายภาพบำบัด${ontime}ราชการ(${types})`
			},
			{
				alignment: 'center',
				fontSize: 16,
				bold: true,
				text: `ประจำวันที่   ${startDate} - ${endDate}`
			},
			{
				margin: [ 0, 10, 0, 0 ],
				text: [
					{ text: 'ผู้ปฏิบัติงาน   ', fontSize: 16 },
					{ text: data.data_mapAll[0].fullname, fontSize: 16, bold: true }
				]
			},
			{
				margin: [ 0, 10, 0, 0 ],
				table: {
					widths: [ '10%', '38%', '10%', '18%', '11%', '13%' ],
					body: [
						[
							{
								border: [ false, false, false, true ],
								fontSize: 16,
								text: 'วันที่'
							},
							{
								border: [ false, false, false, true ],
								fontSize: 16,
								text: 'ชื่อผู้รับบริการ'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 16,
								text: 'เงินสด'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'center',
								fontSize: 16,
								text: 'เลขที่ใบเสร็จ'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 16,
								text: 'สวัสดิการ'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 16,
								text: 'ค่าตอบแทน'
							}
						]
					]
				}
			},
			datas.length > 0 &&
				datas.sort((a, b) => moment(a.name) - moment(b.name)).map((e) => ({
					columns: [
						[
							{
								columns: [
									{
										fontSize: 16,
										width: '12%',
										text: moment(e.name).add(543, 'year').format('DD-MMM-YY')
									},
									[
										e.details
											.map((el) => ({
												...el,
												namefull: el.th_name + ' ' + el.th_lastname + ', ' + el.th_prefix,
												pay_price: JSON.parse(el.service_treatment).reduce(
													fn.add('pay_price'),
													0
												)
											}))
											.map((el) => ({
												columns: [
													{
														width: '40%',
														fontSize: 16,
														text: `${el.namefull}`
													},
													{
														width: '10%',
														fontSize: 16,
														alignment: 'right',
														text:
															el.service_claim === 3
																? 0
																: el.service_price.toLocaleString()
													},
													{
														alignment: 'center',
														width: '23%',
														fontSize: 16,
														text:
															el.book_number && `${el.book_number + '/' + el.bill_number}`
													},
													{
														width: '10.5%',
														fontSize: 16,
														alignment: 'right',
														text:
															el.service_claim === 3
																? el.service_price.toLocaleString()
																: 0
													},
													{
														margin: [ 0, 0, 5, 0 ],
														width: '16.5%',
														fontSize: 16,
														alignment: 'right',
														text: el.pay_price ? el.pay_price.toLocaleString() : 0
													}
												]
											}))
									]
								]
							},
							{
								table: {
									widths: [ '100%' ],
									body: [
										[
											{
												border: [ false, true, false, false ],
												text: ''
											}
										]
									]
								}
							},
							{
								layout: 'headerLineOnly',
								table: {
									headerRows: 1,
									widths: [ '45%', '13.5%', '29.5%', '12%' ],
									body: [
										[
											{
												alignment: 'center',
												fontSize: 16,
												text: 'รวมทั้งวัน'
											},
											{
												margin: [ 0, 0, 1, 0 ],
												alignment: 'right',
												fontSize: 16,
												text: e.details
													.map((el) => ({
														sum: el.service_claim === 3 ? 0 : el.service_price
													}))
													.reduce(fn.add('sum'), 0)
													.toLocaleString()
											},
											{
												margin: [ 0, 0, 2.5, 0 ],
												alignment: 'right',
												fontSize: 16,
												text: e.details
													.map((el) => ({
														sum: el.service_claim === 3 ? el.service_price : 0
													}))
													.reduce(fn.add('sum'), 0)
													.toLocaleString()
											},
											{
												margin: [ 0, 0, 5, 0 ],
												alignment: 'right',
												fontSize: 16,
												text: e.details
													.map((el) => ({
														price:
															JSON.parse(el.service_treatment).reduce(
																fn.add('pay_price'),
																0
															) || 0
													}))
													.reduce(fn.add('price'), 0)
													.toLocaleString()
											}
										],
										[ '', '', '', '' ]
									]
								}
							}
						]
					]
				})),
			{
				margin: [ 0, 15, 0, 0 ],
				table: {
					widths: [ '100%' ],
					body: [
						[
							{
								border: [ false, true, false, false ],
								text: ''
							}
						]
					]
				}
			},
			{
				layout: 'headerLineOnly',
				table: {
					headerRows: 1,
					widths: [ '45%', '13.5%', '29.5%', '12%' ],
					body: [
						[
							{
								alignment: 'center',
								fontSize: 16,
								text: 'รวมทั้งหมด'
							},
							{
								margin: [ 0, 0, 1, 0 ],
								alignment: 'right',
								fontSize: 16,
								text: data.data_mapAll
									.map((el) => ({
										sum: el.service_claim === 3 ? 0 : el.service_price
									}))
									.reduce(fn.add('sum'), 0)
									.toLocaleString()
							},
							{
								margin: [ 0, 0, 2.5, 0 ],
								alignment: 'right',
								fontSize: 16,
								text: data.data_mapAll
									.map((el) => ({
										sum: el.service_claim === 3 ? el.service_price : 0
									}))
									.reduce(fn.add('sum'), 0)
									.toLocaleString()
							},
							{
								margin: [ 0, 0, 5, 0 ],
								alignment: 'right',
								fontSize: 16,
								text: data.data_mapAll
									.map((el) => ({
										price: JSON.parse(el.service_treatment).reduce(fn.add('pay_price'), 0) || 0
									}))
									.reduce(fn.add('price'), 0)
									.toLocaleString()
							}
						],
						[ '', '', '', '' ]
					]
				}
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const PaysAll = (data) => {
	let startDate = moment(data.startDate).add(543, 'year').format('DD/MM/YYYY');
	let endDate = moment(data.endDate).add(543, 'year').format('DD/MM/YYYY');
	let ontime = Number(data.ontime) ? 'ในเวลา' : 'นอกเวลา';
	let types = data.Service_type === 'tm' ? 'นวดไทย' : 'กายภาพ';
	let TF = data.Service_type === 'tm' ? true : false;
	let unique_date = [ ...new Set(data.data_mapAll.map((e) => e.fullname)) ];
	let datas = [];
	unique_date.forEach((el) => {
		let details = data.data_mapAll.filter((e) => e.fullname === el);
		let obj = {
			name: el + ', ' + details[0].th_prefix,
			details
		};
		datas.push(obj);
	});
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				alignment: 'left',
				fontSize: 20,
				bold: true,
				text: `สรุปจำนวนผู้ใช้บริการ${types} ${data.Service_unit} ${ontime}`
			},
			{
				alignment: 'left',
				fontSize: 16,
				bold: true,
				text: `ระหว่างวันที่   ${startDate} - ${endDate}`
			},
			{
				alignment: 'left',
				fontSize: 16,
				bold: true,
				text: `เดือน   ${moment(data.startDate).add(543, 'year').format('MM-YYYY')}`
			},
			{
				margin: [ 0, 10, 0, 0 ],
				table: {
					widths: TF ? [ '30%', '17.5%', '17.5%', '17.5%', '17.5%' ] : [ '30%', '10%', '20%', '18%', '22%' ],
					body: [
						[
							{
								border: [ false, false, false, true ],
								alignment: 'left',
								fontSize: 13,
								text: data.Service_type === 'tm' ? 'ผู้นวด' : 'นักกายภาพบำบัด'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 13,
								text: 'จำนวนคนไข้'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 13,
								text: data.Service_type === 'tm' ? 'นวดเงินสด' : 'กายภาพบำบัดเงินสด'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 13,
								text: data.Service_type === 'tm' ? 'นวดสวัสดิการ' : 'กายภาพบำบัดสวัสดิการ'
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 13,
								text: data.Service_type === 'tm' ? 'ค่าตอบแทนผู้นวด' : 'ค่าตอบแทนนักกายภาพบำบัด'
							}
						]
					]
				}
			},
			datas.length > 0 &&
				datas.map((e) => ({
					table: {
						widths: TF
							? [ '30%', '17.5%', '17.5%', '17.5%', '17.5%' ]
							: [ '30%', '10%', '20%', '18%', '22%' ],
						body: [
							[
								{
									border: [ false, false, false, true ],
									alignment: 'left',
									fontSize: 14,
									text: e.name
								},
								{
									border: [ false, false, false, true ],
									alignment: 'right',
									fontSize: 14,
									text: e.details.length.toLocaleString()
								},
								{
									border: [ false, false, false, true ],
									alignment: 'right',
									fontSize: 14,
									text: e.details
										.filter((el) => el.service_claim < 3)
										.reduce(fn.add('service_price'), 0)
										.toLocaleString()
								},
								{
									border: [ false, false, false, true ],
									alignment: 'right',
									fontSize: 14,
									text: e.details
										.filter((el) => el.service_claim === 3)
										.reduce(fn.add('service_price'), 0)
										.toLocaleString()
								},
								{
									border: [ false, false, false, true ],
									alignment: 'right',
									fontSize: 14,
									text: e.details
										.map((el) => ({
											pay_price: JSON.parse(el.service_treatment)
												.map((ele) => ({ pay_price: ele.pay_price || 0 }))
												.reduce(fn.add('pay_price'), 0)
										}))
										.reduce(fn.add('pay_price'), 0)
										.toLocaleString()
								}
							]
						]
					}
				})),
			{
				table: {
					widths: TF ? [ '30%', '17.5%', '17.5%', '17.5%', '17.5%' ] : [ '30%', '10%', '20%', '18%', '22%' ],
					body: [
						[
							{
								border: [ false, false, false, true ],
								alignment: 'center',
								fontSize: 14,
								text: `รวมเดือน ${moment(data.startDate).add(543, 'year').format('MM-YYYY')}`
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll.length.toLocaleString()
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.filter((el) => el.service_claim < 3)
									.reduce(fn.add('service_price'), 0)
									.toLocaleString()
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.filter((el) => el.service_claim === 3)
									.reduce(fn.add('service_price'), 0)
									.toLocaleString()
							},
							{
								border: [ false, false, false, true ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.map((e) => ({
										price: JSON.parse(e.service_treatment).reduce(fn.add('pay_price'), 0) || 0
									}))
									.reduce(fn.add('price'), 0)
									.toLocaleString()
							}
						],
						[
							{
								border: [ false, false, false, false ],
								alignment: 'center',
								fontSize: 14,
								text: ``
							},
							{
								border: [ false, false, false, false ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll.length.toLocaleString()
							},
							{
								border: [ false, false, false, false ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.filter((el) => el.service_claim < 3)
									.reduce(fn.add('service_price'), 0)
									.toLocaleString()
							},
							{
								border: [ false, false, false, false ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.filter((el) => el.service_claim === 3)
									.reduce(fn.add('service_price'), 0)
									.toLocaleString()
							},
							{
								border: [ false, false, false, false ],
								alignment: 'right',
								fontSize: 14,
								text: data.data_mapAll
									.map((e) => ({
										price: JSON.parse(e.service_treatment).reduce(fn.add('pay_price'), 0) || 0
									}))
									.reduce(fn.add('price'), 0)
									.toLocaleString()
							}
						]
					]
				}
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const PdfInvoice = (data) => {
	let type = data.Service_type === 'tm' ? 'แจ้งหนี้ค่านวดไทย' : 'แจ้งหนี้ค่ารักษาพยาบาล';
	let date_now = moment(new Date()).add(543, 'year').format('LL');
	let Invoice_claim =
		Number(data.claim_official) === 1
			? 'ตามที่ข้าราชการและลูกจ้างประจำ'
			: Number(data.claim_official) === 2
				? 'ตามที่บุคคลในครอบครัวข้าราชการและลูกจ้างประจำ'
				: Number(data.claim_official) === 3 ? 'ตามที่ข้าราชการบำนาญ' : 'ตามที่บุคคลในครอบครัวข้าราชการบำนาญ';
	let a = moment(data.startDate).date();
	let b = moment(data.endDate).add(543, 'year').format('LL');
	let number = data.dataInvoice.length || 0;
	let ser_price = data.Service_type === 'tm' ? 'ค่านวดไทย' : 'ค่าบริการ';
	let prefix = User.prefix === 'นาย' ? 'นาย' : User.prefix === 'นาง' ? 'นาง' : 'นางสาว';
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				alignment: 'center',
				fontSize: 20,
				bold: true,
				text: `ใบ${type}`
			},
			{
				margin: [ 0, 10, 0, 0 ],
				columns: [
					{
						width: '33.33%',
						alignment: 'left',
						fontSize: 16,
						text: 'อว. 660301.8.3.1/'
					},
					{
						width: '20.33%',
						text: ''
					},
					{
						width: '46.33%',
						alignment: 'right',
						fontSize: 16,
						text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
					}
				]
			},
			{
				margin: [ 0, 10, 0, 0 ],
				columns: [
					{
						width: '50%',
						text: ''
					},
					{
						width: '50%',
						alignment: 'left',
						fontSize: 16,
						text: `วันที่ ${date_now}`
					}
				]
			},
			Number(data.claim_official) === 1
				? {
						margin: [ 50, 10, 0, 0 ],
						fontSize: 16,
						text: `${Invoice_claim}มหาวิทยาลัยขอนแก่น ได้รับการรักษาพยาบาล ณ สถานบริการสุขภาพเทคนิค`
					}
				: Number(data.claim_official) === 2
					? {
							margin: [ 50, 10, 0, 0 ],
							fontSize: 16,
							text: `${Invoice_claim}มหาวิทยาลัยขอนแก่น  ได้รับการรักษาพยาบาล  ณ  สถาน`
						}
					: Number(data.claim_official) === 3
						? {
								margin: [ 50, 10, 0, 0 ],
								fontSize: 16,
								text: `${Invoice_claim}มหาวิทยาลัยขอนแก่น ได้รับการรักษาพยาบาล ณ สถานบริการสุขภาพเทคนิคการแพทย์`
							}
						: {
								margin: [ 45, 10, 0, 0 ],
								fontSize: 16,
								text: `${Invoice_claim}มหาวิทยาลัยขอนแก่น ได้รับการรักษาพยาบาล ณ สถานบริการสุขภาพ`
							},
			Number(data.claim_official) === 1
				? {
						alignment: 'justify',
						fontSize: 16,
						text: `การแพทย์และกายภาพบำบัด ในวันที่ ${a} - ${b} จำนวน ${number.toLocaleString()} ราย นั้น โครงงานบริการกายภาพบำบัดใน เวลาราชการ สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด ใคร่ขอ${type} ตามรายละเอียดดังนี้`
					}
				: Number(data.claim_official) === 2
					? {
							alignment: 'justify',
							fontSize: 16,
							text: `บริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด ในวันที่ ${a} - ${b} จำนวน ${number.toLocaleString()} ราย นั้น โครงงานบริการ กายภาพบำบัดในเวลาราชการ สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด ใคร่ขอ${type} ตามรายละเอียดดังนี้`
						}
					: Number(data.claim_official) === 3
						? {
								alignment: 'justify',
								fontSize: 16,
								text: `และกายภาพบำบัด ในวันที่ ${a} - ${b} จำนวน ${number.toLocaleString()} ราย นั้น โครงงานบริการกายภาพบำบัดในเวลาราชการ สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด ใคร่ขอ${type} ตามรายละเอียดดังนี้`
							}
						: {
								alignment: 'justify',
								fontSize: 16,
								text: `เทคนิคการแพทย์และกายภาพบำบัด ในวันที่ ${a} - ${b} จำนวน ${number.toLocaleString()} ราย นั้น โครงงานบริการ กายภาพบำบัดในเวลาราชการ สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด ใคร่ขอ${type} ตามรายละเอียดดังนี้`
							},
			{
				margin: [ 0, 10, 0, 0 ],
				table: {
					widths: [ '10%', '80%', '10%' ],
					body: [
						[
							{ fontSize: 16, alignment: 'center', text: 'ลำดับที่' },
							{ fontSize: 16, text: 'ชื่อ - สกุล' },
							{ fontSize: 16, alignment: 'center', text: ser_price }
						]
					]
				}
			},
			data.dataInvoice.map((e, i) => ({
				table: {
					widths: [ '10%', '80%', '10%' ],
					body: [
						[
							{ fontSize: 16, alignment: 'center', text: i + 1 },
							{ fontSize: 16, text: e.names },
							{
								fontSize: 16,
								alignment: 'right',
								text: e.details.reduce(fn.add('service_price'), 0).toLocaleString()
							}
						]
					]
				}
			})),
			{
				margin: [ 0, 80, 0, 0 ],
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: 'ลงชื่อ...................................................ผู้จัดทำ'
					},
					{
						fontSize: 16,
						text: 'ลงชื่อ...................................................'
					}
				]
			},
			{
				margin: [ 0, 5, 0, 0 ],
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: prefix + User.name + ' ' + User.lastname
					},
					{
						fontSize: 16,
						text: data.president
					}
				]
			},
			{
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: 'การเงิน'
					},
					{
						fontSize: 16,
						text: 'หัวหน้าโครงงานบริการกายภาพบำบัดฯ'
					}
				]
			},
			{
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: 'เจ้าหน้าที่การเงิน'
					},
					{
						fontSize: 16,
						text: ''
					}
				]
			},
			{
				margin: [ 0, 50, 0, 0 ],
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: 'ลงชื่อ...................................................'
					},
					{
						fontSize: 16,
						text: 'ลงชื่อ...................................................'
					}
				]
			},
			{
				margin: [ 0, 5, 0, 0 ],
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: data.manager
					},
					{
						fontSize: 16,
						text: data.inspector
					}
				]
			},
			{
				alignment: 'center',
				columns: [
					{
						fontSize: 16,
						text: 'ผู้จัดการศูนย์บริการสู่ชุมชน มหาวิทยาลัยขอนแก่น'
					},
					{
						fontSize: 16,
						text: 'ผู้ตรวจสอบ'
					}
				]
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const PdfReport = (data, detail, obj) => {
	data.sort((a, b) => moment(a.service_at) - moment(b.service_at));
	let prefix =
		detail.th_prefix === 'น.ส.'
			? 'นางสาว'
			: detail.th_prefix === 'ด.ช.' ? 'เด็กชาย' : detail.th_prefix === 'ด.ญ.' ? 'เด็กหญิง' : detail.th_prefix;
	let name = prefix + detail.th_name + ' ' + detail.th_lastname;
	let c = detail.birthday && moment(new Date()).year() - moment(detail.birthday).year() > 0;
	let age = c
		? moment(new Date()).year() - moment(detail.birthday).year()
		: moment(new Date()).add(543, 'year').year() - moment(detail.birthday).year();
	let year = Number(obj.year) + 543;
	var docDefinition = {
		footer: function(currentPage, pageCount) {
			return [
				{
					text: currentPage.toString() + ' of ' + pageCount,
					alignment: 'right',
					margin: [ 0, 0, 40, 0 ]
				}
			];
		},
		content: [
			{
				table: {
					widths: [ '50%', '50%' ],
					body: [
						[
							{
								margin: [ 0, 8, 0, 0 ],
								text: [
									{ text: 'ใบคิดค่ารักษาคลินิกกายภาพบำบัด\n', style: 'header_left' },
									{ text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด', style: 'header_left' }
								]
							},
							[
								{ text: 'โครงงานบริการกายภาพบำบัดและพัฒนาสุขภาพ', style: 'header_right' },
								{
									text: [
										{ text: 'ชื่อ-สกุล', style: 'header_center' },
										{ text: `   ${name ? name : ''}   `, style: 'under_line' },
										{ text: 'HN', style: 'header_center' },
										{ text: `      `, style: 'under_line' }
									]
								},
								{
									widths: [ '20%', '20%', '20%', '20%', '20%' ],
									columns: [
										{ text: 'เพศ  ', style: 'header_center' },
										detail.sex === 'ชาย' || detail.sex === 'male'
											? {
													width: 15,
													height: 15,
													image: imgFile64.img_filebase64_result
												}
											: {
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
										{ text: '  ชาย  ', style: 'header_center' },
										detail.sex === 'ชาย' || detail.sex === 'male'
											? {
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												}
											: {
													width: 15,
													height: 15,
													image: imgFile64.img_filebase64_result
												},
										{ text: '  หญิง  ', style: 'header_center' },
										{ text: 'อายุ', style: 'header_center' },
										{ text: `  ${age > 0 ? age : '-'}  `, style: 'under_line' },
										{ text: 'ปี', style: 'header_center' }
									]
								}
							]
						]
					]
				}
			},
			{
				table: {
					widths: [ '100%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								columns: [
									[
										{
											columns: [
												obj.sub_type === 'Ortho'
													? {
															margin: [ 0, 3, 0, 0 ],
															width: 15,
															height: 15,
															image: imgFile64.img_filebase64_result
														}
													: {
															margin: [ 0, 3, 0, 0 ],
															width: 12,
															height: 12,
															image: imgFile64.shapes_symbols
														},
												{ text: 'Ortho', style: 'under_head' }
											]
										},
										{
											columns: [
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Cervical', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Hip', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Shoulder', style: 'under_head_body' }
											]
										},
										{
											columns: [
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Thoracic', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Knee', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Elbow', style: 'under_head_body' }
											]
										},
										{
											columns: [
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Lumbar', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Ankle', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Wrist', style: 'under_head_body' }
											]
										}
									],
									[
										{
											columns: [
												obj.sub_type === 'Ortho'
													? {
															margin: [ 0, 3, 0, 0 ],
															width: 12,
															height: 12,
															image: imgFile64.shapes_symbols
														}
													: {
															margin: [ 0, 3, 0, 0 ],
															width: 15,
															height: 15,
															image: imgFile64.img_filebase64_result
														},
												{ text: 'Neuro', style: 'under_head', margin: [ 3, 0, 0, 0 ] }
											]
										},
										{
											columns: [
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Hemiplegia', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'Paraparesis', style: 'under_head_body' },
												{
													margin: [ 10, 3, 0, 0 ],
													width: 12,
													height: 12,
													image: imgFile64.shapes_symbols
												},
												{ text: 'CP', style: 'under_head_body' }
											]
										}
									]
								]
							}
						]
					]
				}
			},
			{
				table: {
					widths: [ '6%', '42%', '5%', '7%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%' ],
					body: [
						[
							{ text: 'รหัส', border: [ true, false, true, true ], style: 'head_table', rowSpan: 2 },
							{ text: 'รายการ', border: [ true, false, true, true ], style: 'head_table', rowSpan: 2 },
							{ text: 'ราคา', border: [ true, false, true, true ], style: 'head_table', rowSpan: 2 },
							{ text: 'หน่วย', border: [ true, false, true, true ], style: 'head_table', rowSpan: 2 },
							{
								text: [
									'เดือน',
									{
										text: `  ${obj.month}  `,
										decoration: 'underline',
										decorationStyle: 'dotted'
									},
									'พ.ศ',
									{
										text: `  ${year}  `,
										decoration: 'underline',
										decorationStyle: 'dotted'
									}
								],
								border: [ true, false, true, true ],
								style: 'head_table2',
								colSpan: 7
							},
							{},
							{},
							{},
							{},
							{},
							{}
						],
						[
							{},
							{},
							{},
							{},
							{ text: data[0] ? moment(data[0].service_at).date() : '', style: 'head_table2' },
							{ text: data[1] ? moment(data[1].service_at).date() : '', style: 'head_table2' },
							{ text: data[2] ? moment(data[2].service_at).date() : '', style: 'head_table2' },
							{ text: data[3] ? moment(data[3].service_at).date() : '', style: 'head_table2' },
							{ text: data[4] ? moment(data[4].service_at).date() : '', style: 'head_table2' },
							{ text: data[5] ? moment(data[5].service_at).date() : '', style: 'head_table2' },
							{ text: data[6] ? moment(data[6].service_at).date() : '', style: 'head_table2' }
						],
						[
							{ text: '', alignment: 'center' },
							{
								text: [
									'Evaluation and planning of physical therapy\n',
									'(ตรวจประเมินและวางแผนการรักษาทางกายภาพบำบัด)'
								]
							},
							{ text: '-', alignment: 'center', margin: [ 0, 10, 0, 0 ] },
							{ text: 'ครั้ง/วัน', alignment: 'center', margin: [ 0, 10, 0, 0 ] },
							{
								text: data[0]
									? JSON.parse(data[0].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[0].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[1]
									? JSON.parse(data[1].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[1].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[2]
									? JSON.parse(data[2].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[2].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[3]
									? JSON.parse(data[3].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[3].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[4]
									? JSON.parse(data[4].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[4].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[5]
									? JSON.parse(data[5].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[5].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							},
							{
								text: data[6]
									? JSON.parse(data[6].service_treatment).find(
											(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
										)
										? JSON.parse(data[6].service_treatment).find(
												(e) => e.treatment_name === 'Evaluation and planning of physical therapy'
											).cost
										: ''
									: '',
								alignment: 'center',
								margin: [ 0, 10, 0, 0 ]
							}
						]
					]
				}
			},
			arrList.map((e) => ({
				table: {
					widths: [ '6%', '42%', '5%', '7%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%' ],
					body: [
						[
							{
								text: e.cgd_code ? e.cgd_code : '',
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: e.name && e.detail ? e.name + ` (${e.detail})` : e.name,
								border: [ true, false, true, true ]
							},
							{ text: e.cost ? e.cost : '-', alignment: 'center', border: [ true, false, true, true ] },
							{ text: e.type, alignment: 'center', border: [ true, false, true, true ] },
							{
								text: data[0]
									? JSON.parse(data[0].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[0].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[1]
									? JSON.parse(data[1].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[1].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[2]
									? JSON.parse(data[2].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[2].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[3]
									? JSON.parse(data[3].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[3].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[4]
									? JSON.parse(data[4].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[4].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[5]
									? JSON.parse(data[5].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[5].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[6]
									? JSON.parse(data[6].service_treatment).find((el) => el.treatment_name === e.name)
										? JSON.parse(data[6].service_treatment).find((el) => el.treatment_name === e.name)
												.cost
										: ''
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '6%', '42%', '5%', '7%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%', '5.71%' ],
					body: [
						[
							{
								colSpan: 4,
								text: 'รวมค่ารักษาต่อวัน',
								alignment: 'right',
								border: [ true, false, true, true ]
							},
							{},
							{},
							{},
							{
								text: data[0]
									? JSON.parse(data[0].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center'
							},
							{
								text: data[1]
									? JSON.parse(data[1].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: data[2]
									? JSON.parse(data[2].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: data[3]
									? JSON.parse(data[3].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: data[4]
									? JSON.parse(data[4].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: data[5]
									? JSON.parse(data[5].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							},
							{
								text: data[6]
									? JSON.parse(data[6].service_treatment).reduce(fn.add('cost'), 0).toLocaleString()
									: '',
								border: [ true, false, true, true ],
								alignment: 'center',
								border: [ true, false, true, true ]
							}
						]
					]
				}
			}
		],
		styles: {
			header_left: {
				fontSize: 14,
				bold: true,
				alignment: 'center'
			},
			header_right: {
				fontSize: 16,
				bold: true,
				alignment: 'center'
			},
			header_center: {
				alignment: 'left',
				fontSize: 14,
				bold: true
			},
			under_head: {
				fontSize: 16,
				bold: true
			},
			under_head_body: {
				margin: [ 12, 0, 0, 0 ],
				fontSize: 14
			},
			under_line: {
				alignment: 'left',
				fontSize: 14,
				bold: true,
				decoration: 'underline',
				decorationStyle: 'dotted'
			},
			head_table: {
				margin: [ 0, 10, 0, 0 ],
				alignment: 'center',
				bold: true
			},
			head_table2: {
				alignment: 'center',
				bold: true
			}
		},
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const billMT = (data, front) => {
	let prefix = data.th_prefix === 'นาย' ? 'นาย' : data.th_prefix === 'นาง' ? 'นาง' : 'นางสาว';
	let fullname = prefix + data.th_name + ' ' + data.th_lastname;
	let arr = data.service_treatment;
	let sum = data.service_treatment.reduce(fn.add('cost'), 0);
	let thaibath = fn.ArabicNumberToText(sum ? sum : 0);
	var docDefinition = {
		content: [
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{
									fontSize: 18,
									text: !data.book_number ? '' : parseInt(data.book_number.split(',')[1])
								}
							]
						}
					],
					[ { alignment: 'center', fontSize: 18, bold: true, text: 'ใบเสร็จรับเงิน' } ],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{
									fontSize: 18,
									text: !data.bill_number ? '' : parseInt(data.bill_number.split(',')[1])
								}
							]
						}
					]
				]
			},
			{
				alignment: 'center',
				fontSize: 18,
				bold: true,
				text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
			},
			{
				columns: [
					[
						{
							margin: [ 20, 0, 0, 0 ],
							width: 38,
							height: 58,
							image: file64.logo_kku
						},
						{
							margin: [ 32, 0, 0, 0 ],
							fontSize: 16,
							bold: true,
							text: '(ท)'
						}
					],
					[
						{
							alignment: 'right',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043) 202081'
						},
						{
							margin: [ 0, 0, 0, 0 ],
							fontSize: 16,
							alignment: 'right',
							text: [
								'วันที่',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).format('DD')}   `
								},
								'เดือน',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).format('MMMM')}   `
								},
								'พ.ศ.',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).add(543, 'year').format('YYYY')}   `
								}
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: `   ${fullname}   `
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{ bold: true, fontSize: 14, alignment: 'center', text: 'รายการ' },
							{
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน (บาท)'
							}
						]
					]
				}
			},
			arr.map((e, i) => ({
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								columns: [
									{
										width: '65%',
										text: [
											{ text: `${i + 1}. ${e.treatment_name} ` },
											{ text: e.detail ? `(${e.detail})` : null }
										]
									},
									{
										width: '35%',
										alignment: 'right',
										text: e.cgd_code
									}
								]
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								alignment: 'right',
								text: e.treatment_name === 'X-Ray' ? 400 : e.cost ? e.cost.toLocaleString() : 0
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								bold: true,
								alignment: 'right',
								fontSize: 14,
								text: 'รวม'
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: sum.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: `   ${thaibath}   `
					}
				]
			},
			{
				fontSize: 14,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 14,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: front
							},
							' )'
						],
						fontSize: 14,
						alignment: 'center'
					}
				]
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const billYearly = (data, front) => {
	let prefix = data.th_prefix === 'นาย' ? 'นาย' : data.th_prefix === 'นาง' ? 'นาง' : 'นางสาว';
	let fullname = prefix + data.th_name + ' ' + data.th_lastname;
	let arr = data.service_treatment;
	let sumYear = arr.filter((el) => arrYearly.some((e) => e.find === el.treatment_name)).reduce(fn.add('cost'), 0);
	let thaibathYear = fn.ArabicNumberToText(sumYear ? sumYear : 0);
	let arrPlain = arr.filter((el) => !arrYearly.some((e) => e.find === el.treatment_name));
	let x_ray = arr.some((e) => e.treatment_name === 'X-Ray');
	if (arrPlain.length > 0 && x_ray) {
		arrPlain.unshift(arr.find((e) => e.treatment_name === 'X-Ray'));
	}
	let sum = arrPlain.reduce(fn.add('cost'), 0);
	let thaibath = fn.ArabicNumberToText(sum ? sum : 0);
	var docDefinition = {
		content: [
			{
				columns: [
					[
						{
							color: 'red',
							alignment: 'left',
							text: [
								'เล่มที่ ',
								{
									fontSize: 18,
									text: !data.book_number ? '' : parseInt(data.book_number.split(',')[0])
								}
							]
						}
					],
					[ { alignment: 'center', fontSize: 18, bold: true, text: 'ใบเสร็จรับเงิน' } ],
					[
						{
							color: 'red',
							alignment: 'right',
							text: [
								'เลขที่ ',
								{
									fontSize: 18,
									text: !data.bill_number ? '' : parseInt(data.bill_number.split(',')[0])
								}
							]
						}
					]
				]
			},
			{
				fontSize: 14,
				alignment: 'center',
				text: [
					'สถานพยาบาลของทางราชการตามนัยมาตรา 4 แห่งพระราชกฤษฎีกา\n',
					'เงินสวัสดิการเกี่ยวกับการรักษาพยาบาล พ.ศ. 2523'
				]
			},
			{
				alignment: 'center',
				fontSize: 18,
				bold: true,
				text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
			},
			{
				columns: [
					[
						{
							margin: [ 20, 0, 0, 0 ],
							width: 38,
							height: 58,
							image: file64.logo_kku
						},
						{
							margin: [ 32, 0, 0, 0 ],
							fontSize: 16,
							bold: true,
							text: '(ท)'
						}
					],
					[
						{
							alignment: 'right',
							fontSize: 16,
							text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043) 202081'
						},
						{
							margin: [ 0, 0, 0, 0 ],
							fontSize: 16,
							alignment: 'right',
							text: [
								'วันที่',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).format('DD')}   `
								},
								'เดือน',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).format('MMMM')}   `
								},
								'พ.ศ.',
								{
									decoration: 'underline',
									decorationStyle: 'dotted',
									text: `   ${moment(new Date()).add(543, 'year').format('YYYY')}   `
								}
							]
						}
					]
				]
			},
			{
				columns: [
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'ได้รับเงินจาก ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: `   ${fullname}   `
							}
						]
					},
					{
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						alignment: 'right',
						text: 'ตามรายละเอียดดังนี้'
					}
				]
			},
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{ bold: true, fontSize: 14, alignment: 'center', text: 'รายการ' },
							{
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: 'จำนวนเงิน (บาท)'
							}
						]
					]
				}
			},
			arrYearly.map((e, i) => ({
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								columns: [
									{
										width: '65%',
										text: `${i + 1}. ${e.thName} ${e.engName}`
									},
									{
										width: '35%',
										alignment: 'right',
										text: arr.find((el) => el.treatment_name === e.find)
											? arr.find((el) => el.treatment_name === e.find).cgd_code
											: null
									}
								]
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								alignment: 'right',
								text: arr.find((el) => el.treatment_name === e.find)
									? e.find === 'X-Ray'
										? 170
										: arr.find((el) => el.treatment_name === e.find).cost.toLocaleString()
									: 0
							}
						]
					]
				}
			})),
			{
				table: {
					widths: [ '80%', '20%' ],
					body: [
						[
							{
								border: [ true, false, true, true ],
								columns: [
									[
										{
											fontSize: 16,
											bold: true,
											alignment: 'right',
											text: 'ตรวจสุขภาพประจำปี'
										}
									],
									[
										{
											fontSize: 14,
											bold: true,
											alignment: 'right',
											text: 'รวม'
										}
									]
								]
							},
							{
								border: [ true, false, true, true ],
								fontSize: 14,
								bold: true,
								alignment: 'right',
								text: sumYear.toLocaleString()
							}
						]
					]
				}
			},
			{
				margin: [ 0, 5, 0, 0 ],
				fontSize: 14,
				text: [
					'(ตัวอักษร) ',
					{
						decoration: 'underline',
						decorationStyle: 'dotted',
						text: `   ${thaibathYear}   `
					}
				]
			},
			{
				fontSize: 14,
				text: 'ไว้เป็นการถูกต้องแล้ว'
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'ลงชื่อ ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: '                            '
							},
							' ผู้รับเงิน'
						],
						fontSize: 14,
						alignment: 'center'
					}
				]
			},
			{
				columns: [
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: ''
					},
					{
						width: '33.33%',
						text: [
							'( ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: front
							},
							' )'
						],
						fontSize: 14,
						alignment: 'center'
					}
				],
				pageBreak: arrPlain.length > 0 && 'after'
			},
			arrPlain.length > 0
				? {
						columns: [
							[
								{
									color: 'red',
									alignment: 'left',
									text: [
										'เล่มที่ ',
										{
											fontSize: 18,
											text: !data.book_number ? '' : parseInt(data.book_number.split(',')[1])
										}
									]
								}
							],
							[ { alignment: 'center', fontSize: 18, bold: true, text: 'ใบเสร็จรับเงิน' } ],
							[
								{
									color: 'red',
									alignment: 'right',
									text: [
										'เลขที่ ',
										{
											fontSize: 18,
											text: !data.bill_number ? '' : parseInt(data.bill_number.split(',')[1])
										}
									]
								}
							]
						]
					}
				: null,
			arrPlain.length > 0
				? {
						alignment: 'center',
						fontSize: 18,
						bold: true,
						text: 'สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด'
					}
				: null,
			arrPlain.length > 0
				? {
						columns: [
							[
								{
									margin: [ 20, 0, 0, 0 ],
									width: 38,
									height: 58,
									image: file64.logo_kku
								},
								{
									margin: [ 32, 0, 0, 0 ],
									fontSize: 16,
									bold: true,
									text: '(ท)'
								}
							],
							[
								{
									alignment: 'right',
									fontSize: 16,
									text: 'คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น 40002 โทรศัพท์/โทรสาร (043) 202081'
								},
								{
									margin: [ 0, 0, 0, 0 ],
									fontSize: 16,
									alignment: 'right',
									text: [
										'วันที่',
										{
											decoration: 'underline',
											decorationStyle: 'dotted',
											text: `   ${moment(new Date()).format('DD')}   `
										},
										'เดือน',
										{
											decoration: 'underline',
											decorationStyle: 'dotted',
											text: `   ${moment(new Date()).format('MMMM')}   `
										},
										'พ.ศ.',
										{
											decoration: 'underline',
											decorationStyle: 'dotted',
											text: `   ${moment(new Date()).add(543, 'year').format('YYYY')}   `
										}
									]
								}
							]
						]
					}
				: null,
			arrPlain.length > 0
				? {
						columns: [
							{
								margin: [ 0, 5, 0, 0 ],
								fontSize: 14,
								text: [
									'ได้รับเงินจาก ',
									{
										decoration: 'underline',
										decorationStyle: 'dotted',
										text: `   ${fullname}   `
									}
								]
							},
							{
								margin: [ 0, 5, 0, 0 ],
								fontSize: 14,
								alignment: 'right',
								text: 'ตามรายละเอียดดังนี้'
							}
						]
					}
				: null,
			arrPlain.length > 0
				? {
						table: {
							widths: [ '80%', '20%' ],
							body: [
								[
									{ bold: true, fontSize: 14, alignment: 'center', text: 'รายการ' },
									{
										fontSize: 14,
										bold: true,
										alignment: 'right',
										text: 'จำนวนเงิน (บาท)'
									}
								]
							]
						}
					}
				: null,
			arrPlain.length > 0
				? arrPlain.map((e, i) => ({
						table: {
							widths: [ '80%', '20%' ],
							body: [
								[
									{
										border: [ true, false, true, true ],
										fontSize: 14,
										columns: [
											{
												width: '65%',
												text: [
													{ text: `${i + 1}. ${e.treatment_name} ` },
													{ text: e.detail ? `(${e.detail})` : null }
												]
											},
											{
												width: '35%',
												alignment: 'right',
												text: e.cgd_code
											}
										]
									},
									{
										border: [ true, false, true, true ],
										fontSize: 14,
										alignment: 'right',
										text: e.treatment_name === 'X-Ray' ? 230 : e.cost ? e.cost.toLocaleString() : 0
									}
								]
							]
						}
					}))
				: null,
			arrPlain.length > 0
				? {
						table: {
							widths: [ '80%', '20%' ],
							body: [
								[
									{
										border: [ true, false, true, true ],
										bold: true,
										alignment: 'right',
										fontSize: 14,
										text: 'รวม'
									},
									{
										border: [ true, false, true, true ],
										fontSize: 14,
										bold: true,
										alignment: 'right',
										text: sum.toLocaleString()
									}
								]
							]
						}
					}
				: null,
			arrPlain.length > 0
				? {
						margin: [ 0, 5, 0, 0 ],
						fontSize: 14,
						text: [
							'(ตัวอักษร) ',
							{
								decoration: 'underline',
								decorationStyle: 'dotted',
								text: `   ${thaibath}   `
							}
						]
					}
				: null,
			arrPlain.length > 0
				? {
						fontSize: 14,
						text: 'ไว้เป็นการถูกต้องแล้ว'
					}
				: null,
			arrPlain.length > 0
				? {
						columns: [
							{
								width: '33.33%',
								text: ''
							},
							{
								width: '33.33%',
								text: ''
							},
							{
								width: '33.33%',
								text: [
									'ลงชื่อ ',
									{
										decoration: 'underline',
										decorationStyle: 'dotted',
										text: '                            '
									},
									' ผู้รับเงิน'
								],
								fontSize: 14,
								alignment: 'center'
							}
						]
					}
				: null,
			arrPlain.length > 0
				? {
						columns: [
							{
								width: '33.33%',
								text: ''
							},
							{
								width: '33.33%',
								text: ''
							},
							{
								width: '33.33%',
								text: [
									'( ',
									{
										decoration: 'underline',
										decorationStyle: 'dotted',
										text: front
									},
									' )'
								],
								fontSize: 14,
								alignment: 'center'
							}
						]
					}
				: null
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
export const docKet = (data) => {
	let Invoice_name =
		Number(data.claim_official) === 1
			? 'ข้าราชการและลูกจ้างประจำ'
			: Number(data.claim_official) === 2
				? 'ข้าราชการและลูกจ้างประจำ(บุคคลในครอบครัว)'
				: Number(data.claim_official) === 3 ? 'ข้าราชการบำนาญ' : 'ข้าราชการบำนาญ(บุคคลในครอบครัว)';
	let sum = data.dataInvoice
		.map((e) => ({
			sum: e.details.reduce(fn.add('service_price'), 0)
		}))
		.reduce(fn.add('sum'), 0)
		.toLocaleString();
	let thaibath = fn.ArabicNumberToText(sum ? sum : 0);
	var docDefinition = {
		pageMargins: [ 55, 53, 55, 100 ],
		content: [
			{
				width: 48,
				height: 68,
				image: file64.logo_kku,
				absolutePosition: { x: 50, y: 30 }
			},
			{
				margin: [ 0, 15, 0, 0 ],
				alignment: 'center',
				fontSize: 20,
				bold: true,
				text: 'บันทึกข้อความ'
			},
			{
				columns: [
					{
						width: '10%',
						fontSize: 16,
						bold: true,
						text: 'ส่วนงาน'
					},
					{
						width: '90%',
						fontSize: 16,
						alignment: 'justify',
						text: 'คณะเทคนิคการแพทย์ สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัด โทร. 50074'
					}
				]
			},
			{
				columns: [
					[
						{
							columns: [
								{
									width: '20%',
									fontSize: 16,
									bold: true,
									text: 'ที่'
								},
								{
									width: '80%',
									fontSize: 16,
									text: 'อว 660301.8.3.1/'
								}
							]
						}
					],
					[
						{
							fontSize: 16,
							text: `วันที่ ${moment().utc().add(543, 'year').format('LL')}`
						}
					]
				]
			},
			{
				columns: [
					{
						width: '10%',
						fontSize: 16,
						bold: true,
						text: 'เรื่อง'
					},
					{
						width: '90%',
						fontSize: 16,
						text: `แจ้งหนี้ค่าตรวจสุขภาพประจำปี${Invoice_name} มหาวิทยาลัยขอนแก่น`
					}
				]
			},
			{
				columns: [
					{
						width: '10%',
						fontSize: 16,
						bold: true,
						text: 'เรียน'
					},
					{
						width: '90%',
						fontSize: 16,
						text: 'อธิการบดี'
					}
				]
			},
			{
				margin: [ 80, 10, 0, 0 ],
				fontSize: 16,
				alignment: 'justify',
				text: 'โครงงานห้องปฏิบัติการชุมชน สถานบริการสุขภาพเทคนิคการแพทย์และกายภาพบำบัดขอแจ้งหนี้'
			},
			{
				fontSize: 16,
				alignment: 'justify',
				text: `ค่าตรวจสุขภาพประจำปี และค่าตรวจทางห้องปฏิบัติการของ ${Invoice_name} มหาวิทยาลัยขอนแก่น ประจำเดือน${moment(
					data.startDate
				)
					.add(543, 'year')
					.format('MMMM YYYY')} จำนวน ${data.dataInvoice
					.length} ราย รวมเป็นเงินทั้งสิ้น ${sum} บาท (${thaibath}) ตามรายละเอียด`
			},
			{
				margin: [ 80, 10, 0, 0 ],
				fontSize: 16,
				alignment: 'justify',
				text: 'จึงเรียนมาเพื่อโปรดพิจารณาอนุมัติเบิกจ่ายเงินจำนวนดังกล่าว  ให้แก่สถานบริการสุขภาพเทคนิค'
			},
			{
				fontSize: 16,
				text: 'การแพทย์และกายภาพบำบัด คณะเทคนิคการแพทย์ จักเป็นพระคุณยิ่ง'
			},
			{
				columns: [
					{},
					{
						margin: [ 0, 120, 0, 0 ],
						alignment: 'center',
						fontSize: 16,
						text: [ `(${data.president})\n`, `คณบดีคณะเทคนิคการแพทย์` ]
					}
				]
			}
		],
		defaultStyle: {
			font: 'THSarabunNew'
		}
	};
	pdfMake.createPdf(docDefinition).open();
};
