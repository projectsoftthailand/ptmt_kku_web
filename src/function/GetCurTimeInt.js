export default function() {
	let d = new Date();
	let hh = +d.getHours();
	hh = hh * 60;
	let mm = +d.getMinutes();

	return hh + mm;
}
