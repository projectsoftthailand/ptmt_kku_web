export default function() {

    let d = new Date();

    let m = ''+(d.getMonth()+1)
    m = ('0'+m).slice(-2)

    let dd = ''+(d.getDate())
    dd = ('0'+dd).slice(-2)

    let hh = ''+d.getHours()
    hh = ('0'+hh).slice(-2)

    let mm = ''+d.getMinutes()
    mm = ('0'+mm).slice(-2)

    // 2019-08-25T25:08
    return d.getFullYear()+'-'+m+'-'+dd+'T'+hh+':'+mm
}