import React, { Component } from 'react';
import { SketchField, Tools } from 'react-sketch';
import '../App.css';
import Responsive from 'react-responsive';
import { ip, GET_PICTURE_SYMPTOMS } from '../service/service';

let default_symptom = require('../assets/image/antomy.png');

let S349 = (props) => <Responsive {...props} maxWidth={349} />;
let S350_389 = (props) => <Responsive {...props} minWidth={350} maxWidth={389} />;
let S390_419 = (props) => <Responsive {...props} minWidth={390} maxWidth={419} />;
let S420_479 = (props) => <Responsive {...props} minWidth={420} maxWidth={479} />;
let S480_529 = (props) => <Responsive {...props} minWidth={480} maxWidth={529} />;
let S530_699 = (props) => <Responsive {...props} minWidth={530} maxWidth={699} />;
let S700_999 = (props) => <Responsive {...props} minWidth={700} maxWidth={999} />;
let S1000_1199 = (props) => <Responsive {...props} minWidth={1000} maxWidth={1199} />;
let S1200 = (props) => <Responsive {...props} minWidth={1200} />;

export default class SketchFieldes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sizePen: 1,
			colorPen: '#FF0000',
			sketchImg: ''
		};
	}

	componentWillMount = async () => {
		this.props.onRef(this);
		let width = window.screen.width;
		let img = default_symptom;
		// let url = ip + (await GET_PICTURE_SYMPTOMS(this.props.symptom_picture));
		// if (width >= 1200) {
		// 	//------ 1200 ^
		// 	this._sketch.setBackgroundFromDataUrl(url, {
		// 		left: 150,
		// 		top: 150,
		// 		scale: 0.28
		// 	});
		// }
		await setTimeout(async () => {
			// let state = await JSON.parse(localStorage.getItem('stateSketchFieldes'));
			// if (state) {
			// 	if (state.list_id == this.props.list_id && this.props.save == false) {
			// 		await this.setState(state);
			// 		img = this.state.sketchImg;
			// 	}
			// } else {
			// 	this.state['list_id'] = this.props.list_id;
			// 	this.state.sketchImg = this._sketch.toDataURL();
			// 	localStorage.setItem('stateSketchFieldes', JSON.stringify(this.state));
			// }
			if (width >= 1200) {
				//------ 1200 ^
				await this._sketch.addImg(img, {
					left: 150,
					top: 150,
					scale: 0.28
				});
			}
			if (width >= 1000 && width <= 1199) {
				//------ 1000 - 1199
				await this._sketch.addImg(img, {
					left: 110,
					top: 100,
					scale: 0.23
				});
			}
			if (width >= 700 && width <= 999) {
				//------ 700 - 999
				await this._sketch.addImg(img, {
					left: 60,
					top: 60,
					scale: 0.15
				});
			}
			if (width >= 530 && width <= 699) {
				//------ 530 - 699
				await this._sketch.addImg(img, {
					left: 35,
					top: 40,
					scale: 0.11
				});
			}
			if (width >= 480 && width <= 529) {
				//------ 480 - 529
				await this._sketch.addImg(img, {
					left: 37.5,
					top: 40,
					scale: 0.09
				});
			}
			if (width >= 420 && width <= 479) {
				//------ 420 - 479
				await this._sketch.addImg(img, {
					left: 12,
					top: 20,
					scale: 0.09
				});
			}
			if (width >= 390 && width <= 419) {
				//------ 390 - 419
				await this._sketch.addImg(img, {
					left: 15,
					top: 20,
					scale: 0.08
				});
			}
			if (width >= 350 && width <= 389) {
				//------ 350 - 389
				await this._sketch.addImg(img, {
					left: 0,
					top: 5,
					scale: 0.08
				});
			}
			if (width <= 349) {
				//------ 310 - 349
				await this._sketch.addImg(img, {
					left: 5,
					top: 10,
					scale: 0.07
				});
			}
		}, 0.1);
	};
	componentWillUnmount() {
		// this.state['list_id'] = this.props.list_id;
		// this.state.sketchImg = this._sketch.toDataURL();
		// localStorage.setItem('stateSketchFieldes', JSON.stringify(this.state));
	}
	reTurn = () => {
		return {
			base64: this._sketch.toDataURL().split(`data:image/png;base64,`)[1]
		};
	};
	render() {
		let { sizePen, colorPen } = this.state;
		return (
			<div className="pt-3">
				<div>
					<strong>Area of symptoms:</strong>
					<div className="d-flex mt-2">
						<small className="mt-2 ml-3 mr-2 w-auto">Size</small>
						<div>
							<button
								id={sizePen == 1 ? 'btn-size-pen-dis' : 'btn-size-pen'}
								disabled={sizePen == 1}
								onClick={() => this.setState({ sizePen: 1 })}
							/>
							{'  '}
							<button
								id={sizePen == 5 ? 'btn-size-pen-dis1' : 'btn-size-pen1'}
								disabled={sizePen == 5}
								onClick={() => this.setState({ sizePen: 5 })}
							/>
							{'  '}
							<button
								id={sizePen == 10 ? 'btn-size-pen-dis2' : 'btn-size-pen2'}
								disabled={sizePen == 10}
								onClick={() => this.setState({ sizePen: 10 })}
							/>
							{'  '}
							<button
								id={sizePen == 15 ? 'btn-size-pen-dis3' : 'btn-size-pen3'}
								disabled={sizePen == 15}
								onClick={() => this.setState({ sizePen: 15 })}
							/>
							{'  '}
						</div>
					</div>
					<div className="d-flex mt-2">
						<small className="mt-1 ml-3 mr-2 w-auto">Color</small>
						<div>
							<button
								id="btn-FF0000"
								disabled={colorPen === '#FF0000'}
								onClick={() => this.setState({ colorPen: '#FF0000' })}
							/>
							{'  '}
							<button
								id="btn-FFA500"
								disabled={colorPen === '#FFA500'}
								onClick={() => this.setState({ colorPen: '#FFA500' })}
							/>
							{'  '}
							<button
								id="btn-FFFF00"
								disabled={colorPen === '#FFFF00'}
								onClick={() => this.setState({ colorPen: '#FFFF00' })}
							/>
							{'  '}
							<button
								id="btn-008000"
								disabled={colorPen === '#008000'}
								onClick={() => this.setState({ colorPen: '#008000' })}
							/>
							{'  '}
							<button
								id="btn-0000FF"
								disabled={colorPen === '#0000FF'}
								onClick={() => this.setState({ colorPen: '#0000FF' })}
							/>
							{'  '}
							<button
								id="btn-4B0082"
								disabled={colorPen === '#4B0082'}
								onClick={() => this.setState({ colorPen: '#4B0082' })}
							/>
							{'  '}
							<button
								id="btn-EE82EE"
								disabled={colorPen === '#EE82EE'}
								onClick={() => this.setState({ colorPen: '#EE82EE' })}
							/>
							{'  '}
						</div>
					</div>
				</div>
				<div style={{ marginTop: '0.5rem' }}>
					<button onClick={() => this._sketch.undo()}>Undo</button>
					<button onClick={() => this._sketch.redo()}>Redo</button>
					<button
						onClick={() => {
							this._sketch.clear();
							this._sketch.addImg(default_symptom, {
								left: 60,
								top: 80,
								scale: 0.13
							});
						}}
					>
						New
					</button>
					{/* Can clear but backgroundImageWill Disappear */}
					{/* <button onClick={() => this._sketch.clear()}>Clear</button> */}
				</div>
				{/* body painter */}
				<div className="sympon-width-hight mt-3 mb-3">
					<S1200>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="1000px"
							height="1000px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S1200>
					<S1000_1199>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="800px"
							height="800px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S1000_1199>
					<S700_999>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="500px"
							height="500px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S700_999>
					<S530_699>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="350px"
							height="350px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S530_699>
					<S480_529>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="300px"
							height="300px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S480_529>
					<S420_479>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="250px"
							height="250px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S420_479>
					<S390_419>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="230px"
							height="230px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S390_419>
					<S350_389>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="200px"
							height="200px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S350_389>
					<S349>
						<SketchField
							ref={(c) => (this._sketch = c)}
							tool={Tools.Pencil}
							lineColor={colorPen}
							widthCorrection={0.1}
							lineWidth={sizePen}
							width="190px"
							height="190px"
							style={{ boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)' }}
						/>
					</S349>
				</div>
			</div>
		);
	}
}
