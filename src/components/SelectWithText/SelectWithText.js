import React, { Component } from "react";
import { Form, FormGroup, Label, Input } from "reactstrap";
import "./Styled/style.css";

export default class SelectWithText extends Component {
  render() {
    let { text, name, value, onChange, status, options, width } = this.props;

    return (
      <Form className="d-flex justify-content-between align-items-center mr-2">
        <FormGroup controlId="formBasicText" style={width ? { width: width } : { width: "100%" }}>
          <Label className="LabelType">{text}</Label>
          <Input type="select" bsClass="FormType" name={name} placeholder={text} onChange={onChange} value={value}>
            {status
              ? options.map((d, index) => (
                  <option key={index.toString()} value={d.id}>
                    {d.name}
                  </option>
                ))
              : options.map((d, index) => (
                  <option key={index.toString()} value={d}>
                    {d}
                  </option>
                ))}
          </Input>
        </FormGroup>
      </Form>
    );
  }
}
