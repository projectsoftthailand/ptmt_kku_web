import React, { Component } from 'react';
import Clock from 'react-live-clock';
import moment from 'moment';
import Modal from 'react-modal';
import { Input, Label } from 'reactstrap';
import { IoMdClose } from 'react-icons/io';
import Color from '../Color';
import { Animated } from 'react-animated-css';
import ReactLoading from 'react-loading';
import ReactLightCalendar from '@lls/react-light-calendar';
import '@lls/react-light-calendar/dist/index.css';
import { CREATE_HOLIDAY, POST } from '../../service/service';
import User from '../../mobx/user/user';

const DAY_LABELS = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];
const MONTH_LABELS = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

export default class HolidayModal extends Component {
	constructor(props) {
		super(props);
		let date = new Date();
		let startDate = date.getTime();
		let endDate = date.getTime();
		this.state = {
			startDate, // Today
			endDate
			// wa
		};
	}

	onChange = (startDate, endDate) => {
		// console.log('startDate', startDate, 'endDate', endDate);
		this.setState({ startDate, endDate });
	};
	insert_holiday = async () => {
		let { startDate, endDate } = this.state;
		let id = User.user_id;
		let obj = {
			startDate,
			endDate,
			id
		};
		try {
			// this.setState({ wa });
			let res = await POST(CREATE_HOLIDAY, obj);
		} catch (error) {
			// console.log(error);
		}
	};
	render() {
		let { isOpenModal, CloseModal } = this.props;
		let { startDate, endDate } = this.state;
		return (
			<Modal isOpen={isOpenModal} onRequestClose={CloseModal} style={customStyles}>
				<Animated animationIn="fadeInDown" isVisible={true}>
					<div
						style={{
							width: 400,
							borderRadius: 10,
							backgroundColor: '#fff',
							paddingTop: 20,
							paddingBottom: 20,
							fontSize: 12,
							position: 'relative'
						}}
						className="container mx-auto text-center"
					>
						<div
							style={{
								fontSize: 11,
								position: 'absolute',
								display: 'flex',
								top: 10,
								left: 10,
								color: Color.Blue
							}}
						>
							<div className="mr-2">{moment().format('DD/MM/') + (+moment().format('YYYY') + 543)}</div>
							<Clock format={'HH:mm:ss'} ticking={true} timezone={'US/Pacific'} />
						</div>
						<div
							className="btn-exit-hover"
							onClick={() => CloseModal()}
							style={{
								position: 'absolute',
								backgroundColor: Color.Blue,
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'center',
								borderRadius: '100%',
								width: 30,
								height: 30,
								cursor: 'pointer',
								top: 20,
								right: 20,
								marginTop: -10,
								marginRight: -10
							}}
						>
							<IoMdClose color="white" size={18} />
						</div>
						<div style={{ width: '90%' }} className="pt-5 pb-4 mx-auto d-flex justify-content-center">
							<ReactLightCalendar
								startDate={startDate}
								endDate={endDate}
								onChange={this.onChange}
								range
								disableDates={(date) => date < new Date().getTime()}
							/>
						</div>
						<button onClick={this.insert_holiday} className="btn-form">
							ยืนยัน
						</button>
					</div>
				</Animated>
			</Modal>
		);
	}
}
