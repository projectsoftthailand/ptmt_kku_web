export default {
    logo: require("../assets/logo/ams_logo.png"),
    logowhite: require("../assets/logo/Logo-New.png"),
    logoonly: require("../assets/logo/Logo-Only.png"),
};
