import React, { Component } from 'react';
import './results.css';
import excel from '../assets/image/uploadfileExcel.png';
import pdf from '../assets/image/pdf.png';
import { POST, GET, UPLOAD_RESULT, GET_RESULT, APPROVE_RESULT } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import { Table, Row, Col, TabContent, TabPane, Nav, NavItem, NavLink, Label } from 'reactstrap';
import classnames from 'classnames';
import { FaTimesCircle } from 'react-icons/fa';
import Modal from 'react-modal';
import ReactLoading from 'react-loading';
import { Link } from 'react-router-dom';
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
export default class Outpatient_results extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeTab: '1',
			headers: [],
			results: [],
			check_file: [],
			fileData: [],
			wait_loading: false,
			PDF: false
		};
	}

	componentDidMount = async () => {
		let type = 'all';
		let date = moment().format('YYYY-MM-DD');
		// let date = "all";
		let res = await GET(GET_RESULT(type, date));
		// console.log(res);
		if (res.success) {
			let result = res.result;
			if (typeof result[0] !== 'undefined') {
				this.setState(
					{
						results: result.map((e) => {
							let result = JSON.parse(e.result);
							result.isApprove = e.isApprove;
							result.result_id = e.result_id;
							result.service_unit = e.service_unit;
							result.service_status = e.service_status;
							result.user_id = e.user_id;
							result.list_id = e.list_id;
							return result;
						})
					},
					() => {
						var header = [];
						result.map((el) => JSON.parse(el.result)).forEach((el, i) => {
							header.push(Object.keys(el));
							if (i === result.length - 1) {
								header = [].concat.apply([], header);
							}
						});
						var uniqueArray = header.filter(function(el, i) {
							return header.indexOf(el) == i;
						});
						this.description(uniqueArray.sort());
					}
				);
			}
		}
	};

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}
	uploadFiles = async (e) => {
		// this.setState({ fileData: e.target.files });
		for (let i = 0; i < e.target.files.length; i++) {
			// console.log('e', e.target.files[i]);
			let obj = { name: e.target.files[i].name, size: e.target.files[i].size };
			let { check_file, fileData } = this.state;
			check_file.push(obj);
			fileData.push(e.target.files[i]);
			this.setState({ check_file: check_file, fileData });
		}
	};
	description(e) {
		this.move(e, -27, 0);
		this.move(e, -22, 1);
		this.move(e, -15, 2);
		this.move(e, -7, 3);
		this.move(e, -22, 4);
		this.move(e, -16, 5);
		this.move(e, -17, 6);
		this.move(e, -16, 7);
		this.move(e, -25, 8);
		this.move(e, -6, 9);
		this.move(e, -16, 10);
		this.move(e, -22, 11);
		this.move(e, -10, 12);
		this.move(e, -15, 13);
		this.move(e, -1, 14);
		this.move(e, -1, 15);
		this.move(e, -3, 16);
		this.move(e, -2, 17);
		this.move(e, -1, 18);
		this.move(e, -7, 19);
		this.move(e, -7, 20);
		this.move(e, -9, 21);
		this.move(e, -9, 22);
		this.move(e, -9, 23);
		this.move(e, -9, 24);
		this.move(e, -3, 25);
		this.move(e, -3, 26);
		this.move(e, -3, 27);
		this.move(e, -3, 28);
		this.move(e, -1, 29);
		this.move(e, -1, 30);
		this.setState({
			headers: e
		});
	}
	move(arr, old_index, new_index) {
		while (old_index < 0) {
			old_index += arr.length;
		}
		while (new_index < 0) {
			new_index += arr.length;
		}
		if (new_index >= arr.length) {
			var k = new_index - arr.length;
			while (k-- + 1) {
				arr.push(undefined);
			}
		}
		arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
		return arr;
	}
	Submit = async (e) => {
		let result_id = e;
		try {
			this.setState({ wait_loading: true });
			let res = await GET(APPROVE_RESULT(result_id));
			if (res.success === true) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 2000
				}).then(() => {
					window.location.reload();
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			swal('NetworkError!', error, 'error', {
				buttons: false,
				timer: 3000
			});
		}
	};
	delCheckfile(i) {
		let { check_file } = this.state;
		check_file.splice(i, 1);
		this.setState({ check_file: check_file });
	}
	SubmitUploadfile = async () => {
		let { check_file } = this.state;
		if (check_file.length == 2) {
			let formdata = new FormData();
			let { fileData } = this.state;

			formdata.append('fileData', fileData[0]);
			formdata.append('fileData', fileData[1]);
			try {
				this.setState({ wait_loading: true });
				let res = await POST(UPLOAD_RESULT, formdata, true);
				// console.log('fileData', fileData);
				if (res.success) {
					this.setState({ wait_loading: false });
					// console.log('res', res.result.map((el) => JSON.parse(el.result)));
					swal('สำเร็จ!', 'อัปโหลดไฟล์สำเร็จ', 'success').then(async () => {
						let result = res.result;
						let maps = result.map((e) => {
							let result = JSON.parse(e.result);
							result.isApprove = e.isApprove;
							result.result_id = e.result_id;
							return result;
						});
						await this.setState({
							results: maps
						});
						var header = [];
						await result.map((el) => JSON.parse(el.result)).forEach((el, i) => {
							header.push(Object.keys(el));
							if (i === result.length - 1) {
								header = [].concat.apply([], header);
							}
						});
						var uniqueArray = header.filter(function(el, i) {
							return header.indexOf(el) == i;
						});
						// await this.description(uniqueArray.sort())
						await window.location.reload();
					});
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error');
				}
			} catch (error) {
				this.setState({ wait_loading: false });
				// console.log(error);
			}
		} else {
			swal('ไฟล์เอกสารไม่ครบ!', 'อัปโหลดไฟล์ไม่สำเร็จ', 'warning');
		}
	};
	render() {
		let { headers, results, check_file, wait_loading, PDF } = this.state;
		// console.log('results', results);
		return (
			<div id="div-vh">
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<div style={{ height: '100%', width: '97%', marginTop: '20px' }}>
					<Nav tabs>
						<NavItem>
							<NavLink
								className={classnames({ active: this.state.activeTab === '1' })}
								onClick={() => {
									this.toggle('1');
								}}
							>
								<label id="label-result">อัปโหลดผลตรวจ</label>
							</NavLink>
						</NavItem>
						<NavItem>
							<NavLink
								className={classnames({ active: this.state.activeTab === '2' })}
								onClick={() => {
									this.toggle('2');
								}}
							>
								<label id="label-result">ผลตรวจ</label>
							</NavLink>
						</NavItem>
					</Nav>
					<TabContent
						activeTab={this.state.activeTab}
						style={{ borderStyle: 'solid', borderWidth: '0px 1px 1px 1px', borderColor: '#dee2e6' }}
					>
						<TabPane tabId="1">
							<Row>
								<Col sm="12">
									<div id="div-button-results">
										{check_file.length == 0 && (
											<div
												className="d-flex justify-content-around"
												style={{ width: '70%', height: '50%' }}
											>
												<div id="div-solid" onClick={() => this.setState({ PDF: false })}>
													<label style={{ cursor: 'pointer', width: '100%', height: '100%' }}>
														<div
															style={{
																display: 'flex',
																alignItems: 'center',
																flexDirection: 'column',
																justifyContent: 'center',
																height: '100%'
															}}
														>
															<input
																multiple
																type="file"
																id="uploadfile"
																onChange={this.uploadFiles}
																accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
															/>
															<img
																src={excel}
																style={{
																	width: '7rem',
																	height: 'auto',
																	fontWeight: 'bold'
																}}
															/>
															<h3>Upload your files here.</h3>
															<br />
															<h5>อัพโหลดไฟล์ Excel ที่นี้!</h5>
														</div>
													</label>
												</div>
												{/* <div id="div-solid" onClick={() => this.setState({ PDF: true })}>
													<label style={{ cursor: 'pointer', width: '100%', height: '100%' }}>
														<div
															style={{
																display: 'flex',
																alignItems: 'center',
																flexDirection: 'column',
																justifyContent: 'center',
																height: '100%'
															}}
														>
															<input
																multiple
																type="file"
																id="uploadfile"
																onChange={this.uploadFiles}
																accept="application/pdf"
															/>
															<img
																src={pdf}
																style={{
																	width: '7rem',
																	height: 'auto',
																	fontWeight: 'bold'
																}}
															/>
															<h3>Upload your files here.</h3>
															<br />
															<h5>อัพโหลดไฟล์ Pdf ที่นี้!</h5>
														</div>
													</label>
												</div> */}
											</div>
										)}
										{check_file.length > 0 && (
											<div id="div-solid" style={{ width: '65%', height: '85%' }}>
												<div id="div-file">
													<div>ไฟล์ {PDF ? 'Pdf*' : 'Excel*'}</div>
													<div>
														<label className="custom-file-upload">
															<div id="div-label">
																<input
																	multiple
																	type="file"
																	id="uploadfile"
																	onChange={this.uploadFiles}
																	accept={
																		PDF ? (
																			'application/pdf'
																		) : (
																			'.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
																		)
																	}
																/>
																<img
																	src={PDF ? pdf : excel}
																	style={{
																		width: 10,
																		height: 10,
																		marginRight: '0.5rem'
																	}}
																/>
																<div id="h3">Upload your files here.</div>
															</div>
														</label>
													</div>
												</div>
												<br />
												<div id="div-list">
													<div id="div-listdata">ลำดับที่</div>
													<div style={{ textAlign: 'center' }} id="div-listdata">
														ชื่อไฟล์
													</div>
													<div style={{ textAlign: 'right', width: '30%' }}>ขนาดไฟล์</div>
												</div>
												<hr />
												{check_file.map((e, i) => (
													<div id="div-list">
														<div id="div-listdata" style={{ paddingLeft: '0.5rem' }}>
															<img
																src={PDF ? pdf : excel}
																style={{ width: 20, height: 20 }}
															/>
														</div>
														<div style={{ textAlign: 'center' }} id="div-listdata">
															{e.name}
														</div>
														<div style={{ textAlign: 'right', width: '30%' }}>
															{e.size >= 1024 ? (
																(Number(e.size) / 1024).toFixed(2) + ' mb.'
															) : (
																e.size + ' kb.'
															)}
														</div>
														<div style={{ textAlign: 'right', width: '5%' }}>
															<FaTimesCircle
																id="btn-cancal"
																onClick={() => this.delCheckfile(i)}
															/>
														</div>
													</div>
												))}
												<div>
													<br />
													<button onClick={this.SubmitUploadfile} id="btn-submit">
														ยืนยัน
													</button>
												</div>
											</div>
										)}
									</div>
								</Col>
							</Row>
						</TabPane>
						<TabPane tabId="2">
							<Row>
								<Col sm="12">
									{results.length == 0 ? (
										<div id="div-button-results">---ไม่มีข้อมูล---</div>
									) : (
										<Table style={{ whiteSpace: 'pre' }} responsive striped>
											<thead>
												<tr>
													<th />
													{headers.map((el, index) => <th key={index.toString()}>{el}</th>)}
												</tr>
											</thead>

											<tbody>
												{results.map((e, i) => (
													<tr key={i.toString()}>
														<td>
															<button
																disabled={e.isApprove == 1}
																onClick={() => this.Submit(e.result_id)}
																style={
																	e.isApprove == 1 ? (
																		{
																			color: 'white',
																			border: 'none',
																			borderRadius: '3px',
																			backgroundColor: 'rgb(183, 54, 142)',
																			width: '55px'
																		}
																	) : (
																		{
																			color: 'white',
																			border: 'none',
																			borderRadius: '3px',
																			backgroundColor: 'rgb(70, 121, 199)',
																			width: '55px'
																		}
																	)
																}
															>
																{e.isApprove == 1 ? 'ยืนยันแล้ว' : 'ยืนยัน'}
															</button>
														</td>
														{headers.map((els, index) => (
															<td
																key={index.toString()}
																onClick={() =>
																	this.gotoHistory(
																		e.service_unit,
																		e.service_status,
																		e.user_id,
																		e.list_id
																	)}
															>
																{e[els]}
															</td>
														))}
													</tr>
												))}
											</tbody>
										</Table>
									)}
								</Col>
							</Row>
						</TabPane>
					</TabContent>
				</div>
			</div>
		);
	}
	gotoHistory = (unit, status, id, list_id) => {
		this.props.history.push(`/home/table/customer/${unit}/${status}/${id}/${list_id}`);
	};
}
