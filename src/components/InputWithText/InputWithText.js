import React, { Component } from "react";
import { Form, FormGroup, Label, Input } from "reactstrap";
import "./Styled/style.css";

export default class InputWithText extends Component {
  render() {
    let { text, name, value, onChange, width } = this.props;

    return (
      <Form className="d-flex justify-content-between align-items-center mr-2">
        <FormGroup controlId="formBasicText" style={width ? { width: width } : { width: "100%" }}>
          <Label className="LabelType">{text}</Label>
          <Input type="text" bsClass="FormType" name={name} placeholder="ค้นหา..." onChange={onChange} value={value} />
        </FormGroup>
      </Form>
    );
  }
}
