import React, { Component } from "react";
import Today from "../Today/Today";
import socketIOClient from "socket.io-client";
import { _ip } from "../../service/service";

export default class AllWorkTable extends Component {
  componentWillMount() {
    // this.response();
  }

  response() {
    socketIOClient(_ip).on("rerender", res => {
      this.reRender();
    });
  }

  componentWillUnmount() {
    socketIOClient(_ip).disconnect();
  }

  reRender() {
    this.reRenderToday.reRender();
  }

  render() {
    return (
      <div className="col-md-12" style={{ fontSize: "1.3rem" }}>
        <Today onRef={ref => (this.reRenderToday = ref)} AllTable={true} />
      </div>
    );
  }
}
