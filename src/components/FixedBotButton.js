import React from 'react'
import Color from './Color';
import { FaRegSave } from "react-icons/fa";

export default function ({ text, Method }) {
    return (
        <div style={{
            position: 'fixed',
            bottom: 0,
            backgroundColor: 'white',
            textAlign: 'right',
            width: '100%',
            zIndex: 20,
            paddingRight: 12,
            boxShadow: '0px -1px 1px #ccc',
        }}>
            <div
                style={{
                    backgroundColor: Color.Blue,
                    color: 'white',
                    paddingRight: 8,
                    paddingLeft: 8,
                    paddingTop: 5,
                    paddingBottom: 5,
                    borderColor: 'transparent',
                }}
                onClick={Method} className="btn btn-primary button-blue-hover">
                <FaRegSave size={15} />
                <span style={{ paddingTop: 2, paddingLeft: 5 }}>{text}</span>
            </div>
        </div>
    )
}


