import React, { Component } from "react";
import "./Styled/style.css";
import { GET, GET_TREATMENT } from "../../../service/service";
import CheckboxProgram from "./CheckboxProgram";

class CardProgram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      curCheck: [],
      servicePrice: 0,
      lab_result: "",
      isValid: false,
      isReject: false,
      treatment: [],
      treatmentData: [],
      checkboxesValid: [],
      isValidAll: false,
      isRejectAll: false
    };
  }

  async GetTreatment() {
    try {
      let res = await GET(GET_TREATMENT("all"));
      // console.log(res)
      let d = [];
      let cur = [];
      let curPrice = 0;
      if (res.success) {
        res.result.forEach((e, index) => {
          d.push([
            e.name,
            e.cost,
            <input
              key={"in" + index}
              id={"" + index}
              onClick={() => this.onClick(index + 1)}
              onChange={e => console.log(e.target.value)}
              checked={false}
              type="checkbox"
            />
          ]);
          cur.push(false);
        });
        this.setState({
          treatment: d,
          treatmentData: res.result,
          curCheck: cur,
          servicePrice: curPrice,
          checkboxesValid: res.result.map(el => ({
            id: el.id,
            name: el.treatment_name,
            detail: el.detail,
            type: el.type,
            value: el.treatment_name,
            isValid: false,
            isReject: false
          })),
          loading: false
        });
        // console.log(d, res.result, this.state.checkboxesValid)
      }
    } catch (error) {}
  }

  componentWillMount() {
    this.GetTreatment();
  }

  handleAllValidChecked = event => {
    let checkboxs = this.state.checkboxesValid;
    checkboxs.forEach(el => (el.isValid = event.target.checked));
    this.setState(prevState => ({ checkboxesValid: checkboxs, isValidAll: !prevState.isValidAll }));
  };

  handleAllRejectChecked = event => {
    let checkboxs = this.state.checkboxesValid;
    checkboxs.forEach(el => (el.isReject = event.target.checked));
    this.setState(prevState => ({ checkboxesValid: checkboxs, isRejectAll: !prevState.isRejectAll }));
  };

  handleCheckValid = (event, index) => {
    let checkboxs = this.state.checkboxesValid;
    checkboxs.forEach((el, i) => {
      if (i === index && el.value === event.target.value && el.isReject === true) {
        el.isValid = event.target.checked;
        el.isReject = !event.target.checked;
      } else if (el.value === event.target.value) el.isValid = event.target.checked;
    });
    this.setState({ checkboxesValid: checkboxs, isValidAll: false, isRejectAll: false });
  };

  handleCheckReject = (event, index) => {
    let checkboxs = this.state.checkboxesValid;
    checkboxs.forEach((el, i) => {
      if (i === index && el.value === event.target.value && el.isValid === true) {
        el.isReject = event.target.checked;
        el.isValid = !event.target.checked;
      } else if (el.value === event.target.value) el.isReject = event.target.checked;
    });
    this.setState({ checkboxesValid: checkboxs, isValidAll: false, isRejectAll: false });
  };

  render() {
    // let { id } = this.props;
    let { isValidAll, isRejectAll, checkboxesValid } = this.state;
    // let new_id = sevenDigits(id);
    // console.log(treatmentData)

    return (
      <div className="div-table-program-all">
        <div className="div-table-program-detail">
          <div className="col-md-3 div-subject-list-checkbox">
            <form>
              <label className="font-style-color-wht">
                <input
                  name="isValid"
                  type="checkbox"
                  style={{ marginRight: "0.5vw" }}
                  checked={isValidAll}
                  onChange={this.handleAllValidChecked}
                  value="isValid"
                />
                Valid
              </label>
            </form>
            <form>
              <label className="font-style-color-wht">
                <input
                  name="isReject"
                  type="checkbox"
                  style={{ marginRight: "0.5vw" }}
                  checked={isRejectAll}
                  onChange={this.handleAllRejectChecked}
                  value="isReject"
                />
                Reject
              </label>
            </form>
          </div>
          <div className="col-md-4 div-subject-list">
            <label style={{ fontSize: 14, fontWeight: "normal" }}>โปรแกรมตรวจสุขภาพ</label>
          </div>
          <div className="col-md-3 div-subject-list">
            <label style={{ fontSize: 14, fontWeight: "normal" }}>ผลตรวจ</label>
          </div>
          <div className="col-md-2" />
        </div>
        <div className="div-table-program-list">
          {checkboxesValid.map((el, index) => {
            return (
              <div className="div-treatment-list" key={index}>
                <div className="col-md-3 div-treatment-list m-0">
                  <CheckboxProgram
                    handleCheckChieldElement={event => this.handleCheckValid(event, index)}
                    {...el}
                    Label="Valid"
                  />
                  <CheckboxProgram
                    handleCheckChieldElement={event => this.handleCheckReject(event, index)}
                    {...el}
                    Label="Reject"
                  />
                </div>
                <div className="col-md-4 m-0">
                  <label style={{ fontSize: 14, fontWeight: "normal" }}>{el.name}</label>
                </div>
                <div className="col-md-3 div-treatment-list-result m-0">
                  <input
                    type="text"
                    name="lab_result"
                    onChange={this.handleChange}
                    disabled={el.isValid ? false : true}
                    style={el.isValid ? { backgroundColor: "transparent" } : { backgroundColor: "#BDBDBD" }}
                  />
                  {el.name === "วัดความดันโลหิต" ? <label className="label-font-result-list">mmHg</label> : <label />}
                </div>
                <div className="col-md-2 m-0">
                  <div className="btn-history-treatment">ผลย้อนหลัง</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
export default CardProgram;
