import React from "react";
// ({ key, label, isSelected, onCheckboxChange })
const CheckboxProgram = props => (
    <form>
        <label className="font-style-color-wht" style={{ color: '#000', fontWeight: 'normal' }}>
            <input
                style={{ marginRight: '0.5vw' }}
                key={props.id}
                onClick={props.handleCheckChieldElement}
                type="checkbox"
                checked={props.Label === "Valid" ? props.isValid : props.isReject}
                value={props.value}
            />
            {props.Label}
        </label>
    </form>
);

export default CheckboxProgram;