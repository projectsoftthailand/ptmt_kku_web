import React, { Component } from 'react';
// import { FaMapMarkerAlt } from "react-icons/fa";
import './Styled/style.css';
import { getRoleName, getOntimeName } from '../../../function/function';
import {
	Row,
	Col,
	Table,
	FormGroup,
	Label,
	Input,
	TabContent,
	TabPane,
	Nav,
	NavItem,
	NavLink,
	Button
} from 'reactstrap';
import {
	POST,
	GET,
	GET_LIST_BY_ID,
	GET_USERS,
	GET_RESULT,
	APPROVE_RESULT,
	GET_UNIT,
	GET_STATUS,
	UPDATE_LIST_BY_FRONT,
	GET_CLAIM,
	UPDATE_SYMPTOM,
	CREATE_BILL,
	RESULTPDF,
	GET_RESULTPDF
} from '../../../service/service';
import { pdfResult } from '../../../function/PDF';
import classnames from 'classnames';
import swal from 'sweetalert';
import { FaRegEdit, FaPrint, FaTimesCircle, FaFileUpload } from 'react-icons/fa';
import { TiDocumentText } from 'react-icons/ti';
import moment from 'moment';
import Edit from './EditTeartment';
import Time_docterPT from '../../../pages/front/Home/AddCustomer/Time_docterPT';
import PT_body from '../../../pages/front/Home/AddCustomer/PT_body';

import { Animated } from 'react-animated-css';
import ReactLoading from 'react-loading';
import Modal from 'react-modal';
import { Route, withRouter } from 'react-router-dom';
import Responsive from 'react-responsive';
import Color from '../../Color';
import mobx from '../../../mobx/user/user';
import pdf from '../../../assets/image/pdf.png';

let Max = (props) => <Responsive {...props} maxWidth={688} />;
let Min = (props) => <Responsive {...props} minWidth={689} />;

let ThMax = (props) => <Responsive {...props} maxWidth={676} />;
let ThMin = (props) => <Responsive {...props} minWidth={677} />;

function LgLabel(name) {
	return (
		<Label className="label-user-detail" size="lg">
			{name}
		</Label>
	);
}

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

class CardList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			doctors: [],
			list_unit: [],
			list_status: [],
			list_data: {},
			activeTab: '1',

			result: [],
			new_result: [],
			header: [],
			data_detail: {},
			doctor: 0,
			unit: 0,
			status: 0,
			service_at: '',
			company: '',
			ln: '',

			edit: 0,
			result_pdf: {},

			reported_date: '',
			sampling_date: '',
			received_date: '',

			service_price: 0,
			claim: [],
			payType: 0,
			service_treatment: [],

			holiday: '',
			date_tmpt: moment(new Date()).format('YYYY-MM-DD'),
			time_tmpt: '00:00',
			type_time: 1,

			wait_loading: false,
			checkedit: 0,
			checkPDF: false,
			filepdf: {}
		};
	}

	async componentWillMount() {
		this.props.onRef(this);
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		await setTimeout(async () => {
			let state = await JSON.parse(localStorage.getItem('stateCardList'));
			if (state) {
				if (state.list_id == this.props.list_id && this.props.save == false) {
					await this.setState(state);
				} else {
					await Promise.all([ this.fetch(), this.getData() ]);
				}
			} else {
				await Promise.all([ this.fetch(), this.getData() ]);
				this.state['list_id'] = this.props.list_id;
				localStorage.setItem('stateCardList', JSON.stringify(this.state));
			}
		}, 0.01);
		await this.setState({ loading: false });
	}
	componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		localStorage.setItem('stateCardList', JSON.stringify(this.state));
	}
	async getData() {
		let type = 'all';
		let date = 'all';
		let res = await GET(GET_RESULT(type, date));
		if (res.success) {
			if (typeof res.result[0] !== 'undefined') {
				let result = res.result.filter((e) => {
					return e.list_id == this.props.list_id;
				});

				if (result.length > 0) {
					let myObject = JSON.parse(result[0].result);
					Object.keys(myObject).map(function(key, index) {
						myObject[key] = String(myObject[key]);
					});
					// console.log('result', JSON.parse(result[0].result));
					this.setState({
						result: result,
						result_pdf: myObject,
						reported_date: moment(JSON.parse(result[0].result).ValidateTime).format('LL'),
						sampling_date: moment(JSON.parse(result[0].result).RequestTime).format('LL'),
						received_date: moment(JSON.parse(result[0].result).ServiceDate).format('LL')
					});
					var new_result = [];
					new_result = JSON.parse(result[0].result);
					var header = [];
					result.map((e) => JSON.parse(e.result)).forEach((el, i) => {
						header.push(Object.keys(el));
						if (i === result.length - 1) {
							header = [].concat.apply([], header);
						}
					});
					this.setState({
						new_result,
						header: header.filter((e) => !isNaN(e.slice(0, 1))),
						data_detail: {
							AgeSYear: new_result.AgeSYear,
							BMI: new_result.BMI,
							Cname: new_result.Cname,
							HN: new_result.HN,
							HeartRate: new_result.HeartRate,
							Height: new_result.Height,
							LabNo: new_result.LabNo,
							fullname: new_result.OTitle + new_result.OFname + ' ' + new_result.OLname,
							ServiceDate: new_result.ServiceDate,
							Systolic: new_result.Systolic,
							Ward: new_result.Ward,
							Weight: new_result.Weight,
							isApprove: result[0].isApprove,
							result_id: result[0].result_id
						}
					});
				}
			}
		}
	}
	async fetch() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		let { list_id } = this.props;
		let res = await GET(GET_CLAIM('all'));
		// console.log('res', res);
		let list = await GET(GET_LIST_BY_ID(list_id));
		// console.log('GET_LIST_BY_ID', list);
		let unit = await GET(GET_UNIT);
		let status = await GET(GET_STATUS);
		let listData = list.result[0];
		let doctors = await GET(GET_USERS(listData.type == 'tm' || listData.type == 'pt' ? 'pt' : 'mt'));
		let type = listData.sub_type;
		let doctorData =
			USER.role == 'frontMT' || USER.role == 'mt'
				? doctors.result
				: doctors.result.filter((el, i) => {
						if (el.sub_role.some((e) => e == type)) {
							return true;
						}
					});
		//-----------------------------------------------------
		let paytype = res.result.filter((e) => e.claim_name == listData.claim_name);
		// console.log('listData.service_at', listData.service_at);
		let obj = {
			ln: listData.ln,
			doctor: listData.doctor,
			unit: listData.service_unit,
			status: listData.service_status,
			service_at: listData.service_at,
			payType: paytype[0].service_claim_id,
			service_treatment: listData.service_treatment,
			service_price: listData.service_price
		};
		//-----------------------------------------------------
		this.props.length_treatment(listData.service_treatment);
		this.props.setisPaid(listData.isPaid);
		this.props.update_LAB(obj);
		this.props.setStatus(listData.service_status);
		this.props.setType(listData.type);
		this.props.setPrice(listData.service_price);
		// console.log('obj', obj);
		// console.log('listData.service_at.slice(0, 16)', listData.service_at.slice(0, 16));
		// console.log(
		// 	'listData.service_at.split(" ")',
		// 	listData.service_at.split(' ')[0] + 'T' + listData.service_at.split(' ')[1]
		// );
		this.setState({
			list_data: listData,
			doctors: doctorData,
			list_unit: unit.result,
			list_status: status.result,
			service_at: moment(listData.service_at).format('YYYY-MM-DDTHH:mm'),
			doctor: listData.doctor == null ? 0 : listData.doctor,
			unit: listData.service_unit,
			status: listData.service_status,
			company: listData.company,
			claim: res.result,
			payType: paytype[0].service_claim_id,
			service_treatment: listData.service_treatment,
			time_tmpt: obj.service_at.split(' ')[1],
			date_tmpt: obj.service_at.split(' ')[0],
			ln: listData.ln
		});
	}
	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}
	Submit = async (e) => {
		let result_id = e;
		// console.log('result_id', result_id);
		try {
			this.setState({ wait_loading: true });
			let res = await GET(APPROVE_RESULT(result_id));
			if (res.success === true) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 2000
				}).then(() => {
					window.location.reload();
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			swal('NetworkError!', error, 'error', {
				buttons: false,
				timer: 3000
			});
		}
	};
	confirmisPaid = async () => {
		swal('ยืนยัน', 'ยืนยันการชำระเงินใช่หรือไม่', 'warning', {
			buttons: {
				cancel: 'ยกเลิก',
				confirm: {
					text: 'ยืนยัน',
					value: 'confirm'
				}
			}
		}).then((value) => {
			switch (value) {
				case 'confirm':
					this.isPaid();
					break;
			}
		});
	};
	isPaid = async () => {
		let { list_data } = this.state;
		let list_id = list_data.list_id;
		let obj = {
			isPaid: 1,
			service_status: 5
		};
		try {
			this.setState({ wait_loading: true });
			let res = await POST(UPDATE_LIST_BY_FRONT(list_id), obj);
			if (res.success) {
				if (list_data.type === 'tm' || list_data.type === 'pt') {
					await POST(CREATE_BILL, { id: list_id, type: list_data.type });
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 2000
					}).then(() => {
						window.location.reload();
					});
				} else {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 2000
					}).then(() => {
						window.location.reload();
					});
				}
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
		}
	};
	stateBill = () => {
		return this.state.list_data;
	};
	update_LAB() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		let { doctor, unit, status, service_at, payType, service_treatment, date_tmpt, time_tmpt, ln } = this.state;
		// console.log('date_tmpt', date_tmpt);
		let obj = {
			ln,
			doctor:
				mobx.role == 'frontPT'
					? USER.role == 'frontMT' || USER.role == 'mt' ? doctor : this.Time_docterPT.returnState().docterID
					: null,
			unit,
			status,
			service_at: USER.role == 'frontMT' || USER.role == 'mt' ? service_at : date_tmpt + ' ' + time_tmpt,
			payType,
			service_treatment
		};
		this.props.update_LAB(obj);
	}
	updateSymptom = async () => {
		let { obj, test_id, total } = this.props;
		let { list_data, checkedit } = this.state;
		let dataSym = this.ptBody.getData();
		let list_id = list_data.list_id;
		if (checkedit) {
			// let text_treatment =
			// 	obj.service_treatment.length > 0
			// 		? obj.service_treatment
			// 				.map((e) => e.treatment_name + ' ' + (e.detail ? `(${e.detail})` : ''))
			// 				.toString()
			// 		: null;
			let dataList = {
				ln: !obj.ln ? null : Number(obj.ln),
				doctor: Number(obj.doctor == 0 ? null : obj.doctor),
				service_unit: Number(obj.unit),
				service_status: Number(test_id),
				service_at: moment(obj.service_at).format('YYYY-MM-DD HH:mm'),
				service_claim: Number(obj.payType),
				service_price: Number(total) || obj.service_price,
				service_treatment: JSON.stringify(obj.service_treatment)
			};
			let objSym = {
				diagnosis: dataSym.diagnosis,
				problem: dataSym.problem,
				chief_complaint: dataSym.chief_complaint,
				so_examination: dataSym.so_examination,
				physiotherapist: dataSym.physiotherapist,
				goal_of_treatment: dataSym.goal_of_treatment,
				treatment: dataSym.treatment,
				symptom_picture: dataSym.base64
			};

			try {
				this.props.modal();
				this.setState({ wait_loading: true });
				let res = await POST(UPDATE_LIST_BY_FRONT(list_id), dataList);
				await POST(UPDATE_SYMPTOM(list_id), objSym);
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 2000
					}).then(async () => {
						window.location.reload();
					});
				} else {
					this.setState({ wait_loading: false });
					swal('ผิดพลาด!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			} catch (error) {
				this.setState({ wait_loading: false });
			}
		} else {
			this.ptBody.getData();
		}
	};
	updateSymptomTwo = async (e) => {
		let { obj, test_id, total } = this.props;
		let dataSym = e;
		let { list_data } = this.state;
		let list_id = list_data.list_id;
		let dataList = {
			ln: !obj.ln ? null : Number(obj.ln),
			doctor: Number(obj.doctor == 0 ? null : obj.doctor),
			service_unit: Number(obj.unit),
			service_status: Number(test_id),
			service_at: moment(obj.service_at).format('YYYY-MM-DD HH:mm'),
			service_claim: Number(obj.payType),
			service_price: Number(total) || obj.service_price,
			service_treatment: JSON.stringify(obj.service_treatment)
		};
		let objSym = {
			diagnosis: dataSym.diagnosis,
			problem: dataSym.problem,
			chief_complaint: dataSym.chief_complaint,
			so_examination: dataSym.so_examination,
			physiotherapist: dataSym.physiotherapist,
			goal_of_treatment: dataSym.goal_of_treatment,
			treatment: dataSym.treatment,
			symptom_picture: dataSym.base64
		};
		try {
			this.props.modal();
			this.setState({ wait_loading: true });
			let res = await POST(UPDATE_LIST_BY_FRONT(list_id), dataList);
			await POST(UPDATE_SYMPTOM(list_id), objSym);
			if (res.success) {
				this.setState({ wait_loading: false });

				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 2000
				}).then(async () => {
					window.location.reload();
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
		}
	};
	uploadFiles = async (e) => {
		this.setState({ filepdf: e.target.files[0], checkPDF: true });
	};
	resultPdf = async () => {
		try {
			let { list_id } = this.props;
			let { filepdf } = this.state;
			let formdata = new FormData();
			formdata.append('filepdf', filepdf);
			formdata.append('list_id', list_id);
			let res = await POST(RESULTPDF, formdata, true);
			if (res.success)
				swal(res.massage, ``, `success`).then(async () => {
					await localStorage.removeItem('stateCardList');
					await window.location.reload();
				});
			else swal('อัปโหลดไฟล์ไม่ได้', ``, `error`);
		} catch (error) {
			swal('อัปโหลดไฟล์ไม่ได้', error, `error`);
			// console.log('e')
		}
	};
	delResultList = async () => {
		swal({
			title: 'คุณต้องการลบใช้หรือไม่?',
			text: 'Once deleted, you will not be able to recover this file!',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let { list_id } = this.props;
				let { data_detail } = this.state;
				try {
					let res = await POST('/del_result', { list_id: Number(list_id), result_id: data_detail.result_id });
					if (res.success) {
						swal('สำเร็จ', '', `success`).then(() => window.location.reload());
					} else {
						swal('ลบไม่สำเร็จ', res.massage, `error`);
					}
				} catch (error) {
					swal('ลบไม่สำเร็จ', error, `error`);
				}
			}
		});
	};
	render() {
		let {
			list_data,
			ln,
			doctors,
			result,
			data_detail,
			service_at,
			doctor,
			list_status,
			list_unit,
			unit,
			status,
			company,
			edit,
			result_pdf,
			reported_date,
			received_date,
			sampling_date,
			service_price,
			claim,
			payType,
			type_time,
			wait_loading,
			loading,
			holiday,
			checkPDF,
			filepdf
		} = this.state;
		let { list_id, setEdit, toTal, print, activeTab, report, obj, savestate, save } = this.props;
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : (
			<div className="div-table-program-all">
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Nav tabs id="border-bottom">
					<NavItem>
						<NavLink
							className={classnames({ active: this.state.activeTab === '1' })}
							onClick={() => {
								this.toggle('1');
								activeTab(1);
							}}
							id={this.state.activeTab === '2' ? 'nav-link-left' : 'nav-link-active-right'}
						>
							<Label
								size="lg"
								className="col font-style-color-wht m-0 font-weight-bold"
								style={{ cursor: 'pointer' }}
							>
								รายละเอียด
							</Label>
						</NavLink>
					</NavItem>
					<NavItem>
						<NavLink
							disabled={list_data.type == 'tm' ? true : false}
							className={classnames({ active: this.state.activeTab === '2' })}
							onClick={() => {
								this.toggle('2');
								activeTab(2);
								if (mobx.role == 'pt' || mobx.role == 'frontPT') this.ptBody.scrollToBottom();
							}}
							id={this.state.activeTab === '1' ? 'nav-link-right' : 'nav-link-active-left'}
						>
							<Label
								size="lg"
								className="col font-style-color-wht m-0 font-weight-bold"
								style={{ cursor: 'pointer' }}
							>
								{list_data.type == 'mt' ? 'ผลตรวจ' : 'แบบบันทึก'}
							</Label>
						</NavLink>
					</NavItem>
				</Nav>
				<TabContent activeTab={this.state.activeTab}>
					<TabPane tabId="1">
						<Row>
							<Col sm="12">
								<div className="div-table-program-list p-4">
									<Min>
										<Row>
											<Col xs={6}>
												{list_data.type == 'mt' ? edit == 0 ? (
													<FormGroup row>
														<Label className="label-user-detail" xs={12} size="lg">
															LAB No. {!list_data.ln ? '-' : list_data.ln}
														</Label>
													</FormGroup>
												) : (
													<FormGroup row>
														<Label className="label-user-detail" size="lg" xs={3}>
															LAB No.
														</Label>
														<Col xs={6}>
															<Input
																disabled={edit == 0}
																value={ln}
																type="text"
																placeholder="กรุณาใส่เลข lap number"
																onChange={(e) => {
																	this.setState({ ln: e.target.value }, () => {
																		this.update_LAB();
																	});
																}}
															/>
														</Col>
													</FormGroup>
												) : (
													<FormGroup row>
														<Label className="label-user-detail" xs={12} size="lg">
															LIST Id. {list_data.list_id || '-'}
														</Label>
													</FormGroup>
												)}
											</Col>
											<Col xs={6} className="text-right">
												<FormGroup>
													<Label className="label-user-detail" size="lg">{`${getRoleName(
														list_data.type == 'pt' ? list_data.sub_type : list_data.type
													)} (${getOntimeName(list_data.service_ontime)})`}</Label>
												</FormGroup>
											</Col>
										</Row>
									</Min>
									<Max>
										<Row>
											<Col xs={12} className="text-center">
												{list_data.type == 'mt' ? edit == 0 ? (
													<FormGroup row>
														<Label className="label-user-detail" xs={12} size="lg">
															LAB No. {!list_data.ln ? '-' : list_data.ln}
														</Label>
													</FormGroup>
												) : (
													<FormGroup row>
														<Label className="label-user-detail" size="lg" xs={12}>
															LAB No.
														</Label>
														<Col xs={12} className="d-flex justify-content-center">
															<Input
																className="w-25"
																disabled={edit == 0}
																value={ln}
																type="text"
																onChange={(e) => {
																	this.setState({ ln: e.target.value }, () => {
																		this.update_LAB();
																	});
																}}
															/>
														</Col>
													</FormGroup>
												) : (
													<FormGroup row>
														<Label className="label-user-detail" xs={12} size="lg">
															LIST Id. {list_data.list_id || '-'}
														</Label>
													</FormGroup>
												)}
											</Col>
											<Col xs={12} className="text-center">
												<FormGroup row>
													<Label
														className="label-user-detail"
														xs={12}
														size="lg"
													>{`${getRoleName(
														list_data.type == 'pt' ? list_data.sub_type : list_data.type
													)} (${getOntimeName(list_data.service_ontime)})`}</Label>
												</FormGroup>
											</Col>
										</Row>
									</Max>
									<Row>
										{list_data.type == 'mt' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														วันที่รับบริการ
													</Label>
													<Input
														disabled={edit == 0}
														value={service_at}
														type="datetime-local"
														onChange={(e) => {
															this.setState({ service_at: e.target.value }, () => {
																this.update_LAB();
															});
														}}
													/>
												</FormGroup>
											</Col>
										)}
										{list_data.type !== 'mt' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														วันที่รับบริการ
													</Label>
													<Input
														disabled
														value={service_at}
														type="datetime-local"
														onChange={(e) => {
															this.setState({ service_at: e.target.value }, () => {
																this.update_LAB();
															});
														}}
													/>
												</FormGroup>
											</Col>
										)}
										{list_data.type == 'mt' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														นักเทคนิคการแพทย์
													</Label>
													<Input
														type="select"
														value={doctor}
														disabled={edit == 0}
														onChange={(e) => {
															this.setState({ doctor: e.target.value }, () => {
																this.update_LAB();
															});
														}}
													>
														<option value={0}>-</option>
														{doctors.map((el, i) => (
															<option key={i} value={el.user_id}>
																{`${el.th_name} ${el.th_lastname}`}
															</option>
														))}
													</Input>
												</FormGroup>
											</Col>
										)}
										{list_data.type !== 'mt' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														{list_data.type === 'pt' ? (
															'นักกายภาพบำบัด'
														) : (
															'เจ้าหน้าที่นวดไทย'
														)}
													</Label>
													<Input
														type="select"
														value={doctor}
														disabled
														onChange={(e) => {
															this.setState({ doctor: e.target.value }, () => {
																this.update_LAB();
															});
														}}
													>
														<option value={0}>-</option>
														{doctors.map((el, i) => (
															<option key={i} value={el.user_id}>
																{`${el.th_name} ${el.th_lastname}`}
															</option>
														))}
													</Input>
												</FormGroup>
											</Col>
										)}
										<Col md={4}>
											<FormGroup>
												<Label className="label-user-detail" size="lg">
													สถานะบริการ
												</Label>
												<Input type="select" value={status} disabled>
													{list_status.map((el, i) => (
														<option key={i} value={el.id}>
															{`${el.name}`}
														</option>
													))}
												</Input>
											</FormGroup>
										</Col>
										<Col md={4}>
											<FormGroup>
												<Label className="label-user-detail" size="lg">
													สิทธิ์การรักษา
												</Label>
												<Input
													type="select"
													value={payType}
													disabled
													onChange={(e) => {
														this.setState({ payType: e.target.value }, () => {
															this.update_LAB();
															// if (list_data.type !== 'tm') {
															// 	this.edit.onClick();
															// }
														});
													}}
												>
													{claim.map((el, i) => (
														<option key={i} value={el.service_claim_id}>
															{`${el.claim_name}`}
														</option>
													))}
												</Input>
											</FormGroup>
										</Col>
										<Col md={4}>
											<FormGroup>
												<Label className="label-user-detail" size="lg">
													หน่วยบริการ
												</Label>
												<Input
													type="select"
													value={unit}
													disabled={edit == 0 ? true : unit == 3 ? true : false}
													onChange={(e) => {
														this.setState({ unit: e.target.value }, () => {
															this.update_LAB();
														});
													}}
												>
													{list_unit.map((el, i) => (
														<option key={i} value={el.id} disabled={el.id == 3}>
															{`${el.name}`}
														</option>
													))}
												</Input>
											</FormGroup>
										</Col>
										{list_data.type !== 'mt' &&
										list_data.sub_type == 'ThaiMassage' &&
										edit == 1 &&
										mobx.role == 'frontPT' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														เวลาบริการ
													</Label>
													<Input
														// disabled={edit == 0}
														type="select"
														value={type_time}
														onChange={(e) => this.setState({ type_time: e.target.value })}
													>
														<option value={1}>ในเวลา</option>
														<option value={2}>นอกเวลา</option>
													</Input>
												</FormGroup>
											</Col>
										)}
										{list_data.type !== 'mt' &&
										edit == 1 &&
										list_data.sub_type !== 'ThaiMassage' &&
										mobx.role == 'frontPT' &&
										(holiday == 'เสาร์' || holiday == 'อาทิตย์') ? (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														เวลาบริการ
													</Label>
													<Input disabled type="select">
														<option>ผู้รับบริการนอกเวลา</option>
													</Input>
												</FormGroup>
											</Col>
										) : (
											list_data.type !== 'mt' &&
											edit == 1 &&
											list_data.sub_type !== 'ThaiMassage' &&
											mobx.role == 'frontPT' &&
											(holiday !== 'เสาร์' || !holiday == 'อาทิตย์') && (
												<Col md={4}>
													<FormGroup>
														<Label className="label-user-detail" size="lg">
															เวลาบริการ
														</Label>
														<Input
															// disabled={edit == 0}
															type="select"
															value={type_time}
															onChange={(e) =>
																this.setState({ type_time: e.target.value })}
														>
															<option value={1}>ในเวลา</option>
															<option value={2}>นอกเวลา</option>
														</Input>
													</FormGroup>
												</Col>
											)
										)}
										{list_data.type == 'mt' && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														ประเภทบริการ
													</Label>
													<Input disabled type="select">
														<option>{list_data.service_package}</option>
													</Input>
												</FormGroup>
											</Col>
										)}
										{list_data.company && (
											<Col md={4}>
												<FormGroup>
													<Label className="label-user-detail" size="lg">
														บริษัท
													</Label>
													<Input value={company} disabled />
												</FormGroup>
											</Col>
										)}
									</Row>
									{/** ------------------------------------------ Table ---------------------------------------------- */}
									{typeof list_data.service_treatment !== 'undefined' &&
									list_data.service_treatment.length > 0 &&
									list_data.type == 'pt' &&
									edit == 0 && (
										<Row style={{ fontSize: '1.4rem' }}>
											<Col>
												<Table bordered striped responsive>
													<thead>
														<tr className="text-center">
															<ThMax>
																<th>{LgLabel('รายการตรวจ')}</th>
																<th>{LgLabel('ราคา')}</th>
																<th>{LgLabel('จำนวน')}</th>
																<th>{LgLabel('รวม')}</th>
															</ThMax>
															<ThMin>
																<th>รายการตรวจ</th>
																<th>ราคา (บาท)</th>
																<th>จำนวน (หน่วย)</th>
																<th>ราคารวม (บาท)</th>
															</ThMin>
														</tr>
													</thead>
													<tbody>
														{typeof list_data.service_treatment !== 'undefined' &&
															list_data.service_treatment.map((el, i) => {
																let detail = el.detail && ' ( ' + el.detail + ' )';
																let name_all =
																	el.treatment_name &&
																	el.treatment_name + (detail || '');
																return (
																	<tr key={i.toString()}>
																		<td>
																			<div
																				style={{ width: '100%' }}
																				className="d-flex justify-content-between"
																			>
																				{LgLabel(name_all)}
																				{LgLabel(el.cgd_code)}
																			</div>
																		</td>
																		<td className="text-center">
																			{LgLabel(el.cost / el.amount)}
																		</td>
																		<td className="text-center">
																			{LgLabel(el.amount)}
																		</td>
																		<td className="text-center">
																			{LgLabel(el.cost)}
																		</td>
																	</tr>
																);
															})}
													</tbody>
												</Table>
											</Col>
										</Row>
									)}
									{typeof list_data.service_treatment !== 'undefined' &&
									list_data.service_treatment.length > 0 &&
									list_data.type !== 'pt' &&
									edit == 0 && (
										<Row style={{ fontSize: '1.4rem' }}>
											<Col>
												<Table size="sm" bordered striped responsive>
													<thead>
														<tr className="text-center">
															<ThMax>
																<th>{LgLabel('รายการตรวจ')}</th>
																<th>{LgLabel('ราคา')}</th>
															</ThMax>
															<ThMin>
																<th>รายการตรวจ</th>
																<th>ราคา (บาท)</th>
															</ThMin>
														</tr>
													</thead>
													<tbody>
														{typeof list_data.service_treatment !== 'undefined' &&
															list_data.service_treatment.map((el, i) => {
																let detail = el.detail && ' ( ' + el.detail + ' )';
																let name_all =
																	el.treatment_name &&
																	el.treatment_name + (detail || '');
																return (
																	<tr key={i.toString()}>
																		<td>
																			<div
																				style={{
																					width: '100%',
																					display: 'flex',
																					justifyContent: 'space-between'
																				}}
																			>
																				{LgLabel(
																					(el.treatment_name || '') +
																						' ' +
																						`${el.detail &&
																							`(${el.detail})`}`
																				)}
																				{LgLabel(el.cgd_code)}
																			</div>
																		</td>
																		<td className="text-center">
																			{LgLabel(
																				el.cost && el.cost.toLocaleString()
																			)}
																		</td>
																	</tr>
																);
															})}
													</tbody>
												</Table>
											</Col>
										</Row>
									)}
									{/** ------------------------------------------ Collapse ---------------------------------------------- */}
									{/* <Collapse isOpen={edit}> */}
									{list_data.type !== 'mt' &&
									edit == 1 &&
									mobx.role == 'frontPT' && (
										<Time_docterPT
											save={this.props.save}
											list_id={this.props.list_id}
											onRef={(ref) => (this.Time_docterPT = ref)}
											type_Time={type_time}
											type_Service={
												list_data.sub_type == 'ThaiMassage' ? (
													1
												) : list_data.sub_type == 'Ortho' ? (
													2
												) : list_data.sub_type == 'Neuro' ? (
													3
												) : list_data.sub_type == 'Chest' ? (
													4
												) : (
													5
												)
											}
											holiDay={(e) => this.setState({ holiday: moment(e).format('ddd') })}
											Date_tmpt={(e) =>
												this.setState({ date_tmpt: e }, () => {
													this.update_LAB();
												})}
											Time_tmpt={(e) =>
												this.setState({ time_tmpt: e }, () => {
													this.update_LAB();
												})}
										/>
									)}
									{list_data.isPaid == 0 &&
									list_data.type == 'pt' &&
									edit == 1 &&
									(list_data.sub_type == 'Ortho' ||
										list_data.sub_type == 'Neuro' ||
										list_data.sub_type == 'Chest') && (
										<Edit
											save={this.props.save}
											onRef={(ref) => (this.edit = ref)}
											list_id={list_id}
											service_price={(e) => {
												this.setState({ service_price: e });
												toTal(e);
											}}
											payType={payType}
											service_treatment={(e) => {
												this.setState({ service_treatment: e }, () => {
													this.update_LAB();
												});
											}}
										/>
									)}
									{list_data.isPaid == 0 &&
									list_data.type == 'mt' &&
									edit == 1 && (
										<Edit
											save={this.props.save}
											onRef={(ref) => (this.edit = ref)}
											list_id={list_id}
											service_price={(e) => {
												this.setState({ service_price: e });
												toTal(e);
											}}
											payType={payType}
											service_treatment={(e) => {
												this.setState({ service_treatment: e }, () => {
													this.update_LAB();
												});
											}}
										/>
									)}
									{/* </Collapse> */}
									<Row>
										<Col className="d-flex justify-content-between">
											<FormGroup className="m-0">
												<Label className="label-user-detail" size="lg">
													ค่าบริการ{' '}
													{list_data.type == 'tm' ? (
														list_data.service_price &&
														list_data.service_price.toLocaleString()
													) : edit == 0 || list_data.isPaid == 1 ? list_data.service_price ==
													null ? (
														0
													) : (
														list_data.service_price.toLocaleString()
													) : (
														service_price.toLocaleString()
													)}{' '}
													บาท
												</Label>
											</FormGroup>
											<FormGroup className="m-0">
												<Label className="label-user-detail mr-2" size="lg">
													สถานะ
												</Label>
												<Label
													className="label-user-detail"
													size="lg"
													style={{ color: list_data.isPaid ? 'green' : 'red' }}
												>
													{list_data.isPaid ? `ชำระแล้ว ` : 'ยังไม่ชำระ '}
												</Label>
												{list_data.transfer_slip && (
													<a onClick={null} className="mx-2">{`(ดูสลิปการชำระ)`}</a>
												)}
												<Label size="lg">
													<FaPrint
														className="ml-2"
														style={{
															width: '2rem',
															height: 'auto',
															cursor: 'pointer'
														}}
														onClick={print}
													/>
												</Label>
												{(mobx.role === 'pt' || mobx.role === 'frontPT') &&
												list_data.type === 'pt' && (
													<Label size="lg">
														<TiDocumentText
															className="ml-2"
															style={{
																width: '2rem',
																height: 'auto',
																cursor: 'pointer'
															}}
															onClick={report}
														/>
													</Label>
												)}
											</FormGroup>
										</Col>
									</Row>
									<div id="btnfoot">
										{edit == 0 ? (
											<div
												id="edit-data"
												onClick={() => {
													this.setState({ edit: 1 });
													setEdit(1);
													savestate(false);
													// pdfResult(result_pdf, reported_date, received_date, sampling_date);
												}}
											>
												<FaRegEdit className="ic-btn-blue-subject mr-2" />
												<span>แก้ไขรายการ</span>
											</div>
										) : (
											<div
												id="cancal-data"
												onClick={() => {
													this.setState({ edit: 0 });
													this.fetch();
													setEdit(0);
												}}
											>
												<FaRegEdit className="ic-btn-blue-subject mr-2" />
												<span>ยกเลิก</span>
											</div>
										)}
										{/* {list_data.isPaid ? null : (
											<div
												id="isPaid"
												onClick={this.confirmisPaid}
												className="btn btn-primary button-blue-hover"
											>
												<span style={{ paddingTop: 5 }}>ชำระเงิน</span>
											</div>
										)} */}
									</div>
								</div>
							</Col>
						</Row>
					</TabPane>
					<TabPane tabId="2">
						{list_data.type == 'mt' ? (
							<Row>
								<Col sm="12">
									<div className="div-table-program-list p-4" style={{ fontSize: '1.4rem' }}>
										{list_data.result_pdf && (
											<div className="d-flex justify-content-center">
												<Button
													size="lg"
													color="link"
													className="w-25"
													onClick={() => window.open(GET_RESULTPDF(list_data.result_pdf))}
												>
													<img
														src={pdf}
														style={{
															width: 20,
															height: 20,
															marginRight: '0.5rem'
														}}
													/>{' '}
													คลิกดูผลตรวจ(pdf)
												</Button>
											</div>
										)}
										{!list_data.result_pdf &&
										!checkPDF && (
											<div className="d-flex align-items-center flex-column my-4">
												<div className="mt-3" id="div-solid">
													<label
														className="m-0 p-2"
														style={{ cursor: 'pointer', width: '100%', height: '100%' }}
													>
														<div
															className="d-flex flex-column align-items-center justify-content-center"
															style={{
																height: '100%'
															}}
														>
															<input
																type="file"
																id="uploadfile"
																onChange={this.uploadFiles}
																accept="application/pdf"
															/>
															<img
																src={pdf}
																style={{
																	width: '4rem',
																	height: 'auto',
																	fontWeight: 'bold'
																}}
															/>
															<h5>Upload your files here.</h5>
															{/* <br /> */}
															<h5 className="mt-1">อัพโหลดไฟล์ Pdf ที่นี้!</h5>
														</div>
													</label>
												</div>
											</div>
										)}
										{!list_data.result_pdf &&
										checkPDF && (
											<div className="d-flex align-items-center flex-column my-4">
												<div className="mt-2 p-2" id="div-solid">
													<div className="d-flex justify-content-between mt-2">
														<div>
															<img
																src={pdf}
																style={{
																	width: 20,
																	height: 20,
																	marginRight: '0.5rem'
																}}
															/>
															{filepdf.name}
														</div>
														<div className="ml-5">
															{filepdf.size >= 1024 ? (
																(Number(filepdf.size) / 1024).toLocaleString('th-Th', {
																	minimumFractionDigits: 2,
																	maximumFractionDigits: 2
																}) + ' mb.'
															) : (
																filepdf.size + ' kb.'
															)}{' '}
															<FaTimesCircle
																id="btn-cancal"
																onClick={() =>
																	this.setState({ filepdf: {}, checkPDF: false })}
															/>
														</div>
													</div>
													<div className="d-flex justify-content-center mt-2 mb-2">
														<Button size="lg" color="success" onClick={this.resultPdf}>
															<FaFileUpload id="btn-cancal" /> อัปโหลดไฟล์
														</Button>
													</div>
												</div>
											</div>
										)}
										{result.length > 0 && (
											<div>
												<Row>
													<Col xs={8}>
														<FormGroup>
															<Row className="m-0 p-0">
																<Label style={{ fontSize: '1.6rem' }}>
																	{`HN No. ${data_detail.HN}`}
																	<button
																		className="mx-2"
																		disabled={data_detail.isApprove == 1}
																		onClick={() =>
																			this.Submit(data_detail.result_id)}
																		style={
																			data_detail.isApprove == 1 ? (
																				{
																					color: 'white',
																					border: 'none',
																					borderRadius: '3px',
																					backgroundColor:
																						'rgb(183, 54, 142)',
																					marginLeft: '5px',
																					fontSize: '1.3rem'
																				}
																			) : (
																				{
																					color: 'white',
																					border: 'none',
																					borderRadius: '3px',
																					backgroundColor:
																						'rgb(70, 121, 199)',
																					marginLeft: '5px',
																					fontSize: '1.3rem'
																				}
																			)
																		}
																	>
																		{data_detail.isApprove == 1 ? (
																			'ยืนยันแล้ว'
																		) : (
																			'ยืนยัน'
																		)}
																	</button>
																</Label>
															</Row>
															<Row className="m-0 p-0">
																<Label
																	style={{ fontSize: '1.4rem' }}
																>{`วอร์ด : ${data_detail.Ward}`}</Label>
															</Row>
														</FormGroup>
													</Col>
													<Col xs={4}>
														<FormGroup className="d-flex justify-content-end">
															{data_detail ? (
																<div>
																	<label
																		for="result"
																		className="btn-green-subject"
																		onClick={() =>
																			pdfResult(
																				result_pdf,
																				reported_date,
																				received_date,
																				sampling_date
																			)}
																	>
																		<FaPrint className="ic-btn-green-subject" />
																		พิมพ์ผลตรวจ
																	</label>
																</div>
															) : null}
															{data_detail ? (
																<div>
																	<label
																		for="result"
																		className="btn-red-subject"
																		onClick={() => this.delResultList()}
																	>
																		<FaPrint className="ic-btn-green-subject" />
																		ลบผลตรวจ
																	</label>
																</div>
															) : null}
														</FormGroup>
													</Col>
												</Row>
												<Row style={{ fontSize: '1.4rem' }}>
													<Col md={3}>
														<FormGroup>
															<Label>ผู้เข้ารับการตรวจ</Label>
															<Input disabled value={data_detail.fullname} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>หน่วยบริการ</Label>
															<Input name="Input" disabled value={data_detail.Cname} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>AgeSYear</Label>
															<Input disabled value={data_detail.AgeSYear} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>HeartRate</Label>
															<Input
																name="Input"
																disabled
																value={data_detail.HeartRate}
															/>
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>Weight</Label>
															<Input disabled value={data_detail.Weight} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>Height</Label>
															<Input name="Input" disabled value={data_detail.Height} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>BMI</Label>
															<Input disabled value={data_detail.BMI} />
														</FormGroup>
													</Col>
													<Col md={3}>
														<FormGroup>
															<Label>Systolic</Label>
															<Input name="Input" disabled value={data_detail.Systolic} />
														</FormGroup>
													</Col>
												</Row>
												<Table size="sm" bordered striped>
													<thead>
														<tr className="text-center">
															<th>รายการตรวจ</th>
															<th>ผลตรวจ</th>
														</tr>
													</thead>
													<tbody>
														{typeof this.state.header !== 'undefined' ? (
															this.state.header.map((el, i) => (
																<tr key={i.toString()}>
																	<th>{el}</th>
																	<th>{this.state.new_result[el]}</th>
																</tr>
															))
														) : null}
													</tbody>
												</Table>
											</div>
										)}
									</div>
								</Col>
							</Row>
						) : (
							<div className="div-table-program-list p-4" style={{ fontSize: '1.4rem' }}>
								<Row>
									<Col lg="12">
										<PT_body
											onRef={(ref) => (this.ptBody = ref)}
											pt="pt"
											type_role="pt"
											list_id={this.props.list_id}
											id={this.props.id}
											checkedit={(e) => this.setState({ checkedit: e })}
											obj={obj}
											card={true}
											updateSymptomTwo={(e) => this.updateSymptomTwo(e)}
											save={save}
											savestate={(e) => savestate(e)}
										/>
									</Col>
								</Row>
							</div>
						)}
					</TabPane>
				</TabContent>
			</div>
		);
	}
}

export default CardList;
