import React, { Component } from 'react';
import { GET, GET_TREATMENT, GET_LIST_BY_ID } from '../../../service/service';
import Col_left from '../../../pages/front/Home/AddCustomer/Col_left';
import { createBrowserHistory } from 'history';
import moment from 'moment';
import swal from 'sweetalert';
import User from '../../../mobx/user/user';
import fn from '../../../function/function';
import mobx from '../../../mobx/user/user';

export default class EditTeartment extends Component {
	constructor(props) {
		super(props);

		this.state = {
			_package: [],
			treatment: [],
			treatmentData: [],
			treatmentDataClick: [],
			curServiceType: '',
			tmArray: [],
			pricenew: 0,
			newPackage: [],
			oldPackage: [],
			newTreatment: [],
			discount: 0,
			list_by_id: [],
			type_service: 2
		};
	}

	async componentWillMount() {
		let USER = JSON.parse(localStorage.getItem('USER'));
		let { list_id } = this.props;
		this.props.onRef(this);
		let list = await GET(GET_LIST_BY_ID(list_id));
		// console.log('list', list);
		let newPacKage = list.result[0].service_treatment.filter((e) => !e.id);
		await setTimeout(async () => {
			let state = mobx.stateEditTeartment;
			if (state) {
				// console.log('state.curServiceType', state.tmArray);
				if (state.list_id == this.props.list_id && this.props.save == false) {
					await this.setState(state);
					// await this.GetTreatment();
					// console.log('stateEditTeartment', state);
					await this.GetTreatment();
				} else {
					await this.setState({
						tmArray: list.result[0].service_treatment.map((e) => e.id),
						// tmArray: [ 86, 87 ],
						curServiceType: USER.role == 'frontMT' || USER.role == 'mt' ? 'mt' : 'pt',
						list_by_id: list.result[0],
						type_service: list.result[0].type === 'tm' ? 1 : 2,
						oldPackage: newPacKage
					});
					await this.GetTreatment();
				}
			} else {
				await this.setState({
					tmArray: list.result[0].service_treatment.map((e) => e.id),
					// tmArray: [ 86, 87 ],
					curServiceType: USER.role == 'frontMT' || USER.role == 'mt' ? 'mt' : 'pt',
					list_by_id: list.result[0],
					type_service: list.result[0].type === 'tm' ? 1 : 2,
					oldPackage: newPacKage
				});
				await this.GetTreatment();
				this.state['list_id'] = this.props.list_id;
				await mobx.editTeartment(this.state);
			}
		}, 0.01);
	}
	async componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		// console.log('this.state', this.state);
		await mobx.editTeartment(this.state);
	}
	async GetTreatment() {
		try {
			let { list_by_id, tmArray, curServiceType } = this.state;
			let res = await GET(GET_TREATMENT(curServiceType));
			let d = [];
			let cur = [];
			let curPrice = 0;
			res.result
				.map((e) => ({ ...e, total: e.cost, amount: e.amount }))
				// .filter((e) => e.type === curServiceType)
				.forEach((e, index) => {
					if (this.state.curServiceType === e.type) {
						d.push([
							e.treatment_name,
							e.cost,
							Number(e.cost) - Number(e.disburseable),
							<input
								key={'in' + index}
								id={'' + index}
								onClick={() => this.onClick(index)}
								onChange={(e) => console.log(e.target.value)}
								checked={false}
								type="checkbox"
							/>,
							e.detail,
							e.cgd_code,
							Number(e.amount),
							Number(e.total),
							e.treatment_name_mobile
						]);
						cur.push(false);
					}
				});
			let t = res.result.map((e) => {
				let list = list_by_id.service_treatment;
				let TF = list.some((el) => el.id === e.id);
				let amount = TF ? list.filter((el) => el.id === e.id)[0].amount : e.amount;
				let cost = TF ? list.filter((el) => el.id === e.id)[0].cost : e.cost;
				return {
					amount: amount,
					cgd_code: e.cgd_code,
					cost: cost,
					detail: e.detail,
					disburseable: e.disburseable,
					id: e.id,
					pay_neuro: e.pay_neuro,
					pay_ortho: e.pay_ortho,
					total: e.cost,
					treatment_name: e.treatment_name,
					type: e.type,
					treatment_name_mobile: e.treatment_name_mobile
				};
			});
			// console.log('t', t);
			this.setState(
				{
					old_treatment: d,
					treatment: d,
					treatmentData: t,
					treatmentDataClick: t,
					curCheck: cur,
					servicePrice: curPrice
				},
				() => {
					this.selectPackage();
				}
			);
		} catch (error) {
			// console.log('error GetTreatment : ', error)
		}
	}

	onClick = (id) => {
		let { list_by_id, tmArray, treatmentDataClick, treatmentData } = this.state;
		let cur = this.state.curCheck;
		let { payType } = this.props;
		let curPrice = 0;
		let discounts = 0;
		// console.log('treatmentData', treatmentData);
		cur[id] = !cur[id];
		let d = [];
		treatmentData.forEach((e, i) => {
			if (this.state.curServiceType === e.type) {
				d.push([
					e.treatment_name,
					e.cost,
					Number(e.cost) - Number(e.disburseable),
					<input
						key={'in' + i}
						id={'' + i}
						onClick={() => this.onClick(i)}
						value={e.treatment_name}
						onChange={(ei) => console.log(ei.target.value)}
						checked={cur[i]}
						type="checkbox"
					/>,
					e.detail,
					e.cgd_code,
					Number(e.amount),
					Number(e.total),
					e.treatment_name_mobile
				]);
				if (cur[i]) discounts += +e.cost;
				// if (payType < 2) {
				if (cur[i]) curPrice += +e.cost;
				// } else {
				// 	if (cur[i]) curPrice += +(Number(cost) - Number(e.disburseable));
				// }
			}
		});

		// console.log('d', d);
		this.setState(
			{
				old_treatment: d,
				treatment: d,
				curCheck: cur,
				servicePrice: curPrice,
				discount: discounts,
				newTreatment: []
			},
			() => {
				this.total();
			}
		);
	};
	selectPackage = () => {
		let { curServiceType, list_by_id, tmArray, treatmentDataClick, treatmentData } = this.state;
		let cur = [];
		let curPrice = 0;
		let discounts = 0;
		let { payType } = this.props;
		let d = [];
		treatmentData.forEach((e, index) => {
			let cost = tmArray.some((el) => el === e.id)
				? list_by_id.service_treatment.filter((el) => el.id === e.id)[0].cost
				: e.cost;
			let amount =
				User.role === 'frontPT' || User.role === 'pt'
					? tmArray.some((el) => el === e.id)
						? list_by_id.service_treatment.filter((el) => el.id === e.id)[0].amount
						: e.amount
					: 0;
			if (curServiceType === e.type) {
				// console.log('tmArray.indexOf(index + 1)', tmArray.indexOf());
				d.push([
					e.treatment_name,
					cost,
					Number(e.cost) - Number(e.disburseable),
					<input
						value={''}
						key={'in' + index}
						id={'' + index}
						onClick={() => this.onClick(index)}
						onChange={(ei) => console.log(ei.target.value)}
						checked={tmArray.some((el) => el === e.id) ? true : false}
						type="checkbox"
					/>,
					e.detail,
					e.cgd_code,
					amount,
					Number(e.total),
					e.treatment_name_mobile
				]);
			}
			if (tmArray.some((el) => el === e.id)) {
				discounts += +cost;
			}
			// if (payType < 2) {
			if (tmArray.some((el) => el === e.id)) {
				cur.push(true);
				curPrice += +cost;
			} else {
				cur.push(false);
			}
			// } else {
			// 	if (tmArray.indexOf(index + 1) !== -1) {
			// 		cur.push(true);
			// 		curPrice += +(Number(cost) - Number(e.disburseable));
			// 	} else {
			// 		cur.push(false);
			// 	}
			// }
		});
		// console.log('d', d);
		this.setState(
			{
				old_treatment: d,
				treatment: d,
				curCheck: cur,
				servicePrice: curPrice,
				discount: discounts,
				newTreatment: []
			},
			() => {
				this.total();
			}
		);
	};
	total = () => {
		let { pricenew, servicePrice, curCheck, discount, list_by_id, tmArray } = this.state;
		let service_treatment = [];
		curCheck.forEach((e, index) => {
			// console.log('index', index);
			if (e) service_treatment.push(index);
		});
		// console.log('service_treatment', service_treatment);
		// console.log('list_by_id', list_by_id);
		let d = [];
		service_treatment.forEach((e, i) => {
			this.state.treatmentData.filter((el, index) => {
				if (e === index) {
					let { newTreatment } = this.state;
					let { payType } = this.props;
					let rate = list_by_id.sub_type === 'Neuro' ? el.pay_neuro / 100 : el.pay_ortho / 100;
					let pay_price = el.cost * rate;
					// console.log('el.cost', el.cost, 'rate', rate, 'price', el.cost * rate);
					// let pay_price = el.cost
					let obj =
						User.role === 'frontPT' || User.role === 'pt'
							? {
									id: el.id,
									treatment_name: el.treatment_name,
									cost: el.cost,
									disburseable: el.disburseable,
									detail: el.detail,
									cgd_code: el.cgd_code,
									type: el.type,
									amount: el.amount,
									percent: list_by_id.sub_type === 'Neuro' ? el.pay_neuro : el.pay_ortho,
									pay_price: pay_price
								}
							: {
									id: el.id,
									treatment_name: el.treatment_name,
									cost: el.cost,
									disburseable: el.disburseable,
									detail: el.detail,
									cgd_code: el.cgd_code,
									type: el.type
								};
					newTreatment.push(obj);
					d.push(obj);
					this.setState({ newTreatment: newTreatment, list_by_id });
				}
			});
		});
		list_by_id.service_treatment = d;
		tmArray = list_by_id.service_treatment.map((e) => e.id);
		this.setState({ list_by_id, tmArray });
		let { newTreatment, newPackage, oldPackage } = this.state;
		// console.log('newTreatment', newTreatment);
		let priceold = oldPackage.reduce(fn.add('cost'), 0);
		this.props.service_treatment(newTreatment.concat(oldPackage).concat(newPackage));
		this.props.service_price(pricenew + servicePrice + priceold);
	};
	Searchtext = () => {
		let texts = this.state.searchtext.toLowerCase();
		let copyTreatment = this.state.old_treatment;
		let data = copyTreatment.filter((e) => e[0].toLowerCase().match(texts));
		this.setState({ treatment: data });
	};
	delOldpackage = (i) => {
		let { oldPackage } = this.state;
		oldPackage.splice(i, 1);
		this.setState({ oldPackage: oldPackage }, () => this.total());
	};
	render() {
		let { _package, curServiceType, treatment, treatmentData, oldPackage, type_service } = this.state;
		let { payType, save, list_id } = this.props;
		return (
			<Col_left
				searchtext={(e) => this.setState({ searchtext: e.target.value }, () => this.Searchtext())}
				// setPackage={(index) => this.selectPackage(index)}
				_package={_package}
				curServiceType={curServiceType}
				treatment={treatment}
				treatmentData={treatmentData}
				setNewTreatment={(e) => {
					this.setState({ treatment: e });
					// this.onClick();
				}}
				settreatmentData={(e) => {
					// console.log('e', e);
					this.setState({ treatmentData: e, treatmentDataClick: e });
					this.onClick();
				}}
				priceAll={(e) =>
					this.setState({ pricenew: e, newTreatment: [] }, () => {
						this.total();
					})}
				newPackage={(e) => this.setState({ newPackage: e })}
				payType={payType}
				type_Service={type_service}
				oldPackage={oldPackage}
				delOldpackage={(e) => this.delOldpackage(e)}
				save={save}
				list_id={list_id}
			/>
		);
	}
}
