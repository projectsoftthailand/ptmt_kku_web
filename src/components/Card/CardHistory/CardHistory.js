import React, { Component } from 'react';
import './Styled/style.css';
import { Table, Label } from 'reactstrap';
import { GET, GET_LIST_BY_ID } from '../../../service/service';
import { sevenDigits } from '../../../function/function';
import Pagination from '../../Pagination';
import moment from 'moment';
import ReactLoading from 'react-loading';
import Color from '../../Color';

// let ListCustomerHistory = [];
// let ListCustomerHistoryRow = [];
let numberPage = [ 3, 6, 9, 12, 15, 25, 35, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
let head = [ 'ว/ด/ป', 'เวลา', 'รหัสพนักงาน', 'ชื่อ-นามสกุล', 'การแก้ไข' ];

class CardHistory extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			currentPage: 1,
			pageSize: 3,
			historyData: [],
			historyDataRow: []
		};
	}

	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		await this.getUserHistory();
		await this.setState({ loading: false });
	}

	async getUserHistory() {
		let { id, list_id } = this.props;
		let { historyData, historyDataRow } = this.state;

		try {
			let res = await GET(GET_LIST_BY_ID(list_id));

			if (res.success) {
				this.setState({ historyData: res.result[0].service_log });

				// console.log(id, res.result[0].service_log ? res.result[0].service_log : "ไม่พบประวัติการแก้ไข")
				// console.log(id, res.result, res.result[0].service_log)

				let rows = [];
				res.result[0].service_log.forEach((e) => {
					let date = moment(e.updated_at).locale('th').format('DD/MM/');
					let year = (Number(moment(e.updated_at).locale('th').format('YYYY')) + 543).toString();
					// date = date.split('-')
					// date = date[2] + '/' + date[1] + '/' + date[0]
					let time = moment(e.updated_at).locale('th').format('HH:mm:ss');
					// console.log(date + year, time, res.result[0].service_log)
					rows.push([ date + year, time, sevenDigits(e.user_id), e.th_name + ' ' + e.th_lastname, e.event ]);
				});
				// console.log(rows)
				this.setState({ historyData: rows, historyDataRow: rows });
				// ListCustomerHistoryRow = rows;
				// console.log(historyData, historyDataRow)
				// .map(el => el.service_log) !== "null" ? res.result.map(el => el.service_log) : "no")
			}
		} catch (err) {}
	}

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { id } = this.props;
		let { currentPage, pageSize, historyData, historyDataRow, loading } = this.state;
		// let pagesCount = Math.ceil(historyDataShow.length / pageSize);

		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : (
			<div className="div-history-table-mt pb-4">
				{/* <label className="label-history-subject-mt">ประวัติการแก้ไข</label> */}
				{historyDataRow.length !== 0 ? (
					<div>
						<Table style={{ whiteSpace: 'pre' }} responsive>
							{head && (
								<thead>
									<tr>{head.map((el, index) => <th key={'a' + index}>{el}</th>)}</tr>
								</thead>
							)}
							<tbody>
								{historyDataRow
									.slice((currentPage - 1) * pageSize, currentPage * pageSize)
									.map((row, index) => (
										<tr key={'b' + index}>
											{row.map((d, index) => {
												return (
													<td
														key={'c' + index}
														style={{ backgroundColor: '#fff', paddingTop: '0.6rem' }}
													>
														{d}
													</td>
												);
											})}
										</tr>
									))}
							</tbody>
						</Table>

						<div className="d-flex justify-content-between">
							<select
								className="select-page-size"
								name="pageSize"
								onChange={async (e) => {
									await this.setState({ [e.target.name]: +e.target.value });
								}}
							>
								{numberPage.map((el, i) => (
									<option key={i} value={el}>
										{el}
									</option>
								))}
							</select>
							<Pagination
								totalRecords={historyDataRow.length}
								pageLimit={pageSize}
								pageNeighbours={2}
								onPageChanged={this.onPageChanged}
							/>
						</div>
					</div>
				) : (
					<div className="EmptyListShow">---ไม่พบประวัติการแก้ไข---</div>
				)}
			</div>
		);
	}
}
export default CardHistory;
