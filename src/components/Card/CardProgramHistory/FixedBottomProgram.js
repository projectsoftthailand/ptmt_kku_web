import React from 'react';
import Color from '../../Color';
import { FaRegSave } from 'react-icons/fa';

export default function({ isSave, isFinish, isValid }) {
	return (
		<div
			style={{
				position: 'fixed',
				bottom: 0,
				left: 0,
				backgroundColor: 'white',
				textAlign: 'right',
				width: '100%',
				zIndex: 99,
				boxShadow: '0px -1px 1px #ccc'
			}}
		>
			<div
				style={{
					backgroundColor: Color.Blue,
					color: 'white',
					padding: '6px 8px',
					borderColor: 'transparent',
					width: 150
				}}
				onClick={isSave}
				className="btn btn-primary button-blue-hover"
			>
				<FaRegSave size={15} />
				<span style={{ paddingTop: 2, paddingLeft: 5 }}>บันทึก</span>
			</div>
			{/* <div
        style={{
          backgroundColor: isValid ? "#7bc8c6" : "#BDBDBD",
          color: "white",
          padding: "6px 8px",
          borderColor: "transparent",
          width: 150
        }}
        onClick={isFinish}
        className={isValid ? "btn button-green-hover" : "btn button-gray-hover"}
      >
        <span style={{ paddingTop: 2, paddingLeft: 5 }}>เสร็จสิ้น</span>
      </div> */}
		</div>
	);
}
