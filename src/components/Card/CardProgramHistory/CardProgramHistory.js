import React, { Component } from 'react';
import CardList from '../CardList/CardList';
// import CardProgram from "../CardProgram/CardProgram";
import { Route, withRouter } from 'react-router-dom';
// import CardHistory from '../CardHistory/CardHistory';
import CardTableDetail from '../CardTableDetail/CardTableDetail';
import FixedBottomProgram from './FixedBottomProgram';
import swal from 'sweetalert';
import CardLocation from '../CardLocation/CardLocation';
import {
	POST,
	UPDATE_LIST_BY_FRONT,
	GET_STATUS,
	GET,
	GET_LIST_BY_ID,
	CREATE_BILL,
	LIST_MONTH,
	UPDATE_SYMPTOM
} from '../../../service/service';
import { billYearly, billPT, billTM, PdfReport, billMT } from '../../../function/PDF';
import Modal from 'react-modal';
import { Input, FormGroup, Label, Col, Form, CustomInput } from 'reactstrap';
import { IoMdClose } from 'react-icons/io';
import { Animated } from 'react-animated-css';
import ReactLoading from 'react-loading';
import moment from 'moment';
import User from '../../../mobx/user/user';
import Responsive from 'react-responsive';
import { arrYearly } from '../../../const/subRole';
import { months } from '../../../const/subRole';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

var start = 2019;
var end = new Date().getFullYear();
var Years = [];
for (var i = start; i <= end; i++) {
	Years.push({ year: i, value: i });
}

let Max = (props) => <Responsive {...props} maxWidth={450} />;
let Min = (props) => <Responsive {...props} minWidth={451} />;

class CardProgramHistory extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isValid: false,
			list_data: {},
			obj: {},
			modal: false,
			AllStatusToChange: [],
			test_name: '',
			test_id: '',
			service_status: '',
			edit: 0,
			total: 0,
			address: '',
			lats: 0,
			lngs: 0,
			treatment: [],
			isPaid: -1,
			detailList: {},
			treatmentData: {},
			dis: 0,
			type: '',
			price: 0,
			wait_loading: false,
			activeTab: 1,

			report: false,
			arrDate: [],
			arrList: [],
			newArrList: [],
			month: moment().month() + 1,
			year: moment().year(),
			sub_type: 'Ortho',

			save: false
		};
	}
	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		await setTimeout(async () => {
			let state = await JSON.parse(localStorage.getItem('stateCardProgramHistory'));
			if (state) {
				if (state.list_id == this.props.list_id && this.state.save == false) {
					await this.setState(state);
				}
			} else {
				this.state['list_id'] = this.props.list_id;
				localStorage.setItem('stateCardProgramHistory', JSON.stringify(this.state));
			}
		}, 0.01);
		await Promise.all([ this.GetStatusToChange(), this.GetLocation() ]);
	}
	componentWillUnmount() {
		this.state['list_id'] = this.props.list_id;
		localStorage.setItem('stateCardProgramHistory', JSON.stringify(this.state));
	}
	GetLocation = async () => {
		let { id, list_id, unit, status } = this.props;
		try {
			let res = await GET(GET_LIST_BY_ID(list_id));
			if (res.success) {
				this.setState({
					address: res.result[0].location_address,
					lats: res.result[0].latitude,
					lngs: res.result[0].longitude,
					treatmentData: res.result[0],
					discount: res.result[0].service_treatment.forEach((element) => {
						let { dis } = this.state;
						let a = dis + Number(element.cost);
						this.setState({ dis: a });
					})
				});
			} else {
				swal('ผิดพลาด!', res.message, 'warning', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (e) {}
	};
	GetStatusToChange = async () => {
		try {
			let res = await GET(GET_STATUS);
			let dump = res.result;
			this.setState({ AllStatusToChange: dump });
		} catch (err) {}
	};

	isFinish = () => {
		let { isValid } = this.state;
		if (isValid) {
			swal('สำเร็จ!', 'บันทึกข้อมูลผลตรวจเรียบร้อย', 'success', {
				buttons: false,
				timer: 2000
			});
		} else {
			swal('ผิดพลาด!', 'ไม่สามารถบันทึกข้อมูลผลตรวจได้', 'error', {
				buttons: false,
				timer: 2000
			});
		}
	};

	handleChangeValid = (event) => {
		this.setState({ isValid: event.target.checked });
	};
	submitChange = async () => {
		let bill = this.cardList.stateBill();
		let a = localStorage.getItem('USER');
		let front = JSON.parse(a);
		let { obj, test_id, total, treatment } = this.state;
		let { list_id } = this.props;
		let data = {
			ln: !obj.ln ? null : Number(obj.ln),
			doctor: Number(obj.doctor == 0 ? null : obj.doctor),
			service_unit: Number(obj.unit),
			service_status: Number(test_id),
			service_at: moment(obj.service_at).format('YYYY-MM-DD HH:mm'),
			service_claim: Number(obj.payType),
			service_price: Number(total) || obj.service_price,
			service_treatment: JSON.stringify(obj.service_treatment)
		};
		try {
			this.setState({ modal: false });
			this.setState({ wait_loading: true });
			let res = await POST(UPDATE_LIST_BY_FRONT(list_id), data);
			// let text_treatment =
			// 	obj.service_treatment.length > 0
			// 		? obj.service_treatment
			// 				.map((e) => e.treatment_name + ' ' + (e.detail ? `(${e.detail})` : ''))
			// 				.toString()
			// 		: null;
			// await POST(UPDATE_SYMPTOM(list_id), {
			// 	treatment: text_treatment
			// });
			if (res.success) {
				if (bill.isPaid === 0 && data.service_status === 5) {
					if (
						bill.service_claim_id === 2 &&
						(bill.service_package === 'Standard (> 35 ปี)' || bill.service_package === 'Standard (< 35 ปี)')
					) {
						let checks = bill.service_treatment.filter(
							(el) => !arrYearly.some((e) => e.find === el.treatment_name)
						);
						let claim_type = checks.length > 0 ? 1 : 0;
						await POST(CREATE_BILL, {
							id: bill.list_id,
							type: bill.type,
							claim: bill.service_claim_id,
							claim_type
						});
					} else {
						await POST(CREATE_BILL, {
							id: bill.list_id,
							type: bill.type,
							claim: 1
						});
					}
				}
				this.setState({ wait_loading: false, save: true });
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 2000
				}).then(async () => {
					this.state['list_id'] = this.props.list_id;
					localStorage.setItem('stateCardProgramHistory', JSON.stringify(this.state));
					await window.location.reload();
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
		}
	};
	handleChange(eventId, eventName) {
		this.setState({
			test_name: eventName,
			test_id: eventId
		});
	}
	print = () => {
		let { treatmentData, dis } = this.state;
		let role = User.role;
		let prefix = User.th_prefix === 'นาย' ? 'นาย' : User.th_prefix === 'นาง' ? 'นาง' : 'นางสาว';
		let fullname = prefix + User.name + ' ' + User.lastname;
		let namelast = User.name + ' ' + User.lastname;
		if (role === 'mt' || role === 'frontMT') {
			if (
				treatmentData.service_claim_id === 2 &&
				(treatmentData.service_package === 'Standard (> 35 ปี)' ||
					treatmentData.service_package === 'Standard (< 35 ปี)')
			) {
				billYearly(treatmentData, fullname);
			} else {
				billMT(treatmentData, fullname);
			}
		} else {
			if (treatmentData.type === 'pt') {
				billPT(dis, treatmentData, fullname, namelast);
			}
			if (treatmentData.type === 'tm') {
				if (treatmentData.service_claim > 1) {
					billTM(treatmentData.service_price, treatmentData, fullname, namelast);
				} else {
					billTM(dis, treatmentData, fullname, namelast);
				}
			}
		}
	};
	report = async () => {
		let { year, month, sub_type } = this.state;
		let obj = {
			id: this.props.id,
			month: month,
			year: year,
			sub_type: sub_type
		};
		try {
			let res = await POST(LIST_MONTH, obj);
			if (res.success) {
				this.setState(
					{ arrDate: res.result.map((e) => moment(e.service_at).date()), arrList: res.result },
					() => this.setState({ report: true })
				);
			} else {
				this.setState({ arrDate: [], arrList: [] }, () => this.setState({ report: true }));
			}
		} catch (error) {}
	};
	saveDate = (e) => {
		let date = Number(e);
		let { arrList, newArrList } = this.state;
		const index = newArrList.find((e) => moment(e.service_at).date() === date);
		if (!index) {
			let obj = arrList.find((e) => moment(e.service_at).date() === date);
			newArrList.push(obj);
			this.setState({ newArrList });
		} else {
			let i = newArrList.findIndex((e) => moment(e.service_at).date() === date);
			newArrList.splice(i, 1);
			this.setState({ newArrList });
		}
	};
	printReport = () => {
		let detail = this.cardTableDetail.getDetail();
		let { newArrList, month, year, sub_type } = this.state;
		let obj = {
			year,
			month: months.find((e) => Number(e.type) === Number(month)).name,
			sub_type
		};
		PdfReport(newArrList, detail, obj);
	};
	render() {
		let { id, list_id, unit, status } = this.props;
		let {
			isValid,
			list_data,
			modal,
			AllStatusToChange,
			test_name,
			service_status,
			edit,
			total,
			isPaid,
			dis,
			type,
			price,
			wait_loading,
			obj,
			test_id,
			report,
			arrDate,
			month,
			year,
			sub_type,
			newArrList,
			save
		} = this.state;
		// let new_list_id = sevenDigits(list_id);
		// console.log('discount', dis);

		return (
			<div className="column w-100">
				<div className="d-flex row mx-0 w-100">
					<div className="col-xs-12 w-100 h-100 p-0">
						<Max>
							<div className="col-xs-12 p-0">
								<CardTableDetail
									onRef={(ref) => (this.cardTableDetail = ref)}
									id={id}
									list_id={list_id}
									unit={unit}
									status={status}
								/>
							</div>
						</Max>
						<Min>
							<div className="col-xs-12">
								<CardTableDetail
									onRef={(ref) => (this.cardTableDetail = ref)}
									id={id}
									list_id={list_id}
									unit={unit}
									status={status}
								/>
							</div>
						</Min>

						{/* <div className="col-xs-5">
							<CardHistory id={id} list_id={list_id} />
						</div> */}
						{/* {unit === 3 ? (
							<CardLocation
								id={id}
								list_id={list_id}
								unit={unit}
								status={status}
								lats={this.state.lats}
								lngs={this.state.lngs}
								address={this.state.address}
							/>
						) : (
							<div />
						)} */}
					</div>
					<div className="col-xs-12 w-100 h-100 p-0">
						{/* <CardProgram id={id} list_id={list_id} /> */}
						<Max>
							<div className="col-xs-12 p-0">
								<CardList
									onRef={(ref) => (this.cardList = ref)}
									id={id}
									list_id={list_id}
									save={save}
									update_LAB={(e) => this.setState({ obj: e })}
									setStatus={(e) => this.setState({ service_status: e, test_id: e })}
									setEdit={(e) => this.setState({ edit: e })}
									toTal={(e) => this.setState({ total: e })}
									length_treatment={(e) => this.setState({ treatment: e })}
									setisPaid={(e) => this.setState({ isPaid: e })}
									print={this.print}
									setType={(e) => this.setState({ type: e })}
									setPrice={(e) => this.setState({ price: e })}
									activeTab={(e) => this.setState({ activeTab: e })}
									obj={obj}
									test_id={test_id}
									total={total}
									modal={() => this.setState({ modal: false })}
									report={this.report}
									savestate={(e) => this.setState({ save: e })}
								/>
							</div>
						</Max>
						<Min>
							<div className="col-xs-12">
								<CardList
									onRef={(ref) => (this.cardList = ref)}
									id={id}
									list_id={list_id}
									save={save}
									update_LAB={(e) => this.setState({ obj: e })}
									setStatus={(e) => this.setState({ service_status: e, test_id: e })}
									setEdit={(e) => this.setState({ edit: e })}
									toTal={(e) => this.setState({ total: e })}
									length_treatment={(e) => this.setState({ treatment: e })}
									setisPaid={(e) => this.setState({ isPaid: e })}
									print={this.print}
									setType={(e) => this.setState({ type: e })}
									setPrice={(e) => this.setState({ price: e })}
									activeTab={(e) => this.setState({ activeTab: e })}
									obj={obj}
									test_id={test_id}
									total={total}
									modal={() => this.setState({ modal: false })}
									report={this.report}
									savestate={(e) => this.setState({ save: e })}
								/>
							</div>
						</Min>
					</div>
				</div>
				<FixedBottomProgram
					isSave={() => this.setState({ modal: true })}
					isFinish={this.isFinish}
					isValid={isValid}
				/>
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Modal
					isOpen={modal}
					onRequestClose={() => this.setState({ modal: false, test_name: '', test_id: service_status })}
					style={customStyles}
				>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<div className="mx-auto text-center div-modal-change-status">
							<div className="label-modal-change-status">เปลี่ยนสถานะ</div>

							<div
								className="btn-exit-hover button-close-modal-change-status"
								style={{ cursor: 'pointer' }}
								onClick={() =>
									this.setState({ test: '', test_name: '', modal: false, test_id: service_status })}
							>
								<IoMdClose color="white" size={18} />
							</div>

							<div
								className="pt-4 pb-4 mx-auto"
								style={{
									width: '50%',
									display: 'flex',
									flexDirection: 'column',
									alignItems: 'flex-start',
									justifyContent: 'center'
								}}
							>
								{AllStatusToChange.map((el, index) => {
									return (
										<FormGroup check key={index}>
											<Label check className="label-modal-status-radio">
												<Input
													disabled={User.role == 'pt' && el.id == 5}
													type="radio"
													name="radio2"
													style={{ marginLeft: '-1.5vw' }}
													value={el.name}
													checked={
														test_name ? test_name === el.name : service_status === el.id
													}
													onChange={() => this.handleChange(el.id, el.name)}
												/>
												<text style={{ paddingLeft: '1rem' }}>{el.name}</text>
											</Label>
										</FormGroup>
									);
								})}
							</div>
							{edit == 1 &&
							isPaid == 0 && (
								<div>
									<text style={{ fontSize: '1.5rem', fontWeight: 'bold' }}>
										ยอดที่ต้องชำระ {type == 'tm' ? (
											price.toLocaleString()
										) : (
											total.toLocaleString()
										)}{' '}
										บาท
									</text>
								</div>
							)}
							<hr style={{ width: '60%' }} />
							<button
								onClick={() => {
									let { activeTab } = this.state;
									if (Number(activeTab) == 1) {
										this.submitChange();
									} else {
										this.setState({ save: true });
										this.cardList.updateSymptom();
									}
								}}
								className="btn-form"
							>
								บันทึก
							</button>
						</div>
					</Animated>
				</Modal>
				<Modal isOpen={report} style={customStyles} onRequestClose={() => this.setState({ report: false })}>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<div className="mx-auto text-center div-modal-change-status">
							<div className="label-modal-change-status">ออกใบคิดค่ารักษา</div>
							<div
								className="btn-exit-hover button-close-modal-change-status"
								style={{ cursor: 'pointer' }}
								onClick={() => this.setState({ report: false })}
							>
								<IoMdClose color="white" size={18} />
							</div>
							<div className="container mt-3">
								<FormGroup row>
									<Label sm={2}>ประเภท</Label>
									<Col sm={10}>
										<Input
											bsSize="lg"
											value={sub_type}
											onChange={(e) =>
												this.setState({ sub_type: e.target.value }, () => this.report())}
											type="select"
										>
											<option value="Ortho">กระดูกและกล้ามเนื้อ</option>
											<option value="Neuro">ระบบประสาท</option>
										</Input>
									</Col>
								</FormGroup>
								<FormGroup row>
									<Label sm={2}>ปี</Label>
									<Col sm={10}>
										<Input
											bsSize="lg"
											value={year}
											onChange={(e) =>
												this.setState({ year: e.target.value }, () => this.report())}
											type="select"
										>
											{Years.map((e, index) => (
												<option key={index.toString()} value={e.value}>
													{Number(e.year + 543)}
												</option>
											))}
										</Input>
									</Col>
								</FormGroup>
								<FormGroup row>
									<Label sm={2}>เดือน</Label>
									<Col sm={10}>
										<Input
											bsSize="lg"
											value={month}
											onChange={(e) =>
												this.setState({ month: e.target.value }, () => this.report())}
											type="select"
										>
											{months.map((e, index) => (
												<option key={index.toString()} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Col>
								</FormGroup>
								<FormGroup row>
									<Label className="mt-2" xs="12">
										เลือกวันที่ (เลือกได้สูงสุด 7 วัน)
									</Label>
								</FormGroup>
								<div className="row mb-5">
									{arrDate.length > 0 ? (
										arrDate.map((e, i) => (
											<Col className="text-left" xs={4}>
												<FormGroup key={i} check>
													<Input
														type="checkbox"
														name={e}
														id={e}
														onClick={() => this.saveDate(e)}
														checked={newArrList.some(
															(el) => Number(moment(el.service_at).date()) === Number(e)
														)}
													/>
													<Label className="ml-4" check>
														วันที่ {e}
													</Label>
												</FormGroup>
											</Col>
										))
									) : (
										<Label className="mt-2" xs="12">
											--- ไม่มีวันรับการรักษา ---
										</Label>
									)}
								</div>
							</div>
							<button onClick={this.printReport} className="btn-form">
								บันทึก
							</button>
						</div>
					</Animated>
				</Modal>
			</div>
		);
	}
}
export default CardProgramHistory;
