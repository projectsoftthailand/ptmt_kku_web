import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker } from 'react-google-maps';
import Geocode from 'react-geocode';
Geocode.setApiKey('AIzaSyDZSZWJ0NKckLq5_AOvsUGogDgxsGbQIew');
Geocode.enableDebug();
const AsyncMap = withScriptjs(
	withGoogleMap((props) => (
		<GoogleMap defaultZoom={14} defaultCenter={{ lat: props.lat, lng: props.lng }}>
			<InfoWindow onClose={onInfoWindowClose} position={{ lat: props.lat + 0.0018, lng: props.lng }}>
				<div>
					<span style={{ padding: 0, margin: 0 }}>{props.address}</span>
				</div>
			</InfoWindow>
			<Marker position={{ lat: props.lat, lng: props.lng }} />
			<Marker />
		</GoogleMap>
	))
);
function onInfoWindowClose(event) {}
class CardLocation extends Component {
	render() {
		let { lats, lngs, address } = this.props;
		let map;
		map = (
			<div style={{ height: '350px' }}>
				<AsyncMap
					googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZSZWJ0NKckLq5_AOvsUGogDgxsGbQIew&libraries=places&callback=initMap"
					loadingElement={<div style={{ height: `100%` }} />}
					containerElement={<div style={{ height: `300px` }} />}
					mapElement={<div style={{ height: `100%` }} />}
					lat={lats}
					lng={lngs}
					address={address}
				/>
			</div>
		);
		return map;
	}
}
export default CardLocation;
