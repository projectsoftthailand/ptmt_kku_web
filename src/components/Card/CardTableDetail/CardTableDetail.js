import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ip, GET, GET_USERS, GET_PICTURE, UPDATE_USER_PROFILE, POST } from '../../../service/service';
import { FaRegFileAlt, FaRegEdit, FaCalendarAlt } from 'react-icons/fa';
import { IoIosArrowDropupCircle, IoIosArrowDropdownCircle } from 'react-icons/io';
import './Styled/style.css';
import Barcode from 'react-barcode';
import moment from 'moment';
import Color from '../../Color';
import ReactLoading from 'react-loading';
import Responsive from 'react-responsive';
import swal from 'sweetalert';
import CardHistory from '../CardHistory/CardHistory';
import { sevenDigits } from '../../../function/function';
// import default_picture from "../../../assets/image/man-user.png";
import Modal from 'react-modal';
import { Row, Col, FormGroup, Label, Input, Collapse } from 'reactstrap';

function name(prefix, name, lastname) {
	return name ? `${prefix}${name} ${lastname}` : '-';
}

function idcard(id) {
	return id == 'null' || id == null || id == '' || id == undefined || id == 'undefined' || id == NaN
		? '-'
		: id.slice(0, 1) + '-' + id.slice(1, 5) + '-' + id.slice(5, 10) + '-' + id.slice(10, 12) + '-' + id.slice(12);
}

function phone(phone) {
	return phone ? phone.slice(0, 3) + '-' + phone.slice(3) : '-';
}

function sex(sex) {
	return sex ? (sex == 'female' ? 'ผู้หญิง' : sex == 'male' ? 'ผู้ชาย' : sex) : '-';
}

function birthday(dob) {
	let c = dob && moment(new Date()).year() - moment(dob).year() > 0;
	if (c) {
		return dob ? moment(dob).add(543, 'years').format('D MMM YYYY') : '-';
	} else {
		return dob ? moment(dob).format('D MMM YYYY') : '-';
	}
}
let Max = (props) => <Responsive {...props} maxWidth={501} />;
let Min = (props) => <Responsive {...props} minWidth={502} />;
let EditMin = (props) => <Responsive {...props} minWidth={576} />;

let CMax = (props) => <Responsive {...props} maxWidth={354} />;
let CMin = (props) => <Responsive {...props} minWidth={355} />;

export default class CardTableDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			detail: [],
			user_id: '',
			picture_id: '',

			edit: 0,
			note: '',
			wait_loading: false,
			isOpen: false
		};
	}
	componentWillMount() {
		this.props.onRef(this);
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.GetHistory();
	}

	GetHistory = async () => {
		let { id } = this.props;
		let new_id = sevenDigits(id);
		try {
			let res = await GET(GET_USERS(id));
			// console.log(res);
			if (res.success) {
				this.setState({
					user_id: new_id,
					detail: res.result[0],
					picture_id: res.result[0].picture,
					loading: false,
					note: res.result[0].allergy
				});
			} else {
				swal('ผิดพลาด!', res.message, 'warning', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (err) {
			// console.log(err);
		}
	};
	getDetail = () => {
		return this.state.detail;
	};
	render() {
		let { detail, user_id, picture_id, loading, edit, wait_loading, isOpen } = this.state;
		let { id, list_id } = this.props;
		return (
			<div>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : (
					<div className="div-table-program-all">
						<div
							className="div-table-program-detail"
							onClick={() => this.setState((prev) => ({ isOpen: !prev.isOpen }))}
						>
							<div className="col font-style-color-wht m-0" style={{ cursor: 'pointer' }}>
								<label>ข้อมูลผู้ป่วย</label>{' '}
								{isOpen ? <IoIosArrowDropupCircle /> : <IoIosArrowDropdownCircle />}
							</div>
						</div>
						<Collapse isOpen={isOpen}>
							<div className="div-table-program-list p-4">
								<Row className="mx-0 px-0">
									<Col xs={12} xl={2}>
										<div className="d-flex flex-column align-items-center">
											<img
												src={ip + GET_PICTURE(picture_id || 'null')}
												className="avatar-card-detail-table"
												alt="avatar"
											/>
											<Barcode
												value={`HN${user_id}`}
												width={0.8}
												height={25}
												fontSize={12}
												background={'transparent'}
											/>
										</div>
									</Col>
									<Col xs={12} sm={7} xl={6} style={{ fontSize: '1.2rem', marginTop: '0.5rem' }}>
										<Row>
											<CMin>
												<Col className="my-2">
													<b className="mr-2">บัตรประชาชน :</b>
													{idcard(detail.citizen_id)}
												</Col>
											</CMin>
											<CMax>
												<Col className="my-2">
													<b className="mr-2">บัตรประชาชน :</b>
													{detail.citizen_id}
												</Col>
											</CMax>
										</Row>
										<Row>
											<Col lg={6} className="my-2">
												<b className="mr-2">ชื่อ :</b>
												{name(detail.th_prefix, detail.th_name, detail.th_lastname)}
											</Col>
											<Col lg={6} className="my-2">
												<b className="mr-2">โทรศัพท์ :</b>
												{phone(detail.phone)}
											</Col>
										</Row>
										<Row>
											<Col lg={6} className="my-2">
												<b className="mr-2">เพศ :</b>
												{sex(detail.sex)}
											</Col>
											<Col lg={6} className="my-2">
												<b className="mr-2">เกิดวันที่ :</b>
												{birthday(detail.birthday)}
											</Col>
										</Row>
										<Row>
											<Col className="my-2">
												<b className="mr-2">ที่อยู่ :</b>
												{detail.address ? detail.address : '-'}
											</Col>
										</Row>
										<Row>
											<Col className="my-2" style={{ color: Color.Orange }}>
												<b className="mr-2">หมายเหตุ :</b>
												{edit == 0 ? detail.allergy ? (
													detail.allergy
												) : (
													'-'
												) : (
													<FormGroup>
														<Input
															value={this.state.note}
															type="textarea"
															name="text"
															id="exampleText"
															onChange={(e) => this.setState({ note: e.target.value })}
														/>
													</FormGroup>
												)}
											</Col>
										</Row>
									</Col>
									<EditMin>
										<Col xs={5} sm={5} xl={4}>
											<CardHistory id={id} list_id={list_id} />
										</Col>
									</EditMin>
									{edit == 0 ? (
										<Col xs={12}>
											<Min>
												<div className="row">
													<div
														className="btn-blue-subject"
														style={{ backgroundColor: Color.Green, height: 'fit-content' }}
														onClick={() => this.setState({ edit: 1 })}
													>
														<FaRegEdit className="ic-btn-blue-subject mr-2" />
														<div>แก้ไขหมายเหตุ</div>
													</div>
													<Link
														style={{ textDecoration: 'none', height: 'fit-content' }}
														to={{
															pathname: '/history/' + detail.user_id
														}}
													>
														<div className="btn-blue-subject">
															<FaRegFileAlt className="ic-btn-blue-subject mr-2" />
															<div>ประวัติการรักษา</div>
														</div>
													</Link>
													<Link
														style={{ textDecoration: 'none', height: 'fit-content' }}
														to={{
															pathname: '/home/addnew/' + detail.user_id
														}}
													>
														<div className="btn-blue-subject">
															<FaCalendarAlt className="ic-btn-blue-subject mr-2" />
															<div>นัดหมายครั้งถัดไป</div>
														</div>
													</Link>
												</div>
											</Min>
											<Max>
												<div
													className="btn-blue-subject"
													style={{ backgroundColor: Color.Green, height: 'fit-content' }}
													onClick={() => this.setState({ edit: 1 })}
												>
													<FaRegEdit className="ic-btn-blue-subject mr-2" />
													<div>แก้ไขหมายเหตุ</div>
												</div>
												<Link
													style={{ textDecoration: 'none' }}
													to={{
														pathname: '/history/' + detail.user_id
													}}
												>
													<div className="btn-blue-subject" style={{ height: 'fit-content' }}>
														<FaRegFileAlt className="ic-btn-blue-subject mr-2" />
														<div>ประวัติการรักษา</div>
													</div>
												</Link>
												<Link
													style={{ textDecoration: 'none' }}
													to={{
														pathname: '/home/addnew/' + detail.user_id
													}}
												>
													<div className="btn-blue-subject" style={{ height: 'fit-content' }}>
														<FaCalendarAlt className="ic-btn-blue-subject mr-2" />
														<div>นัดหมายครั้งถัดไป</div>
													</div>
												</Link>
											</Max>
										</Col>
									) : (
										<Col xs={12}>
											<Min>
												<Row>
													<div className="cancal" onClick={() => this.setState({ edit: 0 })}>
														<FaRegEdit className="ic-btn-blue-subject mr-2" />
														<div>ยกเลิก</div>
													</div>
													<div className="submit" onClick={this.onSubmit}>
														<FaRegFileAlt className="ic-btn-blue-subject mr-2" />
														<div>บันทึก</div>
													</div>
												</Row>
											</Min>
											<Max>
												<div className="cancal" onClick={() => this.setState({ edit: 0 })}>
													<FaRegEdit className="ic-btn-blue-subject mr-2" />
													<div>ยกเลิก</div>
												</div>
												<div className="submit" onClick={this.onSubmit}>
													<FaRegFileAlt className="ic-btn-blue-subject mr-2" />
													<div>บันทึก</div>
												</div>
											</Max>
										</Col>
									)}
								</Row>
							</div>
						</Collapse>
					</div>
				)}
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
	onSubmit = async () => {
		let { note, detail } = this.state;
		let obj = {
			allergy: note,
			user_id: detail.user_id
		};
		try {
			this.setState({ wait_loading: true });
			let res = await POST(UPDATE_USER_PROFILE, obj);
			if (res.success === true) {
				this.setState({ wait_loading: false });
				swal('สำเร็จ!', res.message, 'success', {
					buttons: false,
					timer: 2000
				}).then(() => {
					window.location.reload();
				});
			} else {
				this.setState({ wait_loading: false });
				swal('ผิดพลาด!', res.message, 'error', {
					buttons: false,
					timer: 2000
				});
			}
		} catch (error) {
			this.setState({ wait_loading: false });
			// console.log(error);
		}
	};
}
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};
