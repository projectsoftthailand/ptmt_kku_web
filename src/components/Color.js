export default {
  Blue: "#467ac8",
  Orange: "#e8ad6a",
  OrageActive: "#e29850",
  Violet: "#c595d3",
  VioletActive: "#b980ce",
  Green: "#7bc8c6",
  GreenActive: "#66bfb8",
  // Gray: '#f1f2f4',
  TextBlack: "#333",
  TextGray: "#999",
  // GrayBG: '#F1F2F4',

  // block status
  EmptyGray: "#9AD4F6",
  Empty: "#9AD4F6",
  Servicing: "#7BC8C6",
  WaitComfirm: "#E8AD6A",
  InQueue: "#C595D3",
  Done: "yellow",
  ActiveBed: "#467ac8",
  //
  White: "#fff",
  Brush: "#FF7F7F70",
  // Delete
  Red: "#E25050"
};
