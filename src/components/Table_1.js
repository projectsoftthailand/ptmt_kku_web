import React, { Component } from 'react';
import { Table, Input } from 'reactstrap';
import Responsive from 'react-responsive';
import './results.css';

let Min = (props) => <Responsive {...props} minWidth={768} />;
let Max = (props) => <Responsive {...props} maxWidth={767} />;

export default class Table_1 extends Component {
	onChangs = (index, e) => {
		// console.log('index :::', index);
		// console.log('e :::', e);
		let { payTypes } = this.props;
		// if (Number(payTypes) === 1) {
		let row = this.props.rows;
		row[index][1] = Number(e);
		// console.log('payTypes = 1', payTypes);
		// console.log('row', row);
		//----------------------------------------------------------
		let treatmentData = this.props.treatmentData;
		treatmentData[index].cost = Number(e);
		// console.log('treatmentData', treatmentData);
		this.props.setNewTreatment(row);
		this.props.settreatmentData(treatmentData);
		// } else {
		// 	let row = this.props.rows;
		// 	let treatmentData = this.props.treatmentData;
		// 	let amount = Number(treatmentData[index].disburseable) + Number(e);
		// 	// console.log('payTypes > 1', payTypes);
		// 	// console.log('row', row);
		// 	//----------------------------------------------------------
		// 	row[index][1] = Number(amount);
		// 	treatmentData[index].cost = Number(amount);
		// 	// console.log('treatmentData', treatmentData);
		// 	this.props.setNewTreatment(row);
		// 	this.props.settreatmentData(treatmentData);
		// }
	};

	render() {
		let { rows, payTypes, role, treatmentData } = this.props;
		// console.log('rows', rows);
		// console.log('payTypes', payTypes);
		return (
			<Table responsive>
				{role == 'pt' ||
					(role == 'frontPT' && (
						<thead>
							<tr>
								<th>รายการ</th>
								<Min>
									<th />
								</Min>
								<Min>
									<th>ราคา / จำนวน / รวม</th>
								</Min>
								<Max>
									<th>จำนวน / ราคารวม</th>
								</Max>
								<th>เลือก</th>
							</tr>
						</thead>
					))}
				<tbody>
					{rows.map((row, index) => (
						<tr style={{ borderWidth: 0 }}>
							<td
								style={{
									backgroundColor: 'white',
									borderLeftWidth: 0,
									borderRightWidth: 0,
									padding: '0.5rem'
								}}
							>
								{(role == 'mt' || role == 'frontMT') && (
									<span>
										{row[0]} {row[4] !== null ? '(' + row[4] + ')' : null}
									</span>
								)}
								{(role == 'pt' || role == 'frontPT') && (
									<span>
										<Min>
											{row[0]} {row[4] !== null ? '(' + row[4] + ')' : null}
										</Min>
										<Max>{row[8]}</Max>
									</span>
								)}
							</td>
							{(role == 'mt' || role == 'frontMT') && (
								<td
									style={{
										backgroundColor: 'white',
										borderLeftWidth: 0,
										borderRightWidth: 0,
										padding: '0.5rem'
									}}
								>
									<span>{row[5]}</span>
								</td>
							)}
							<Min>
								{(role == 'pt' || role == 'frontPT') && (
									<td
										style={{
											backgroundColor: 'white',
											borderLeftWidth: 0,
											borderRightWidth: 0,
											padding: '0.5rem'
										}}
									>
										<span>{row[5]}</span>
									</td>
								)}
							</Min>
							<td
								style={{
									backgroundColor: 'white',
									borderLeftWidth: 0,
									borderRightWidth: 0,
									padding: '0.5rem',
									width: role == 'mt' || role == 'frontMT' ? 'auto' : '17rem'
								}}
							>
								{(role == 'mt' || role == 'frontMT') && (
									<input
										value={row[1]}
										type="number"
										placeholder="ราคา"
										// disabled
										style={{
											width: '50px',
											borderRadius: '3px',
											borderWidth: '0.5px',
											outline: 'none',
											textAlign: 'right'
										}}
										onChange={(e) => this.onChangs(index, e.target.value)}
									/>
								)}

								{(role == 'pt' || role == 'frontPT') && (
									<div className="disable-input d-flex">
										<Min>
											<div>
												<Input
													value={row[7]}
													type="number"
													placeholder="ราคา"
													disabled
													style={{
														width: '5rem',
														// borderRadius: '3px',
														// borderWidth: '0.5px',
														// outline: 'none',
														textAlign: 'right'
													}}
												/>
											</div>
										</Min>
										<div className="d-flex">
											<button className="btn-minus" onClick={() => this.mutiple(index, 'minus')}>
												-
											</button>
											<input
												value={row[6]}
												type="number"
												disabled
												// onChange={(e) => {
												// 	if (e.target.value >= 1) {
												// 		this.mutiple(index, e.target.value);
												// 	}
												// }}
												className="width-input-amount"
											/>
											<button className="btn-plus" onClick={() => this.mutiple(index, 'plus')}>
												+
											</button>
										</div>
										<div>
											<Input
												// value={treatmentData
												// 	.filter((e) => e.type === 'pt')
												// 	.filter((el, i) => index == i)
												// 	.map((e) => e.cost)}
												value={Number(row[7]) * Number(row[6])}
												type="number"
												// placeholder="ราคา"
												disabled
												style={{
													width: '5rem',
													// borderRadius: '3px',
													// borderWidth: '0.5px',
													// outline: 'none',
													textAlign: 'right'
												}}
												className="width-input"
												// onChange={(e) => this.onChangs(index, e.target.value)}
											/>
										</div>
									</div>
								)}
							</td>
							<td
								style={{
									backgroundColor: 'white',
									borderLeftWidth: 0,
									borderRightWidth: 0,
									padding: '0.5rem'
								}}
							>
								{row[3]}
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		);
	}
	mutiple = (index, e) => {
		let row = this.props.rows;
		let treatmentData = this.props.treatmentData;
		if (e === 'plus') {
			treatmentData[index].amount = Number(treatmentData[index].amount) + 1;
			treatmentData[index].cost = Number(row[index][7]) * Number(treatmentData[index].amount);
			this.props.settreatmentData(treatmentData);
		}
		if (e === 'minus' && Number(treatmentData[index].amount) > 1) {
			treatmentData[index].amount = Number(treatmentData[index].amount) - 1;
			treatmentData[index].cost = Number(row[index][7]) * Number(treatmentData[index].amount);
			this.props.settreatmentData(treatmentData);
		}
	};
}
