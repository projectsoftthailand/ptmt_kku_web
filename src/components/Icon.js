export default {
    user: require("../assets/icon/username.png"),
    password: require("../assets/icon/password.png"),
    clipboard: require("../assets/icon/clipboard.svg"),
    list: require("../assets/icon/list.png"),
};
