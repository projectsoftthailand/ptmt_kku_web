import React, { Component } from 'react'
import {Table} from 'reactstrap'
import Color from '../Color';

export default class TableBed extends Component {
  render() {      
    let {head, rows} = this.props

    return (
    <Table scroll="no" style={{}} responsive>
        { head &&
        <thead>
            <tr style={{backgroundColor: Color.Blue}}>
            {head.map((el, index)=> (
                <th style={{color: 'white', borderBottomWidth: 0, padding: 5, whiteSpace: 'pre'}} key={'a'+index}>{el}</th>
            ))}
            </tr>
        </thead>
        }
        <tbody>
            {rows.map((row, index)=> (
                <tr style={{borderWidth: 0}} key={'b'+index}>
                    {row.map((d, index)=> (
                        <td key={'c'+index} style={{color: 'black', padding: 0, backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0}}>{d}</td>
                    ))}
                </tr>
            ))}
        </tbody>
    </Table>
    )
  }
}
