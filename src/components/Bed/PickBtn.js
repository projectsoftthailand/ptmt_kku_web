import React, { Component } from 'react'
import Color from '../Color';
import { GoCheck } from "react-icons/go";

export default class PickBtn extends Component {

  render() {
    let {onPick, isActive} = this.props
      if(!isActive) {
        return (
        <div 
            className="rounded"
            onClick={onPick}
            style={{ 
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
                backgroundColor: Color.Blue,
                marginRight: 10,
                // width: 60,
                cursor: 'pointer'
            }}>เลือก</div>
        )
      }else {
          return (
          <div     
              className="rounded"
              onClick={onPick}
              style={{ 
                  display: 'flex',
                  position: 'relative',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  backgroundColor: Color.Blue,
                  marginRight: 10,
                  // width: 60,
                  cursor: 'pointer'
              }}>
              <GoCheck style={{position: 'absolute', left: 0,}} className="ml-1" />เลือก
              </div>
          )
      }
  }
}
