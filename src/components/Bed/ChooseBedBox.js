import React, { Component } from 'react';
import { GET, GET_BOOKING, GET_BED, GET_STATUS, _ip } from '../../service/service';
import BedBox from './BedBox';
import { Accordion, AccordionItem } from 'react-sanfona';
import Color from '../Color';
import { FaAngleDoubleDown } from 'react-icons/fa';
import Time from '../Content/Time';
import moment from 'moment';
import { Input } from 'reactstrap';
import { timeStartIntArray, timeEndIntArray, dateInt } from '../Content/TimeUse';
import socketIOClient from 'socket.io-client';

export default class ChooseBedBox extends Component {
	constructor(props) {
		super(props);
		this._refBed = new Map();

		this.state = {
			activeClickedItems: [ 0 ],

			status: [],
			bed: [],
			booking: [],

			isReady: false,

			actionIndex: -1,
			bedWithStatus: [],

			curTimeInt: -1,
			curDate: moment().format('YYYY-MM-DD'),

			curBedPicked: -1,
			curTimePicked: '',

			defaultBed: -1, // bed selected from home page
			defaultCheck: [],

			curBookTimeIndex: []
		};
	}

	async GetBooking() {
		try {
			let res = await GET(GET_BOOKING('pt', this.state.curDate));
			// console.log('res GetBooking is : ', res)
			// data_at: "15:02:2019 15:42:39"
			if (res.success) {
				await this.setState({ booking: res.result });
			} else {
				await this.setState({ booking: [] });
			}
			let curTimeServer = res.data_at.split(' ')[1].split(':');
			let curTimeServerInt = +curTimeServer[0] * 60 + +curTimeServer[1];
			await this.setState({ curTimeInt: curTimeServerInt });
			// console.log('curTimeServerInt is ', curTimeServerInt)
		} catch (error) {
			// console.log('error GetBooking is :', error)
		}
	}

	async GetBed() {
		try {
			let res = await GET(GET_BED);
			// console.log('res GetBed : ', res)
			await this.setState({ bed: res.result });
		} catch (error) {
			// console.log('error GetBed : ', error)
		}
	}

	async GetStatus() {
		try {
			let res = await GET(GET_STATUS);
			// console.log('res GetStatus : ', res.result)
			await this.setState({ status: res.result });
		} catch (error) {
			// console.log('error GetStatus : ', error)
		}
	}

	async componentDidMount() {
		this.props.onRef(this);
		await Promise.all([
			this.GetStatus(),
			this.GetBed(),
			this.GetBooking()
			// this.initCurCheck() // in this fn have to check is have booked from home page ?
		]);
		await this.realTimeHandle();
		let { defaultBed } = this.props;
		// ['<bed>_<timeIndex>'] => ['5_18', '5_19']
		if (defaultBed && defaultBed.length > 0) {
			let bedID = +defaultBed[0].split('_')[0];
			let defaultCheck = [];
			defaultBed.forEach((e, i) => {
				defaultCheck.push(+e.split('_')[1]);
			});
			await this.setState({
				defaultBed: bedID,
				defaultCheck,
				curBedPicked: bedID
			});
			// console.log('defaultCheck is ', this.state.defaultCheck)
			let timeString =
				Time[defaultCheck[0]].split(' - ')[0] +
				' - ' +
				Time[defaultCheck[defaultCheck.length - 1]].split(' - ')[1];
			await this.setState({ curTimePicked: timeString });
		}
		// await this.createBooking(-1)
		// console.log('done loading')
		// console.log('000 s', this.state.curCheck)
		// this.response()
		this.setState({ isReady: true });
	}

	getBookedBed = () => {
		return {
			curBedPicked: this.state.curBedPicked,
			booking: this.state.curTimePicked,
			curDate: this.state.curDate
		};
	};

	componentWillUnmount() {
		socketIOClient(_ip).disconnect();
	}

	response() {
		socketIOClient(_ip).on('rerender', async (res) => {
			if (res) {
				// any res
				// console.log('res socket is ', res)
				if (res.type === 'create_list_by_front') {
					await this.GetBooking();
					await this.reRender();
				} else if (res.type === 'rerender every 30 min') {
					if (res.dateTimer) {
						let curTimeServer = res.dateTimer.split(' ')[1].split(':');
						let curTimeServerInt = +curTimeServer[0] * 60 + +curTimeServer[1];
						await this.setState({ curTimeInt: curTimeServerInt });
						await this.reRender(res.dateTimer);
					}
					// console.log('wtf socket')
				}
			}
			//    console.log('ssssss')
		});
	}

	reRender() {
		this.realTimeHandle();
	}

	isBetween(x, min, max) {
		return x >= min && x <= max;
	}

	createBooking(type) {
		// if type = -999 is mean today have to check realCurrentTime
		// if type = -1 is mean future
		// if type = -2 is mean pass
		let { bed, booking } = this.state;
		let timeBed = [];
		this.setState({ actionIndex: -999 }); // shouldComponentWillUpdate setActionIndex: -999 mean rerender all <BedBox />
		//Init empty booking
		if (type !== -999) {
			bed.forEach((e, i) => {
				let timeArray = [];
				Time.forEach((e2, j) => {
					timeArray.push({
						startTime: timeStartIntArray[j],
						endTime: timeEndIntArray[j],
						status: type,
						name: ''
					});
				});
				timeBed.push({
					id: e.id,
					timeArray
				});
			});
		} else {
			// need to check currentRealTime
			bed.forEach((e, i) => {
				let timeArray = [];
				Time.forEach((e2, j) => {
					let bedStatus;
					// check currentTime for set status
					if (this.state.curTimeInt <= timeStartIntArray[j]) {
						// is time out
						bedStatus = -1;
					} else {
						bedStatus = -2;
					}
					timeArray.push({
						startTime: timeStartIntArray[j],
						endTime: timeEndIntArray[j],
						status: bedStatus,
						name: ''
					});
				});
				timeBed.push({
					id: e.id,
					timeArray
				});
			});
		}

		//setBooking
		booking.forEach((e, i) => {
			let index = timeBed.findIndex((x) => x.id === e.bed);

			let bookingAt = e.booking_at.split('T')[1].split('.')[0];
			bookingAt = bookingAt.split(':');
			let bookingAtInt = +bookingAt[0] * 60 + +bookingAt[1];

			let bookingEnd = e.booking_end.split('T')[1].split('.')[0];
			bookingEnd = bookingEnd.split(':');
			let bookingEndInt = +bookingEnd[0] * 60 + +bookingEnd[1];

			// find time array index

			// console.log('cur bed = ', e.bed)
			// console.log('bookingAt = ', bookingAtInt)
			// console.log('bookingENd = ', bookingEndInt)

			for (let j = 0; j < timeBed[index].timeArray.length; j++) {
				if (this.isBetween(timeBed[index].timeArray[j].startTime, bookingAtInt, bookingEndInt)) {
					let startPos = j;
					let endPos = j;

					timeBed[index].timeArray[startPos].status = e.service_status;
					timeBed[index].timeArray[startPos].name = e.th_name + ' ' + e.th_lastname;

					for (let k = j; k < timeBed[index].timeArray.length; k++) {
						if (this.isBetween(timeBed[index].timeArray[k].startTime, bookingAtInt, bookingEndInt)) {
							endPos = k;
							timeBed[index].timeArray[endPos].status = e.service_status;
							timeBed[index].timeArray[endPos].name = e.th_name + ' ' + e.th_lastname;
						} else {
							break;
						}
					}
					// console.log('startPos is ', startPos)
					// console.log('endPos is ', endPos)
					// console.log('------------------')
					break;
				}
			}
		});

		// console.log('timeBed is ', timeBed)
		this.setState({ bedWithStatus: timeBed });
	}

	realTimeHandle = async () => {
		let today = moment().format('YYYY-MM-DD');
		let curDateInt = dateInt(this.state.curDate);
		today = dateInt(today);
		if (curDateInt === today) {
			// if is today
			// console.log('is today')
			await this.createBooking(-999);
		} else if (curDateInt > today) {
			// if is future
			// console.log('is future')
			await this.createBooking(-1);
		} else if (curDateInt < today) {
			// if is pass
			// console.log('is pass')
			await this.createBooking(-2);
		}
	};

	onChangeDate = async (e) => {
		await this.setState({ curDate: e.target.value });
		await this.GetBooking();
		// await this.createBooking(-1)
		await this.realTimeHandle();
		// console.log('create new date')
	};

	clearCurCheck = (index) => {
		// console.log('clearcheck on index :', index)
		this.setState({ curBedPicked: -1, curTimePicked: '' });
		this._refBed.get(index).clearCheck();
	};

	updatePickedBed = (bedIndex, curCheck) => {
		let { bedWithStatus } = this.state;
		// console.log("bedID : ", bedWithStatus[bedIndex].id)
		// console.log('curCheck: ', curCheck)

		let startIndex = curCheck.indexOf(true);
		let endIndex = curCheck.lastIndexOf(true);
		// console.log('timeIndex is ', startIndex, ' , ', endIndex)
		let timeString = '';
		if (Time[startIndex] && Time[endIndex]) {
			timeString = Time[startIndex].split(' - ')[0] + ' - ' + Time[endIndex].split(' - ')[1];
		}

		this.setState({
			curBedPicked: bedWithStatus[bedIndex].id,
			curTimePicked: timeString
		});
	};

	render() {
		let { isReady, bedWithStatus, curDate, curBedPicked, curTimePicked } = this.state;
		if (isReady) {
			return (
				<div className="bg-white rounded">
					<div className="px-4 pt-4 d-flex justify-content-between">
						<div className="d-flex pb-4">
							<label style={{ fontWeight: '100', marginTop: 7, marginRight: 10 }} htmlFor="dateInput">
								เลือกวัน
							</label>
							<Input
								onChange={this.onChangeDate}
								value={curDate}
								id="dateInput"
								className="mt-1"
								style={{ width: 120 }}
								type="date"
							/>
						</div>
						{curBedPicked === -1 ? (
							<div style={{ marginTop: 8 }}>กรุณาเลือกเตียง</div>
						) : (
							<div style={{ marginTop: 8 }}>
								เลือก เตียงที่ {curBedPicked} เวลา {curTimePicked}
							</div>
						)}
					</div>

					<Accordion>
						{bedWithStatus.map((item, index) => {
							return (
								<AccordionItem
									// onExpand={()=>this.setState({curBedPicked: item.id})}
									onClose={this.clearCurCheck}
									style={{
										backgroundColor: Color.Blue,
										position: 'relative',
										color: 'white',
										textAlign: 'center'
									}}
									className="border"
									key={index}
									title={`เตียงที่ ${item.id}`}
									expanded={item.id === this.state.defaultBed}
								>
									<FaAngleDoubleDown
										size={12}
										style={{
											pointerEvents: 'none',
											position: 'absolute',
											top: 0,
											right: 0,
											marginRight: 10,
											marginTop: 5,
											cursor: 'pointer'
										}}
									/>
									<BedBox
										bedID={item.id}
										defaultBed={this.state.defaultBed}
										defaultCheck={this.state.defaultCheck}
										onUpdateBed={this.updatePickedBed}
										onRef={(c) => this._refBed.set(index, c)}
										status={this.state.status}
										bookedData={item}
										actionIndex={this.state.actionIndex}
										index={index}
									/>
								</AccordionItem>
							);
						})}
					</Accordion>
				</div>
			);
		} else {
			return <div>Loading...</div>;
		}
	}
}
