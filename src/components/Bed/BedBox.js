import React, { Component } from 'react'
import Color from '../Color';
import TableBed from './TableBed';
import { timeStartStringArray, timeEndStringArray } from '../Content/TimeUse';
import Time from '../Content/Time';
import PickBtn from './PickBtn';
import swal from "sweetalert";

export default class BedBox extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         curCheck: [],
         isReady: false
      }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if(nextProps.actionIndex===-999) {
            // console.log('is Render...')
            return true
        }
        if(this.props.index===nextProps.actionIndex) {
            // console.log('is Render...1')
            // console.log('render ', this.props.index)
            // console.log('statt', this.props.status)
            return true
        }
        return false
    }

    componentDidMount() {
        // console.log('index :', this.props.index)
        this.props.onRef(this)
        this.initCurCheck()
    }

    initCurCheck() {
        let {defaultCheck, defaultBed, bedID} = this.props
        if(defaultBed!==-1) { // same bed
            if(defaultBed===bedID) {
                if(defaultCheck) { // have booked bed from home page
                    if(defaultCheck.length>0) {
                        let temp = []
                        Time.forEach(()=>
                        temp.push(false)
                        )
                        defaultCheck.forEach((e, i) => {
                            temp[e] = true
                        })
                        
                        this.setState({curCheck: temp, isReady: true})
                        return
                    }
                }
            }
        }
        
        let temp = []
        Time.forEach(()=>
            temp.push(false)
        )
        this.setState({curCheck: temp, isReady: true})
    }

    clearCheck = () => {
        this.setState({curCheck: []})
        // console.log('is cleared : ', this.props.index)
    }

    checkPickOne(indexPicked) {
        let {curCheck} = this.state
        // console.log('curCHeck is ', JSON.stringify(curCheck))
        // console.log('found true : ', curCheck.indexOf(true))
        let index = curCheck.indexOf(true)
        if(index===-1) { // anyWhere can check
            return 1
        }else { // check is <-1 _1stIndex> or <+1 _lastIndex>
            let minIndex = index-1
            let maxIndex = curCheck.lastIndexOf(true)+1
            // console.log('indexPick is ', indexPicked, ' , min : ', minIndex, ', max : ', maxIndex)
            if(indexPicked===minIndex || indexPicked===maxIndex) {
                return 1
            }
        }
        swal("ผิดพลาด!", "ไม่สามารถจองข้ามเวลาได้", "error", {
            buttons: false,
            timer: 2000
        });
        return -1
    }

    checkUnPickOne(indexPicked) {
        let {curCheck} = this.state
        // console.log('curCHeck is ', JSON.stringify(curCheck))
        // console.log('found true : ', curCheck.indexOf(true))
        let minIndex = curCheck.indexOf(true)
        let maxIndex = curCheck.lastIndexOf(true)
        // console.log('indexPick is ', indexPicked, ' , min : ', minIndex, ', max : ', maxIndex)
        if(indexPicked===minIndex || indexPicked===maxIndex) {
            return 1
        }
        swal("ผิดพลาด!", "ไม่สามารถยกเลิกจองข้ามเวลาได้", "error", {
            buttons: false,
            timer: 2000
        });
        return -1
    }

    pickOne = (index) => {
        let status = -1
        if(this.state.curCheck[index]) { // need to uncheck
            status = this.checkUnPickOne(index)
        }else { // need to check
            status = this.checkPickOne(index)
        }
        if(status===-1) return
        // console.log('now bed : ', this.props.index)
        // console.log('pick on index', index)
        let temp = [...this.state.curCheck]
        temp[index] = !temp[index]
        this.setState({curCheck: temp})
        this.props.onUpdateBed(this.props.index, temp)
    }

    render() {
        // console.log('render self')
        // console.log('index is ', this.props.index)
        let { bookedData, status } = this.props
        let {isReady,} = this.state
        let rows = []
        
        Time.forEach((e, i) => {
            let curStatus = bookedData.timeArray[i].status
            let displayStatus

            let color
        
            if(curStatus===-2) {
                color = '#ccc'
            }else if(curStatus===-1) {
                color = 'black'
            }else if(curStatus===1) {
                color = Color.WaitComfirm
            }else if(curStatus===2) {
                color = Color.InQueue
            }else if(curStatus===3) {
                color = Color.Servicing
            }else if(curStatus===4) {
                color = Color.Done
            }

            let choose
            if(curStatus===-1) {
                displayStatus = 'ว่าง'
                choose = <PickBtn
                            onPick={()=>this.pickOne(i)} 
                            isActive={this.state.curCheck[i]}
                            />
            }else if(curStatus===-2) {
                displayStatus = 'ว่าง'
                choose = ''
            }else {
                displayStatus = status[status.findIndex(x => x.id===bookedData.timeArray[i].status)].name
                choose = ''
            }
            rows.push([
                <span style={{color}}>{timeStartStringArray[i]}</span>, 
                <span style={{color}}>{timeEndStringArray[i]}</span>,
                <span style={{color}}>{displayStatus}</span>,
                <span style={{color}}>{bookedData.timeArray[i].name}</span>,
                <span style={{color}}>{choose}</span>
            ])
        }) 

        if(isReady) {
            return (
                <div>
                    <TableBed 
                        head={['เวลาเริ่ม', 'เวลาสิ้นสุด', 'สถานะ', 'ชื่อผู้ป่วย', '']}
                        rows={rows}
                        />
                </div>
            )
        }else {
            return (<div></div>)
        }
        
    }
}
