import React, { Component } from 'react'
import Color from '../Color';

export default class BedItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isActive: false
    }
  }

  render() {
    // status
    // -1 => empty & can not book (time is outed)
    // -2 => empty & can book (future) 
    let { status, statusTypes, number, setSelected, setUnSelected, isDisable } = this.props
    let { isActive } = this.state
    let index = statusTypes.findIndex(x => x.id === status)
    let typeString
    let color
    let activeColor = Color.ActiveBed
    if (status === -2) {
      color = '#ccc'
      typeString = '-'
    } else if (status === -1) {
      color = Color.EmptyGray
      typeString = 'ว่าง'
    } else if (status === 1) {
      color = Color.WaitComfirm
    } else if (status === 2) {
      color = Color.InQueue
    } else if (status === 3) {
      color = Color.Servicing
    } else if (status === 4) {
      color = Color.Done
    }

    if (status !== -1 && status !== -2) {
      typeString = statusTypes[index].name
    }

    if (status === -1) { // is can book
      if (isDisable && isDisable === true) {
        return (
          <div
            style={{
              borderRadius: 6,
              backgroundColor: isActive ? activeColor : color,
              cursor: status === -1 || status === -2 ? 'normal' : 'pointer'
            }}
            className="border px-3 text-center text-white">
            <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>เตียง {number}</p>
            <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>{!isActive ? typeString : 'เลือก'}</p>
          </div>
        )
      } else {
        return (
          <div onClick={async () => {
            await this.setState({ isActive: !this.state.isActive })
            if (this.state.isActive) {
              setSelected().then(async (d) => {
                if (d === true) {
                  await this.setState({ isActive: true })
                } else {
                  await this.setState({ isActive: false })
                }
              })
            }
            else {
              setUnSelected().then(async (d) => {
                if (d === true) {
                  await this.setState({ isActive: false })
                } else {
                  await this.setState({ isActive: true })
                }
              })
            }
          }}
            style={{
              borderRadius: 6,
              backgroundColor: isActive ? activeColor : color,
              cursor: 'pointer'
            }}
            className="border px-3 text-center text-white">
            <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>เตียง {number}</p>
            <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>{!isActive ? typeString : 'เลือก'}</p>
          </div>
        )
      }
    } else {
      return (
        <div
          onClick={() => {
            if (!isDisable || status === -2) {
              // console.log('normal')
            } else alert('change status')
          }}
          style={{
            borderRadius: 6,
            backgroundColor: color,
            cursor: !isDisable || status === -2 ? 'normal' : 'pointer'
          }}
          className="border px-3 text-center text-white">
          <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>เตียง {number}</p>
          <p style={{ whiteSpace: 'pre', margin: 0, padding: 0 }}>{typeString}</p>
        </div>
      )
    }
  }
}
