import React, { Component } from "react";
import Color from "../Color";
import { GET, GET_BED, GET_STATUS, GET_BOOKING, _ip } from "../../service/service";
import Table_2 from "../Table_2";
import moment from "moment";
import socketIOClient from "socket.io-client";
import BedItem from "./BedItem";
import swal from "sweetalert";
import Time from "../Content/Time";
import { Link } from "react-router-dom";

const timeString = Time;
export default class ChooseBed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bedData: [],
      displayBedComponent: [],
      isReady: false,
      timeMnArray: [],
      bedStatus: [],

      bedTimeArray: [],
      displayBed: [],

      bookDate: "",
      status: [],

      initDateTimeArray: [],

      initCurTime: "",

      curSelected: [],

      bedChoosed: "",
      startTimeChoosed: "",
      endTimeChoosed: ""
    };
  }

  componentDidMount = async () => {
    let curDate = moment().format("YYYY-MM-DD");
    await this.setState({ bookDate: curDate });
    await this.GetStatus();
    await this.GetBed();
    await this.createTimeMnArray();
    //   await this.GetBooking()
    //   await this.createBedArray()
    await this.setState({ isReady: true });
    // this.response();
  };

  response() {
    socketIOClient(_ip).on("rerender", res => {
      this.reRender(res.dateTimer);
    });
  }

  componentWillUnmount() {
    socketIOClient(_ip).disconnect();
  }

  async reRender(t) {
    if (t) {
      let curTime = t.split(" ")[1];
      let curTimeInt = curTime.split(":");
      curTimeInt = +curTimeInt[0] * 60 + +curTimeInt[1];
      await this.setState({ initCurTime: curTimeInt });
    }
    this.GetBooking();
  }

  isBetween(x, min, max) {
    return x >= min && x <= max;
  }

  async createTimeMnArray() {
    let { bedData } = this.state;
    let timeArray = [];

    timeString.forEach((e, index) => {
      let tString = e.split(" - ");
      let startTimeString = tString[0];
      let endTimeString = tString[1];

      let startTimeInt = startTimeString.split(":");
      startTimeInt = +startTimeInt[0] * 60 + +startTimeInt[1];

      let endTimeInt = endTimeString.split(":");
      endTimeInt = +endTimeInt[0] * 60 + +endTimeInt[1];

      timeArray.push({ start: startTimeInt, end: endTimeInt, status: -1 });
    });
    let bedTimeArray = [];
    // console.log('timeArray is ', timeArray)
    for (let i = 0; i < bedData.length; i++) {
      let t = JSON.parse(JSON.stringify(timeArray)); // without reference
      bedTimeArray.push({ id: bedData[i].id, timeArray: t });
    }
    this.setState(
      {
        displayBed: bedTimeArray,
        bedTimeArray: bedTimeArray,
        initDateTimeArray: JSON.parse(JSON.stringify(bedTimeArray))
      },
      () => {
        // console.log('bedTimeArray : ', this.state.displayBed)
        this.GetBooking();
      }
    );
  }

  async GetBed() {
    try {
      let res = await GET(GET_BED);
      // console.log('res GetBed : ', res)
      await this.setState({ bedData: res.result });
    } catch (error) {
      // console.log('error GetBed : ', error)
    }
  }

  async GetStatus() {
    try {
      let res = await GET(GET_STATUS);
      // console.log('res GetStatus : ', res.result)
      this.setState({ status: res.result });
    } catch (error) {
      // console.log('error GetStatus : ', error)
    }
  }

  async GetBooking() {
    // clear 1st
    await this.setState({ displayBed: this.state.initDateTimeArray });
    try {
      let bedTimeArray = this.state.bedTimeArray;
      let bookDate = this.state.bookDate;
      let res = await GET(GET_BOOKING("pt", bookDate));
      // console.log('res GetBooking : ', res.result)
      // console.log('res DataAt : ', res.data_at)
      let curTime = res.data_at.split(" ")[1];
      let curTimeInt = curTime.split(":");
      curTimeInt = +curTimeInt[0] * 60 + +curTimeInt[1];
      await this.setState({ initCurTime: curTimeInt });
      if (res.success === false) {
        this.createBedArray(true);
        return;
      }
      res.result.forEach((e, i) => {
        let bookingAtString = e.booking_at
          .split("T")[1]
          .split(".")[0]
          .split(":");
        let bookingAtInt = +bookingAtString[0] * 60 + +bookingAtString[1];

        let bookingEndString = e.booking_end
          .split("T")[1]
          .split(".")[0]
          .split(":");
        let bookingEndInt = +bookingEndString[0] * 60 + +bookingEndString[1];

        let index = bedTimeArray.findIndex(x => x.id === e.bed); // bedindex
        bedTimeArray[index].timeArray.forEach(async (ei, i) => {
          if (this.isBetween(bookingAtInt, ei.start, ei.end)) {
            for (let j = i; j < bedTimeArray[index].timeArray.length; j++) {
              if (
                this.isBetween(
                  bookingEndInt,
                  bedTimeArray[index].timeArray[j].start,
                  bedTimeArray[index].timeArray[j].end
                )
              ) {
                let db = JSON.parse(JSON.stringify(this.state.displayBed));
                // let db = this.state.displayBed
                for (let m = i; m <= j; m++) {
                  db[index].timeArray[m].status = e.service_status;
                }
                await this.setState({ displayBed: db });
                break;
              }
            }
            return;
          }
        });

        if (i === res.result.length - 1) {
          this.createBedArray();
        }
      });
    } catch (error) {
      // console.log('error GetBooking : ', error)
    }
  }

  setTimeChoosed() {
    // startTimeChoosed: -1,
    // endTimeChoosed: -1,
    let cs = this.state.curSelected;
    if (!cs || cs.length <= 0) {
      this.setState({ bedChoosed: "" });
      return;
    }
    let min = 99999,
      max = -99999;
    for (let i = 0; i < cs.length; i++) {
      let num = +cs[i].split("_")[1];
      if (num < min) {
        min = num;
      }
      if (num > max) {
        max = num;
      }
    }
    // get index of min, max
    this.setState({
      bedChoosed: cs[0].split("_")[0],
      startTimeChoosed: timeString[min].split(" - ")[0],
      endTimeChoosed: timeString[max].split(" - ")[1]
    });
    // 15:31 - 16:00
  }

  async setSelected(e, j) {
    let bed = "" + e.id + "_" + j;
    let b = "" + e.id + "_";
    // Check can not book cross bed (More than 1 bed)
    let isCross = false;
    for (let i = 0; i < this.state.curSelected.length; i++) {
      if (this.state.curSelected[i].indexOf(b) === -1) {
        // console.log('check bet '+b+' , '+this.state.curSelected[i])
        // console.log('is Cross bed')
        swal("ผิดพลาด!", "ไม่สามารถจองหลายเตียงได้", "error", {
          buttons: false,
          timer: 2000
        });
        isCross = true;
        break;
      }
    }

    let isSort = false;
    if (isCross) return false;
    else {
      // Check book index much sorted like => 1,2,3 || 10,11
      for (let i = 0; i < this.state.curSelected.length; i++) {
        if (
          +j === +this.state.curSelected[i].split("_")[1] + 1 ||
          +j === +this.state.curSelected[i].split("_")[1] - 1
        ) {
          // ok
          isSort = true;
          break;
        }
      }
    }

    if (!isSort && this.state.curSelected.length > 0) {
      swal("ผิดพลาด!", "ไม่สามารถจองข้ามเวลาได้", "error", {
        buttons: false,
        timer: 2000
      });
      return false;
    }
    // console.log('Sel e is ', e)
    // console.log('j is ', j)
    await this.setState({
      // <bedID>_<timeArrayIndex>
      curSelected: [...this.state.curSelected, bed]
    });
    // console.log('curSelected is : ', this.state.curSelected);
    this.setTimeChoosed();
    return true;
  }

  setUnSelected = async (e, j) => {
    // Check only can unselected on 1stIndex and lastIndex
    // find max, min
    let cs = this.state.curSelected;
    let index = -1;

    let min = 99999,
      max = -99999;
    cs.forEach((ei, i) => {
      // <bedID>_<timeArrayIndex>
      if (ei === "" + e.id + "_" + j) {
        // console.log('i found is ', i)
        index = i;
      }
      let num = +ei.split("_")[1];
      if (num < min) min = num;
      if (num > max) max = num;
    });
    if (j === min || j === max) {
      // console.log('max is ', max)
      // console.log('min is ', min)
      // console.log('index is ', index)
      let a = cs.slice(0, index);
      // console.log('a is ', a)
      let b = cs.slice(index + 1, this.state.curSelected.length);
      // console.log('b is ', b)
      await this.setState({
        curSelected: a.concat(b)
      });
      // console.log('curSelected is ', this.state.curSelected)
      this.setTimeChoosed();
      return true;
    } else {
      swal("ผิดพลาด!", "ไม่สามารถยกเลิกจองข้ามเวลาได้", "error", {
        buttons: false,
        timer: 2000
      });
      return false;
    }
  };

  createBedArray(isNew) {
    let { status, bookDate } = this.state;
    let displayBed;
    let d = [];
    let today = moment().format("YYYY-MM-DD");

    let todayInt = today.split("-");
    todayInt = +todayInt[0] * 365 + +todayInt[1] * 30 + +todayInt[2];
    let bookdateInt = bookDate.split("-");
    bookdateInt = +bookdateInt[0] * 365 + +bookdateInt[1] * 30 + +bookdateInt[2];

    // if isNew go check date and time for disable booking
    // if same day need to check time, else not need to check time

    if (isNew) {
      // is new is mean no data in that selected date
      // console.log('is not have')
      // set displayBed to init
      displayBed = this.state.initDateTimeArray;
      // if is old day
      // console.log('bookdate is ' + bookdateInt + ' , curdate is ' + todayInt)
      if (bookdateInt < todayInt) {
        // is old date
        //set all can not book
        displayBed.forEach((e, i) => {
          let s = [];
          e.timeArray.forEach((e2, j) => {
            s.push(
              <BedItem
                isDisable={this.props.isDisable}
                status={-2}
                statusTypes={status}
                number={e.id}
                setSelected={() => this.setSelected(e, j)}
                setUnSelected={() => this.setUnSelected(e, j)}
              />
            );
          });
          d.push(s);
        });
      } else {
        // is today or future
        // set can book
        if (todayInt === bookdateInt) {
          // if today
          // have to check cur time
          // if still in time set can book but not in time set cannot book
          // console.log('------- is Today -------')
          displayBed.forEach((e, i) => {
            let s = [];
            e.timeArray.forEach((e2, j) => {
              s.push(
                <BedItem
                  isDisable={this.props.isDisable}
                  status={e2.start <= this.state.initCurTime ? -2 : -1}
                  statusTypes={status}
                  number={e.id}
                  setSelected={() => this.setSelected(e, j)}
                  setUnSelected={() => this.setUnSelected(e, j)}
                />
              );
            });
            d.push(s);
          });
          // console.log('------------------------')
        } else {
          // is future
          displayBed.forEach((e, i) => {
            let s = [];
            e.timeArray.forEach((e2, j) => {
              s.push(
                <BedItem
                  isDisable={this.props.isDisable}
                  status={-1}
                  statusTypes={status}
                  number={e.id}
                  setSelected={() => this.setSelected(e, j)}
                  setUnSelected={() => this.setUnSelected(e, j)}
                />
              );
            });
            d.push(s);
          });
        }
      }
    } else {
      // is mean have some booked in that date
      // console.log('is have')
      if (bookdateInt < todayInt) {
        // is old date
        // set all can not book with still have old status booked
        displayBed = this.state.displayBed;
        displayBed.forEach((e, i) => {
          let s = [];
          e.timeArray.forEach((e2, j) => {
            s.push(
              <BedItem
                isDisable={this.props.isDisable}
                status={e2.status !== -1 ? e2.status : -2}
                statusTypes={status}
                number={e.id}
                setSelected={() => this.setSelected(e, j)}
                setUnSelected={() => this.setUnSelected(e, j)}
              />
            );
          });
          d.push(s);
        });
      } else {
        // is today of future
        // set can book
        if (todayInt === bookdateInt) {
          // if today
          // have to check cur time
          // if still in time set can book but not in time set cannot book
          // console.log('is have')
          if (this.state.displayBed && this.state.displayBed.length > 0) {
            displayBed = this.state.displayBed;
            // console.log('ready')
            displayBed.forEach((e, i) => {
              let s = [];
              e.timeArray.forEach((e2, j) => {
                let curStatus;
                if (e2.status === -1) {
                  if (e2.start <= this.state.initCurTime) {
                    curStatus = -2;
                  } else {
                    curStatus = -1;
                  }
                } else {
                  curStatus = e2.status;
                }
                s.push(
                  <BedItem
                    isDisable={this.props.isDisable}
                    status={curStatus}
                    statusTypes={status}
                    number={e.id}
                    setSelected={() => this.setSelected(e, j)}
                    setUnSelected={() => this.setUnSelected(e, j)}
                  />
                );
              });
              d.push(s);
            });
          }
        } else {
          // is future
          // set can book all with old status
          displayBed = this.state.displayBed;
          displayBed.forEach((e, i) => {
            let s = [];
            e.timeArray.forEach((e2, j) => {
              s.push(
                <BedItem
                  isDisable={this.props.isDisable}
                  status={e2.status !== -1 ? e2.status : -1}
                  statusTypes={status}
                  number={e.id}
                  setSelected={() => this.setSelected(e, j)}
                  setUnSelected={() => this.setUnSelected(e, j)}
                />
              );
            });
            d.push(s);
          });
        }
      }
    }
    this.setState({ displayBedComponent: d });
  }

  OnChangeDate = async newDate => {
    // let init = this.state.initDateTimeArray
    // console.log('init is ', init)
    await this.setState({ bookDate: newDate });
    // clear data 1st
    this.GetBooking();
  };

  render() {
    let { isReady, displayBedComponent } = this.state;
    let { isDisable } = this.props;

    if (isReady) {
      return (
        <div style={{ marginTop: -10 }} className="bg-white rounded">
          {/* <div className="py-4 px-2">
                  { !isDisable &&
                    <h4 className="ml-2" style={{color: Color.Blue}}>เลือกเตียง</h4>
                  }
                  
              </div> */}
          <div className="px-3">
            <Table_2 head={timeString} rows={displayBedComponent} />
          </div>
          {!isDisable && (
            <div className="d-flex justify-content-between px-2 align-items-center">
              {/* <div className="d-flex">
                          <Label className="mt-2" style={{width: 100}}>เวลานัดหมาย</Label>
                          <Input onChange={(e)=>this.OnChangeDate(e.target.value)} value={bookDate} className="mt-1" style={{width: '55%'}} type="date" />
                      </div> */}
              <div />
              <div className="d-flex mt-2">
                <div>
                  {this.state.bedChoosed && this.state.bedChoosed !== "" ? (
                    <p className="pr-2 mt-1">
                      เลือก เตียง {this.state.bedChoosed} เวลา {this.state.startTimeChoosed}
                      {" - "}
                      {this.state.endTimeChoosed}
                    </p>
                  ) : (
                    <p className="pr-2 mt-1">กรุณาเลือกเตียง</p>
                  )}
                </div>
                <Link
                  to={{
                    pathname: "/home/addnew",
                    state: { bedBooked: this.state.curSelected }
                  }}
                  // onClick={()=>console.log('curSelected :', this.state.curSelected)}
                  className="mx-3 mb-3 rounded d-flex justify-content-center align-items-center text-white button-blue-hover"
                  style={{
                    textDecoration: "none",
                    backgroundColor: Color.Blue,
                    cursor: "pointer",
                    paddingLeft: 8,
                    paddingRight: 8,
                    paddingTop: 2,
                    paddingBottom: 2
                  }}
                >
                  ทำการจอง
                </Link>
              </div>
            </div>
          )}
        </div>
      );
    } else {
      return <div>Loading...</div>;
    }
  }
}
