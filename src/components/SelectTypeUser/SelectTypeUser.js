import React, { Component } from 'react';
import { Form, Label, Input, FormGroup } from 'reactstrap';
import functions from '../../function/function';
import './Styled/style.css';

export default class SelectTypeUser extends Component {
  render() {
    let { text, name, value, getType, options, width } = this.props;
    return (
      <Form className="d-flex justify-content-between align-items-center mr-2">
        <FormGroup controlId="formBasicText" style={width ? { width: width } : { width: '100%' }}>
          <Label className="LabelType">{text}</Label>
          <Input type="select" bsClass="FormType" name={name} placeholder={text} onChange={getType} value={value}>
            {options.map((d, i) => (
              <option key={i.toString()} value={d.role_id}>
                {functions.getRole(d.role_name)}
              </option>
            ))}
          </Input>
        </FormGroup>
      </Form>
    );
  }
}
