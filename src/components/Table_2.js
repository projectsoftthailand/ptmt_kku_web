import React, { Component } from 'react'
import {Table} from 'reactstrap'

export default class Table_2 extends Component {
  render() {      
    let {head, rows} = this.props

    return (
    <Table responsive>
        { head &&
        <thead>
            <tr style={{}}>
            {head.map((el, index)=> (
                <th style={{borderBottomWidth: 0, padding: 5, backgroundColor: 'white', whiteSpace: 'pre'}} key={'a'+index}>{el}</th>
            ))}
            </tr>
        </thead>
        }
        <tbody>
            {rows.map((row, index)=> (
                <tr style={{borderWidth: 0}} key={'b'+index}>
                    {row.map((d, index)=> (
                        <td key={'c'+index} style={{padding: 0, backgroundColor: 'white', borderLeftWidth: 0, borderRightWidth: 0}}>{d}</td>
                    ))}
                </tr>
            ))}
        </tbody>
    </Table>
    )
  }
}
