import React, { Component } from "react";
import CardProgramHistory from "../Card/CardProgramHistory/CardProgramHistory";
import { Label } from "reactstrap";
import "./Styled/style.css";

export default class TableDetail extends Component {
  render() {
    let { unit, status, id, list_id } = this.props.match.params;
    return (
      <div id="addcustomer-top" className="pb-3 px-4">
        <Label className="btn-back label-back pb-2" onClick={() => this.props.history.goBack()}>
          {"< กลับ"}
        </Label>
        <CardProgramHistory id={id} list_id={list_id} unit={Number(unit)} status={Number(status)} />
      </div>
    );
  }
}
