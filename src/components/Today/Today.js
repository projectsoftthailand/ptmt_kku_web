import React, { Component } from 'react';
import { FaListAlt, FaCheck, FaMapMarkerAlt } from 'react-icons/fa';
import { Table, Row, Col, Label } from 'reactstrap';
import { Route, withRouter } from 'react-router-dom';
import Responsive from 'react-responsive';
import ReactLoading from 'react-loading';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import moment from 'moment';
import ChangeStatus from '../ChangeStatus/ChangeStatus';
import { POST, GET, GET_LIST, GET_TYPE, GET_UNIT, GET_STATUS, DONE_LIST_BY_DOCTOR } from '../../service/service';
import SelectWithText from '../SelectWithText/SelectWithText';
import InputWithText from '../InputWithText/InputWithText';
import { getListID } from '../../function/function';
import User from '../../mobx/user/user';
import Color from '../Color';
import Pagination from '../Pagination';
import './Styled/style.css';

// let Mobile = (props) => <Responsive {...props} maxWidth={767} />;
// let Tablet = (props) => <Responsive {...props} minWidth={768} maxWidth={1090} />;
// let Desktop = (props) => <Responsive {...props} minWidth={1091} />;

let allListData = [];
let months = [
	{
		id: 0,
		name: 'เดือนทั้งหมด'
	},
	{
		id: 1,
		name: 'มกราคม'
	},
	{
		id: 2,
		name: 'กุมภาพันธ์'
	},
	{
		id: 3,
		name: 'มีนาคม'
	},
	{
		id: 4,
		name: 'เมษายน'
	},
	{
		id: 5,
		name: 'พฤษภาคม'
	},
	{
		id: 6,
		name: 'มิถุนายน'
	},
	{
		id: 7,
		name: 'กรกฏาคม'
	},
	{
		id: 8,
		name: 'สิงหาคม'
	},
	{
		id: 9,
		name: 'กันยายน'
	},
	{
		id: 10,
		name: 'ตุลาคม'
	},
	{
		id: 11,
		name: 'พฤศจิกายน'
	},
	{
		id: 12,
		name: 'ธันวาคม'
	}
];
let start = 2000;
let end = new Date().getFullYear();
let years = [ { name: 'ปีทั้งหมด', id: 0 } ];
for (var year = start; year <= end; year++) {
	years.push({ name: year, id: year });
}
let date_start = 1;
let date_end = 31;
let dates = [ { name: 'วันที่ทั้งหมด', id: 0 } ];
for (var date = date_start; date <= date_end; date++) {
	dates.push({ name: date, id: date });
}

let numberPage = [ 10, 15, 20, 25, 30, 35, 40, 45, 50 ];
let head = [ 'ว/ด/ป', 'เวลา', 'รหัส', 'ประเภทบริการ', 'หน่วยบริการ', 'ชื่อ', 'นามสกุล', '', 'สถานะ' ];

let special = /[$^[\]{};'"\\|<>@#]/;

// @withRouter
class Today extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modal: false,
			status_id: '',
			status_now: '',
			list_now: '',
			all_status_to_change: [],

			ListData: [],
			type: [ 'การให้บริการทั้งหมด' ],
			unit: [ 'หน่วยบริการทั้งหมด' ],
			status: [ 'สถานะทั้งหมด' ],
			loading: true,

			currentPage: 1,
			pageSize: 10,

			searchType: 'การให้บริการทั้งหมด',
			searchUnit: 'หน่วยบริการทั้งหมด',
			searchStatus: 0,
			searchText: '',
			date: 0,
			month: 0,
			year: 0
		};
	}

	async componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.props.onRef(this);
		await this.GetType();
		await Promise.all([ this.GetUnit(), this.GetStatus(), this.GetStatusToChange(), this.GetList() ]);
		await this.setState({ loading: false });
	}
	initData(res) {
		this.setState({ ListData: res, currentPage: 1 });
	}

	GetList = async () => {
		try {
			let { Today, History, AllTable, id } = this.props;
			// console.log('History',History);
			let { role, user_id } = User;
			let now = moment().format('YYYY-MM-DD');
			let curDate = Today ? now : 'all';
			let res = [];
			res = await GET(GET_LIST('all', curDate));
			// console.log('res', res);
			allListData = res.result
				// .filter((el) => (role !== 'frontMT' && role !== 'frontPT' ? el.doctor === user_id : el))
				.filter((el) => (role === 'pt' ? el.doctor === user_id : el))
				.filter(
					(el) =>
						role === 'mt' || role === 'frontMT' ? el.type === 'mt' : el.type === 'pt' || el.type === 'tm'
				)
				.filter((el) => (Today || AllTable ? el.service_status <= 4 : el))
				.filter((el) => (AllTable ? moment(el.service_at).format('YYYY-MM-DD') >= now : el))
				.filter(
					(el) => (History && moment(el.service_at).format('YYYY-MM-DD') === now ? el.service_status > 2 : el)
				)
				// .filter(
				// 	(el) =>
				// 		History
				// 			? moment(el.service_at).format('YYYY-MM-DD') <= now
				// 			: moment(el.service_at).format('YYYY-MM-DD') >= now
				// )
				.filter((el) => (id == undefined ? el : el.user_id == id))
				.sort(
					(a, b) =>
						moment(a.service_at) < moment(b.service_at)
							? 1
							: moment(b.service_at) < moment(a.service_at) ? -1 : 0
				);
			// console.log('allListData', allListData);
			this.initData(allListData);
		} catch (err) {
			// console.log(err);
		}
	};

	GetType = async () => {
		try {
			let d = this.state.type;
			let res = await GET(GET_TYPE);
			let dump = res.result;
			dump.forEach((e) => {
				if (e.type !== 'mt') {
					d.push(e.name);
				}
			});
			this.setState({ type: d });
		} catch (err) {
			// console.log(err);
		}
	};

	GetUnit = async () => {
		try {
			let d = this.state.unit;
			let res = await GET(GET_UNIT);
			let dump = res.result;
			dump.forEach((e) => d.push(e.name));
			this.setState({ unit: d });
		} catch (err) {
			// console.log(err);
		}
	};

	GetStatus = async () => {
		try {
			let res = await GET(GET_STATUS);
			let dump = res.result;
			dump.unshift({ id: 0, name: 'สถานะทั้งหมด' });
			this.setState({ status: dump });
		} catch (err) {
			// console.log(err);
		}
	};

	GetStatusToChange = async () => {
		try {
			let res = await GET(GET_STATUS);
			let dump = res.result;
			this.setState({ all_status_to_change: dump });
		} catch (err) {
			// console.log(err);
		}
	};

	done_list_by_doctor = (list_id) => {
		swal({
			title: 'ท่านต้องการอัพเดทสถานะ?',
			icon: 'warning',
			buttons: true
		}).then(async (yes) => {
			if (yes) {
				let res = await GET(DONE_LIST_BY_DOCTOR(list_id));
				if (res.success) {
					swal('สำเร็จ!', 'เปลี่ยนสถานะสำเร็จ', 'success', {
						buttons: false,
						timer: 2000
					}).then(() => {
						this.componentWillMount();
					});
				} else {
					swal('ไม่สำเร็จ!', res.message, 'error', {
						buttons: false,
						timer: 2000
					});
				}
			} else {
				swal('ยกเลิกสำเร็จ!');
			}
		});
	};

	getSelectType = (e) => {
		let target = e.target;
		let value = target.value;
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSelectUnit = (e) => {
		let target = e.target;
		let value = target.value;
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSelectStatus = (e) => {
		let target = e.target;
		let value = Number(target.value);
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSelectDate = (e) => {
		let target = e.target;
		let value = Number(target.value);
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSelectMonth = (e) => {
		let target = e.target;
		let value = Number(target.value);
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSelectYear = (e) => {
		let target = e.target;
		let value = Number(target.value);
		let name = target.name;
		this.setState({ [name]: value }, () => this.submitSearch());
	};
	getSearchText = (e) => {
		let target = e.target;
		let value = target.value;
		let lowerValue = value.trim().toLowerCase();
		let name = target.name;
		this.setState({ searchText: lowerValue });
		if (special.test(lowerValue)) {
			swal('ตัวอักษรพิเศษ!', 'ไม่สามารถกรอกตัวอักษรพิเศษได้', 'warning', {
				buttons: false,
				timer: 2000
			}).then(this.setState({ searchText: '' }));
		} else {
			this.setState({ [name]: value }, () => this.submitSearch());
		}
	};

	submitSearch = () => {
		let { searchType, searchUnit, searchStatus, searchText, month, year, date } = this.state;
		// console.log('allListData', allListData);
		let res = allListData
			.filter((el) => (searchType !== 'การให้บริการทั้งหมด' ? el.type_name === searchType : el))
			.filter((el) => (searchUnit !== 'หน่วยบริการทั้งหมด' ? el.unit_name === searchUnit : el))
			.filter((el) => (searchStatus !== 0 ? el.service_status === searchStatus : el))
			.filter((el) => (date !== 0 ? Number(el.service_at.split('-')[2].split(' ')[0]) === date : el))
			.filter((el) => (month !== 0 ? Number(el.service_at.split('-')[1]) === month : el))
			.filter((el) => (year !== 0 ? Number(el.service_at.split('-')[0]) === year : el))
			.filter(
				(el) =>
					(el.ln && el.ln.toString().match(searchText)) ||
					el.th_name.toLowerCase().match(searchText) ||
					el.th_lastname.toLowerCase().match(searchText) ||
					(el.list_id && el.list_id.toString().match(searchText))
			);
		this.initData(res);
	};

	searchStatusNow = (e) => {
		let { status } = this.state;
		let res = status.filter((el) => el.id === e);
		return res !== 0 ? res.map((el) => el.name) : 'นอกสถานที่';
	};

	classNameStatus = (e) => {
		return e === 1
			? 'button-status-1'
			: e === 2
				? 'button-status-2'
				: e === 3
					? 'button-status-3'
					: e === 4
						? 'button-status-4'
						: e === 5 ? 'button-status-5' : e === 99 ? 'button-status-99' : 'button-status-0';
	};

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};
	render() {
		let {
			modal,
			list_now,
			status_now,
			all_status_to_change,
			currentPage,
			ListData,
			pageSize,
			type,
			unit,
			status,
			loading,
			searchType,
			searchUnit,
			searchStatus,
			searchText,
			date,
			month,
			year
		} = this.state;
		let { Today, Path } = this.props;

		return (
			<div>
				<ChangeStatus
					statusNow={status_now}
					listNow={list_now}
					isOpenModal={modal}
					CloseModal={() => this.setState({ modal: false })}
					AllStatusToChange={all_status_to_change}
					refesh={() => this.GetList()}
				/>
				<div>
					<div className={'d-flex justify-content-between align-items-center my-2'}>
						{Today ? (
							<div className="LabelTodayTable">ตารางงานวันนี้</div>
						) : (
							<div className="LabelTodayTable">ตารางงานทั้งหมด</div>
						)}
						{Today ? (
							<Link to={'/home/table'} className="btn-all-table">
								ตารางงานทั้งหมด
							</Link>
						) : null}
					</div>
					<Row>
						{/* {(User.role == 'frontPT' || User.role == 'pt') && (
									<Col xs={3}>
										<SelectWithText
											name="searchType"
											text="ประเภทบริการ"
											options={type}
											onChange={this.getSelectType}
											value={searchType}
										/>
									</Col>
								)} */}
						<Col sm={12} md={6} lg={3}>
							<SelectWithText
								name="searchUnit"
								text="หน่วยบริการ"
								options={unit}
								onChange={this.getSelectUnit}
								value={searchUnit}
							/>
						</Col>
						<Col sm={12} md={6} lg={3}>
							<SelectWithText
								name="searchStatus"
								text="สถานะทั้งหมด"
								options={status}
								onChange={this.getSelectStatus}
								value={searchStatus}
								status={true}
							/>
						</Col>
						{Path === 'history' ? (
							<Col sm={12} md={12} lg={3} className="d-flex">
								<Col xs={4}>
									<SelectWithText
										name="date"
										text="วัน"
										options={dates}
										onChange={this.getSelectDate}
										value={date}
										status={true}
									/>
								</Col>
								<Col xs={4}>
									<SelectWithText
										name="month"
										text="เดือน"
										options={months}
										onChange={this.getSelectMonth}
										value={month}
										status={true}
									/>
								</Col>
								<Col xs={4}>
									<SelectWithText
										name="year"
										text="ปี"
										options={years}
										onChange={this.getSelectYear}
										value={year}
										status={true}
									/>
								</Col>
							</Col>
						) : User.role == 'frontPT' ||
						User.role == 'pt' ||
						User.role == 'frontMT' ||
						User.role !== 'mt' ? (
							<Col sm={12} md={12} lg={3} />
						) : null}
						<Col sm={12} md={12} lg={3}>
							<InputWithText
								name="searchText"
								text="ค้นหา"
								onChange={this.getSearchText}
								value={searchText}
							/>
						</Col>
					</Row>
				</div>

				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : ListData.sort(
					(a, b) =>
						Path === 'history'
							? moment(b.service_at) - moment(a.service_at)
							: moment(a.service_at) - moment(b.service_at)
				).length !== 0 ? (
					<div>
						<Table style={{ whiteSpace: 'pre', fontSize: '1.3rem' }} responsive striped hover>
							{head && (
								<thead>
									<tr>{head.map((el, index) => <th key={index.toString()}>{el}</th>)}</tr>
								</thead>
							)}
							<tbody>
								{ListData.slice(
									(currentPage - 1) * pageSize,
									currentPage * pageSize
								).map((el, index) => (
									<tr
										key={index.toString()}
										onClick={() =>
											this.props.history.push(
												Today
													? `/home/table/customer/${el.service_unit_id}/${el.service_status}/${el.user_id}/${el.list_id}`
													: `/history/table/customer/${el.service_unit_id}/${el.service_status}/${el.user_id}/${el.list_id}`
											)}
									>
										<td>{moment(el.service_at).format('DD MMM YYYY')}</td>
										<td>{moment(el.service_at).format('HH:mm')} น.</td>
										<td>
											{User.role == 'pt' || User.role == 'frontPT' ? (
												getListID(el.type, el.list_id)
											) : !el.ln ? (
												'-'
											) : (
												el.ln
											)}
										</td>
										{/* <td>{!el.ln ? '-' : el.ln}</td> */}
										<td>{el.type_name}</td>
										<td>{el.service_unit_id === 2 ? 'ศูนย์บริการในเมือง' : el.unit_name}</td>
										<td>{el.th_name}</td>
										<td>{el.th_lastname}</td>
										<td>
											{el.service_unit_id === 3 ? (
												<FaMapMarkerAlt className="icon-location" />
											) : null}
										</td>
										<td>
											<div
												onClick={() =>
													User.role === 'frontMT' || User.role === 'frontPT'
														? this.setState({
																modal: true,
																status_now: el.service_status,
																list_now: el.list_id
															})
														: null}
												className={this.classNameStatus(el.service_status)}
											>
												{el.service_unit_id === 3 && el.service_status === 1 ? (
													'นอกสถานที่'
												) : (
													this.searchStatusNow(el.service_status)
												)}
											</div>
										</td>
										{/* <td>
													<div className="row">
														<Link
															className="mx-2 icon-card"
															to={{
																pathname: Today
																	? `/home/table/customer/${el.service_unit_id}/${el.service_status}/${el.user_id}/${el.list_id}`
																	: `/history/table/customer/${el.service_unit_id}/${el.service_status}/${el.user_id}/${el.list_id}`
															}}
														>
															<FaListAlt className="icon-card" />
														</Link>

														{User.role !== 'frontMT' &&
														User.role !== 'frontPT' &&
														el.service_status <= 3 ? (
															<div
																className="mx-2 icon-card"
																onClick={() => this.done_list_by_doctor(el.list_id)}
															>
																<FaCheck />
															</div>
														) : null}
													</div>
												</td> */}
									</tr>
								))}
							</tbody>
						</Table>

						<div className="d-flex justify-content-between">
							<select
								className="select-page-size"
								name="pageSize"
								onChange={async (e) => {
									await this.setState({ [e.target.name]: +e.target.value });
								}}
							>
								{numberPage.map((el, i) => (
									<option key={i.toString()} value={el}>
										{el}
									</option>
								))}
							</select>
							<Pagination
								totalRecords={ListData.length}
								pageLimit={pageSize}
								pageNeighbours={2}
								onPageChanged={this.onPageChanged}
							/>
						</div>
					</div>
				) : (
					<div className="EmptyListShow">---ไม่พบข้อมูล---</div>
				)}
			</div>
		);
	}
}
export default withRouter(Today);
