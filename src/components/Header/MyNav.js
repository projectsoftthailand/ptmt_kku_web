import {
	Nav,
	Navbar,
	NavItem,
	NavLink,
	Collapse,
	NavbarBrand,
	NavbarToggler,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap';
import { withRouter, NavLink as RRNavLink } from 'react-router-dom';
import Responsive from 'react-responsive';
import React, { Component } from 'react';
import swal from 'sweetalert';
import { _ip, ip, GET, GET_PICTURE, GET_LIST, UPDATE_NOTIFY } from '../../service/service';
import User from '../../mobx/user/user';
import Color from '../Color';
import Logo from '../Logo';
import moment from 'moment';
import { FaBell } from 'react-icons/fa';
import socketIOClient from 'socket.io-client';
import { sevenDigits } from '../../function/function';

import './style.css';

const Mobile = (props) => <Responsive {...props} maxWidth={767} />;
const TabletL = (props) => <Responsive {...props} minWidth={837} maxWidth={1090} />;
const DesktopS = (props) => <Responsive {...props} minWidth={1091} maxWidth={1323} />;
const DesktopL = (props) => <Responsive {...props} minWidth={1324} />;
const DesktopBar = (props) => <Responsive {...props} minWidth={1293} />;
const TabletS = (props) => <Responsive {...props} minWidth={768} maxWidth={836} />;

const MaxNav = (props) => <Responsive {...props} maxWidth={324} />;
const MinNav = (props) => <Responsive {...props} minWidth={325} />;

const centerDivBig = {
	height: '100%',
	display: 'flex',
	marginRight: '1rem',
	justifyContent: 'center',
	alignItems: 'center'
};
const modifiers = {
	setMaxHeight: {
		enabled: true,
		fn: (data) => {
			return {
				...data,
				styles: {
					...data.styles,
					overflow: 'auto',
					maxHeight: 300
				}
			};
		}
	}
};

const Nav_Link = (link, title) => (
	<NavItem className="NavItem">
		<DesktopS>
			<NavLink to={`/${link}`} className="NavLink-S" activeClassName="NavLink-active" tag={RRNavLink}>
				{title}
			</NavLink>
		</DesktopS>
		<DesktopL>
			<NavLink to={`/${link}`} className="NavLink-L" activeClassName="NavLink-active" tag={RRNavLink}>
				{title}
			</NavLink>
		</DesktopL>
		<TabletL>
			<NavLink to={`/${link}`} className="NavLink-Fontsize-L" activeClassName="NavLink-active" tag={RRNavLink}>
				{title}
			</NavLink>
		</TabletL>
		<TabletS>
			<NavLink to={`/${link}`} className="NavLink-Fontsize-S" activeClassName="NavLink-active" tag={RRNavLink}>
				{title}
			</NavLink>
		</TabletS>
	</NavItem>
);

const Nav_Link_Mobile = (link, title) => (
	<NavItem className="NavItem">
		<NavLink
			to={link === 'profile' ? `/${link}/${User.user_id}` : `/${link}`}
			className="NavLinkMobile"
			style={{ fontSize: 'smaller' }}
			activeClassName="NavLinkMobile-active"
			tag={RRNavLink}
		>
			{title}
		</NavLink>
	</NavItem>
);

@withRouter
class MyNav extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.toggleNotify = this.toggleNotify.bind(this);
		this.toggleProfile = this.toggleProfile.bind(this);
		this.state = {
			isOpen: false,
			profileOpen: false,
			notifyOpen: false,
			notifyData: [],
			notifyLenght: null
		};
	}

	componentDidMount() {
		if (!User.auth) {
			this.props.history.push('/');
		} else {
			if (User.role !== 'marketing') {
				this.fetch();
			}
		}
		// this.response();
	}

	response() {
		socketIOClient(_ip).on('rerender', (res) => {
			this.fetch();
		});
	}

	componentWillUnmount() {
		socketIOClient(_ip).disconnect();
	}

	fetch = async () => {
		if (User.auth) {
			let res = await GET(GET_LIST('all', 'all'));
			let dates = moment(new Date()).format('YYYY-MM-DD 00:00:00');
			if (res.success) {
				let allNotify = res.result
					.filter(
						(el) =>
							User.role === 'admin' || User.role === 'frontPT' || User.role === 'frontMT'
								? el.isFrontNotify
								: User.role === 'pt' || User.role === 'mt' ? el.isDoctorNotify : el
					)
					.filter(
						(el) =>
							User.role === 'pt'
								? el.doctor === User.user_id
								: User.role === 'frontPT'
									? el.type === 'pt' || el.type === 'tm'
									: User.role === 'frontMT' || User.role === 'mt' ? el.type === 'mt' : el
					)
					.filter((el) => moment(el.service_at).valueOf() >= moment(dates).valueOf());
				let allId = [];
				allNotify.forEach((el) => {
					allId.push(el['list_id']);
				});
				this.setState({ notifyData: allNotify, notifyLenght: allNotify.length });
			}
		}
	};

	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	toggleProfile() {
		this.props.history.push(`/profile/${User.user_id}`);
	}

	toggleNotify = () => {
		this.setState({
			notifyOpen: !this.state.notifyOpen
		});
	};

	openNotify = async (id) => {
		if (this.state.notifyLenght > 0) {
			await GET(
				UPDATE_NOTIFY(
					User.role === 'frontPT' || User.role === 'admin'
						? 'front'
						: User.role === 'pt'
							? 'doctor'
							: User.role === 'mt' || User.role === 'frontMT' ? 'type_mt' : null,
					id
				)
			).then((res) => {
				if (res.success) {
					let { notifyData } = this.state;
					let index = notifyData.findIndex((el) => el.list_id === id);
					notifyData[index].isDoctorNotify = 0;
					notifyData[index].isFrontNotify = 0;
					this.setState({ notifyLenght: this.state.notifyLenght - 1, notifyData });
					let _n = notifyData[index];
					this.props.history.push(
						`/home/table/customer/${_n.service_unit_id}/${_n.service_status}/${_n.user_id}/${_n.list_id}`
					);
					window.location.reload();
				}
			});
		}
	};

	onLogout = async () => {
		try {
			await GET('user/logout').then((res) => {
				if (res.success) {
					swal('สำเร็จ!', 'ออกจากระบบสำเร็จ', 'success', {
						buttons: false,
						timer: 2000
					});
					User.logout();
					this.props.history.push('/');
				} else {
					swal('ผิดพลาด!', res.message, 'warning', {
						buttons: false,
						timer: 2000
					});
				}
			});
		} catch (err) {
			alert(err);
		}
	};

	NavbarLink = () => {
		switch (User.role) {
			case 'frontPT':
			case 'frontMT':
				return (
					<div className="d-flex">
						{Nav_Link('home', 'หน้าหลัก')}
						{Nav_Link('history', 'ประวัติ')}
						{Nav_Link('graph', 'กราฟ')}
						{Nav_Link('home-marketing', 'แบนเนอร์')}
						{Nav_Link('home-admin', 'บุคคลากร')}
						{Nav_Link('checklist', 'รายการตรวจ')}
						{Nav_Link('Income_document', 'รายได้และใบแจ้งหนี้')}
						{/* {User.role == 'frontMT' && Nav_Link("result", "ผลตรวจ")} */}
					</div>
				);
			case 'mt':
			case 'pt':
				return (
					<div className="d-flex">
						{Nav_Link('home', 'หน้าหลัก')}
						{Nav_Link('history', 'ประวัติ')}
						{User.role == 'mt' && Nav_Link('result', 'ผลตรวจ')}
					</div>
				);
			case 'admin':
				return (
					<div className="d-flex">
						{Nav_Link('home', 'หน้าหลัก')}
						{Nav_Link('add-member', 'เพิ่มบุคคลากร')}
						{Nav_Link('checklist', 'รายการตรวจ')}
						{Nav_Link('graph', 'กราฟ')}
					</div>
				);
			default:
				return null;
		}
	};

	NavbarBrand = () => {
		return (
			<NavbarBrand className="mr-auto" style={{ display: 'flex', alignItems: 'center' }} href="/home">
				<img src={Logo.logo} className="ImgLogo" alt="Logo" />
				<DesktopBar>
					<div>
						<b style={{ color: Color.White, fontSize: '1.1rem' }}>
							สถานบริการสุขภาพเทคนิคการแพทย์ และกายภาพบำบัด
						</b>
						<div style={{ color: Color.White, fontSize: '1rem' }}>คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น</div>
					</div>
				</DesktopBar>
				<Mobile>
					<div>
						<b style={{ color: Color.White, fontSize: '0.9rem' }}>
							สถานบริการสุขภาพเทคนิคการแพทย์ และกายภาพบำบัด
						</b>
						<div style={{ color: Color.White, fontSize: '0.8rem' }}>
							คณะเทคนิคการแพทย์ มหาวิทยาลัยขอนแก่น
						</div>
					</div>
				</Mobile>
			</NavbarBrand>
		);
	};

	NavbarLinkMobile = () => {
		switch (User.role) {
			case 'frontPT':
			case 'frontMT':
				return (
					<div>
						{Nav_Link_Mobile('home', 'หน้าหลัก')}
						{Nav_Link_Mobile('history', 'ประวัติ')}
						{Nav_Link_Mobile('graph', 'กราฟ')}
						{Nav_Link_Mobile('home-marketing', 'แบนเนอร์')}
						{Nav_Link_Mobile('home-admin', 'บุคคลากร')}
						{Nav_Link_Mobile('checklist', 'รายการตรวจ')}
						{Nav_Link_Mobile('Income_document', 'รายได้และใบแจ้งหนี้')}
						{Nav_Link_Mobile('profile', 'ข้อมูลส่วนตัว')}
						{/* {User.role == 'frontMT' && Nav_Link("result", "ผลตรวจ")} */}
					</div>
				);
			case 'mt':
			case 'pt':
				return (
					<div>
						{Nav_Link_Mobile('home', 'หน้าหลัก')}
						{Nav_Link_Mobile('history', 'ประวัติ')}
						{User.role == 'mt' && Nav_Link_Mobile('result', 'ผลตรวจ')}
						{Nav_Link_Mobile('profile', 'ข้อมูลส่วนตัว')}
					</div>
				);
			case 'admin':
				return (
					<div>
						{Nav_Link_Mobile('home', 'หน้าหลัก')}
						{Nav_Link_Mobile('add-member', 'เพิ่มบุคคลากร')}
						{Nav_Link_Mobile('checklist', 'รายการตรวจ')}
						{Nav_Link_Mobile('graph', 'กราฟ')}
						{Nav_Link_Mobile('profile', 'ข้อมูลส่วนตัว')}
					</div>
				);
			default:
				return null;
		}
	};
	// NavbarLink = () => {
	// 	switch (User.role) {
	// 		case 'frontPT':
	// 		case 'frontMT':
	// 			return (
	// 				<div className="d-flex">
	// 					{Nav_Link('home', 'หน้าหลัก')}
	// 					{Nav_Link('history', 'ประวัติ')}
	// 					{Nav_Link('graph', 'กราฟ')}
	// 					{Nav_Link('home-marketing', 'แบนเนอร์')}
	// 					{Nav_Link('home-admin', 'บุคคลากร')}
	// 					{Nav_Link('checklist', 'รายการตรวจ')}
	// 					{Nav_Link('Income_document', 'รายได้และใบแจ้งหนี้')}
	// 					{/* {User.role == 'frontMT' && Nav_Link("result", "ผลตรวจ")} */}
	// 				</div>
	// 			);
	// 		case 'mt':
	// 		case 'pt':
	// 			return (
	// 				<div className="d-flex">
	// 					{Nav_Link('home', 'หน้าหลัก')}
	// 					{Nav_Link('history', 'ประวัติ')}
	// 					{User.role == 'mt' && Nav_Link('result', 'ผลตรวจ')}
	// 				</div>
	// 			);
	// 		case 'admin':
	// 			return (
	// 				<div className="d-flex">
	// 					{Nav_Link('home', 'หน้าหลัก')}
	// 					{Nav_Link('add-member', 'เพิ่มบุคคลากร')}
	// 					{Nav_Link('checklist', 'รายการตรวจ')}
	// 					{Nav_Link('graph', 'กราฟ')}
	// 				</div>
	// 			);
	// 		default:
	// 			return null;
	// 	}
	// };

	// NavbarLinkMobile = () => {
	// 	switch (User.role) {
	// 		case 'frontPT':
	// 		case 'frontMT':
	// 			return (
	// 				<div>
	// 					{Nav_Link_Mobile('home', 'หน้าหลัก')}
	// 					{Nav_Link_Mobile('history', 'ประวัติ')}
	// 					{Nav_Link_Mobile('graph', 'กราฟ')}
	// 					{Nav_Link_Mobile('home-admin', 'บุคคลากร')}
	// 					{Nav_Link_Mobile('checklist', 'รายการตรวจ')}
	// 					{Nav_Link_Mobile('Income_document', 'รายได้และใบแจ้งหนี้')}
	// 					{/* {User.role == 'frontMT' && Nav_Link("result", "ผลตรวจ")} */}
	// 				</div>
	// 			);
	// 		case 'mt':
	// 		case 'pt':
	// 			return (
	// 				<div>
	// 					{Nav_Link_Mobile('home', 'หน้าหลัก')}
	// 					{Nav_Link_Mobile('history', 'ประวัติ')}
	// 					{User.role == 'mt' && Nav_Link_Mobile('result', 'ผลตรวจ')}
	// 					{/* {Nav_Link_Mobile('home-marketing', 'แบนเนอร์')} */}
	// 				</div>
	// 			);
	// 		case 'admin':
	// 			return (
	// 				<div>
	// 					{Nav_Link_Mobile('home', 'หน้าหลัก')}
	// 					{Nav_Link_Mobile('add-member', 'เพิ่มบุคคลากร')}
	// 					{Nav_Link_Mobile('checklist', 'รายการตรวจ')}
	// 					{Nav_Link_Mobile('graph', 'กราฟ')}
	// 				</div>
	// 			);
	// 		default:
	// 			return null;
	// 	}
	// };

	NavbarRight = () => {
		let { notifyData, notifyLenght, profileOpen, notifyOpen } = this.state;
		return (
			<div className="ml-auto d-flex">
				{User.role !== 'marketing' && User.role !== 'admin' ? (
					<NavItem style={centerDivBig}>
						<Dropdown isOpen={notifyOpen} toggle={this.toggleNotify}>
							<DropdownToggle style={centerDivBig} nav>
								<FaBell className="Noti-icon" />
								{notifyLenght ? <div className="Badge">{notifyLenght}</div> : null}
							</DropdownToggle>
							<DropdownMenu modifiers={modifiers} className="p-0 m-0" right>
								{typeof notifyData[0] !== 'undefined' ? (
									notifyData.map((el, i) => (
										<div
											key={i.toString()}
											style={
												((User.role === 'admin' ||
													User.role === 'frontPT' ||
													User.role === 'frontMT') &&
													el.isFrontNotify) ||
												((User.role === 'pt' || User.role === 'mt') && el.isFrontNotify) ? (
													{ backgroundColor: 'rgba(70, 122, 200, 0.2)' }
												) : (
													{}
												)
											}
											onClick={() => this.openNotify(el.list_id)}
										>
											<DropdownItem className="py-2">
												<div style={{ fontSize: '1.2rem' }}>
													<b>รหัส : </b>
													{User.role === 'frontPT' || User.role === 'pt' ? (
														el.type.toUpperCase() + sevenDigits(el.list_id)
													) : User.role === 'frontMT' || User.role === 'mt' ? !el.ln ? (
														'-'
													) : (
														el.ln
													) : null}
												</div>
												<div style={{ fontSize: '1.2rem' }}>
													<b>ชื่อ : </b>
													{el.th_name + ' ' + el.th_lastname} (
													{el.type === 'pt' ? (
														'บริการกายภาพบำบัด'
													) : el.type === 'tm' ? (
														'บริการนวดไทย'
													) : el.type === 'mt' ? (
														'บริการตรวจสุขภาพ'
													) : null}
													)
												</div>
												<div style={{ fontSize: '1.2rem' }}>
													<b>รับบริการ : </b>
													{moment(el.service_at).locale('th').format('DD MMM YYYY HH:mm น.')}
												</div>
											</DropdownItem>
											<DropdownItem divider className="p-0 m-0" />
										</div>
									))
								) : (
									<DropdownItem className="text-center"> --- ไม่มีการแจ้งเตือน ---</DropdownItem>
								)}
							</DropdownMenu>
						</Dropdown>
					</NavItem>
				) : null}
				<NavItem style={centerDivBig}>
					<Dropdown isOpen={profileOpen} toggle={this.toggleProfile}>
						<DropdownToggle style={centerDivBig} nav>
							<img src={ip + GET_PICTURE(User.picture || 'null')} className="Avatar" alt="avatar" />
							<div>
								<b style={{ color: Color.White, fontSize: '1.1rem' }}>{User.getFullname()}</b>
								<div style={{ color: Color.White, fontSize: '1.1rem' }}>{User.getRole()}</div>
							</div>
						</DropdownToggle>
						<DropdownMenu right>
							<DropdownItem header tag="div" className="text-center">
								<strong>{User.getFullname()}</strong>
							</DropdownItem>
							<DropdownItem href="">ข้อมูลผู้ใช้</DropdownItem>
						</DropdownMenu>
					</Dropdown>
				</NavItem>
				<NavItem style={centerDivBig}>
					<b className="ButtonLogout" onClick={this.onLogout}>
						ออกจากระบบ
					</b>
				</NavItem>
			</div>
		);
	};
	NavbarRightTablet = () => {
		let { notifyData, notifyLenght, profileOpen, notifyOpen } = this.state;
		return (
			<div className="ml-auto d-flex">
				<NavItem style={centerDivBig}>
					<Dropdown isOpen={profileOpen} toggle={this.toggleProfile}>
						<DropdownToggle style={centerDivBig} nav>
							<div style={{ color: Color.White, fontSize: 'smaller' }}>
								<b>{User.getFullname()}</b>
								<div>{User.getRole()}</div>
							</div>
						</DropdownToggle>
					</Dropdown>
				</NavItem>
				<NavItem style={centerDivBig}>
					<b className="ButtonLogout" style={{ fontSize: 'smaller' }} onClick={this.onLogout}>
						ออกจากระบบ
					</b>
				</NavItem>
			</div>
		);
	};

	render() {
		let { isOpen } = this.state;
		return (
			<div>
				<Navbar className="mainNav" dark expand="md">
					{this.NavbarBrand()}
					<NavbarToggler onClick={this.toggle} className="mr-3" />
					<Collapse isOpen={isOpen} navbar>
						<DesktopS>
							<Nav className="w-100" navbar>
								{this.NavbarLink()}
								{this.NavbarRight()}
							</Nav>
						</DesktopS>
						<DesktopL>
							<Nav className="w-100" navbar>
								{this.NavbarLink()}
								{this.NavbarRight()}
							</Nav>
						</DesktopL>
						<TabletL>
							<Nav className="w-100" navbar>
								{this.NavbarLink()}
								{/* {this.NavbarRight()} */}
								{this.NavbarRightTablet()}
							</Nav>
						</TabletL>
						<TabletS>
							<Nav className="w-100" navbar>
								{this.NavbarLink()}
								{/* {this.NavbarRight()} */}
								{this.NavbarRightTablet()}
							</Nav>
						</TabletS>
						<Mobile>
							<Nav className="w-100" navbar>
								<NavItem className="text-center" style={{ padding: '0.75rem 2.5rem' }}>
									<div style={{ color: 'white', fontSize: 'small', fontWeight: 'bold' }}>
										{User.getFullname()} ({User.getRole()})
									</div>
								</NavItem>
								{this.NavbarLinkMobile()}
								<NavItem className="d-flex justify-content-end" style={{ padding: '0.75rem 2.5rem' }}>
									<div onClick={this.onLogout} style={{ color: 'white', fontSize: 'small' }}>
										ออกจากระบบ
									</div>
								</NavItem>
							</Nav>
						</Mobile>
					</Collapse>
				</Navbar>
			</div>
		);
	}
}

export default MyNav;
