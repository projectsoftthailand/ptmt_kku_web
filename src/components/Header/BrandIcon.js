import React, { Component } from "react";
import Logo from "../Logo";
import "./style.css";

export default class BrandIcon extends Component {
  render() {
    return <img src={Logo.logo} className="ImgLogo" alt="Logo" />;
  }
}
