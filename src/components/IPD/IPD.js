import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FaIdCard } from 'react-icons/fa';
import { Table, Label } from 'reactstrap';
import { GET, GET_USERS } from '../../service/service';
import ReactLoading from 'react-loading';
import './Styled/style.css';
import Color from '../Color';
import swal from 'sweetalert';
import InputWithText from '../InputWithText/InputWithText';
import { sevenDigits } from '../../function/function';
import Pagination from '../Pagination';

let dumpList = [];
let numberPage = [ 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 ];
let head = [ 'รหัสผู้ป่วย', 'ชื่อ', 'นามสกุล', 'เบอร์โทร', '' ];
let special = /[$^[\]{};'"\\|<>@#]/;

export default class IPD extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ListUser: [],
			ListUserShow: [],

			unit: [],
			type: [],
			loading: true,

			searchType: '',
			searchText: '',

			currentPage: 1,
			pageSize: 10
		};
	}
	async componentWillMount() {
		this.props.onRef(this);
		await this.GetUsers();
		await this.setState({ loading: false });
	}
	GetUsers = async () => {
		try {
			let res = await GET(GET_USERS('all'));
			let path = window.location.pathname;
			let rows = [];
			res.result.filter((e) => path === '/home' && e.active === 1).filter((e) => e.role_id !== 1).forEach((e) => {
				let phone = e.phone.slice(0, 3) + '-' + e.phone.slice(3);
				//citizen_id, th_name, th_lastname, phone
				let id = sevenDigits(e.user_id);
				rows.push([ id, e.th_name, e.th_lastname, phone, e.user_id ]);
			});
			this.setState({ ListUser: rows, ListUserShow: rows });
			dumpList = res.result;
		} catch (error) {}
	};

	searchData = (e) => {
		let event = e.target.value;
		let searchText = event.trim().toLowerCase();
		this.setState({ searchText: searchText });
		if (special.test(searchText)) {
			swal('ตัวอักษรพิเศษ!', 'ไม่สามารถกรอกตัวอักษรพิเศษได้', 'warning', {
				buttons: false,
				timer: 2000
			}).then(this.setState({ searchText: '' }));
		} else {
			let res = dumpList.filter((el) => {
				return (
					el.user_id === Number(searchText) ||
					el.th_name.toLowerCase().match(searchText) ||
					el.th_lastname.toLowerCase().match(searchText) ||
					el.phone.toLowerCase().match(searchText)
				);
			});
			this.mapData(res);
		}
	};

	mapData(res) {
		let rows = [];
		res.forEach((e) => {
			let phone = e.phone.slice(0, 3) + '-' + e.phone.slice(3);
			let id = sevenDigits(e.user_id);
			rows.push([ id, e.th_name, e.th_lastname, phone, e.user_id ]);
		});
		this.setState({ ListUser: rows, ListUserShow: rows });
	}

	reRender() {
		this.componentWillMount();
	}

	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};

	render() {
		let { ListUserShow, loading, currentPage, pageSize, searchText } = this.state;
		return (
			<div>
				<div className="d-flex justify-content-between align-items-center my-2">
					<div className="LabelTodayTable">รายชื่อผู้ป่วยในระบบ</div>
					<Link to={'/home/addnew'} className="btn-all-table">
						เพิ่มลูกค้าใหม่
					</Link>
				</div>
				<div className="search mx-2">
					<InputWithText name="searchTextUser" onChange={this.searchData} value={searchText} />
				</div>
				{loading ? (
					<div className="loading d-flex flex-column">
						<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
						<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
					</div>
				) : ListUserShow.length !== 0 ? (
					<div>
						<Table style={{ whiteSpace: 'pre', fontSize: '1.3rem' }} responsive striped>
							<thead>{head && <tr>{head.map((el, index) => <th key={'a' + index}>{el}</th>)}</tr>}</thead>
							<tbody>
								{ListUserShow.slice(
									(currentPage - 1) * pageSize,
									currentPage * pageSize
								).map((row, index) => (
									<tr key={'b' + index}>
										{row.map((d, index) => {
											if (index < 4) {
												return <td key={'c' + index}>{d}</td>;
											} else {
												return (
													<td key={'c' + index}>
														<Link
															to={{
																pathname: '/home/addnew/' + d
															}}
														>
															<FaIdCard className="icon-card" />
														</Link>
													</td>
												);
											}
										})}
									</tr>
								))}
							</tbody>
						</Table>
						<div className="d-flex justify-content-between">
							<select
								className="select-page-size"
								name="pageSize"
								onChange={async (e) => {
									await this.setState({ [e.target.name]: +e.target.value });
								}}
							>
								{numberPage.map((el, i) => (
									<option key={i} value={el}>
										{el}
									</option>
								))}
							</select>
							<Pagination
								totalRecords={ListUserShow.length}
								pageLimit={pageSize}
								pageNeighbours={2}
								onPageChanged={this.onPageChanged}
							/>
						</div>
					</div>
				) : (
					<div className="EmptyListShow">---ไม่พบข้อมูล---</div>
				)}
			</div>
		);
	}
}
