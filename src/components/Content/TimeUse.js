import Time from './Time.js'

let TimeStartStringArray = []
let TimeEndStringArray = []

let TimeStartIntArray = []
let TimeEndIntArray = []

Time.forEach((e) => {
    let t = e.split(' - ')
    TimeStartStringArray.push(t[0])
    TimeEndStringArray.push(t[1])

    let m1 = t[0].split(':')
    TimeStartIntArray.push( (+m1[0])*60+(+m1[1]) )
    let m2 = t[1].split(':')
    TimeEndIntArray.push( (+m2[0])*60+(+m2[1]) )
})

export const timeStartStringArray = TimeStartStringArray
export const timeEndStringArray = TimeEndStringArray
export const timeStartIntArray = TimeStartIntArray
export const timeEndIntArray = TimeEndIntArray

export const dateInt = (dateString) => {
    //YYYY-MM-DD
    let d = dateString.split('-')
    let yy = (+d[0])*365
    let mm = (+d[1])*30
    return yy+mm+(+d[2])
} 