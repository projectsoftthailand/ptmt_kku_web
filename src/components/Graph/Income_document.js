import React, { Component } from 'react';
import './Styled/Graph.css';
import {
	Label,
	Input,
	FormGroup,
	Table,
	Row,
	Col,
	Container,
	InputGroup,
	InputGroupAddon,
	Button,
	Form
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import { registerLocale, setDefaultLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { GET, GET_YEAR_INCOME, GET_USERS, GET_YEAR_INCOME_ALL, INVOICE, UP_INVOICE, POST } from '../../service/service';
import moment from 'moment';
import { getRoleName } from '../../function/function';
import { convert_status } from '../../function/function';
import XlsExport from 'xlsexport';
import Pagination from '../Pagination';
import User from '../../mobx/user/user';
import th from 'date-fns/locale/th';
import { Pays, PaysAll, PdfInvoice, docKet } from '../../function/PDF';
import swal from 'sweetalert';
import Color from '../Color';
import ReactLoading from 'react-loading';
import { units, types, months, DMYmt, DMYpt, Invoice_claim, DDMMYYS, status_claim } from '../../const/subRole';

registerLocale('th', th);

export default class Income_document extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			checkDate: 2,
			startDate: '',
			endDate: '',
			years: [],
			data_income_document: [],
			Service_type: User.role === 'frontMT' ? 'mt' : 'pt',
			month_month: moment(new Date()).month() + 1,
			year_month: moment(new Date()).year(),
			year_year: moment(new Date()).year(),
			data_mapAll: [],
			Service_unit: 'ศูนย์บริการมหาวิทยาลัยขอนแก่น',
			claim_filter: 999,
			pageSize: 15,
			currentPage: 1,
			nameDocter: '',
			ontime: 1,
			all_docter: [],
			invoice: 0,
			president: '',
			manager: '',
			inspector: '',
			claim_official: 1,
			dataInvoice: []
			// invoice: 0
		};
	}
	componentDidMount = async () => {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		await Promise.all([ this.getYears(), this.Getres_year(), this.Invoice() ]);
		await this.setState({ loading: false });
	};
	getYears = () => {
		var start = 2019;
		var end = new Date().getFullYear();
		var options = [];
		for (var year = start; year <= end; year++) {
			options.push({ year: year, value: year });
		}
		this.setState({
			years: options
		});
	};
	Invoice = async () => {
		try {
			let res = await GET(INVOICE);
			this.setState({
				president: res.result[0].name,
				manager: res.result[1].name,
				inspector: res.result[2].name
			});
		} catch (err) {}
	};
	Up_Invoice = async (id) => {
		try {
			let { president, manager, inspector } = this.state;
			let obj = {
				id: id,
				name: id === 1 || id === 4 ? president : id === 2 || id === 5 ? manager : inspector
			};
			let res = await POST(UP_INVOICE, obj);
			if (res.success) {
				swal(res.massage, '', 'success').then(() => this.Invoice());
			} else {
				swal(res.massage, '', 'error');
			}
		} catch (err) {}
	};
	Getres_year = async () => {
		try {
			let years = moment(new Date()).format('YYYY');
			let claim = 'all';
			let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
			let getuser = await GET(GET_USERS('all'));
			// console.log('getuser :', getuser);
			if (res_year.success) {
				let { Service_unit, ontime } = this.state;
				let data_mt = res_year.result.mt.data;
				let data_pt = res_year.result.pt.data;
				let data_tm = res_year.result.tm.data;
				let children = data_mt.concat(data_pt);
				let data_All = children.concat(data_tm);
				// console.log('data_mt :', data_mt);
				// console.log('data_pt :', data_pt);
				// console.log('data_tm :', data_tm);
				// console.log("data_All :", data_All);
				this.setState({
					all_docter: getuser.result
						.filter((e) => (User.role === 'frontMT' ? e.role_id === 3 : e.role_id === 2))
						.map((e) => ({ fullname: e.th_name + ' ' + e.th_lastname })),
					data_income_document: res_year.result.mt.data,
					data_mapAll: data_All
						.filter((e) => e.service_status === 5)
						.map((e) => ({
							...e,
							namedocter: getuser.result.filter((el) => Number(el.user_id) === Number(e.doctor))
						}))
						.filter((e) => Number(e.service_ontime) === Number(ontime))
						.filter((e) => e.unit_name === Service_unit)
						.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type === 'pt'))
						.map((e) => ({
							...e,
							fullname:
								e.namedocter.length > 0
									? e.namedocter[0].th_name + ' ' + e.namedocter[0].th_lastname
									: '-'
						}))
				});
			}
		} catch (err) {}
	};
	handleChangestartDate = (date) => {
		this.setState({
			startDate: date
		});
	};
	handleChangeendDate = (date) => {
		this.setState({
			endDate: date
		});
	};
	DateFilter = async () => {
		this.setState({ loading: true });
		let { checkDate, Service_type, Service_unit, claim_filter, nameDocter, ontime } = this.state;
		if (Number(checkDate) == 2) {
			let years = moment(new Date()).format('YYYY');
			let claim = 'all';
			try {
				let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
				let getuser = await GET(GET_USERS('all'));
				///--------------------------------------------------------------------
				if (res_year.success) {
					let data_mt = res_year.result.mt.data;
					let data_pt = res_year.result.pt.data;
					let data_tm = res_year.result.tm.data;
					let children = data_mt.concat(data_pt);
					let data_All = children
						.concat(data_tm)
						.map((e) => ({
							...e,
							namedocter: getuser.result.filter((el) => Number(el.user_id) === Number(e.doctor))
						}))
						.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'))
						.map((e) => ({
							...e,
							fullname:
								e.namedocter.length > 0
									? e.namedocter[0].th_name + ' ' + e.namedocter[0].th_lastname
									: '-'
						}));
					let { startDate, endDate } = this.state;
					// console.log('service_at :', moment(data_income_document[0].service_at).format('DD'));
					let data = data_All
						.filter((e) => e.service_status === 5)
						.filter(
							(e) =>
								startDate || endDate
									? moment(e.service_at).format('YYYY-MM-DD') >=
											moment(startDate).format('YYYY-MM-DD') &&
										moment(e.service_at).format('YYYY-MM-DD') <= moment(endDate).format('YYYY-MM-DD')
									: e
							// moment(e.service_at).format('DD') >= date_s && moment(e.service_at).format('DD') <= date_e
						)
						.filter((e) => (Service_type !== 'all' ? e.type === Service_type : e))
						.filter((e) => (ontime ? Number(e.service_ontime) === Number(ontime) : e))
						.filter((e) => (Service_unit !== 'หน่วยบริการทั้งหมด' ? e.unit_name === Service_unit : e))
						.filter(
							(e) => (Number(claim_filter) !== 999 ? Number(e.service_claim) === Number(claim_filter) : e)
						)
						.filter(
							(e, i) =>
								nameDocter !== ''
									? String(e.fullname).toLowerCase().indexOf(String(nameDocter).toLowerCase()) > -1
									: e
						);
					this.setState({
						data_mapAll: data,
						loading: false
					});
				} else this.setState({ loading: false });
			} catch (error) {
				this.setState({ loading: false });
			}
		}
		if (Number(checkDate) == 3) {
			let years = moment(new Date()).format('YYYY');
			let claim = 'all';
			let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
			let getuser = await GET(GET_USERS('all'));
			///--------------------------------------------------------------------
			if (res_year.success) {
				let data_mt = res_year.result.mt.data;
				let data_pt = res_year.result.pt.data;
				let data_tm = res_year.result.tm.data;
				let children = data_mt.concat(data_pt);
				let data_All = children
					.concat(data_tm)
					.map((e) => ({
						...e,
						namedocter: getuser.result.filter((el) => Number(el.user_id) === Number(e.doctor))
					}))
					.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'))
					.map((e) => ({
						...e,
						fullname:
							e.namedocter.length > 0 ? e.namedocter[0].th_name + ' ' + e.namedocter[0].th_lastname : '-'
					}));
				let months = this.state.month_month;
				let year = this.state.year_month;
				// console.log('months', months);
				// console.log('year', year);
				// console.log('data_income_document', Number(moment(data_income_document[0].service_at).format('MM')));
				let data = data_All
					.filter((e) => e.service_status === 5)
					.filter((e) => {
						return (
							Number(moment(e.service_at).format('MM')) == months &&
							Number(moment(e.service_at).format('YYYY')) == year
						);
					})
					.filter((e) => (Service_type !== 'all' ? e.type === Service_type : e))
					.filter((e) => (ontime ? Number(e.service_ontime) === Number(ontime) : e))
					.filter((e) => (Service_unit !== 'หน่วยบริการทั้งหมด' ? e.unit_name === Service_unit : e))
					.filter(
						(e) => (Number(claim_filter) !== 999 ? Number(e.service_claim) === Number(claim_filter) : e)
					)
					.filter(
						(e, i) =>
							nameDocter !== ''
								? String(e.fullname).toLowerCase().indexOf(String(nameDocter).toLowerCase()) > -1
								: e
					);
				this.setState({
					data_mapAll: data
				});
			}
		}
		if (Number(checkDate) == 4) {
			let years = moment(new Date()).format('YYYY');
			let claim = 'all';
			let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
			let getuser = await GET(GET_USERS('all'));
			///--------------------------------------------------------------------
			if (res_year.success) {
				let data_mt = res_year.result.mt.data;
				let data_pt = res_year.result.pt.data;
				let data_tm = res_year.result.tm.data;
				let children = data_mt.concat(data_pt);
				let data_All = children
					.concat(data_tm)
					.map((e) => ({
						...e,
						namedocter: getuser.result.filter((el) => Number(el.user_id) === Number(e.doctor))
					}))
					.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'))
					.map((e) => ({
						...e,
						fullname:
							e.namedocter.length > 0 ? e.namedocter[0].th_name + ' ' + e.namedocter[0].th_lastname : '-'
					}));
				let year = this.state.year_year;
				// console.log('months', months);
				// console.log('year', year);
				// console.log('data_income_document', Number(moment(data_income_document[0].service_at).format('MM')));
				let data = data_All
					.filter((e) => e.service_status === 5)
					.filter((e) => {
						return Number(moment(e.service_at).format('YYYY')) == year;
					})
					.filter((e) => (Service_type !== 'all' ? e.type === Service_type : e))
					.filter((e) => (ontime ? Number(e.service_ontime) === Number(ontime) : e))
					.filter((e) => (Service_unit !== 'หน่วยบริการทั้งหมด' ? e.unit_name === Service_unit : e))
					.filter(
						(e) => (Number(claim_filter) !== 999 ? Number(e.service_claim) === Number(claim_filter) : e)
					)
					.filter(
						(e, i) =>
							nameDocter !== ''
								? String(e.fullname).toLowerCase().indexOf(String(nameDocter).toLowerCase()) > -1
								: e
					);
				this.setState({
					data_mapAll: data
				});
			}
		}
		if (Number(checkDate) == 1) {
			let years = moment(new Date()).format('YYYY');
			let claim = 'all';
			let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
			let getuser = await GET(GET_USERS('all'));
			///--------------------------------------------------------------------
			if (res_year.success) {
				let data_mt = res_year.result.mt.data;
				let data_pt = res_year.result.pt.data;
				let data_tm = res_year.result.tm.data;
				let children = data_mt.concat(data_pt);
				let data_All = children
					.concat(data_tm)
					.filter((e) => e.service_status === 5)
					.map((e) => ({
						...e,
						namedocter: getuser.result.filter((el) => Number(el.user_id) === Number(e.doctor))
					}))
					.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'))
					.map((e) => ({
						...e,
						fullname:
							e.namedocter.length > 0 ? e.namedocter[0].th_name + ' ' + e.namedocter[0].th_lastname : '-'
					}))
					.filter((e) => (Service_type !== 'all' ? e.type === Service_type : e))
					.filter((e) => (ontime ? Number(e.service_ontime) === Number(ontime) : e))
					.filter((e) => (Service_unit !== 'หน่วยบริการทั้งหมด' ? e.unit_name === Service_unit : e))
					.filter(
						(e) => (Number(claim_filter) !== 999 ? Number(e.service_claim) === Number(claim_filter) : e)
					)
					.filter(
						(e, i) =>
							nameDocter !== ''
								? String(e.fullname).toLowerCase().indexOf(String(nameDocter).toLowerCase()) > -1
								: e
					);
				this.setState({
					data_mapAll: data_All
				});
			}
		}
	};
	Filterinvoice = async () => {
		this.setState({ loading: true });
		let { checkDate, Service_type, Service_unit, nameDocter, ontime, claim_official } = this.state;
		let official = Number(claim_official);
		let years = moment(new Date()).format('YYYY');
		let claim = 'all';
		try {
			let res_year = await GET(GET_YEAR_INCOME_ALL(claim, years));
			let getuser = await GET(GET_USERS('all'));
			///--------------------------------------------------------------------
			if (res_year.success) {
				let data_mt = res_year.result.mt.data;
				let data_pt = res_year.result.pt.data;
				let data_tm = res_year.result.tm.data;
				let children = data_mt.concat(data_pt);
				let data_All = children
					.concat(data_tm)
					.filter((e) => (User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'));
				let { startDate, endDate } = this.state;
				let data = data_All
					.filter((e) => e.service_status === 5)
					.filter(
						(e) =>
							startDate || endDate
								? moment(e.service_at).format('YYYY-MM-DD') >= moment(startDate).format('YYYY-MM-DD') &&
									moment(e.service_at).format('YYYY-MM-DD') <= moment(endDate).format('YYYY-MM-DD')
								: e
						// moment(e.service_at).format('DD') >= date_s && moment(e.service_at).format('DD') <= date_e
					)
					.filter((e) => e.type === Service_type)
					.filter((e) => Number(e.service_ontime) === Number(ontime))
					.filter((e) => e.unit_name === Service_unit)
					.filter(
						(e) =>
							official === 1
								? Number(e.official) === 1 && Number(e.right_id) === 1
								: official === 2
									? Number(e.official) === 1 && Number(e.right_id) === 2
									: official === 3
										? Number(e.official) === 2 && Number(e.right_id) === 1
										: Number(e.official) === 2 && Number(e.right_id) === 2
					);
				if (official === 1 || official === 3) {
					let names = data.map((e) => e.th_prefix + e.th_name + ' ' + e.th_lastname);
					let unique_names = [ ...new Set(names.map((e) => e)) ];
					let datas = [];
					unique_names.forEach((el) => {
						let details = data
							.map((e) => ({ ...e, names: e.th_prefix + e.th_name + ' ' + e.th_lastname }))
							.filter((e) => e.names === el);
						let prefix =
							details[0].th_prefix === 'น.ส.'
								? 'นางสาว'
								: details[0].th_prefix === 'ด.ช.'
									? 'เด็กชาย'
									: details[0].th_prefix === 'ด.ญ.' ? 'เด็กหญิง' : details[0].th_prefix;
						let names = prefix + details[0].th_name + ' ' + details[0].th_lastname;
						let obj = {
							names: names,
							details
						};
						datas.push(obj);
					});
					this.setState({
						dataInvoice: datas,
						loading: false
					});
				} else {
					let names = data.map(
						(e) =>
							e.right_user +
							' สำหรับ ' +
							e.relation +
							'คือ' +
							e.th_prefix +
							e.th_name +
							' ' +
							e.th_lastname
					);
					let unique_names = [ ...new Set(names.map((e) => e)) ];
					// console.log('names', names);
					// console.log('unique_names', unique_names);
					let datas = [];
					unique_names.forEach((el) => {
						let details = data
							.map((e) => ({
								...e,
								names:
									e.right_user +
									' สำหรับ ' +
									e.relation +
									'คือ' +
									e.th_prefix +
									e.th_name +
									' ' +
									e.th_lastname
							}))
							.filter((e) => e.names === el);
						let prefix =
							details[0].th_prefix === 'น.ส.'
								? 'นางสาว'
								: details[0].th_prefix === 'ด.ช.'
									? 'เด็กชาย'
									: details[0].th_prefix === 'ด.ญ.' ? 'เด็กหญิง' : details[0].th_prefix;
						let names =
							details[0].right_user +
							' สำหรับ ' +
							details[0].relation +
							'คือ' +
							prefix +
							details[0].th_name +
							' ' +
							details[0].th_lastname;
						let obj = {
							names: names,
							details
						};
						datas.push(obj);
					});
					this.setState({
						dataInvoice: datas,
						loading: false
					});
				}
			} else this.setState({ loading: false });
		} catch (error) {
			this.setState({ loading: false });
		}
	};
	exportXsl = () => {
		let { data_mapAll } = this.state;
		// console.log('data_mapAll', data_mapAll);
		let download_xls = data_mapAll.map((el) => {
			let date = moment(el.service_at).format('DD-MM-YYYY');
			let type = el.type == 'mt' ? 'บริการตรวจสุขภาพ' : el.type == 'pt' ? 'บริการกายภาพบำบัด' : 'บริการนวดไทย';
			let service_ontime = el.service_ontime == 1 ? 'ผู้ป่วยในเวลา' : 'ผู้ป่วยนอกเวลา';
			let service_claim =
				el.service_claim === 1
					? 'ไม่ใช้สิทธิ์'
					: el.service_claim === 2 ? 'สิทธิ์ข้าราชการทั่วไป' : 'สิทธิ์ข้าราชการมหาวิทยาลัยขอนแก่น(ใบเหลือง)';
			let service_price = el.service_price.toLocaleString();
			let service_treatment = JSON.parse(el.service_treatment).reduce(add('pay_price'), 0);
			let price = !service_treatment ? 0 : service_treatment;
			let status =
				el.service_status === 1
					? 'รอการยืนยัน'
					: el.service_status === 2
						? 'อยู่ในคิว'
						: el.service_status === 3
							? 'กำลังตรวจ'
							: el.service_status === 4
								? 'รอชำระเงิน'
								: el.service_status === 5
									? 'เสร็จสิ้น'
									: el.service_status === 99 ? 'ยกเลิกบริการ' : el.service_status;
			let obj =
				User.role === 'frontPT'
					? {
							'ว/ด/ป': date,
							ประเภทบริการ: type,
							ชื่อ: el.th_name,
							นามสกุล: el.th_lastname,
							// เบอร์โทร: el.phone,
							// สิทธิ์การรักษา: service_claim,
							// หน่วยบริการ: el.unit_name,
							นักกายภาพบำบัด: el.fullname,
							// Tracking: status,
							'รายได้(บาท)': service_price,
							'ค่าตอบแทน (บาท)': price
						}
					: {
							'ว/ด/ป': date,
							ประเภทบริการ: type,
							ชื่อ: el.th_name,
							นามสกุล: el.th_lastname,
							เบอร์โทร: el.phone,
							สิทธิ์การรักษา: service_claim,
							หน่วยบริการ: el.unit_name,
							นักเทคนิคการแพทย์: el.fullname,
							// Tracking: status,
							'รายได้(บาท)': service_price
						};
			return obj;
		});
		// console.log('download_xls :', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'สรุปรายได้');
			xls.exportToXLS(
				'สรุปรายได้' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	Pays = () => {
		let { data_mapAll, startDate, endDate, nameDocter, invoice, dataInvoice } = this.state;
		if (startDate && endDate) {
			if (Number(invoice) === 1) {
				if (dataInvoice.length > 0) {
					PdfInvoice(this.state);
				}
			} else {
				if (data_mapAll.length > 0) {
					if (nameDocter) {
						Pays(this.state);
					} else {
						PaysAll(this.state);
					}
				}
			}
		} else {
			swal('กรุณาเลือกวันที่', '', 'warning');
		}
	};
	printdocKet = () => {
		docKet(this.state);
	};
	render() {
		let {
			checkDate,
			years,
			data_income_document,
			Service_type,
			month_month,
			year_month,
			year_year,
			data_mapAll,
			Service_unit,
			claim_filter,
			pageSize,
			currentPage,
			nameDocter,
			ontime,
			all_docter,
			invoice,
			president,
			manager,
			inspector,
			claim_official,
			dataInvoice,
			loading
		} = this.state;
		return (
			<div>
				<div className="col-xs-12">
					{User.role == 'frontMT' && (
						<div className="col-xs-12 p-0">
							<div className="col-md-6 col-lg-4">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											ประเภท
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={invoice}
												onChange={(e) => {
													// this.Service_unit(e.target.value);
													this.setState({ invoice: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												<option value={0}>รายได้</option>
												<option value={1}>ใบแจ้งหนี้</option>
											</Input>
										</Col>
									</FormGroup>
								</Form>
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											หน่วยบริการ
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={Service_unit}
												onChange={(e) => {
													this.setState({ Service_unit: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												{units.map((e, index) => (
													<option key={'' + index} value={e.name}>
														{e.name}
													</option>
												))}
											</Input>
										</Col>
									</FormGroup>
								</Form>
							</div>
							<div className="col-md-6 col-lg-4">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											เวลาเข้ารับบริการ
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={ontime}
												onChange={(e) => {
													this.setState({ ontime: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												<option value={1}>ในเวลา</option>
												<option value={0}>นอกเวลา</option>
											</Input>
										</Col>
									</FormGroup>
								</Form>
								{Number(invoice) == 1 && (
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												สิทธิ์ข้าราชการ
											</Label>
											<Col sm={7}>
												<Input
													type="select"
													className="income-input"
													value={claim_official}
													onChange={(e) => {
														// this.Status_filter(e.target.value);
														this.setState({ claim_official: e.target.value }, () =>
															this.Filterinvoice()
														);
													}}
												>
													{Invoice_claim.map((e, index) => (
														<option key={'' + index} value={e.value}>
															{e.name}
														</option>
													))}
												</Input>
											</Col>
										</FormGroup>
									</Form>
								)}
							</div>
							{checkDate == 1 ? (
								<div className="col-md-6 col-lg-4" />
							) : checkDate == 2 ? (
								<div className="col-md-6 col-lg-4">
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												วันที่
											</Label>
											<Col sm={7}>
												<DatePicker
													id="div-datepicker"
													className="sub-income-input"
													selected={this.state.startDate}
													onChange={(e) =>
														this.setState({ startDate: e }, () => {
															let { invoice } = this.state;
															if (Number(invoice) === 0) this.DateFilter();
															else this.Filterinvoice();
														})}
													isClearable={true}
													placeholderText="DD-MM-YYYY"
													locale="th"
													dateFormat="dd-MM-yyyy"
												/>
											</Col>
										</FormGroup>
									</Form>
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												ถึงวันที่
											</Label>
											<Col sm={7}>
												<DatePicker
													id="div-datepicker"
													className="sub-income-input"
													selected={this.state.endDate}
													onChange={(e) =>
														this.setState({ endDate: e }, () => {
															let { invoice } = this.state;
															if (Number(invoice) === 0) this.DateFilter();
															else this.Filterinvoice();
														})}
													isClearable={true}
													placeholderText="DD-MM-YYYY"
													locale="th"
													dateFormat="dd-MM-yyyy"
												/>
											</Col>
										</FormGroup>
									</Form>
								</div>
							) : checkDate == 3 ? (
								<div className="col-md-6 col-lg-4">
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">เดือน</Label>
										<Input
											value={month_month}
											className="sub-income-input"
											onChange={(e) => {
												// setDocType(e.target.value);
												this.setState({ month_month: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '80px' }}
											type="select"
										>
											{months.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Col>
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">ปี</Label>
										<Input
											value={year_month}
											className="sub-income-input"
											onChange={(e) => {
												// this.DateFilter(e.target.value);
												this.setState({ year_month: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '60px', marginRight: '5px' }}
											type="select"
										>
											{years.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Col>
								</div>
							) : (
								<div className="col-md-6 col-lg-4">
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">ปีที่</Label>
										<Input
											value={year_year}
											className="sub-income-input"
											onChange={(e) => {
												// graphselectYearMT(e.target.value);
												this.setState({ year_year: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '60px', marginRight: '5px' }}
											type="select"
										>
											{years.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Col>
								</div>
							)}
						</div>
					)}

					{User.role == 'frontPT' && (
						<div className="col-xs-12 p-0">
							<div className="col-sm-6">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											ประเภท
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={invoice}
												onChange={(e) => {
													// this.Service_unit(e.target.value);
													this.setState({ invoice: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												<option value={0}>รายได้</option>
												<option value={1}>ใบแจ้งหนี้</option>
											</Input>
										</Col>
									</FormGroup>
								</Form>
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											หน่วยบริการ
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={Service_unit}
												onChange={(e) => {
													this.setState({ Service_unit: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												{units.map((e, index) => (
													<option key={'' + index} value={e.name}>
														{e.name}
													</option>
												))}
											</Input>
										</Col>
									</FormGroup>
								</Form>
							</div>
							<div className="col-sm-6">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											ประเภทบริการ
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={Service_type}
												onChange={(e) => {
													// this.Service_type(e.target.value);
													this.setState({ Service_type: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												{types
													.filter(
														(e) =>
															User.role === 'frontMT' ? e.type === 'mt' : e.type !== 'mt'
													)
													.map((e, index) => (
														<option key={'' + index} value={e.type}>
															{e.name}
														</option>
													))}
											</Input>
										</Col>
									</FormGroup>
								</Form>
								{Number(invoice) ? (
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												สิทธิ์ข้าราชการ
											</Label>
											<Col sm={7}>
												<Input
													type="select"
													className="income-input"
													value={claim_official}
													onChange={(e) => {
														// this.Status_filter(e.target.value);
														this.setState({ claim_official: e.target.value }, () =>
															this.Filterinvoice()
														);
													}}
												>
													{Invoice_claim.map((e, index) => (
														<option key={'' + index} value={e.value}>
															{e.name}
														</option>
													))}
												</Input>
											</Col>
										</FormGroup>
									</Form>
								) : (
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												{User.role === 'frontPT' ? 'นักกายภาพบำบัด' : 'นักเทคนิคการแพทย์'}
											</Label>
											<Col sm={7}>
												<Input
													type="select"
													className="income-input"
													value={nameDocter}
													onChange={(e) => {
														// this.Service_unit(e.target.value);
														this.setState({ nameDocter: e.target.value }, () =>
															this.DateFilter()
														);
													}}
												>
													<option value="">
														{User.role === 'frontPT' ? (
															'นักกายภาพบำบัดทั้งหมด'
														) : (
															'นักเทคนิคการแพทย์ทั้งหมด'
														)}
													</option>
													{all_docter.map((e, index) => (
														<option key={'' + index} value={e.fullname}>
															{e.fullname}
														</option>
													))}
												</Input>
											</Col>
										</FormGroup>
									</Form>
								)}
							</div>
							<div className="col-sm-6">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											เวลาเข้ารับบริการ
										</Label>
										<Col sm={7}>
											<Input
												type="select"
												className="income-input"
												value={ontime}
												onChange={(e) => {
													this.setState({ ontime: e.target.value }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													});
												}}
											>
												<option value={1}>ในเวลา</option>
												<option value={0}>นอกเวลา</option>
											</Input>
										</Col>
									</FormGroup>
								</Form>
							</div>
							{checkDate == 1 ? (
								<div className="col-sm-6 col-lg-3" />
							) : checkDate == 2 ? (
								<div className="col-sm-6 col-lg-3">
									<Form>
										<FormGroup row>
											<Label sm={5} size="lg">
												วันที่
											</Label>
											<Col sm={7}>
												<DatePicker
													id="div-datepicker"
													className="sub-income-input"
													selected={this.state.startDate}
													onChange={(e) =>
														this.setState({ startDate: e }, () => {
															let { invoice } = this.state;
															if (Number(invoice) === 0) this.DateFilter();
															else this.Filterinvoice();
														})}
													isClearable={true}
													placeholderText="DD-MM-YYYY"
													locale="th"
													dateFormat="dd-MM-yyyy"
												/>
											</Col>
										</FormGroup>
									</Form>
								</div>
							) : checkDate == 3 ? (
								<div className="col-sm-6 col-lg-3">
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">เดือน</Label>
										<Input
											value={month_month}
											className="sub-income-input"
											onChange={(e) => {
												// setDocType(e.target.value);
												this.setState({ month_month: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '80px' }}
											type="select"
										>
											{months.map((e, index) => (
												<option key={'' + index} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Col>
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">ปี</Label>
										<Input
											value={year_month}
											className="sub-income-input"
											onChange={(e) => {
												// this.DateFilter(e.target.value);
												this.setState({ year_month: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '60px', marginRight: '5px' }}
											type="select"
										>
											{years.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Col>
								</div>
							) : (
								<div className="col-sm-6 col-lg-3">
									<Col className="d-flex mx-auto mt-3">
										<Label className="sub-income-label">ปีที่</Label>
										<Input
											value={year_year}
											className="sub-income-input"
											onChange={(e) => {
												// graphselectYearMT(e.target.value);
												this.setState({ year_year: e.target.value }, () => this.DateFilter());
											}}
											style={{ width: '60px', marginRight: '5px' }}
											type="select"
										>
											{years.map((e, index) => (
												<option key={'' + index} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Col>
								</div>
							)}
							<div className="col-sm-6 col-lg-3">
								<Form>
									<FormGroup row>
										<Label sm={5} size="lg">
											ถึงวันที่
										</Label>
										<Col sm={7}>
											<DatePicker
												id="div-datepicker"
												className="sub-income-input"
												selected={this.state.endDate}
												onChange={(e) =>
													this.setState({ endDate: e }, () => {
														let { invoice } = this.state;
														if (Number(invoice) === 0) this.DateFilter();
														else this.Filterinvoice();
													})}
												isClearable={true}
												placeholderText="DD-MM-YYYY"
												locale="th"
												dateFormat="dd-MM-yyyy"
											/>
										</Col>
									</FormGroup>
								</Form>
							</div>
						</div>
					)}
				</div>
				{Number(invoice) === 1 && (
					<Container>
						<Row>
							<Col xs={12} sm={4}>
								<FormGroup>
									<Label for="exampleEmail">ประธานโครงงาน</Label>
									<InputGroup>
										<Input
											style={{ zIndex: '0' }}
											value={president}
											valid={president}
											invalid={!president}
											placeholder="กรุณากรอกชื่อประธานโครงงาน"
											onChange={(e) => this.setState({ president: e.target.value })}
										/>
										<InputGroupAddon addonType="prepend">
											<Button
												color={president ? 'success' : 'danger'}
												onClick={() => this.Up_Invoice(User.role == 'frontPT' ? 1 : 4)}
											>
												อัปเดต
											</Button>
										</InputGroupAddon>
									</InputGroup>
								</FormGroup>
							</Col>
							<Col xs={12} sm={4}>
								<FormGroup>
									<Label for="examplePassword">ผู้จัดการ</Label>
									<InputGroup>
										<Input
											style={{ zIndex: '0' }}
											value={manager}
											valid={manager}
											invalid={!manager}
											onChange={(e) => this.setState({ manager: e.target.value })}
											placeholder="กรุณากรอกชื่อผู้จัดการ"
										/>
										<InputGroupAddon addonType="prepend">
											<Button
												color={manager ? 'success' : 'danger'}
												onClick={() => this.Up_Invoice(User.role == 'frontPT' ? 2 : 5)}
											>
												อัปเดต
											</Button>
										</InputGroupAddon>
									</InputGroup>
								</FormGroup>
							</Col>
							<Col xs={12} sm={4}>
								<FormGroup>
									<Label for="examplePassword">ผู้ตรวจสอบ</Label>
									<InputGroup>
										<Input
											style={{ zIndex: '0' }}
											value={inspector}
											valid={inspector}
											invalid={!inspector}
											onChange={(e) => this.setState({ inspector: e.target.value })}
											placeholder="กรุณากรอกชื่อผู้ตรวจสอบ"
										/>
										<InputGroupAddon addonType="prepend">
											<Button
												color={inspector ? 'success' : 'danger'}
												onClick={() => this.Up_Invoice(User.role == 'frontPT' ? 3 : 6)}
											>
												อัปเดต
											</Button>
										</InputGroupAddon>
									</InputGroup>
								</FormGroup>
							</Col>
						</Row>
					</Container>
				)}

				<div className="div-graph">
					{loading ? (
						<div className="loading d-flex flex-column">
							<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>
								กำลังโหลดข้อมูล
							</Label>
						</div>
					) : Number(invoice) === 0 ? data_mapAll.length > 0 ? (
						<Row className="d-flex mt-3 w-100">
							<Col lg={12} className="d-flex justify-content-end">
								<button
									className="botton-search"
									style={{ backgroundColor: '#b8358d' }}
									onClick={() => {
										if (User.role === 'frontPT') {
											this.Pays();
										} else {
											this.exportXsl();
										}
									}}
								>
									<div>ดาวน์โหลด</div>
								</button>
							</Col>
							<Col>
								<Table style={{ whiteSpace: 'pre' }} responsive striped>
									<thead>
										<tr>
											{(User.role === 'frontPT' ? DMYpt : DMYmt).map((e, i) => (
												<th
													key={'a' + i}
													className={
														e === 'ค่าตอบแทน (บาท)' || e === 'รายได้ (บาท)' ? (
															'text-right'
														) : null
													}
												>
													{e}
												</th>
											))}
										</tr>
									</thead>
									<tbody>
										{data_mapAll
											.slice((currentPage - 1) * pageSize, currentPage * pageSize)
											.map((e, i) => {
												let service_treatment = JSON.parse(e.service_treatment).reduce(
													add('pay_price'),
													0
												);
												let service_claim =
													e.service_claim === 1
														? 'ไม่ใช้สิทธิ์'
														: e.service_claim === 2
															? 'สิทธิ์ข้าราชการทั่วไป'
															: 'สิทธิ์ข้าราชการมหาวิทยาลัยขอนแก่น(ใบเหลือง)';
												return (
													<tr>
														<th scope="row">{moment(e.service_at).format('DD-MM-YYYY')}</th>
														<td>{getRoleName(e.type)}</td>
														<td>{e.service_ontime ? 'ในเวลา' : 'นอกเวลา'}</td>
														<td>{e.th_name + ' ' + e.th_lastname}</td>
														<td>{e.phone || '-'}</td>
														<td>{service_claim}</td>
														<td>{e.unit_name}</td>
														<td>{e.fullname ? e.fullname : '-'}</td>
														<td>
															{
																<text
																	style={
																		e.service_status === 1 ? (
																			{ color: 'black' }
																		) : e.service_status === 2 ? (
																			{ color: 'purple' }
																		) : e.service_status === 3 ? (
																			{ color: 'blue' }
																		) : e.service_status === 4 ? (
																			{ color: 'lightgreen' }
																		) : e.service_status === 5 ? (
																			{ color: 'green' }
																		) : e.service_status === 99 ? (
																			{ color: 'red' }
																		) : (
																			{ color: 'orange' }
																		)
																	}
																>
																	{convert_status(e.service_status)}
																</text>
															}
														</td>
														<td className="text-right">
															{e.service_price == null ? (
																0
															) : (
																e.service_price.toLocaleString()
															)}
														</td>
														{User.role === 'frontPT' && (
															<td className="text-right">
																{e.type === 'pt' && e.service_status === 5 ? (
																	service_treatment.toLocaleString()
																) : (
																	0
																)}
															</td>
														)}
													</tr>
												);
											})}
										{User.role === 'frontPT' && (
											<tr>
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td className="text-right h4">
													<strong>
														{data_mapAll.reduce(add('service_price'), 0).toLocaleString()}
													</strong>
												</td>
												<td className="text-right h4">
													<strong>
														{data_mapAll
															.map((e) => ({
																price:
																	JSON.parse(e.service_treatment).reduce(
																		add('pay_price'),
																		0
																	) || 0
															}))
															.reduce(add('price'), 0)
															.toLocaleString()}
													</strong>
												</td>
											</tr>
										)}
										{User.role === 'frontMT' && (
											<tr>
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td />
												<td className="text-right h4">
													<strong>
														{data_mapAll.reduce(add('service_price'), 0).toLocaleString()}
													</strong>
												</td>
											</tr>
										)}
									</tbody>
								</Table>
								<div className="d-flex justify-content-between">
									<select
										className="select-page-size"
										name="pageSize"
										onChange={async (e) => {
											await this.setState({ [e.target.name]: +e.target.value });
										}}
									>
										{numberPage.map((el, i) => (
											<option key={i} value={el}>
												{el}
											</option>
										))}
									</select>
									<Pagination
										totalRecords={data_mapAll.length}
										pageLimit={pageSize}
										pageNeighbours={2}
										onPageChanged={this.onPageChanged}
									/>
								</div>
							</Col>
						</Row>
					) : (
						<h5 style={{ marginTop: '10rem' }}>---ไม่พบข้อมูล---</h5>
					) : dataInvoice.length > 0 ? (
						<Row className="d-flex mt-3 w-100">
							<Col lg={12} className="d-flex justify-content-end">
								{Number(invoice) === 1 &&
								(Number(claim_official) === 1 || Number(claim_official) === 3) && (
									<button
										className="botton-search mr-3"
										onClick={() => {
											if (User.role === 'frontPT') {
											} else {
												this.printdocKet();
											}
										}}
									>
										<div>ใบปะหน้า</div>
									</button>
								)}
								{User.role == 'frontPT' && (
									<button
										className="botton-search"
										style={{ backgroundColor: '#b8358d' }}
										onClick={() => {
											if (User.role === 'frontPT') {
												this.Pays();
											} else {
												this.exportXsl();
											}
										}}
									>
										<div>ดาวน์โหลด</div>
									</button>
								)}
							</Col>
							<Col>
								<Table style={{ whiteSpace: 'pre' }} responsive striped>
									<thead>
										<tr>
											<th>ลำดับที่</th>
											<th>ชื่อ - สกุล</th>
											<th className="text-right">ค่าบริการ</th>
										</tr>
									</thead>
									<tbody>
										{dataInvoice
											.slice((currentPage - 1) * pageSize, currentPage * pageSize)
											.map((e, i) => {
												let num = e.details.reduce(add('service_price'), 0).toLocaleString();
												return (
													<tr>
														<td width={100}>{i + 1}</td>
														<td>{e.names}</td>
														<td width={300} className="text-right">
															{num}
														</td>
													</tr>
												);
											})}
										<tr>
											<td width={100} />
											<td />
											<td className="text-right h4">
												<strong>
													{dataInvoice
														.map((e) => ({
															sum: e.details.reduce(add('service_price'), 0)
														}))
														.reduce(add('sum'), 0)
														.toLocaleString()}
												</strong>
											</td>
										</tr>
									</tbody>
								</Table>
								<div className="d-flex justify-content-between">
									<select
										className="select-page-size"
										name="pageSize"
										onChange={async (e) => {
											await this.setState({ [e.target.name]: +e.target.value });
										}}
									>
										{numberPage.map((el, i) => (
											<option key={i} value={el}>
												{el}
											</option>
										))}
									</select>
									<Pagination
										totalRecords={data_mapAll.length}
										pageLimit={pageSize}
										pageNeighbours={2}
										onPageChanged={this.onPageChanged}
									/>
								</div>
							</Col>
						</Row>
					) : (
						<h5 style={{ marginTop: '10rem' }}>---ไม่พบข้อมูล---</h5>
					)}
				</div>
			</div>
		);
	}
	onPageChanged = (data) => {
		const { currentPage } = data;
		this.setState({ currentPage });
	};
}
let numberPage = [ 15, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 10000 ];
function add(key) {
	return (a, b) => a + b[key];
}
