import React, { Component } from 'react';
import './Styled/Graph.css';
import $ from 'jquery';
import { Row, Col, Label } from 'reactstrap';
import SubGraph from './SubGraph';
import { GET, GET_MONTH_INCOME, GET_YEAR_INCOME, GET_DATE_INCOME } from '../../service/service';
import moment from 'moment';
import { Link } from 'react-router-dom';
import ReactLoading from 'react-loading';
import Color from '../Color';

const categories = [
	'มกราคม',
	'กุมภาพันธ์',
	'มีนาคม',
	'เมษายน',
	'พฤษภาคม',
	'มิถุนายน',
	'กรกฏาคม',
	'สิงหาคม',
	'กันยายน',
	'ตุลาคม',
	'พฤศจิกายน',
	'ธันวาคม'
];

export default class Graph extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			user_type: '',
			total_graph: [],
			total: [],

			date: moment().format('DD MMMM YYYY'),
			month: new Date().getMonth() + 1,
			year: new Date().getFullYear(),
			//----------------------DATA---PT----------------------------------
			sumIncomeDayPT: '',
			donutIncomePT: [],
			kkuIncomeDayPT: '',
			downtownIncomeDayPT: '',
			sumListDayPT: '',
			donutListPT: [],
			kkuListDayPT: '',
			downtownListDayPT: '',
			graphListMonthPT: {},
			graphListYearPT: {},
			graphIncomeMonthPT: {},
			graphIncomeYearPT: {},
			//----------------------DATA---MT----------------------------------
			sumIncomeDayMT: '',
			donutIncomeMT: [],
			kkuIncomeDayMT: '',
			downtownIncomeDayMT: '',
			sumListDayMT: '',
			donutListMT: [],
			kkuListDayMT: '',
			downtownListDayMT: '',
			graphListMonthMT: {},
			graphListYearMT: {},
			graphIncomeMonthMT: {},
			graphIncomeYearMT: {},
			//----------------------DATA---TM----------------------------------
			sumIncomeDayTM: '',
			donutIncomeTM: [],
			kkuIncomeDayTM: '',
			downtownIncomeDayTM: '',
			sumListDayTM: '',
			donutListTM: [],
			kkuListDayTM: '',
			downtownListDayTM: '',
			graphListMonthTM: {},
			graphListYearTM: {},
			graphIncomeMonthTM: {},
			graphIncomeYearTM: {}
		};
	}

	componentWillMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);

		let res = JSON.parse(localStorage.getItem('USER'));
		this.setState({
			user_type: res.role
		});
	}

	componentDidMount = async () => {
		let incomeByDate = await GET(GET_DATE_INCOME('all', moment().format('YYYY-MM-DD')));
		let incomeByMonth = await GET(GET_MONTH_INCOME('all', moment().format('YYYY-MM')));
		let incomeByYear = await GET(GET_YEAR_INCOME('all', moment().format('YYYY')));

		if (incomeByDate.success && incomeByMonth.success && incomeByYear.success) {
			//----------------------------------MT-----------------------------------
			let kkuIncomeDayMT = incomeByDate.result.mt.sumIncome.kkuIncome;
			let downtownIncomeDayMT = incomeByDate.result.mt.sumIncome.downtownIncome;
			let kkuListDayMT = incomeByDate.result.mt.sumList.kkuList;
			let downtownListDayMT = incomeByDate.result.mt.sumList.downtownList;
			let kkuIncomeMonthMT = incomeByMonth.result.mt.sumIncome.kkuIncome;
			let downtownIncomeMonthMT = incomeByMonth.result.mt.sumIncome.downtownIncome;
			let kkuListMonthMT = incomeByMonth.result.mt.sumList.kkuList;
			let downtownListMonthMT = incomeByMonth.result.mt.sumList.downtownList;
			let kkuIncomeYearMT = incomeByYear.result.mt.sumIncome.kkuIncome;
			let downtownIncomeYearMT = incomeByYear.result.mt.sumIncome.downtownIncome;
			let kkuListYearMT = incomeByYear.result.mt.sumList.kkuList;
			let downtownListYearMT = incomeByYear.result.mt.sumList.downtownList;
			let sumIncomeDayMT = parseInt(kkuIncomeDayMT) + parseInt(downtownIncomeDayMT);
			let sumListDayMT = parseInt(kkuListDayMT) + parseInt(downtownListDayMT);
			//-------------------Month-------------------
			let graphIncomeMonthMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthMT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthMT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthMT,
						color: 'orange'
					}
				]
			};
			let graphListMonthMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthMT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthMT
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthMT,
						color: 'orange'
					}
				]
			};
			//-------------------Year--------------------
			let graphIncomeYearMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearMT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearMT,
						color: 'orange'
					}
				]
			};
			let graphListYearMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearMT
					},
					{
						name: 'ในเมือง',
						data: downtownListYearMT,
						color: 'orange'
					}
				]
			};
			//-------------------DONUT-------------------
			let donutIncomeMT = [
				{
					title: 'มหาวิทยาลัยขอนแก่น ' + kkuIncomeDayMT.toLocaleString() + ' บาท',
					value: kkuIncomeDayMT,
					color: '#467ac8'
				},
				{
					title: 'ในเมือง ' + downtownIncomeDayMT.toLocaleString() + ' บาท',
					value: downtownIncomeDayMT,
					color: '#e8ae00'
				}
			];
			let donutListMT = [
				{ name: 'มหาวิทยาลัยขอนแก่น', value: kkuListDayMT },
				{ name: 'ในเมือง', value: downtownListDayMT }
			];

			//----------------------------------PT-----------------------------------
			let kkuIncomeDayPT = incomeByDate.result.pt.sumIncome.kkuIncome;
			let downtownIncomeDayPT = incomeByDate.result.pt.sumIncome.downtownIncome;
			let kkuListDayPT = incomeByDate.result.pt.sumList.kkuList;
			let downtownListDayPT = incomeByDate.result.pt.sumList.downtownList;
			let kkuIncomeMonthPT = incomeByMonth.result.pt.sumIncome.kkuIncome;
			let downtownIncomeMonthPT = incomeByMonth.result.pt.sumIncome.downtownIncome;
			let kkuListMonthPT = incomeByMonth.result.pt.sumList.kkuList;
			let downtownListMonthPT = incomeByMonth.result.pt.sumList.downtownList;
			let kkuIncomeYearPT = incomeByYear.result.pt.sumIncome.kkuIncome;
			let downtownIncomeYearPT = incomeByYear.result.pt.sumIncome.downtownIncome;
			let kkuListYearPT = incomeByYear.result.pt.sumList.kkuList;
			let downtownListYearPT = incomeByYear.result.pt.sumList.downtownList;
			let sumIncomeDayPT = parseInt(kkuIncomeDayPT) + parseInt(downtownIncomeDayPT);
			let sumListDayPT = parseInt(kkuListDayPT) + parseInt(downtownListDayPT);
			//-------------------Month-------------------
			let graphIncomeMonthPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthPT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthPT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthPT,
						color: 'orange'
					}
				]
			};
			let graphListMonthPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthPT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthPT
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthPT,
						color: 'orange'
					}
				]
			};
			//-------------------Year--------------------
			let graphIncomeYearPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearPT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearPT,
						color: 'orange'
					}
				]
			};
			let graphListYearPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearPT
					},
					{
						name: 'ในเมือง',
						data: downtownListYearPT,
						color: 'orange'
					}
				]
			};
			//-------------------DONUT-------------------
			let donutIncomePT = [
				{
					title: 'มหาวิทยาลัยขอนแก่น ' + kkuIncomeDayPT.toLocaleString() + ' บาท',
					value: kkuIncomeDayPT,
					color: '#467ac8'
				},
				{
					title: 'ในเมือง ' + downtownIncomeDayPT.toLocaleString() + ' บาท',
					value: downtownIncomeDayPT,
					color: '#e8ae00'
				}
			];
			let donutListPT = [
				{ name: 'มหาวิทยาลัยขอนแก่น', value: kkuListDayPT },
				{ name: 'ในเมือง', value: downtownListDayPT }
			];

			//----------------------------------TM-----------------------------------
			let kkuIncomeDayTM = incomeByDate.result.tm.sumIncome.kkuIncome;
			let downtownIncomeDayTM = incomeByDate.result.tm.sumIncome.downtownIncome;
			let kkuListDayTM = incomeByDate.result.tm.sumList.kkuList;
			// console.log('kkuListDayTM',incomeByDate.result.tm.sumList);

			let downtownListDayTM = incomeByDate.result.tm.sumList.downtownList;
			let kkuIncomeMonthTM = incomeByMonth.result.tm.sumIncome.kkuIncome;
			let downtownIncomeMonthTM = incomeByMonth.result.tm.sumIncome.downtownIncome;
			let kkuListMonthTM = incomeByMonth.result.tm.sumList.kkuList;
			let downtownListMonthTM = incomeByMonth.result.tm.sumList.downtownList;
			let kkuIncomeYearTM = incomeByYear.result.tm.sumIncome.kkuIncome;
			let downtownIncomeYearTM = incomeByYear.result.tm.sumIncome.downtownIncome;
			let kkuListYearTM = incomeByYear.result.tm.sumList.kkuList;
			let downtownListYearTM = incomeByYear.result.tm.sumList.downtownList;
			let sumIncomeDayTM = parseInt(kkuIncomeDayTM) + parseInt(downtownIncomeDayTM);
			let sumListDayTM = parseInt(kkuListDayTM) + parseInt(downtownListDayTM);
			//-------------------Month-------------------
			let graphIncomeMonthTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthTM.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthTM
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthTM,
						color: 'orange'
					}
				]
			};
			let graphListMonthTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthTM.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthTM
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthTM,
						color: 'orange'
					}
				]
			};
			//-------------------Year--------------------
			let graphIncomeYearTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearTM
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearTM,
						color: 'orange'
					}
				]
			};
			let graphListYearTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearTM
					},
					{
						name: 'ในเมือง',
						data: downtownListYearTM,
						color: 'orange'
					}
				]
			};
			//-------------------DONUT-------------------
			let donutIncomeTM = [
				{
					title: 'มหาวิทยาลัยขอนแก่น ' + kkuIncomeDayTM.toLocaleString() + ' บาท',
					value: kkuIncomeDayTM,
					color: '#467ac8'
				},
				{
					title: 'ในเมือง ' + downtownIncomeDayTM.toLocaleString() + ' บาท',
					value: downtownIncomeDayTM,
					color: '#e8ae00'
				}
			];
			let donutListTM = [
				{ name: 'มหาวิทยาลัยขอนแก่น', value: kkuListDayTM },
				{ name: 'ในเมือง', value: downtownListDayTM }
			];
			//-----------------------------------------------------------------------

			this.setState({
				//----------MT-------------
				graphIncomeMonthMT: graphIncomeMonthMT,
				graphListMonthMT: graphListMonthMT,
				graphIncomeYearMT: graphIncomeYearMT,
				graphListYearMT: graphListYearMT,
				//------PT----------
				graphIncomeMonthPT: graphIncomeMonthPT,
				graphListMonthPT: graphListMonthPT,
				graphIncomeYearPT: graphIncomeYearPT,
				graphListYearPT: graphListYearPT,
				//------TM---------------
				graphIncomeMonthTM: graphIncomeMonthTM,
				graphListMonthTM: graphListMonthTM,
				graphIncomeYearTM: graphIncomeYearTM,
				graphListYearTM: graphListYearTM,
				//----------------------DATA--DONUT--PT----------------------------------
				donutIncomePT: donutIncomePT,
				sumIncomeDayPT: sumIncomeDayPT,
				kkuIncomeDayPT: kkuIncomeDayPT,
				downtownIncomeDayPT: downtownIncomeDayPT,
				donutListPT: donutListPT,
				sumListDayPT: sumListDayPT,
				kkuListDayPT: kkuListDayPT,
				downtownListDayPT: downtownListDayPT,
				//----------------------DATA--DONUT--MT----------------------------------
				donutIncomeMT: donutIncomeMT,
				sumIncomeDayMT: sumIncomeDayMT,
				kkuIncomeDayMT: kkuIncomeDayMT,
				downtownIncomeDayMT: downtownIncomeDayMT,
				donutListMT: donutListMT,
				sumListDayMT: sumListDayMT,
				kkuListDayMT: kkuListDayMT,
				downtownListDayMT: downtownListDayMT,
				//----------------------DATA--DONUT--TM----------------------------------
				donutIncomeTM: donutIncomeTM,
				sumIncomeDayTM: sumIncomeDayTM,
				kkuIncomeDayTM: kkuIncomeDayTM,
				downtownIncomeDayTM: downtownIncomeDayTM,
				donutListTM: donutListTM,
				sumListDayTM: sumListDayTM,
				kkuListDayTM: kkuListDayTM,
				downtownListDayTM: downtownListDayTM,
				//------------------------//
				loading: false
			});
		}
	};

	formatTooltip(tooltip, x = this.x, y = this.y, series = this.series) {
		return `<b>วันที่ ${x}</b><br/><br/>${series.name} : ${y.toLocaleString()} บาท`;
	}

	formatTooltipYear(tooltip, x = this.x, y = this.y, series = this.series) {
		return `<b>เดือน ${x}</b><br/><br/>${series.name} : ${y.toLocaleString()} บาท`;
	}

	graphSelectMonthMT = async (months) => {
		let claim = 'all';
		let month = new Date().getFullYear() + '-' + months;
		let incomeByMonth = await GET(GET_MONTH_INCOME(claim, month));
		if (incomeByMonth.success) {
			let kkuIncomeMonthMT = incomeByMonth.result.mt.sumIncome.kkuIncome;
			let downtownIncomeMonthMT = incomeByMonth.result.mt.sumIncome.downtownIncome;
			let kkuListMonthMT = incomeByMonth.result.mt.sumList.kkuList;
			let downtownListMonthMT = incomeByMonth.result.mt.sumList.downtownList;
			let graphIncomeMonthMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthMT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthMT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthMT,
						color: 'orange'
					}
				]
			};
			let graphListMonthMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthMT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthMT
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthMT,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeMonthMT: graphIncomeMonthMT,
				graphListMonthMT: graphListMonthMT
			});
		}
	};

	graphSelectYearMT = async (years) => {
		let claim = 'all';
		let incomeByYear = await GET(GET_YEAR_INCOME(claim, years));
		if (incomeByYear.success) {
			let kkuIncomeYearMT = incomeByYear.result.mt.sumIncome.kkuIncome;
			let downtownIncomeYearMT = incomeByYear.result.mt.sumIncome.downtownIncome;
			let kkuListYearMT = incomeByYear.result.mt.sumList.kkuList;
			let downtownListYearMT = incomeByYear.result.mt.sumList.downtownList;
			let graphIncomeYearMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearMT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearMT,
						color: 'orange'
					}
				]
			};
			let graphListYearMT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearMT
					},
					{
						name: 'ในเมือง',
						data: downtownListYearMT,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeYearMT: graphIncomeYearMT,
				graphListYearMT: graphListYearMT
			});
		}
	};

	graphSelectMonthPT = async (months) => {
		let claim = 'all';
		let month = new Date().getFullYear() + '-' + months;
		let incomeByMonth = await GET(GET_MONTH_INCOME(claim, month));
		if (incomeByMonth.success) {
			let kkuIncomeMonthPT = incomeByMonth.result.pt.sumIncome.kkuIncome;
			let downtownIncomeMonthPT = incomeByMonth.result.pt.sumIncome.downtownIncome;
			let kkuListMonthPT = incomeByMonth.result.pt.sumList.kkuList;
			let downtownListMonthPT = incomeByMonth.result.pt.sumList.downtownList;

			let graphIncomeMonthPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthPT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthPT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthPT,
						color: 'orange'
					}
				]
			};
			let graphListMonthPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthPT.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthPT
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthPT,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeMonthPT: graphIncomeMonthPT,
				graphListMonthPT: graphListMonthPT
			});
		}
	};

	graphSelectYearPT = async (years) => {
		let claim = 'all';
		let incomeByYear = await GET(GET_YEAR_INCOME(claim, years));
		if (incomeByYear.success) {
			let kkuIncomeYearPT = incomeByYear.result.pt.sumIncome.kkuIncome;
			let downtownIncomeYearPT = incomeByYear.result.pt.sumIncome.downtownIncome;
			let kkuListYearPT = incomeByYear.result.pt.sumList.kkuList;
			let downtownListYearPT = incomeByYear.result.pt.sumList.downtownList;

			let graphIncomeYearPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearPT
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearPT,
						color: 'orange'
					}
				]
			};
			let graphListYearPT = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearPT
					},
					{
						name: 'ในเมือง',
						data: downtownListYearPT,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeYearPT: graphIncomeYearPT,
				graphListYearPT: graphListYearPT
			});
		}
	};

	graphSelectMonthTM = async (months) => {
		let claim = 'all';
		let month = new Date().getFullYear() + '-' + months;
		let incomeByMonth = await GET(GET_MONTH_INCOME(claim, month));
		if (incomeByMonth.success) {
			let kkuIncomeMonthTM = incomeByMonth.result.tm.sumIncome.kkuIncome;
			let downtownIncomeMonthTM = incomeByMonth.result.tm.sumIncome.downtownIncome;
			let kkuListMonthTM = incomeByMonth.result.tm.sumList.kkuList;
			let downtownListMonthTM = incomeByMonth.result.tm.sumList.downtownList;

			let graphIncomeMonthTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthTM.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltip
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeMonthTM
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeMonthTM,
						color: 'orange'
					}
				]
			};
			let graphListMonthTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'วันที่'
					},
					categories: Array.from(Array(kkuIncomeMonthTM.length), (x, i) => i + 1)
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'วันที่  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListMonthTM
					},
					{
						name: 'ในเมือง',
						data: downtownListMonthTM,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeMonthTM: graphIncomeMonthTM,
				graphListMonthTM: graphListMonthTM
			});
		}
	};

	graphSelectYearTM = async (years) => {
		let claim = 'all';
		let incomeByYear = await GET(GET_YEAR_INCOME(claim, years));
		if (incomeByYear.success) {
			let kkuIncomeYearTM = incomeByYear.result.tm.sumIncome.kkuIncome;
			let downtownIncomeYearTM = incomeByYear.result.tm.sumIncome.downtownIncome;
			let kkuListYearTM = incomeByYear.result.tm.sumList.kkuList;
			let downtownListYearTM = incomeByYear.result.tm.sumList.downtownList;

			let graphIncomeYearTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนเงิน'
					}
				},
				tooltip: {
					crosshairs: true,
					formatter: this.formatTooltipYear
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuIncomeYearTM
					},
					{
						name: 'ในเมือง',
						data: downtownIncomeYearTM,
						color: 'orange'
					}
				]
			};
			let graphListYearTM = {
				title: {
					text: null
				},
				xAxis: {
					title: {
						text: 'เดือน'
					},
					categories: categories
				},
				yAxis: {
					title: {
						text: 'จำนวนรายการ'
					}
				},
				tooltip: {
					formatter: function() {
						var s = '<b>' + 'เดือน  ' + this.x + '</b>',
							sum = 0;
						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ' : ' + point.y + '  รายการ';
							sum += point.y;
						});
						s += '<br/>ทั้งหมด : ' + sum;
						return s;
					},
					crosshairs: true,
					shared: true
				},
				series: [
					{
						name: 'มหาวิทยาลัยขอนแก่น',
						data: kkuListYearTM
					},
					{
						name: 'ในเมือง',
						data: downtownListYearTM,
						color: 'orange'
					}
				]
			};
			this.setState({
				graphIncomeYearTM: graphIncomeYearTM,
				graphListYearTM: graphListYearTM
			});
		}
	};

	render() {
		let {
			month,
			year,
			//---------MT------------
			graphIncomeMonthMT,
			graphListMonthMT,
			graphIncomeYearMT,
			graphListYearMT,
			//---------PT---------------
			graphIncomeMonthPT,
			graphListMonthPT,
			graphIncomeYearPT,
			graphListYearPT,
			//---------TM-------------
			graphIncomeMonthTM,
			graphListMonthTM,
			graphIncomeYearTM,
			graphListYearTM,
			//--------------DATA-DONUT-PT---------
			donutIncomePT,
			sumIncomeDayPT,
			kkuIncomeDayPT,
			downtownIncomeDayPT,
			donutListPT,
			sumListDayPT,
			kkuListDayPT,
			downtownListDayPT,
			//--------------DATA-DONUT-MT---------
			donutIncomeMT,
			sumIncomeDayMT,
			kkuIncomeDayMT,
			downtownIncomeDayMT,
			donutListMT,
			sumListDayMT,
			kkuListDayMT,
			downtownListDayMT,
			//--------------DATA-DONUT-TM---------
			donutIncomeTM,
			sumIncomeDayTM,
			kkuIncomeDayTM,
			downtownIncomeDayTM,
			donutListTM,
			sumListDayTM,
			kkuListDayTM,
			downtownListDayTM,
			//-------------------------------------
			date,
			user_type,
			loading
		} = this.state;
		// console.log('kkuTM0',kkuListDayTM);
		return loading ? (
			<div className="loading d-flex flex-column">
				<ReactLoading type={'spinningBubbles'} color={Color.Blue} height={'auto'} width={'5vw'} />
				<Label style={{ fontSize: '2rem', color: '#467ac8', marginTop: '2rem' }}>กำลังโหลดข้อมูล</Label>
			</div>
		) : (
			<div className="div-graph">
				{/* <Row className="w-100">
          <Col className=" d-flex justify-content-end mt-2">
            <Link to="/Income_document">
              <button id="div-button">สรุปรายได้</button>
            </Link>
          </Col>
        </Row> */}
				{donutIncomePT.length > 0 ? user_type === 'frontMT' ? (
					<Row className="w-100">
						<Col>
							<SubGraph
								name="ตรวจสุขภาพ"
								date={date}
								month={month}
								year={year}
								//-------------------
								graphListMonth={graphListMonthMT}
								graphListYear={graphListYearMT}
								graphIncomeMonth={graphIncomeMonthMT}
								graphIncomeYear={graphIncomeYearMT}
								graphSelectMonth={(e) => this.graphSelectMonthMT(e)}
								graphSelectYear={(e) => this.graphSelectYearMT(e)}
								//---------------------------------------
								donutIncome={donutIncomeMT}
								sumIncomeDay={sumIncomeDayMT}
								kkuIncomeDay={kkuIncomeDayMT}
								downtownIncomeDay={downtownIncomeDayMT}
								donutList={donutListMT}
								sumListDay={sumListDayMT}
								kkuListDay={kkuListDayMT}
								downtownListDay={downtownListDayMT}
								//------------------------------------------------------------------
							/>
						</Col>
					</Row>
				) : user_type === 'frontPT' || user_type === 'frontTM' ? (
					<Row className="w-100">
						<Col xl="6">
							<SubGraph
								name="กายภาพบำบัด"
								date={date}
								month={month}
								year={year}
								//-------------------
								graphListMonth={graphListMonthPT}
								graphListYear={graphListYearPT}
								graphIncomeMonth={graphIncomeMonthPT}
								graphIncomeYear={graphIncomeYearPT}
								graphSelectMonth={(e) => this.graphSelectMonthPT(e)}
								graphSelectYear={(e) => this.graphSelectYearPT(e)}
								//---------------------------------------
								donutIncome={donutIncomePT}
								sumIncomeDay={sumIncomeDayPT}
								kkuIncomeDay={kkuIncomeDayPT}
								downtownIncomeDay={downtownIncomeDayPT}
								donutList={donutListPT}
								sumListDay={sumListDayPT}
								kkuListDay={kkuListDayPT}
								downtownListDay={downtownListDayPT}
							/>
						</Col>
						<Col xl="6">
							<SubGraph
								name="นวดไทย"
								date={date}
								month={month}
								year={year}
								//-------------------
								graphListMonth={graphListMonthTM}
								graphListYear={graphListYearTM}
								graphIncomeMonth={graphIncomeMonthTM}
								graphIncomeYear={graphIncomeYearTM}
								graphSelectMonth={(e) => this.graphSelectMonthTM(e)}
								graphSelectYear={(e) => this.graphSelectYearTM(e)}
								//---------------------------------------
								donutIncome={donutIncomeTM}
								sumIncomeDay={sumIncomeDayTM}
								kkuIncomeDay={kkuIncomeDayTM}
								downtownIncomeDay={downtownIncomeDayTM}
								donutList={donutListTM}
								sumListDay={sumListDayTM}
								kkuListDay={kkuListDayTM}
								downtownListDay={downtownListDayTM}
							/>
						</Col>
					</Row>
				) : (
					<Row className="w-100">
						<Col xl="6">
							<SubGraph
								name="กายภาพบำบัด"
								date={date}
								month={month}
								year={year}
								//-------------------
								graphListMonth={graphListMonthPT}
								graphListYear={graphListYearPT}
								graphIncomeMonth={graphIncomeMonthPT}
								graphIncomeYear={graphIncomeYearPT}
								graphSelectMonth={(e) => this.graphSelectMonthPT(e)}
								graphSelectYear={(e) => this.graphSelectYearPT(e)}
								//---------------------------------------
								donutIncome={donutIncomePT}
								sumIncomeDay={sumIncomeDayPT}
								kkuIncomeDay={kkuIncomeDayPT}
								downtownIncomeDay={downtownIncomeDayPT}
								donutList={donutListPT}
								sumListDay={sumListDayPT}
								kkuListDay={kkuListDayPT}
								downtownListDay={downtownListDayPT}
							/>
						</Col>
						<Col xl="6">
							<SubGraph
								name="นวดไทย"
								date={date}
								month={month}
								year={year}
								//-------------------
								graphListMonth={graphListMonthTM}
								graphListYear={graphListYearTM}
								graphIncomeMonth={graphIncomeMonthTM}
								graphIncomeYear={graphIncomeYearTM}
								graphSelectMonth={(e) => this.graphSelectMonthTM(e)}
								graphSelectYear={(e) => this.graphSelectYearTM(e)}
								//---------------------------------------
								donutIncome={donutIncomeTM}
								sumIncomeDay={sumIncomeDayTM}
								kkuIncomeDay={kkuIncomeDayTM}
								downtownIncomeDay={downtownIncomeDayTM}
								donutList={donutListTM}
								sumListDay={sumListDayTM}
								kkuListDay={kkuListDayTM}
								downtownListDay={downtownListDayTM}
							/>
						</Col>
						<Col>
							<SubGraph
								name="ตรวจสุขภาพ"
								date={date}
								month={month}
								year={year}
								//-----------------------------------------------------------------
								graphListMonth={graphListMonthMT}
								graphListYear={graphListYearMT}
								graphIncomeMonth={graphIncomeMonthMT}
								graphIncomeYear={graphIncomeYearMT}
								graphSelectMonth={(e) => this.graphSelectMonthMT(e)}
								graphSelectYear={(e) => this.graphSelectYearMT(e)}
								//-----------------------------------------------------------------
								donutIncome={donutIncomeMT}
								sumIncomeDay={sumIncomeDayMT}
								kkuIncomeDay={kkuIncomeDayMT}
								downtownIncomeDay={downtownIncomeDayMT}
								donutList={donutListMT}
								sumListDay={sumListDayMT}
								kkuListDay={kkuListDayMT}
								downtownListDay={downtownListDayMT}
								//------------------------------------------------------------------
							/>
						</Col>
					</Row>
				) : (
					<h3>----- ไม่มีรายการ -----</h3>
				)}
			</div>
		);
	}
}
