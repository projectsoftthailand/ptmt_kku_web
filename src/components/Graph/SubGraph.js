import React, { Component } from 'react';
import { Label, Input, TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import { PieChart, Pie, Sector, Cell } from 'recharts';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import moment from 'moment';
import ReactSvgPieChart from 'react-svg-piechart';
import './Styled/SubGraph.css';
import './Styled/Graph.css';
import Exporting from 'highcharts/modules/exporting';
import classnames from 'classnames';
Exporting(Highcharts);
const COLORS = [ '#467ac8', '#e8ae00', '#FFBB28', '#FF8042' ];

export default class SubGraph extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeIndex: 0,
			click: 0,
			types: [
				{
					type: 1,
					name: 'มกราคม'
				},
				{
					type: 2,
					name: 'กุมภาพันธ์'
				},
				{
					type: 3,
					name: 'มีนาคม'
				},
				{
					type: 4,
					name: 'เมษายน'
				},
				{
					type: 5,
					name: 'พฤษภาคม'
				},
				{
					type: 6,
					name: 'มิถุนายน'
				},
				{
					type: 7,
					name: 'กรกฏาคม'
				},
				{
					type: 8,
					name: 'สิงหาคม'
				},
				{
					type: 9,
					name: 'กันยายน'
				},
				{
					type: 10,
					name: 'ตุลาคม'
				},
				{
					type: 11,
					name: 'พฤศจิกายน'
				},
				{
					type: 12,
					name: 'ธันวาคม'
				}
			],
			activeTab: '1',
			checkIncome: true,
			month: '',
			options: [],
			year: '',
			date: ''
		};
	}

	componentWillMount = () => {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);

		let date = this.props.date;
		let month = this.props.month;
		let year = this.props.year;
		var start = 2010;
		var end = new Date().getFullYear();
		var options = [];
		for (var i = start; i <= end; i++) {
			options.push({ year: i, value: i });
		}
		this.setState({
			options: options,
			month: month,
			year: year,
			date: date
		});
	};

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

	renderActiveShape = (props) => {
		const { cx, cy, innerRadius, outerRadius, startAngle, endAngle, fill } = props;
		return (
			<g>
				<text style={{ fontSize: '30px' }} x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
					{this.props.sumListDay}
				</text>
				<Sector
					cx={cx}
					cy={cy}
					innerRadius={innerRadius}
					outerRadius={outerRadius}
					startAngle={startAngle}
					endAngle={endAngle}
					fill={fill}
				/>
			</g>
		);
	};

	render() {
		let { types, checkIncome, options, month, year, date } = this.state;
		let {
			name,
			graphSelectYear,
			graphSelectMonth,
			//----------------------------
			donutIncome,
			donutList,
			sumIncomeDay,
			kkuIncomeDay,
			downtownIncomeDay,
			kkuListDay,
			downtownListDay,
			graphIncomeMonth,
			graphIncomeYear,
			graphListMonth,
			graphListYear
		} = this.props;
		// console.log('kku',kkuListDay);

		return (
			<div>
				<div id="font-color" className="mb-2">
					คลีนิก{name}
				</div>
				<Row id="position-graph" className="px-0 mx-0">
					<Col id="border-right">
						<Row className="py-4">
							<Col sm={6} className="d-flex justify-content-center text-center">
								<div style={{ width: '15rem', height: '15rem' }}>
									<ReactSvgPieChart strokeWidth={0} data={donutIncome} expandOnHover />
								</div>
							</Col>
							<Col sm={6} className="d-flex flex-column justify-content-center">
								<div style={{ fontSize: '1.6rem' }}>รายได้ส่วนของคลีนิก{name}</div>
								<div style={{ fontSize: '3rem' }}>
									{sumIncomeDay && sumIncomeDay.toLocaleString()} บาท
								</div>
								<div style={{ fontSize: '1.4rem' }}>รายได้รวมวันที่ {date}</div>
							</Col>
						</Row>
						<Row id="border-solid">
							<Col className="py-2" style={{ borderRight: '1px solid #dededf' }}>
								<Row className="d-flex align-items-center px-4" style={{ fontSize: '1.4rem' }}>
									<div id="block-intime" />
									<div>มหาวิทยาลัยขอนแก่น</div>
								</Row>
								<Row className="px-4" style={{ fontSize: '1.6rem' }}>
									{kkuIncomeDay && kkuIncomeDay.toLocaleString()} บาท
								</Row>
							</Col>
							<Col className="py-2">
								<Row className="d-flex align-items-center px-4" style={{ fontSize: '1.4rem' }}>
									<div id="block-outtime" />
									<div>ในเมือง</div>
								</Row>
								<Row className="px-4" style={{ fontSize: '1.6rem' }}>
									{downtownIncomeDay && downtownIncomeDay.toLocaleString()} บาท
								</Row>
							</Col>
						</Row>
					</Col>
					<Col>
						<Row className="py-4">
							<Col sm={5} className="d-flex justify-content-center text-center">
								<PieChart width={150} height={150}>
									<Pie
										activeIndex={this.state.activeIndex}
										activeShape={this.renderActiveShape}
										data={donutList}
										innerRadius={65}
										outerRadius={70}
										paddingAngle={1}
										dataKey="value"
										fill="#8884d8"
									>
										{donutList &&
											donutList.map((entry, index) => (
												<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
											))}
									</Pie>
								</PieChart>
							</Col>
							<Col sm={7} className="d-flex flex-column justify-content-center">
								<div className="d-flex align-items-center" style={{ fontSize: '1.6rem' }}>
									จำนวนคนที่ใช้บริการ{name}
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.4rem' }}>
									<div id="block-intime" />
									<div>มหาวิทยาลัยขอนแก่น {kkuListDay} รายการ</div>
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.4rem' }}>
									<div id="block-outtime" />
									<div>ในเมือง {downtownListDay} รายการ</div>
								</div>
								<div className="d-flex align-items-center" style={{ fontSize: '1.4rem' }}>
									ข้อมูลวันที่ {date}
								</div>
							</Col>
						</Row>
					</Col>
				</Row>
				<div id="s-graph-right" className="my-4">
					<Row className="px-0 mx-0">
						<Nav tabs className="w-100">
							<NavItem>
								<NavLink
									className={classnames({ active: this.state.activeTab === '1' })}
									onClick={() => {
										this.toggle('1');
									}}
								>
									<h4>กราฟรายได้</h4>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink
									className={classnames({ active: this.state.activeTab === '2' })}
									onClick={() => {
										this.toggle('2');
									}}
								>
									<h4>กราฟลูกค้า</h4>
								</NavLink>
							</NavItem>
						</Nav>
					</Row>
					<TabContent activeTab={this.state.activeTab}>
						<TabPane tabId="1">
							<Row id="s-graph-right-1" className="px-0 mx-0">
								<div className="d-flex justify-content-end m-4 w-100">
									<Row className="px-0 mx-2">
										<input
											onChange={() => {
												this.setState({ checkIncome: true }, () => graphSelectMonth(month));
											}}
											checked={checkIncome ? true : false}
											value={'checkIncome_month_' + name}
											name={'checkIncome_month_' + name}
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											เดือน
										</Label>
										<Input
											value={month}
											onChange={(e) => {
												this.setState({ month: e.target.value }, () => {
													let { month } = this.state;
													graphSelectMonth(month);
												});
											}}
											style={{ width: '15rem' }}
											className="-1"
											type="select"
											disabled={checkIncome === false}
										>
											{types.map((e, index) => (
												<option key={index.toString()} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Row>
									<Row className="px-0 mx-2">
										<input
											onChange={() => {
												this.setState({ checkIncome: false }, () => graphSelectYear(year));
											}}
											checked={checkIncome ? false : true}
											value={'checkIncome_year_' + name}
											name={'checkIncome_year_' + name}
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											ปี
										</Label>
										<Input
											value={year}
											onChange={(e) =>
												this.setState({ year: e.target.value }, () => {
													let { year } = this.state;
													graphSelectYear(year);
												})}
											style={{ width: '15rem' }}
											className="-1"
											type="select"
											disabled={checkIncome === true}
										>
											{options.map((e, index) => (
												<option key={index.toString()} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Row>
								</div>
								<div className="w-100">
									<HighchartsReact
										highcharts={Highcharts}
										options={checkIncome ? graphIncomeMonth : graphIncomeYear}
									/>
								</div>
							</Row>
						</TabPane>
						<TabPane tabId="2">
							<Row id="s-graph-right-1" className="px-0 mx-0">
								<div className="d-flex justify-content-end m-4 w-100">
									<Row className="px-0 mx-2">
										<input
											onChange={() => {
												this.setState({ checkIncome: true }, () => graphSelectMonth(month));
											}}
											checked={checkIncome ? true : false}
											value={'checkList_month_' + name}
											name={'checkList_month_' + name}
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											เดือน
										</Label>
										<Input
											value={month}
											onChange={(e) => {
												this.setState({ month: e.target.value }, () => {
													let { month } = this.state;
													graphSelectMonth(month);
												});
											}}
											style={{ width: '15rem' }}
											className="-1"
											type="select"
											disabled={checkIncome === false}
										>
											{types.map((e, index) => (
												<option key={index.toString()} value={e.type}>
													{e.name}
												</option>
											))}
										</Input>
									</Row>
									<Row className="px-0 mx-2">
										<input
											onChange={() => {
												graphSelectYear(year);
												this.setState({ checkIncome: false });
											}}
											checked={checkIncome ? false : true}
											value={'checkList_year_' + name}
											name={'checkList_year_' + name}
											type="radio"
										/>
										<Label className="m-1" style={{ width: '5rem', fontSize: '1rem' }}>
											ปี
										</Label>
										<Input
											value={year}
											onChange={(e) => {
												graphSelectYear(e.target.value);
												this.setState({ year: e.target.value });
											}}
											style={{ width: '15rem' }}
											className="-1"
											type="select"
											disabled={checkIncome === true}
										>
											{options.map((e, index) => (
												<option key={index.toString()} value={e.value}>
													{e.year}
												</option>
											))}
										</Input>
									</Row>
								</div>
								<div className="w-100">
									<HighchartsReact
										highcharts={Highcharts}
										options={checkIncome ? graphListMonth : graphListYear}
									/>
								</div>
							</Row>
						</TabPane>
					</TabContent>
				</div>
			</div>
		);
	}
}
