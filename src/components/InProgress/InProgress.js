import React, { Component } from 'react';
import './Styled/InProgress.css';
import ic_tool from '../../assets/icon/tools-and-utensils.png';

class InProgress extends Component {
    render() {
        return (
            <div className="div-progress">
                <img
                    src={ic_tool}
                    className="img-tool"
                    alt="อยู่ในระหว่างพัฒนา"
                />
                <label className="label-tool">
                    อยู่ในระหว่างพัฒนา
                </label>
            </div>
        )
    }
}

export default InProgress;