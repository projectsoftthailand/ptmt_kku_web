import React, { Component } from 'react';
import Modal from 'react-modal';
import { Input, FormGroup, Label } from 'reactstrap';
import { IoMdClose } from 'react-icons/io';
import { Animated } from 'react-animated-css';
import './Styled/ChangeStatus.css';
import { POST, UPDATE_LIST_BY_FRONT, GET, CANCEL_LIST } from '../../service/service';
import swal from 'sweetalert';
import ReactLoading from 'react-loading';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		borderRadius: 20
	},
	overlay: {
		zIndex: 5,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
};

Modal.setAppElement(document.getElementById('root'));

export default class ChangeStatus extends Component {
	constructor(props) {
		super(props);

		this.state = {
			test_name: '',
			test_id: '',
			wait_loading: false
		};
	}

	handleChange(eventId, eventName) {
		this.setState({
			test_name: eventName,
			test_id: eventId
		});
	}

	submitChange = async () => {
		let { listNow, CloseModal, statusNow, refesh } = this.props;
		let { test_id } = this.state;
		let obj = {
			service_status: test_id ? test_id : statusNow
		};
		try {
			if (test_id === 99) {
				CloseModal();
				this.setState({ wait_loading: true });
				let res = await GET(CANCEL_LIST(listNow));
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', 'ยกเลิกบริการสำเร็จ', 'success', {
						buttons: {
							confirm: {
								text: 'ยืนยัน',
								value: 'confirm'
							}
						}
					}).then((value) => {
						if (value === 'confirm') {
							refesh();
						}
					});
				} else {
					this.setState({ wait_loading: false });
					swal('ไม่สำเร็จ!', 'ยกเลิกบริการไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 2000
					});
				}
			} else {
				CloseModal();
				this.setState({ wait_loading: true });
				let res = await POST(UPDATE_LIST_BY_FRONT(listNow), obj);
				if (res.success) {
					this.setState({ wait_loading: false });
					swal('สำเร็จ!', 'เปลี่ยนสถานะสำเร็จ', 'success', {
						buttons: {
							confirm: {
								text: 'ยืนยัน',
								value: 'confirm'
							}
						}
					}).then((value) => {
						if (value === 'confirm') {
							refesh();
						}
					});
				} else {
					this.setState({ wait_loading: false });
					swal('ไม่สำเร็จ!', 'เปลี่ยนสถานะไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 2000
					});
				}
			}
		} catch (err) {
			// console.log(err);
		}
	};
	CloseModal = () => {
		this.props.CloseModal();
		this.setState({ test_name: '' });
	};
	render() {
		let { isOpenModal, statusNow, CloseModal, AllStatusToChange } = this.props;
		let { test_name, wait_loading } = this.state;
		// console.log(AllStatusToChange);

		return (
			<div>
				<Modal isOpen={wait_loading} style={customStyles}>
					<div className="loading">
						<div className="d-flex flex-column align-items-center">
							<ReactLoading type={'spinningBubbles'} color={'#fff'} height={'auto'} width={'5vw'} />
							<Label style={{ fontSize: '2rem', color: '#fff', marginTop: '2rem' }}>กรุณารอสักครู่</Label>
						</div>
					</div>
				</Modal>
				<Modal isOpen={isOpenModal} onRequestClose={this.CloseModal} style={customStyles}>
					<Animated animationIn="fadeInDown" isVisible={true}>
						<div className="mx-auto text-center div-modal-change-status">
							<div className="label-modal-change-status">เปลี่ยนสถานะ</div>

							<div
								className="btn-exit-hover button-close-modal-change-status"
								style={{ cursor: 'pointer' }}
								onClick={() => this.setState({ test: '', test_name: '' }, CloseModal())}
							>
								<IoMdClose color="white" size={18} />
							</div>

							<div
								className="pt-4 pb-4 mx-auto"
								style={{
									width: '50%',
									display: 'flex',
									flexDirection: 'column',
									alignItems: 'flex-start',
									justifyContent: 'center'
								}}
							>
								{AllStatusToChange.map((el, index) => {
									return (
										<FormGroup check key={index}>
											<Label check className="label-modal-status-radio">
												<Input
													type="radio"
													name="radio2"
													style={{ marginLeft: '-1.5vw' }}
													value={el.name}
													checked={test_name ? test_name === el.name : statusNow === el.id}
													onChange={() => this.handleChange(el.id, el.name)}
													// this.handleChange}
												/>
												<text style={{ paddingLeft: '1rem' }}>{el.name}</text>
											</Label>
										</FormGroup>
									);
								})}
							</div>

							<button onClick={() => this.submitChange()} className="btn-form">
								บันทึก
							</button>
						</div>
					</Animated>
				</Modal>
			</div>
		);
	}
}
