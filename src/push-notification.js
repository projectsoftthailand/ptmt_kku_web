// import firebase from 'firebase';

// export const initializeFirebase = () => {
// 	firebase.initializeApp({
// 		messagingSenderId: '635245093746'
// 	});

// 	// use other service worker
// 	// navigator.serviceWorker.register('/my-sw.js').then((registration) => {
// 	// 	firebase.messaging().useServiceWorker(registration);
// 	// });
// };

// export const askForPermissioToReceiveNotifications = async () => {
// 	try {
// 		const messaging = firebase.messaging();
// 		await messaging.requestPermission();
// 		const token = await messaging.getToken();
// 		return token;
// 	} catch (error) {
// 		console.error('messaging', error);
// 	}
// };
import * as firebase from 'firebase/app';
import 'firebase/messaging';
const initializedFirebaseApp = firebase.initializeApp({
	messagingSenderId: '635245093746'
});

let messaging = null;

try {
	messaging = initializedFirebaseApp.messaging();
	messaging.usePublicVapidKey(
		'BGsJyb1EPTNEzlj5dlo2ZBO9oV8lllXtjI8A2CJmqTH09v0yrrNwPayHfI27qhPWyZA1q2bElGEqB8Yjpnw_MmQ'
	);
} catch (error) {
	// console.log(error);
}
export { messaging };
